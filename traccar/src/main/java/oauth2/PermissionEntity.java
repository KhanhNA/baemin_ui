package oauth2;

import java.io.Serializable;

public class PermissionEntity implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = -575415150358637616L;

	private Long id;

	private String clientId;

	private String url;

	private String description = "NULL";

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getClientId() {
		return this.clientId;
	}

	public void setClientId(String clientId) {
		this.clientId = clientId;
	}

	public String getUrl() {
		return this.url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

}