package ns.vtc.entity;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name = "sharevan_routing_plan_day")
@Data
@NoArgsConstructor()
public class RoutingPlanDay {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "id")
  private Integer id;

  @Column(name = "routing_Plan_Day_Code")
  private String routingPlanDayCode;

  @Column(name = "date_Plan")
  private java.sql.Date datePlan;

  @Column(name = "vehicle_Id")
  private Integer vehicleId;

  @Column(name = "driver_Id")
  private Integer driverId;

  @Column(name = "latitude")
  private Double latitude;

  @Column(name = "longitude")
  private Double longitude;

  @Column(name = "order_Number")
  private Integer orderNumber;

  @Column(name = "status")
  private String status;

  @Column(name = "capacity_Expected")
  private Double capacityExpected;

  @Column(name = "expected_From_Time")
  private LocalDateTime expectedFromTime;

  @Column(name = "expected_To_Time")
  private java.sql.Timestamp expectedToTime;

  @Column(name = "actual_Time")
  private java.sql.Timestamp actualTime;

  @Column(name = "warehouse_Id")
  private Integer warehouseId;

  @Column(name = "zone_Area_Id")
  private Integer zoneAreaId;
//
//  @Column(name = "warehouse_Type")
//  private String warehouseType;

  @Column(name = "depot_id")
  private Integer depotId;

  @Column(name = "stock_Man_Id")
  private Integer stockManId;

  @Column(name = "create_Uid")
  private Integer createUid;

  @Column(name = "create_Date")
  private java.sql.Timestamp createDate;

  @Column(name = "write_Uid")
  private Integer writeUid;

  @Column(name = "write_Date")
  private java.sql.Timestamp writeDate;

  @Column(name = "ready_time")
  private LocalDateTime readyTime;

  @Column(name = "due_time")
  private LocalDateTime dueTime;

  @Column(name = "next_id")
  private String nextId;

  @Column(name = "previous_id")
  private String previousId;

  @Column(name = "company_id")
  private Integer companyId;

  @Column(name = "partner_id")
  private Integer partnerId;

  @Column(name = "warehouse_type")
  private Integer warehouseType; //refer to WarehouseType

  @Column(name = "ship_type")
  private Integer shipType; //refer to ShipType

}
