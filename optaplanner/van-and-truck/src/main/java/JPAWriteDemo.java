import ns.utils.JPAUtility;
import ns.utils.ObjectUtils;

import ns.vtc.entity.Van;
import ns.vtc.service.DBService;

import java.util.List;

public class JPAWriteDemo {
    public static void main(String[] args) {
        List<Van> vans = JPAUtility.getInstance().jpaFindAll("from Van", null);
        vans = ObjectUtils.safe(vans);
        for(Van v: vans){
            System.out.println("van:" + v.toString());
        }

    }
} 