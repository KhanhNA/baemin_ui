package org.optaplanner.examples.vehiclerouting.customize;

import ns.utils.DateUtils;
import ns.utils.JPAUtility;
import ns.utils.ObjectUtils;
import ns.vtc.entity.*;
import ns.vtc.service.DBService;
import org.optaplanner.core.api.score.buildin.hardsoftlong.HardSoftLongScore;
import org.optaplanner.examples.vehiclerouting.customize.entity.ShipmentPot;
import org.optaplanner.examples.vehiclerouting.customize.entity.ShipmentStandstill;
import org.optaplanner.examples.vehiclerouting.customize.entity.TimeWindowedShipmentPot;
import org.optaplanner.examples.vehiclerouting.customize.maps.GoogleDistanceAPI;
import org.optaplanner.examples.vehiclerouting.customize.types.DistanceInfo;
import org.optaplanner.examples.vehiclerouting.customize.types.WarehouseType;
import org.optaplanner.examples.vehiclerouting.domain.Depot;
import org.optaplanner.examples.vehiclerouting.domain.VehicleRoutingSolutionV2;
import org.optaplanner.examples.vehiclerouting.domain.VehicleV2;
import org.optaplanner.examples.vehiclerouting.domain.location.AirLocation;
import org.optaplanner.examples.vehiclerouting.domain.location.DistanceType;

import org.optaplanner.examples.vehiclerouting.domain.location.Location;
import org.optaplanner.examples.vehiclerouting.domain.timewindowed.TimeWindowedDepot;

import javax.transaction.Transactional;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class StaticData {
    private static HashMap<String, DistanceInfo<Double, Double>> locDataMap;

    public static final String SOLUTION_GROUP_CODE = "default";
    public static final LocalDate SOLUTION_DAY = LocalDate.of(2020, 8, 14);//LocalDate.now();

    public static int LOCATION_ID = 0;

    public static void reset() {//Load du lieu chi phi (cost), thoi gian giua cac kho
        if (locDataMap != null) {
            locDataMap.clear();
        }
        locDataMap = new HashMap<>();

    }

    public static Double getTime(ShipmentStandstill from, ShipmentStandstill to) {
        if (locDataMap == null) {
            return 0d;
        }
        DistanceInfo<Double, Double> d = getDistanceInfo(from, to);

        return d.getMinute();
    }

    public static Double getCost(ShipmentStandstill from, ShipmentStandstill to) {
        if (locDataMap == null) {
            return 0d;
        }
        DistanceInfo<Double, Double> d = getDistanceInfo(from, to);
        return d.getCost();
    }

    public static void addLocationData(List<LocationData> locationDatas) {
//        locDataMap = new HashMap<>();
        for (LocationData ld : locationDatas) {
//            locDataMap.put(ld.getCode(),DistanceInfo.of(ld.getMinutes() * 60, ld.getCost()));
            addLocationData(ld);
        }
    }



//    public static void addDepotData(List<ParkingPointData> parkingPointData) {
//
//        for (ParkingPointData ppd : parkingPointData) {
//            locDataMap.put("FDLAT" + ppd.get() + "-FDLONG" + ppd.getWarehouseId() + "TDLAT" + ppd.getParkingPointId() + "-TWLON" + ppd.getWarehouseId(),
//                    DistanceInfo.of((ppd.getMinutes() * 60), ppd.getCost()));
//        }
//    }

    public static HashMap<String, DistanceInfo<Double, Double>> getLocDataMap() {
        return locDataMap;
    }

    public static void loadProblem(VehicleRoutingSolutionV2 problem) {

        List<Location> locationList = new ArrayList<>();
//        HashMap<String, VehicleV2> vehicleMap;

//        HashMap<String, TimeWindowedShipmentPot> shipmentsMap;
        HashMap<Integer, TimeWindowedDepot> depotMap = new HashMap<>(); //key la location
        LOCATION_ID = 0;
        Map vehicleMap = loadVehices(locationList, depotMap);
        Map shipmentsMap = loadShimentPots(locationList, vehicleMap, depotMap);
        updateProblem(problem, depotMap, vehicleMap, shipmentsMap, locationList);

        //customer
        JPAUtility.getInstance().close();


    }

    private static void updateProblem(VehicleRoutingSolutionV2 problem,
                                      Map depotMap,
                                      Map vehicleMap,
                                      Map shipmentMap, List<Location> locationList) {

        SolutionDay solutionDay = DBService.getInstance().getSolution(SOLUTION_GROUP_CODE, SOLUTION_DAY);
        List<LocationData> locationData = DBService.getInstance().getLocationData();
//        List<ParkingPointData> parkingPointData = DBService.getInstance().getParkingPointData();

        problem.setDepotList(new ArrayList<Depot>(depotMap.values()));
        problem.setVehicleList(new ArrayList<VehicleV2>(vehicleMap.values()));
        problem.setShipmentPotList(new ArrayList<ShipmentPot>(shipmentMap.values()));
        problem.setLocationList(locationList);
        problem.setDistanceType(DistanceType.AIR_DISTANCE);
        StaticData.reset();
        StaticData.addLocationData(locationData);
//        StaticData.addDepotData(parkingPointData);

        if (solutionDay != null) {
            problem.setScore(HardSoftLongScore.of(solutionDay.getHardScore(), solutionDay.getSoftScore()));
            problem.setDatePlan(solutionDay.getDatePlan());
        }
    }

    private static ShipmentStandstill getStandstill(String id, VehicleV2 vehicle,
                                                    Map shipmentsMap) {
        ShipmentStandstill tmpShipment = null;
        if (id != null && id.startsWith("W")) {
            tmpShipment = (ShipmentStandstill)shipmentsMap.get(id);
        } else if (id != null && id.startsWith("D")) {
            tmpShipment = vehicle;
        }
        return tmpShipment;
    }

    private static Map loadShimentPots(List<Location> locationList,
                                       Map vehicleMap, Map depotMap
                                       ) {
        Location locTmp;
//        long locationId = 1;
        HashMap<String, TimeWindowedShipmentPot> shipmentsMap = new HashMap<>();
        ShipmentStandstill preTmpShipmentPot, nextTmpShipmentPot;
        TimeWindowedShipmentPot tmpShipmentPot;
        List<RoutingPlanDay> routingPlanDays = DBService.getInstance().getRoutingPlanDay(SOLUTION_GROUP_CODE, SOLUTION_DAY);
        VehicleV2 vehicle;
        for (RoutingPlanDay routing : routingPlanDays) {
            locTmp = new AirLocation(LOCATION_ID++, routing.getLatitude(), routing.getLongitude());

            tmpShipmentPot = new TimeWindowedShipmentPot(routing.getId(), locTmp, routing.getCapacityExpected(),
                    routing.getReadyTime(), routing.getDueTime(), 1, "" + routing.getPartnerId(), routing.getDepotId(),
                    routing.getWarehouseId(), routing.getPreviousId(), routing.getNextId(), WarehouseType.of(routing.getWarehouseType()));
            tmpShipmentPot.setArrivalTime(DateUtils.toSeconds(routing.getExpectedFromTime()).doubleValue());
            vehicle = routing.getVehicleId() == null ? null : (VehicleV2) vehicleMap.get("D" + routing.getVehicleId());
            tmpShipmentPot.setVehicle(vehicle);

            shipmentsMap.put(tmpShipmentPot.getCodeFromId(), tmpShipmentPot);

            locationList.add(locTmp);
        }
        for (String key : shipmentsMap.keySet()) {
            tmpShipmentPot = shipmentsMap.get(key);

            preTmpShipmentPot = getStandstill(tmpShipmentPot.getPrevId(), tmpShipmentPot.getVehicle(), shipmentsMap);
            nextTmpShipmentPot = getStandstill(tmpShipmentPot.getNextId(), tmpShipmentPot.getVehicle(), shipmentsMap);
            tmpShipmentPot.setPreviousStandstill(preTmpShipmentPot);
            if(preTmpShipmentPot instanceof VehicleV2){
                preTmpShipmentPot.getVehicle().setNextShipmentPot(tmpShipmentPot);
            }
            tmpShipmentPot.setNextShipmentPot((TimeWindowedShipmentPot) nextTmpShipmentPot);
        }
        return shipmentsMap;

    }

    //load data from database
    //map: van -> vehicle; parkingPoint -> depot
    private static HashMap<String, VehicleV2> loadVehices(List<Location> locationList,
                                                          HashMap<Integer, TimeWindowedDepot> depotMap) {

        ParkingPoint ppTmp;
        Integer key;
        TimeWindowedDepot depotTmp;
        AirLocation locTmp;
//        long locationId = 1;
        HashMap<String, VehicleV2> vehicleMap = new HashMap<>();
        List<FleetVehicle> vehicles = DBService.getInstance().getVehicles();
        vehicles = ObjectUtils.safe(vehicles);

        for (FleetVehicle v : vehicles) {
            System.out.println("van:" + v.toString());
        }
        for (FleetVehicle vehicle : vehicles) {
            ppTmp = vehicle.getParkingPoint();
            if (ppTmp == null) {
                continue;
            }
            key = ppTmp.getId();
            if (key == null) {
                continue;
            }
            depotTmp = depotMap.get(ppTmp.getId());
            //update depot
            if (depotTmp == null) {
                locTmp = new AirLocation(LOCATION_ID++, ppTmp.getLatitude(), ppTmp.getLongitude());
                depotTmp = new TimeWindowedDepot(ppTmp.getId(), locTmp, ppTmp.getDayReadyTime(), ppTmp.getDayDueTime());
                depotMap.put(key, depotTmp);
                locationList.add(locTmp);
            }
            //update location
            vehicleMap.put("D" + vehicle.getId(), new VehicleV2(vehicle.getName(), vehicle.getId(), vehicle.getCapacity(), depotTmp, vehicle.getCost()));
        }
        return vehicleMap;
    }

    public static <Solution_> void updateBestSolution(Solution_ solution) throws Exception {
        if (!(solution instanceof VehicleRoutingSolutionV2)) {
            return;
        }
        VehicleRoutingSolutionV2 routingSolution = (VehicleRoutingSolutionV2) solution;
        if(!routingSolution.isSaved()){
            return;
        }
        Long id = DBService.getInstance().insertSolutionDay(SOLUTION_GROUP_CODE, routingSolution.getDatePlan(),
                routingSolution.getScore().getSoftScore(), routingSolution.getScore().getHardScore());
        updateRoutingPlan(id, routingSolution);
    }

    public static <Solution_> void updateRoutingPlan(Long solId, VehicleRoutingSolutionV2 routingSolution) throws Exception {

        List<ShipmentPot> shipmentPotList = routingSolution.getShipmentPotList();

        StringBuffer insertInfo = new StringBuffer(shipmentPotList.size());
//        StringBuffer vanIds = new StringBuffer(shipmentPotList.size());
//        StringBuffer arrivalTimes = new StringBuffer(shipmentPotList.size());
//        StringBuffer updateUser = new StringBuffer(shipmentPotList.size());
//        StringBuffer updateDate = new StringBuffer(shipmentPotList.size());
        TimeWindowedShipmentPot timeShipmentPot;
        String timeTmp;
        ShipmentStandstill previous;
        for (ShipmentPot shipmentPot : shipmentPotList) {

            timeShipmentPot = (TimeWindowedShipmentPot) shipmentPot;
            timeTmp = timeShipmentPot.getArrivalTime() == null?null:DateUtils.toString(timeShipmentPot.getArrivalTime().longValue(), null);
            timeTmp = "TO_TIMESTAMP('" + timeTmp + "','dd/mm/YYYY hh24:mi:ss')";
            previous = shipmentPot.getPreviousStandstill();
            String previousCode = previous == null ? null : previous.getCodeFromId();//previous instanceof ShipmentPot?((ShipmentPot) previous).getId(): ((VehicleV2)previous).getId();
            String nextCode = timeShipmentPot.getNextShipmentPot() == null ? null : timeShipmentPot.getNextShipmentPot().getCodeFromId();
            insertInfo.append("(").append(timeShipmentPot.getId()).append(",") //route id
                    .append(timeShipmentPot.getVehicle().getId()).append(",") //van id
                    .append("'").append(nextCode).append("',") //next_id
                    .append("'").append(previousCode).append("',") //next_id
//                    .append(timeTmp).append(",") //route time
                    .append("current_date,")
                    .append("2, 'running')")
                    .append(",");

        }
        insertInfo.deleteCharAt(insertInfo.length() - 1);
        DBService.getInstance().updateRoutingPlan(solId, insertInfo.toString());

    }

    @Transactional(rollbackOn = Exception.class)
    private static DistanceInfo getDistanceInfo(ShipmentStandstill from, ShipmentStandstill to) {
        Location f = from.getLocation();
        Location t = to.getLocation();
        String timeKey = LocationData.getLocationCode(f.getLatitude(), f.getLongitude(),
                t.getLatitude(), t.getLongitude()); //"F" + from.getCode() + "-T" + to.getCode();
        DistanceInfo<Double, Double> d = locDataMap.get(timeKey);
        if(d == null){
            LocationData data = GoogleDistanceAPI.getDistance(from, to);
            data = JPAUtility.getInstance().save(data);
            JPAUtility.getInstance().close();
            d = addLocationData(data);
        }
        return d;
    }
    private static DistanceInfo addLocationData(LocationData ld){
        DistanceInfo d = DistanceInfo.of(ld.getMinutes() * 60, ld.getCost());
        locDataMap.put(ld.getCode(),d);
        return d;
    }
}
