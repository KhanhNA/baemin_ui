/*
 * Copyright 2012 Red Hat, Inc. and/or its affiliates.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.optaplanner.examples.vehiclerouting.customize.entity;

import org.optaplanner.core.api.domain.entity.PlanningEntity;
import org.optaplanner.core.api.domain.variable.InverseRelationShadowVariable;
import org.optaplanner.examples.vehiclerouting.domain.Standstill;
import org.optaplanner.examples.vehiclerouting.domain.VehicleV2;
import org.optaplanner.examples.vehiclerouting.domain.location.Location;

@PlanningEntity
public interface ShipmentStandstill extends Standstill {

    /**
     * @return never null
     */
    Location getLocation();

    /**
     * @return sometimes null
     */
    VehicleV2 getVehicle();

    /**
     * @return sometimes null
     */
    @InverseRelationShadowVariable(sourceVariableName = "previousStandstill")
    ShipmentPot getNextShipmentPot();
    void setNextShipmentPot(ShipmentPot shipmentPot);
    String getCode();//get code from warehouse or depot id -> dung de load chi phi, thoi gian -> tinh theo warehouse
    String getCodeFromId();//get from id: dung de tinh tuyen vi cung 1 warehouse co the nhieu tuyen den
    String getInfo();
}
