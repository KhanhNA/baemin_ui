package org.optaplanner.examples.vehiclerouting.customize.maps;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class GoogleDistance {
    private List<Route> routes;
}
