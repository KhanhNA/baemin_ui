package org.optaplanner.examples.vehiclerouting.customize.panel;

import ns.utils.DateUtils;
import org.optaplanner.examples.vehiclerouting.customize.types.*;
import org.optaplanner.examples.vehiclerouting.domain.location.AirLocation;
import org.optaplanner.examples.vehiclerouting.domain.location.Location;
import org.optaplanner.examples.vehiclerouting.domain.timewindowed.TimeWindowedDepot;
import org.optaplanner.examples.vehiclerouting.swingui.VehicleRoutingWorldPanel;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.sql.Date;
import java.time.LocalTime;
import java.util.HashMap;

public class DepotPanel extends JPanel implements ObjCRUD {
    private final Integer columnSize = 2;//2 input
    public final String Const_id = "id";
    public final String Const_readyTime = "readyTime";
    public final String Const_dueTime = "readyTime";
    private TimeWindowedDepot depot;
    private VehicleRoutingWorldPanel vehiclePanel;
    private static long id = -1;
    private boolean isNew;
    private final HashMap<String, Object> initValue = new HashMap<String, Object>() {{
        put("customerCode", "value1");
        put("demand", "value2");
    }};

    public DepotPanel(VehicleRoutingWorldPanel vehiclePanel, double latitude, double longitude) throws Exception {
//        serviceDuration, String customerCode, long depotId, long warehouseId

        this(vehiclePanel, new TimeWindowedDepot(--id, new AirLocation(--id, latitude, longitude),
                LocalTime.now().plusHours(2), LocalTime.now().plusHours(5)), true);
//        TimeWindowedDepot(--id, new AirLocation(--id, latitude, longitude), 10,
//                LocalDateTime.now().plusHours(2), LocalDateTime.now().plusHours(5), 1, "1", 1, 1, null, null), true);
    }

    public DepotPanel(VehicleRoutingWorldPanel vehiclePanel, RefObject objFocus, boolean isNew) throws Exception {
        GridBagLayout layout = new GridBagLayout();
        this.vehiclePanel = vehiclePanel;
        this.setLayout(new GridBagLayout());
        this.depot = (TimeWindowedDepot) objFocus;
        GridBagConstraints c = new GridBagConstraints();
        this.isNew = isNew;
        c.fill = GridBagConstraints.HORIZONTAL;
        c.weightx = 0.5;
        c.gridx = 0;
        c.gridy = 0;
        if (depot == null) {
            depot = new TimeWindowedDepot();
        }
        String[] type = {"Depot", "Vehicle", "Shipment"};
        addCell(Keys.type, new JComboBox(type), c)
                .addCell(Keys.id, new FormattedTextField("Id", depot.getId(), Formater.LONG), c)
//                .addCell("readyTime", new FormattedTextField("Ready time", DateUtils.dt2s(shipmentPot.getReadyTime(), null), new SimpleDateFormat(DateUtils.DD_MM_YYYY_HH_MM_SS)), c)
                .addCell(Keys.readyTime, new FormattedTextField("Ready time", new Date(depot.getReadyTime() * 1000), Formater.DATE), c)
                .addCell(Keys.dueTime, new FormattedTextField("Due time", new Date(depot.getDueTime() * 1000), Formater.DATE), c)
//                .addCell(Keys.serviceDuration, new FormattedTextField("Service duration", depot.getServiceDuration(), Formater.LONG), c)
                .addCell(Keys.latitude, new FormattedTextField("Latitude", depot.getLocation().getLatitude(), Formater.LATITUDE), c)
                .addCell(Keys.longitude, new FormattedTextField("Longitude", depot.getLocation().getLongitude(), Formater.LONGITUDE), c);
//                .addCell(Keys.demand, new FormattedTextField("Demand", depot.getDemand(), Formater.LONG), c)
//

    }


    @Override
    public void actionOk(JFrame frame, ActionEvent e) {
//        VehicleRoutingSolutionV2 solutionV2 = (VehicleRoutingSolutionV2)this.vehiclePanel;
        FormattedTextField idField = (FormattedTextField) objMap.get(Keys.id);
        FormattedTextField readyTime = (FormattedTextField) objMap.get(Keys.readyTime);

        JComboBox type = (JComboBox) objMap.get(Keys.type);

        System.out.println("OK1111111111" + idField.getText() + "_" + idField.getValue() +
                readyTime.getText() + "_" + readyTime.getValue() + " type:" + type.getSelectedIndex() + "_" + (String) type.getSelectedItem());


//        shipmentPot.setSaved(false);


        depot.setId((Long) ((FormattedTextField) (objMap.get(Keys.id))).getValue());
        depot.setReadyTime(DateUtils.toSeconds((Date) (((FormattedTextField) objMap.get(Keys.readyTime))).getValue()));
        depot.setDueTime(DateUtils.toSeconds((Date) (((FormattedTextField) objMap.get(Keys.dueTime)).getValue())));
//        shipmentPot.setDueTime((Long)(objMap.get(Keys.dueTime)).getValue());
//        depot.setServiceDuration((Long) (((FormattedTextField)objMap.get(Keys.serviceDuration)).getValue()));
        Location location = depot.getLocation();
        location.setLatitude((Double) (((FormattedTextField) objMap.get(Keys.latitude)).getValue()));
        location.setLongitude((Double) (((FormattedTextField) objMap.get(Keys.longitude)).getValue()));
        if (isNew) {

            vehiclePanel.getVehicleRoutingPanel().getSolution().getDepotList().add(depot);
            vehiclePanel.getVehicleRoutingPanel().getSolution().setSaved(false);
        }

        vehiclePanel.updatePanel(vehiclePanel.getVehicleRoutingPanel().getSolution());

    }


}


