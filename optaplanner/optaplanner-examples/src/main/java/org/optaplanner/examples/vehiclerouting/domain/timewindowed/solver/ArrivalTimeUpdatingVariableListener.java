/*
 * Copyright 2015 Red Hat, Inc. and/or its affiliates.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.optaplanner.examples.vehiclerouting.domain.timewindowed.solver;

import org.optaplanner.core.api.score.director.ScoreDirector;
import org.optaplanner.core.impl.domain.variable.listener.VariableListener;
import org.optaplanner.examples.vehiclerouting.customize.entity.ShipmentPot;
import org.optaplanner.examples.vehiclerouting.customize.entity.TimeWindowedShipmentPot;
import org.optaplanner.examples.vehiclerouting.domain.Standstill;
import org.optaplanner.examples.vehiclerouting.domain.Vehicle;
import org.optaplanner.examples.vehiclerouting.domain.VehicleV2;
import org.optaplanner.examples.vehiclerouting.domain.timewindowed.TimeWindowedDepot;

import java.util.Objects;

// TODO When this class is added only for TimeWindowedCustomer, use TimeWindowedCustomer instead of Customer
public class ArrivalTimeUpdatingVariableListener implements VariableListener<ShipmentPot> {

    @Override
    public void beforeEntityAdded(ScoreDirector scoreDirector, ShipmentPot shipmentPot) {
        // Do nothing
    }

    @Override
    public void afterEntityAdded(ScoreDirector scoreDirector, ShipmentPot shipmentPot) {
        if (shipmentPot instanceof TimeWindowedShipmentPot) {
            updateArrivalTime(scoreDirector, (TimeWindowedShipmentPot) shipmentPot);
        }
    }

    @Override
    public void beforeVariableChanged(ScoreDirector scoreDirector, ShipmentPot shipmentPot) {
        // Do nothing
    }

    @Override
    public void afterVariableChanged(ScoreDirector scoreDirector, ShipmentPot shipmentPot) {
        if (shipmentPot instanceof TimeWindowedShipmentPot) {
            updateArrivalTime(scoreDirector, (TimeWindowedShipmentPot) shipmentPot);
        }
    }

    @Override
    public void beforeEntityRemoved(ScoreDirector scoreDirector, ShipmentPot shipmentPot) {
        // Do nothing
    }

    @Override
    public void afterEntityRemoved(ScoreDirector scoreDirector, ShipmentPot shipmentPot) {
        // Do nothing
    }

    protected void updateArrivalTime(ScoreDirector scoreDirector, TimeWindowedShipmentPot sourceShipmentpot) {
        Standstill previousStandstill = sourceShipmentpot.getPreviousStandstill();
        Double departureTime = previousStandstill == null ? null
                : (previousStandstill instanceof TimeWindowedShipmentPot)
                        ? ((TimeWindowedShipmentPot) previousStandstill).getDepartureTime()
                        : ((TimeWindowedDepot) ((VehicleV2) previousStandstill).getDepot()).getReadyTime();
        TimeWindowedShipmentPot shadowShipmentPot = sourceShipmentpot;
        Double arrivalTime = calculateArrivalTime(shadowShipmentPot, departureTime);
        while (shadowShipmentPot != null && !Objects.equals(shadowShipmentPot.getArrivalTime(), arrivalTime)) {
            scoreDirector.beforeVariableChanged(shadowShipmentPot, "arrivalTime");
            shadowShipmentPot.setArrivalTime(arrivalTime.doubleValue());
            scoreDirector.afterVariableChanged(shadowShipmentPot, "arrivalTime");
            departureTime = shadowShipmentPot.getDepartureTime();
            shadowShipmentPot = shadowShipmentPot.getNextShipmentPot();
            arrivalTime = calculateArrivalTime(shadowShipmentPot, departureTime);
        }
    }

    private Double calculateArrivalTime(TimeWindowedShipmentPot shipmentPot, Double previousDepartureTime) {
        if (shipmentPot == null || shipmentPot.getPreviousStandstill() == null) {
            return null;
        }
        if (shipmentPot.getPreviousStandstill() instanceof VehicleV2) {
            // PreviousStandstill is the Vehicle, so we leave from the Depot at the best suitable time
            return Math.max(shipmentPot.getReadyTime(),
                    previousDepartureTime + shipmentPot.getDistanceFromPreviousStandstill());
        }
        return previousDepartureTime + shipmentPot.getDistanceFromPreviousStandstill();
    }

}
