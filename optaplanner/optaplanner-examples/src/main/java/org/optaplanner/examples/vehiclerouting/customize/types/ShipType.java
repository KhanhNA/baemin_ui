package org.optaplanner.examples.vehiclerouting.customize.types;

public enum ShipType {
    PICKUP, DELIVERY
}
