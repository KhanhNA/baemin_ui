/*
 * Copyright 2012 Red Hat, Inc. and/or its affiliates.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.optaplanner.examples.vehiclerouting.domain;

import org.optaplanner.examples.common.domain.AbstractPersistable;
import org.optaplanner.examples.vehiclerouting.customize.entity.ShipmentPot;
import org.optaplanner.examples.vehiclerouting.customize.entity.ShipmentStandstill;
import org.optaplanner.examples.vehiclerouting.domain.location.Location;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("VrpVehicle")
public class Vehicle extends AbstractPersistable implements ShipmentStandstill {

    protected int capacity;
    protected Depot depot;

    protected Long vehicleCost;
    protected String name;

    // Shadow variables
    protected ShipmentPot nextShipmentPot;

    public Vehicle() {
    }

    public Vehicle(String name, long id, int capacity, Depot depot, Long vehicleCost) {
        super(id);
        this.capacity = capacity;
        this.depot = depot;
        this.vehicleCost = vehicleCost;
        this.name = name;
    }

    public int getCapacity() {
        return capacity;
    }

    public void setCapacity(int capacity) {
        this.capacity = capacity;
    }

    public Depot getDepot() {
        return depot;
    }

    public void setDepot(Depot depot) {
        this.depot = depot;
    }

//    @Override
//    public Customer getNextCustomer() {
//        return nextCustomer;
//    }
//
//    @Override
//    public void setNextCustomer(Customer nextCustomer) {
//        this.nextCustomer = nextCustomer;
//    }

    // ************************************************************************
    // Complex methods
    // ************************************************************************

//    @Override
//    public Vehicle getVehicle() {
//        return this;
//    }

//    @Override
//    public Customer getNextCustomer() {
//        return null;
//    }
//
//    @Override
//    public void setNextCustomer(Customer nextCustomer) {
//
//    }

    @Override
    public ShipmentPot getNextShipmentPot() {
        return nextShipmentPot;
    }

    @Override
    public void setNextShipmentPot(ShipmentPot shipmentPot) {
        this.nextShipmentPot = nextShipmentPot;
    }

    @Override
    public String getCode() {
        return "D" + depot.getId();
    }

    @Override
    public String getCodeFromId() {
        return "D" + depot.getId();
    }

    @Override
    public String getInfo() {
        return this.toString();
    }

    @Override
    public Location getLocation() {
        return depot.getLocation();
    }

    @Override
    public VehicleV2 getVehicle() {
        return null;
    }

    /**
     * @param standstill never null
     * @return a positive number, the distance multiplied by 1000 to avoid floating point arithmetic rounding errors
     */
    public long getDistanceTo(Standstill standstill) {
        return depot.getDistanceTo(standstill);
    }

    @Override
    public String toString() {
        Location location = getLocation();
        if (location.getName() == null) {
            return super.toString();
        }
        return location.getName() + "/" + super.toString();
    }

    public String getName() {
        return name;
    }

    public Long getVehicleCost() {
        return vehicleCost;
    }
}
