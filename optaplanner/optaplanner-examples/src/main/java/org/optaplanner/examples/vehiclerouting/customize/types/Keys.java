package org.optaplanner.examples.vehiclerouting.customize.types;

public enum Keys {
    id,
    readyTime,
    dueTime,
    serviceDuration,
    latitude,
    longitude,
    demand,
    type,
    from,
    to
}
