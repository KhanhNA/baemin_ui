package com.ns.walletlib;

import android.app.Application;
import android.content.Context;

import androidx.annotation.NonNull;

import com.ns.walletlib.utils.HttpHelper;
import com.tsolution.base.listener.ActionsListener;

import lombok.Getter;
import lombok.Setter;
import retrofit2.Retrofit;

@Getter
@Setter
public class LoginVM extends com.tsolution.base.BaseViewModelV2<SOServiceWallet>{

    public static boolean isLogin = false;
    SOServiceWallet walletService;
    public LoginVM(@NonNull Application application) {
        super(application);

    }

    public void getWallet(Context context, WalletVM walletVM, String password, ActionsListener view) {
        AccountPayment accountPayment = walletVM.getAccountPayment().get();
        if(accountPayment == null){
            return;
        }
        Retrofit retrofit = HttpHelper.getWalletRetrofit(accountPayment.getClientAccount(), password, context, 1L);
        walletService = retrofit.create(SOServiceWallet.class);
        walletVM.setWalletService(walletService);
        callApiV2(walletService.getWalletAmount(47L), (call1, response, body, t) -> {
            if(view != null) {
                view.action(call1, response, body, t);
            }
        });


    }

}
