package com.ns.walletlib;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class SavingsAccount {
    private float id;
    private String accountNo;

    private float clientId;
    private String clientName;
    private float savingsProductId;
    private String savingsProductName;
    private float fieldOfficerId;
    ClientStatusDTO status;
    Currency currency;
    private float nominalAnnualInterestRate;
    List<AccountTran> transactions;
}
