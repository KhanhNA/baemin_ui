package com.ns.walletlib;

import com.tsolution.base.TsBaseModel;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AccountTran extends TsBaseModel {

    private float id;
    TransactionType transactionType;
    private float accountId;
    private String accountNo;
    String[] date;
    Currency currency;
    private float amount;
    private float runningBalance;
    private boolean reversed;

    String[] submittedOnDate;
    private boolean interestedPostedAsOn;
    private String submittedByUsername;
    public String getDateStr(){
        return date[2] + "/" + date[1] + "/" + date[0];
    }
    public String getAmountStr(){
        return ""+amount;
    }

}



