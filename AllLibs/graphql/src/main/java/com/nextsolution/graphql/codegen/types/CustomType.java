package com.nextsolution.graphql.codegen.types;

import com.apollographql.apollo.api.ScalarType;
import java.lang.Class;
import java.lang.Override;
import javax.annotation.Generated;
import java.util.Date;
import java.lang.String;

@Generated("Apollo GraphQL")
public enum CustomType implements ScalarType {
  Date {
    @Override
    public String typeName() {
      return "Date";
    }
  
    @Override
    public Class javaType() {
      return Date.class;
    }
  },

  JSON {
    @Override
    public String typeName() {
      return "JSON";
    }
  
    @Override
    public Class javaType() {
      return String.class;
    }
  },

  Upload {
    @Override
    public String typeName() {
      return "Upload";
    }
  
    @Override
    public Class javaType() {
      return String.class;
    }
  }
}
