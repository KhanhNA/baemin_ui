package com.nextsolution.graphql.codegen.business;

import com.apollographql.apollo.api.Query;
import java.lang.String;
import java.lang.Override;
import javax.annotation.Generated;
import com.apollographql.apollo.api.OperationName;
import com.apollographql.apollo.api.Operation;
import com.apollographql.apollo.api.ResponseFieldMapper;
import com.apollographql.apollo.api.internal.Utils;
import java.lang.Integer;
import com.apollographql.apollo.api.ResponseField;
import javax.annotation.Nullable;
import com.nextsolution.graphql.codegen.dto.RoutingVanDay;
import java.util.Collections;
import com.apollographql.apollo.api.internal.UnmodifiableMapBuilder;
import java.lang.Object;
import java.util.List;
import com.apollographql.apollo.api.ResponseReader;
import com.apollographql.apollo.api.ResponseFieldMarshaller;
import com.apollographql.apollo.api.ResponseWriter;
import javax.annotation.Nonnull;
import java.util.LinkedHashMap;
import java.util.Map;
import com.apollographql.apollo.api.InputFieldMarshaller;
import com.apollographql.apollo.api.InputFieldWriter;
import java.io.IOException;

@Generated("Apollo GraphQL")
public final class GetRoutingPlanDayByCustomerIdQuery implements Query<GetRoutingPlanDayByCustomerIdQuery.Data, GetRoutingPlanDayByCustomerIdQuery.Data, GetRoutingPlanDayByCustomerIdQuery.Variables> {
  public static String OPERATION_DEFINITION = "query GetRoutingPlanDayByCustomerId($customerId: Int!, $date: String!) {   getRoutingPlanDayByCustomerId(customerId: $customerId, date: $date) {     ...RoutingVanDay   } }";
  public static String QUERY_DOCUMENT = OPERATION_DEFINITION;
  public static OperationName OPERATION_NAME = new OperationName() {
    @Override
    public String name() {
      return "GetRoutingPlanDayByCustomerId";
    }
  };
  private GetRoutingPlanDayByCustomerIdQuery.Variables variables;
  @Override
   public String queryDocument() {
    return QUERY_DOCUMENT;
  }
  
  @Override
   public GetRoutingPlanDayByCustomerIdQuery.Data wrapData(GetRoutingPlanDayByCustomerIdQuery.Data data) {
    return data;
  }
  
  @Override
   public GetRoutingPlanDayByCustomerIdQuery.Variables variables() {
    return variables;
  }
  
  @Override
   public ResponseFieldMapper<GetRoutingPlanDayByCustomerIdQuery.Data> responseFieldMapper() {
    return new Data.Mapper();
  }
  
  public static Builder builder() {
    return new Builder();
  }
  
  @Override
   public OperationName name() {
    return OPERATION_NAME;
  }
  
  public String operationId() {
    return "a7d96455b3eea0483a6429f5c459f9e0";
  }
  
  public GetRoutingPlanDayByCustomerIdQuery(@Nonnull Integer customerId, @Nonnull String date) {
    Utils.checkNotNull(customerId, "customerId == null");
    Utils.checkNotNull(date, "date == null");
    this.variables = new GetRoutingPlanDayByCustomerIdQuery.Variables(customerId, date);
  }
  @lombok.Getter
  @lombok.Setter
  public static class Data implements Operation.Data {
    private @Nullable List<RoutingVanDay> getRoutingPlanDayByCustomerId;
    private volatile String $toString;
    private volatile int $hashCode;
    private volatile boolean $hashCodeMemoized;
    static ResponseField[] $responseFields = {
        ResponseField.forList("getRoutingPlanDayByCustomerId", "getRoutingPlanDayByCustomerId", new UnmodifiableMapBuilder<String, Object>(2).put("customerId", new UnmodifiableMapBuilder<String, Object>(2).put("kind", "Variable").put("variableName", "customerId").build()).put("date", new UnmodifiableMapBuilder<String, Object>(2).put("kind", "Variable").put("variableName", "date").build()).build(), true, Collections.<ResponseField.Condition>emptyList())
      };
    public Data(@Nullable List<RoutingVanDay> getRoutingPlanDayByCustomerId) {
      this.getRoutingPlanDayByCustomerId = getRoutingPlanDayByCustomerId;
    }
    
    @Override
     public String toString() {
      if ($toString == null) {
        $toString = "Data{"
          + "getRoutingPlanDayByCustomerId=" + getRoutingPlanDayByCustomerId + ", "
          + "}";
      }
      
      return $toString;
    }
    
    public Data() {
      
    }
    
    @Override
     public boolean equals(Object o) {
      if (o == this) {
        return true;
      }
      if (o instanceof Data) {
        Data that = (Data) o;
        return ((this.getRoutingPlanDayByCustomerId == null) ? (that.getRoutingPlanDayByCustomerId == null) : this.getRoutingPlanDayByCustomerId.equals(that.getRoutingPlanDayByCustomerId));
      }
      
      return false;
    }
    
    @Override
     public int hashCode() {
      if (!$hashCodeMemoized) {
        int h = 1;
        h *= 1000003;
        h ^= (getRoutingPlanDayByCustomerId == null) ? 0 : getRoutingPlanDayByCustomerId.hashCode();
        $hashCode = h;
        $hashCodeMemoized = true;
      }
      
      return $hashCode;
    }
    
    public ResponseFieldMarshaller marshaller() {
      return new ResponseFieldMarshaller() {
        @Override
        public void marshal(ResponseWriter writer) {
          writer.writeList($responseFields[0], getRoutingPlanDayByCustomerId, new ResponseWriter.ListWriter() {
            @Override
            public void write(Object value, ResponseWriter.ListItemWriter listItemWriter) {
              listItemWriter.writeObject(((RoutingVanDay) value).marshaller());
            }
          });
        }
      };
    }
    public static final class Mapper implements ResponseFieldMapper<Data> {
      private RoutingVanDay.Mapper getRoutingPlanDayByCustomerIdFieldMapper = new RoutingVanDay.Mapper();
      @Override
       public Data map(ResponseReader reader) {
        final List<RoutingVanDay> getRoutingPlanDayByCustomerId = reader.readList($responseFields[0], new ResponseReader.ListReader<RoutingVanDay>() {
          @Override
          public RoutingVanDay read(ResponseReader.ListItemReader listItemReader) {
            return listItemReader.readObject(new ResponseReader.ObjectReader<RoutingVanDay>() {
                      @Override
                      public RoutingVanDay read(ResponseReader reader) {
                        return getRoutingPlanDayByCustomerIdFieldMapper.map(reader);
                      }
                    });
          }
        });
        return new Data(getRoutingPlanDayByCustomerId);
      }
    }
    
  }
  

  public static final class Builder {
    private @Nullable Integer customerId;
    private @Nullable String date;
    Builder() {
      
    }
    
    public Builder customerId(@Nullable Integer customerId) {
      this.customerId = customerId;
      return this;
    }
    
    public Builder date(@Nullable String date) {
      this.date = date;
      return this;
    }
    
    public GetRoutingPlanDayByCustomerIdQuery build() {
      return new GetRoutingPlanDayByCustomerIdQuery(customerId, date);
    }
  }
  

  public static final class Variables extends Operation.Variables {
    private @Nonnull Integer customerId;
    private @Nonnull String date;
    private transient Map<String, Object> valueMap = new LinkedHashMap<>();
    public Integer customerId() {
      return customerId;
    }
    
    public String date() {
      return date;
    }
    
    public Variables(@Nonnull Integer customerId, @Nonnull String date) {
      this.customerId = customerId;
      this.valueMap.put("customerId", customerId);
      this.date = date;
      this.valueMap.put("date", date);
    }
    
    @Override
     public Map<String, Object> valueMap() {
      return Collections.unmodifiableMap(valueMap);
    }
    
    @Override
     public InputFieldMarshaller marshaller() {
      return new InputFieldMarshaller() {
        @Override
        public void marshal(InputFieldWriter writer) throws IOException {
          writer.writeInt("customerId", customerId);
          writer.writeString("date", date);
        }
      };
    }
  }
  
}

