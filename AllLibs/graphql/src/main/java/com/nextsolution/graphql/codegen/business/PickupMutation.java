package com.nextsolution.graphql.codegen.business;

import com.apollographql.apollo.api.Mutation;
import java.lang.String;
import java.lang.Override;
import javax.annotation.Generated;
import com.apollographql.apollo.api.OperationName;
import com.apollographql.apollo.api.Operation;
import com.apollographql.apollo.api.ResponseFieldMapper;
import com.apollographql.apollo.api.internal.Utils;
import java.lang.Integer;
import com.apollographql.apollo.api.ResponseField;
import javax.annotation.Nullable;
import com.nextsolution.graphql.codegen.dto.RoutingVanDay;
import java.util.Collections;
import com.apollographql.apollo.api.internal.UnmodifiableMapBuilder;
import java.lang.Object;
import com.apollographql.apollo.api.ResponseReader;
import com.apollographql.apollo.api.ResponseFieldMarshaller;
import com.apollographql.apollo.api.ResponseWriter;
import javax.annotation.Nonnull;
import java.util.LinkedHashMap;
import java.util.Map;
import com.apollographql.apollo.api.InputFieldMarshaller;
import com.apollographql.apollo.api.InputFieldWriter;
import java.io.IOException;

@Generated("Apollo GraphQL")
public final class PickupMutation implements Mutation<PickupMutation.Data, PickupMutation.Data, PickupMutation.Variables> {
  public static String OPERATION_DEFINITION = "mutation Pickup($routingPlanDayId: Int!) {   pickup(routingPlanDayId: $routingPlanDayId) {     ...RoutingVanDay   } }";
  public static String QUERY_DOCUMENT = OPERATION_DEFINITION;
  public static OperationName OPERATION_NAME = new OperationName() {
    @Override
    public String name() {
      return "Pickup";
    }
  };
  private PickupMutation.Variables variables;
  @Override
   public String queryDocument() {
    return QUERY_DOCUMENT;
  }
  
  @Override
   public PickupMutation.Data wrapData(PickupMutation.Data data) {
    return data;
  }
  
  @Override
   public PickupMutation.Variables variables() {
    return variables;
  }
  
  @Override
   public ResponseFieldMapper<PickupMutation.Data> responseFieldMapper() {
    return new Data.Mapper();
  }
  
  public static Builder builder() {
    return new Builder();
  }
  
  @Override
   public OperationName name() {
    return OPERATION_NAME;
  }
  
  public String operationId() {
    return "2957b573edadf6e884ad5238e47ee46a";
  }
  
  public PickupMutation(@Nonnull Integer routingPlanDayId) {
    Utils.checkNotNull(routingPlanDayId, "routingPlanDayId == null");
    this.variables = new PickupMutation.Variables(routingPlanDayId);
  }
  @lombok.Getter
  @lombok.Setter
  public static class Data implements Operation.Data {
    private @Nullable RoutingVanDay pickup;
    private volatile String $toString;
    private volatile int $hashCode;
    private volatile boolean $hashCodeMemoized;
    static ResponseField[] $responseFields = {
        ResponseField.forObject("pickup", "pickup", new UnmodifiableMapBuilder<String, Object>(1).put("routingPlanDayId", new UnmodifiableMapBuilder<String, Object>(2).put("kind", "Variable").put("variableName", "routingPlanDayId").build()).build(), true, Collections.<ResponseField.Condition>emptyList())
      };
    public Data(@Nullable RoutingVanDay pickup) {
      this.pickup = pickup;
    }
    
    @Override
     public String toString() {
      if ($toString == null) {
        $toString = "Data{"
          + "pickup=" + pickup + ", "
          + "}";
      }
      
      return $toString;
    }
    
    public Data() {
      
    }
    
    @Override
     public boolean equals(Object o) {
      if (o == this) {
        return true;
      }
      if (o instanceof Data) {
        Data that = (Data) o;
        return ((this.pickup == null) ? (that.pickup == null) : this.pickup.equals(that.pickup));
      }
      
      return false;
    }
    
    @Override
     public int hashCode() {
      if (!$hashCodeMemoized) {
        int h = 1;
        h *= 1000003;
        h ^= (pickup == null) ? 0 : pickup.hashCode();
        $hashCode = h;
        $hashCodeMemoized = true;
      }
      
      return $hashCode;
    }
    
    public ResponseFieldMarshaller marshaller() {
      return new ResponseFieldMarshaller() {
        @Override
        public void marshal(ResponseWriter writer) {
          writer.writeObject($responseFields[0], pickup != null ? pickup.marshaller() : null);
        }
      };
    }
    public static final class Mapper implements ResponseFieldMapper<Data> {
      private RoutingVanDay.Mapper pickupFieldMapper = new RoutingVanDay.Mapper();
      @Override
       public Data map(ResponseReader reader) {
        final RoutingVanDay pickup = reader.readObject($responseFields[0], new ResponseReader.ObjectReader<RoutingVanDay>() {
                  @Override
                  public RoutingVanDay read(ResponseReader reader) {
                    return pickupFieldMapper.map(reader);
                  }
                });
        return new Data(pickup);
      }
    }
    
  }
  

  public static final class Builder {
    private @Nullable Integer routingPlanDayId;
    Builder() {
      
    }
    
    public Builder routingPlanDayId(@Nullable Integer routingPlanDayId) {
      this.routingPlanDayId = routingPlanDayId;
      return this;
    }
    
    public PickupMutation build() {
      return new PickupMutation(routingPlanDayId);
    }
  }
  

  public static final class Variables extends Operation.Variables {
    private @Nonnull Integer routingPlanDayId;
    private transient Map<String, Object> valueMap = new LinkedHashMap<>();
    public Integer routingPlanDayId() {
      return routingPlanDayId;
    }
    
    public Variables(@Nonnull Integer routingPlanDayId) {
      this.routingPlanDayId = routingPlanDayId;
      this.valueMap.put("routingPlanDayId", routingPlanDayId);
    }
    
    @Override
     public Map<String, Object> valueMap() {
      return Collections.unmodifiableMap(valueMap);
    }
    
    @Override
     public InputFieldMarshaller marshaller() {
      return new InputFieldMarshaller() {
        @Override
        public void marshal(InputFieldWriter writer) throws IOException {
          writer.writeInt("routingPlanDayId", routingPlanDayId);
        }
      };
    }
  }
  
}

