package com.nextsolution.graphql.codegen.dto;

import java.util.Arrays;
import com.apollographql.apollo.api.GraphqlFragment;
import java.util.List;
import java.lang.String;
import java.util.Collections;
import java.lang.Override;
import javax.annotation.Generated;
import com.apollographql.apollo.api.ResponseFieldMapper;
import com.apollographql.apollo.api.ResponseField;
import javax.annotation.Nullable;
import java.lang.Integer;
import com.nextsolution.graphql.codegen.types.CustomType;
import javax.annotation.Nonnull;
import com.apollographql.apollo.api.internal.Utils;
import com.apollographql.apollo.api.ResponseReader;
import com.apollographql.apollo.api.ResponseFieldMarshaller;
import com.apollographql.apollo.api.ResponseWriter;

@lombok.Getter
@lombok.Setter
public class Customer implements GraphqlFragment {
  private @Nonnull String __typename;
  private @Nullable String address;
  private @Nullable String address1;
  private @Nullable String address2;
  private @Nullable Integer app_param_value_id;
  private @Nullable String avatar_url;
  private @Nullable String city;
  private @Nullable String code;
  private @Nullable String contact_name;
  private @Nullable java.util.Date create_date;
  private @Nullable String create_user;
  private @Nullable Double discount;
  private @Nullable String discount_type;
  private @Nullable String email;
  private @Nullable String fax;
  private @Nullable Integer has_attachment;
  private @Nullable Integer has_image;
  private @Nullable Integer id;
  private @Nullable String id_no;
  private @Nullable String is_vip;
  private @Nullable String mobile_phone;
  private @Nullable String name;
  private @Nullable java.util.Date pay_by_day;
  private @Nullable String payment_type;
  private @Nullable String phone;
  private @Nullable String phone1;
  private @Nullable String phone2;
  private @Nullable String postal_code;
  private @Nullable String state_province;
  private @Nullable Integer status;
  private @Nullable String tax_code;
  private @Nullable java.util.Date update_date;
  private @Nullable String update_user;
  private @Nullable String wallet_account;
  private @Nullable Integer wallet_id;
  private @Nullable String website;
  private @Nullable String user_name;
  private volatile String $toString;
  private volatile int $hashCode;
  private volatile boolean $hashCodeMemoized;
  static ResponseField[] $responseFields = {
      ResponseField.forString("__typename", "__typename", null, false, Collections.<ResponseField.Condition>emptyList()),
      ResponseField.forString("address", "address", null, true, Collections.<ResponseField.Condition>emptyList()),
      ResponseField.forString("address1", "address1", null, true, Collections.<ResponseField.Condition>emptyList()),
      ResponseField.forString("address2", "address2", null, true, Collections.<ResponseField.Condition>emptyList()),
      ResponseField.forInt("app_param_value_id", "app_param_value_id", null, true, Collections.<ResponseField.Condition>emptyList()),
      ResponseField.forString("avatar_url", "avatar_url", null, true, Collections.<ResponseField.Condition>emptyList()),
      ResponseField.forString("city", "city", null, true, Collections.<ResponseField.Condition>emptyList()),
      ResponseField.forString("code", "code", null, true, Collections.<ResponseField.Condition>emptyList()),
      ResponseField.forString("contact_name", "contact_name", null, true, Collections.<ResponseField.Condition>emptyList()),
      ResponseField.forCustomType("create_date", "create_date", null, true, CustomType.Date, Collections.<ResponseField.Condition>emptyList()),
      ResponseField.forString("create_user", "create_user", null, true, Collections.<ResponseField.Condition>emptyList()),
      ResponseField.forDouble("discount", "discount", null, true, Collections.<ResponseField.Condition>emptyList()),
      ResponseField.forString("discount_type", "discount_type", null, true, Collections.<ResponseField.Condition>emptyList()),
      ResponseField.forString("email", "email", null, true, Collections.<ResponseField.Condition>emptyList()),
      ResponseField.forString("fax", "fax", null, true, Collections.<ResponseField.Condition>emptyList()),
      ResponseField.forInt("has_attachment", "has_attachment", null, true, Collections.<ResponseField.Condition>emptyList()),
      ResponseField.forInt("has_image", "has_image", null, true, Collections.<ResponseField.Condition>emptyList()),
      ResponseField.forInt("id", "id", null, true, Collections.<ResponseField.Condition>emptyList()),
      ResponseField.forString("id_no", "id_no", null, true, Collections.<ResponseField.Condition>emptyList()),
      ResponseField.forString("is_vip", "is_vip", null, true, Collections.<ResponseField.Condition>emptyList()),
      ResponseField.forString("mobile_phone", "mobile_phone", null, true, Collections.<ResponseField.Condition>emptyList()),
      ResponseField.forString("name", "name", null, true, Collections.<ResponseField.Condition>emptyList()),
      ResponseField.forCustomType("pay_by_day", "pay_by_day", null, true, CustomType.Date, Collections.<ResponseField.Condition>emptyList()),
      ResponseField.forString("payment_type", "payment_type", null, true, Collections.<ResponseField.Condition>emptyList()),
      ResponseField.forString("phone", "phone", null, true, Collections.<ResponseField.Condition>emptyList()),
      ResponseField.forString("phone1", "phone1", null, true, Collections.<ResponseField.Condition>emptyList()),
      ResponseField.forString("phone2", "phone2", null, true, Collections.<ResponseField.Condition>emptyList()),
      ResponseField.forString("postal_code", "postal_code", null, true, Collections.<ResponseField.Condition>emptyList()),
      ResponseField.forString("state_province", "state_province", null, true, Collections.<ResponseField.Condition>emptyList()),
      ResponseField.forInt("status", "status", null, true, Collections.<ResponseField.Condition>emptyList()),
      ResponseField.forString("tax_code", "tax_code", null, true, Collections.<ResponseField.Condition>emptyList()),
      ResponseField.forCustomType("update_date", "update_date", null, true, CustomType.Date, Collections.<ResponseField.Condition>emptyList()),
      ResponseField.forString("update_user", "update_user", null, true, Collections.<ResponseField.Condition>emptyList()),
      ResponseField.forString("wallet_account", "wallet_account", null, true, Collections.<ResponseField.Condition>emptyList()),
      ResponseField.forInt("wallet_id", "wallet_id", null, true, Collections.<ResponseField.Condition>emptyList()),
      ResponseField.forString("website", "website", null, true, Collections.<ResponseField.Condition>emptyList()),
      ResponseField.forString("user_name", "user_name", null, true, Collections.<ResponseField.Condition>emptyList())
    };
  public static String FRAGMENT_DEFINITION = "fragment Customer on CustomerDto {   address   address1   address2   app_param_value_id   avatar_url   city   code   contact_name   create_date   create_user   discount   discount_type   email   fax   has_attachment   has_image   id   id_no   is_vip   mobile_phone   name   pay_by_day   payment_type   phone   phone1   phone2   postal_code   state_province   status   tax_code   update_date   update_user   wallet_account   wallet_id   website   user_name }";
  public static List<String> POSSIBLE_TYPES = Collections.unmodifiableList(Arrays.asList("CustomerDto"));
  public Customer(@Nonnull String __typename, @Nullable String address, @Nullable String address1, @Nullable String address2, @Nullable Integer app_param_value_id, @Nullable String avatar_url, @Nullable String city, @Nullable String code, @Nullable String contact_name, @Nullable java.util.Date create_date, @Nullable String create_user, @Nullable Double discount, @Nullable String discount_type, @Nullable String email, @Nullable String fax, @Nullable Integer has_attachment, @Nullable Integer has_image, @Nullable Integer id, @Nullable String id_no, @Nullable String is_vip, @Nullable String mobile_phone, @Nullable String name, @Nullable java.util.Date pay_by_day, @Nullable String payment_type, @Nullable String phone, @Nullable String phone1, @Nullable String phone2, @Nullable String postal_code, @Nullable String state_province, @Nullable Integer status, @Nullable String tax_code, @Nullable java.util.Date update_date, @Nullable String update_user, @Nullable String wallet_account, @Nullable Integer wallet_id, @Nullable String website, @Nullable String user_name) {
    this.__typename = Utils.checkNotNull(__typename, "__typename == null");
    this.address = address;
    this.address1 = address1;
    this.address2 = address2;
    this.app_param_value_id = app_param_value_id;
    this.avatar_url = avatar_url;
    this.city = city;
    this.code = code;
    this.contact_name = contact_name;
    this.create_date = create_date;
    this.create_user = create_user;
    this.discount = discount;
    this.discount_type = discount_type;
    this.email = email;
    this.fax = fax;
    this.has_attachment = has_attachment;
    this.has_image = has_image;
    this.id = id;
    this.id_no = id_no;
    this.is_vip = is_vip;
    this.mobile_phone = mobile_phone;
    this.name = name;
    this.pay_by_day = pay_by_day;
    this.payment_type = payment_type;
    this.phone = phone;
    this.phone1 = phone1;
    this.phone2 = phone2;
    this.postal_code = postal_code;
    this.state_province = state_province;
    this.status = status;
    this.tax_code = tax_code;
    this.update_date = update_date;
    this.update_user = update_user;
    this.wallet_account = wallet_account;
    this.wallet_id = wallet_id;
    this.website = website;
    this.user_name = user_name;
  }
  
  @Override
   public String toString() {
    if ($toString == null) {
      $toString = "Customer{"
        + "__typename=" + __typename + ", "
        + "address=" + address + ", "
        + "address1=" + address1 + ", "
        + "address2=" + address2 + ", "
        + "app_param_value_id=" + app_param_value_id + ", "
        + "avatar_url=" + avatar_url + ", "
        + "city=" + city + ", "
        + "code=" + code + ", "
        + "contact_name=" + contact_name + ", "
        + "create_date=" + create_date + ", "
        + "create_user=" + create_user + ", "
        + "discount=" + discount + ", "
        + "discount_type=" + discount_type + ", "
        + "email=" + email + ", "
        + "fax=" + fax + ", "
        + "has_attachment=" + has_attachment + ", "
        + "has_image=" + has_image + ", "
        + "id=" + id + ", "
        + "id_no=" + id_no + ", "
        + "is_vip=" + is_vip + ", "
        + "mobile_phone=" + mobile_phone + ", "
        + "name=" + name + ", "
        + "pay_by_day=" + pay_by_day + ", "
        + "payment_type=" + payment_type + ", "
        + "phone=" + phone + ", "
        + "phone1=" + phone1 + ", "
        + "phone2=" + phone2 + ", "
        + "postal_code=" + postal_code + ", "
        + "state_province=" + state_province + ", "
        + "status=" + status + ", "
        + "tax_code=" + tax_code + ", "
        + "update_date=" + update_date + ", "
        + "update_user=" + update_user + ", "
        + "wallet_account=" + wallet_account + ", "
        + "wallet_id=" + wallet_id + ", "
        + "website=" + website + ", "
        + "user_name=" + user_name + ", "
        + "}";
    }
    
    return $toString;
  }
  
  public Customer() {
    
  }
  
  @Override
   public boolean equals(Object o) {
    if (o == this) {
      return true;
    }
    if (o instanceof Customer) {
      Customer that = (Customer) o;
      return this.__typename.equals(that.__typename) && ((this.address == null) ? (that.address == null) : this.address.equals(that.address)) && ((this.address1 == null) ? (that.address1 == null) : this.address1.equals(that.address1)) && ((this.address2 == null) ? (that.address2 == null) : this.address2.equals(that.address2)) && ((this.app_param_value_id == null) ? (that.app_param_value_id == null) : this.app_param_value_id.equals(that.app_param_value_id)) && ((this.avatar_url == null) ? (that.avatar_url == null) : this.avatar_url.equals(that.avatar_url)) && ((this.city == null) ? (that.city == null) : this.city.equals(that.city)) && ((this.code == null) ? (that.code == null) : this.code.equals(that.code)) && ((this.contact_name == null) ? (that.contact_name == null) : this.contact_name.equals(that.contact_name)) && ((this.create_date == null) ? (that.create_date == null) : this.create_date.equals(that.create_date)) && ((this.create_user == null) ? (that.create_user == null) : this.create_user.equals(that.create_user)) && ((this.discount == null) ? (that.discount == null) : this.discount.equals(that.discount)) && ((this.discount_type == null) ? (that.discount_type == null) : this.discount_type.equals(that.discount_type)) && ((this.email == null) ? (that.email == null) : this.email.equals(that.email)) && ((this.fax == null) ? (that.fax == null) : this.fax.equals(that.fax)) && ((this.has_attachment == null) ? (that.has_attachment == null) : this.has_attachment.equals(that.has_attachment)) && ((this.has_image == null) ? (that.has_image == null) : this.has_image.equals(that.has_image)) && ((this.id == null) ? (that.id == null) : this.id.equals(that.id)) && ((this.id_no == null) ? (that.id_no == null) : this.id_no.equals(that.id_no)) && ((this.is_vip == null) ? (that.is_vip == null) : this.is_vip.equals(that.is_vip)) && ((this.mobile_phone == null) ? (that.mobile_phone == null) : this.mobile_phone.equals(that.mobile_phone)) && ((this.name == null) ? (that.name == null) : this.name.equals(that.name)) && ((this.pay_by_day == null) ? (that.pay_by_day == null) : this.pay_by_day.equals(that.pay_by_day)) && ((this.payment_type == null) ? (that.payment_type == null) : this.payment_type.equals(that.payment_type)) && ((this.phone == null) ? (that.phone == null) : this.phone.equals(that.phone)) && ((this.phone1 == null) ? (that.phone1 == null) : this.phone1.equals(that.phone1)) && ((this.phone2 == null) ? (that.phone2 == null) : this.phone2.equals(that.phone2)) && ((this.postal_code == null) ? (that.postal_code == null) : this.postal_code.equals(that.postal_code)) && ((this.state_province == null) ? (that.state_province == null) : this.state_province.equals(that.state_province)) && ((this.status == null) ? (that.status == null) : this.status.equals(that.status)) && ((this.tax_code == null) ? (that.tax_code == null) : this.tax_code.equals(that.tax_code)) && ((this.update_date == null) ? (that.update_date == null) : this.update_date.equals(that.update_date)) && ((this.update_user == null) ? (that.update_user == null) : this.update_user.equals(that.update_user)) && ((this.wallet_account == null) ? (that.wallet_account == null) : this.wallet_account.equals(that.wallet_account)) && ((this.wallet_id == null) ? (that.wallet_id == null) : this.wallet_id.equals(that.wallet_id)) && ((this.website == null) ? (that.website == null) : this.website.equals(that.website)) && ((this.user_name == null) ? (that.user_name == null) : this.user_name.equals(that.user_name));
    }
    
    return false;
  }
  
  @Override
   public int hashCode() {
    if (!$hashCodeMemoized) {
      int h = 1;
      h *= 1000003;
      h ^= __typename.hashCode();
      h *= 1000003;
      h ^= (address == null) ? 0 : address.hashCode();
      h *= 1000003;
      h ^= (address1 == null) ? 0 : address1.hashCode();
      h *= 1000003;
      h ^= (address2 == null) ? 0 : address2.hashCode();
      h *= 1000003;
      h ^= (app_param_value_id == null) ? 0 : app_param_value_id.hashCode();
      h *= 1000003;
      h ^= (avatar_url == null) ? 0 : avatar_url.hashCode();
      h *= 1000003;
      h ^= (city == null) ? 0 : city.hashCode();
      h *= 1000003;
      h ^= (code == null) ? 0 : code.hashCode();
      h *= 1000003;
      h ^= (contact_name == null) ? 0 : contact_name.hashCode();
      h *= 1000003;
      h ^= (create_date == null) ? 0 : create_date.hashCode();
      h *= 1000003;
      h ^= (create_user == null) ? 0 : create_user.hashCode();
      h *= 1000003;
      h ^= (discount == null) ? 0 : discount.hashCode();
      h *= 1000003;
      h ^= (discount_type == null) ? 0 : discount_type.hashCode();
      h *= 1000003;
      h ^= (email == null) ? 0 : email.hashCode();
      h *= 1000003;
      h ^= (fax == null) ? 0 : fax.hashCode();
      h *= 1000003;
      h ^= (has_attachment == null) ? 0 : has_attachment.hashCode();
      h *= 1000003;
      h ^= (has_image == null) ? 0 : has_image.hashCode();
      h *= 1000003;
      h ^= (id == null) ? 0 : id.hashCode();
      h *= 1000003;
      h ^= (id_no == null) ? 0 : id_no.hashCode();
      h *= 1000003;
      h ^= (is_vip == null) ? 0 : is_vip.hashCode();
      h *= 1000003;
      h ^= (mobile_phone == null) ? 0 : mobile_phone.hashCode();
      h *= 1000003;
      h ^= (name == null) ? 0 : name.hashCode();
      h *= 1000003;
      h ^= (pay_by_day == null) ? 0 : pay_by_day.hashCode();
      h *= 1000003;
      h ^= (payment_type == null) ? 0 : payment_type.hashCode();
      h *= 1000003;
      h ^= (phone == null) ? 0 : phone.hashCode();
      h *= 1000003;
      h ^= (phone1 == null) ? 0 : phone1.hashCode();
      h *= 1000003;
      h ^= (phone2 == null) ? 0 : phone2.hashCode();
      h *= 1000003;
      h ^= (postal_code == null) ? 0 : postal_code.hashCode();
      h *= 1000003;
      h ^= (state_province == null) ? 0 : state_province.hashCode();
      h *= 1000003;
      h ^= (status == null) ? 0 : status.hashCode();
      h *= 1000003;
      h ^= (tax_code == null) ? 0 : tax_code.hashCode();
      h *= 1000003;
      h ^= (update_date == null) ? 0 : update_date.hashCode();
      h *= 1000003;
      h ^= (update_user == null) ? 0 : update_user.hashCode();
      h *= 1000003;
      h ^= (wallet_account == null) ? 0 : wallet_account.hashCode();
      h *= 1000003;
      h ^= (wallet_id == null) ? 0 : wallet_id.hashCode();
      h *= 1000003;
      h ^= (website == null) ? 0 : website.hashCode();
      h *= 1000003;
      h ^= (user_name == null) ? 0 : user_name.hashCode();
      $hashCode = h;
      $hashCodeMemoized = true;
    }
    
    return $hashCode;
  }
  
  public ResponseFieldMarshaller marshaller() {
    return new ResponseFieldMarshaller() {
      @Override
      public void marshal(ResponseWriter writer) {
        writer.writeString($responseFields[0], __typename);
        writer.writeString($responseFields[1], address != null ? address : null);
        writer.writeString($responseFields[2], address1 != null ? address1 : null);
        writer.writeString($responseFields[3], address2 != null ? address2 : null);
        writer.writeInt($responseFields[4], app_param_value_id != null ? app_param_value_id : null);
        writer.writeString($responseFields[5], avatar_url != null ? avatar_url : null);
        writer.writeString($responseFields[6], city != null ? city : null);
        writer.writeString($responseFields[7], code != null ? code : null);
        writer.writeString($responseFields[8], contact_name != null ? contact_name : null);
        writer.writeCustom((ResponseField.CustomTypeField) $responseFields[9], create_date != null ? create_date : null);
        writer.writeString($responseFields[10], create_user != null ? create_user : null);
        writer.writeDouble($responseFields[11], discount != null ? discount : null);
        writer.writeString($responseFields[12], discount_type != null ? discount_type : null);
        writer.writeString($responseFields[13], email != null ? email : null);
        writer.writeString($responseFields[14], fax != null ? fax : null);
        writer.writeInt($responseFields[15], has_attachment != null ? has_attachment : null);
        writer.writeInt($responseFields[16], has_image != null ? has_image : null);
        writer.writeInt($responseFields[17], id != null ? id : null);
        writer.writeString($responseFields[18], id_no != null ? id_no : null);
        writer.writeString($responseFields[19], is_vip != null ? is_vip : null);
        writer.writeString($responseFields[20], mobile_phone != null ? mobile_phone : null);
        writer.writeString($responseFields[21], name != null ? name : null);
        writer.writeCustom((ResponseField.CustomTypeField) $responseFields[22], pay_by_day != null ? pay_by_day : null);
        writer.writeString($responseFields[23], payment_type != null ? payment_type : null);
        writer.writeString($responseFields[24], phone != null ? phone : null);
        writer.writeString($responseFields[25], phone1 != null ? phone1 : null);
        writer.writeString($responseFields[26], phone2 != null ? phone2 : null);
        writer.writeString($responseFields[27], postal_code != null ? postal_code : null);
        writer.writeString($responseFields[28], state_province != null ? state_province : null);
        writer.writeInt($responseFields[29], status != null ? status : null);
        writer.writeString($responseFields[30], tax_code != null ? tax_code : null);
        writer.writeCustom((ResponseField.CustomTypeField) $responseFields[31], update_date != null ? update_date : null);
        writer.writeString($responseFields[32], update_user != null ? update_user : null);
        writer.writeString($responseFields[33], wallet_account != null ? wallet_account : null);
        writer.writeInt($responseFields[34], wallet_id != null ? wallet_id : null);
        writer.writeString($responseFields[35], website != null ? website : null);
        writer.writeString($responseFields[36], user_name != null ? user_name : null);
      }
    };
  }
  public static final class Mapper implements ResponseFieldMapper<Customer> {
    @Override
     public Customer map(ResponseReader reader) {
      final String __typename = reader.readString($responseFields[0]);
      final String address = reader.readString($responseFields[1]);
      final String address1 = reader.readString($responseFields[2]);
      final String address2 = reader.readString($responseFields[3]);
      final Integer app_param_value_id = reader.readInt($responseFields[4]);
      final String avatar_url = reader.readString($responseFields[5]);
      final String city = reader.readString($responseFields[6]);
      final String code = reader.readString($responseFields[7]);
      final String contact_name = reader.readString($responseFields[8]);
      final java.util.Date create_date = reader.readCustomType((ResponseField.CustomTypeField) $responseFields[9]);
      final String create_user = reader.readString($responseFields[10]);
      final Double discount = reader.readDouble($responseFields[11]);
      final String discount_type = reader.readString($responseFields[12]);
      final String email = reader.readString($responseFields[13]);
      final String fax = reader.readString($responseFields[14]);
      final Integer has_attachment = reader.readInt($responseFields[15]);
      final Integer has_image = reader.readInt($responseFields[16]);
      final Integer id = reader.readInt($responseFields[17]);
      final String id_no = reader.readString($responseFields[18]);
      final String is_vip = reader.readString($responseFields[19]);
      final String mobile_phone = reader.readString($responseFields[20]);
      final String name = reader.readString($responseFields[21]);
      final java.util.Date pay_by_day = reader.readCustomType((ResponseField.CustomTypeField) $responseFields[22]);
      final String payment_type = reader.readString($responseFields[23]);
      final String phone = reader.readString($responseFields[24]);
      final String phone1 = reader.readString($responseFields[25]);
      final String phone2 = reader.readString($responseFields[26]);
      final String postal_code = reader.readString($responseFields[27]);
      final String state_province = reader.readString($responseFields[28]);
      final Integer status = reader.readInt($responseFields[29]);
      final String tax_code = reader.readString($responseFields[30]);
      final java.util.Date update_date = reader.readCustomType((ResponseField.CustomTypeField) $responseFields[31]);
      final String update_user = reader.readString($responseFields[32]);
      final String wallet_account = reader.readString($responseFields[33]);
      final Integer wallet_id = reader.readInt($responseFields[34]);
      final String website = reader.readString($responseFields[35]);
      final String user_name = reader.readString($responseFields[36]);
      return new Customer(__typename, address, address1, address2, app_param_value_id, avatar_url, city, code, contact_name, create_date, create_user, discount, discount_type, email, fax, has_attachment, has_image, id, id_no, is_vip, mobile_phone, name, pay_by_day, payment_type, phone, phone1, phone2, postal_code, state_province, status, tax_code, update_date, update_user, wallet_account, wallet_id, website, user_name);
    }
  }
  
}

