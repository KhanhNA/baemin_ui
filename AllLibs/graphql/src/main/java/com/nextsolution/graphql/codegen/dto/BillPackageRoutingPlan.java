package com.nextsolution.graphql.codegen.dto;

import java.util.Arrays;
import com.apollographql.apollo.api.GraphqlFragment;
import java.util.List;
import java.lang.String;
import java.util.Collections;
import java.lang.Override;
import javax.annotation.Generated;
import com.apollographql.apollo.api.ResponseFieldMapper;
import com.apollographql.apollo.api.ResponseField;
import javax.annotation.Nullable;
import java.lang.Integer;
import com.nextsolution.graphql.codegen.types.CustomType;
import javax.annotation.Nonnull;
import com.apollographql.apollo.api.internal.Utils;
import com.apollographql.apollo.api.ResponseReader;
import com.apollographql.apollo.api.ResponseFieldMarshaller;
import com.apollographql.apollo.api.ResponseWriter;

@lombok.Getter
@lombok.Setter
public class BillPackageRoutingPlan implements GraphqlFragment {
  private @Nonnull String __typename;
  private @Nullable String QRchar;
  private @Nullable Integer bill_lading_detail_id;
  private @Nullable Integer bill_package_id;
  private @Nullable Double capacity;
  private @Nullable java.util.Date create_date;
  private @Nullable String create_user;
  private @Nullable Integer from_warehouse_id;
  private @Nullable Double height;
  private @Nullable Integer id;
  private @Nullable String insurance_name;
  private @Nullable String item_name;
  private @Nullable Double length;
  private @Nullable Integer quantity_plan;
  private @Nullable Integer routing_plan_day_id;
  private @Nullable String service_name;
  private @Nullable Integer to_warehouse_id;
  private @Nullable Double total_weight;
  private @Nullable java.util.Date update_date;
  private @Nullable String update_user;
  private @Nullable Double width;
  private volatile String $toString;
  private volatile int $hashCode;
  private volatile boolean $hashCodeMemoized;
  static ResponseField[] $responseFields = {
      ResponseField.forString("__typename", "__typename", null, false, Collections.<ResponseField.Condition>emptyList()),
      ResponseField.forString("QRchar", "QRchar", null, true, Collections.<ResponseField.Condition>emptyList()),
      ResponseField.forInt("bill_lading_detail_id", "bill_lading_detail_id", null, true, Collections.<ResponseField.Condition>emptyList()),
      ResponseField.forInt("bill_package_id", "bill_package_id", null, true, Collections.<ResponseField.Condition>emptyList()),
      ResponseField.forDouble("capacity", "capacity", null, true, Collections.<ResponseField.Condition>emptyList()),
      ResponseField.forCustomType("create_date", "create_date", null, true, CustomType.Date, Collections.<ResponseField.Condition>emptyList()),
      ResponseField.forString("create_user", "create_user", null, true, Collections.<ResponseField.Condition>emptyList()),
      ResponseField.forInt("from_warehouse_id", "from_warehouse_id", null, true, Collections.<ResponseField.Condition>emptyList()),
      ResponseField.forDouble("height", "height", null, true, Collections.<ResponseField.Condition>emptyList()),
      ResponseField.forInt("id", "id", null, true, Collections.<ResponseField.Condition>emptyList()),
      ResponseField.forString("insurance_name", "insurance_name", null, true, Collections.<ResponseField.Condition>emptyList()),
      ResponseField.forString("item_name", "item_name", null, true, Collections.<ResponseField.Condition>emptyList()),
      ResponseField.forDouble("length", "length", null, true, Collections.<ResponseField.Condition>emptyList()),
      ResponseField.forInt("quantity_plan", "quantity_plan", null, true, Collections.<ResponseField.Condition>emptyList()),
      ResponseField.forInt("routing_plan_day_id", "routing_plan_day_id", null, true, Collections.<ResponseField.Condition>emptyList()),
      ResponseField.forString("service_name", "service_name", null, true, Collections.<ResponseField.Condition>emptyList()),
      ResponseField.forInt("to_warehouse_id", "to_warehouse_id", null, true, Collections.<ResponseField.Condition>emptyList()),
      ResponseField.forDouble("total_weight", "total_weight", null, true, Collections.<ResponseField.Condition>emptyList()),
      ResponseField.forCustomType("update_date", "update_date", null, true, CustomType.Date, Collections.<ResponseField.Condition>emptyList()),
      ResponseField.forString("update_user", "update_user", null, true, Collections.<ResponseField.Condition>emptyList()),
      ResponseField.forDouble("width", "width", null, true, Collections.<ResponseField.Condition>emptyList())
    };
  public static String FRAGMENT_DEFINITION = "fragment BillPackageRoutingPlan on BillPackageRoutingPlanDto {   QRchar   bill_lading_detail_id   bill_package_id   capacity   create_date   create_user   from_warehouse_id   height   id   insurance_name   item_name   length   quantity_plan   routing_plan_day_id   service_name   to_warehouse_id   total_weight   update_date   update_user   width }";
  public static List<String> POSSIBLE_TYPES = Collections.unmodifiableList(Arrays.asList("BillPackageRoutingPlanDto"));
  public BillPackageRoutingPlan(@Nonnull String __typename, @Nullable String QRchar, @Nullable Integer bill_lading_detail_id, @Nullable Integer bill_package_id, @Nullable Double capacity, @Nullable java.util.Date create_date, @Nullable String create_user, @Nullable Integer from_warehouse_id, @Nullable Double height, @Nullable Integer id, @Nullable String insurance_name, @Nullable String item_name, @Nullable Double length, @Nullable Integer quantity_plan, @Nullable Integer routing_plan_day_id, @Nullable String service_name, @Nullable Integer to_warehouse_id, @Nullable Double total_weight, @Nullable java.util.Date update_date, @Nullable String update_user, @Nullable Double width) {
    this.__typename = Utils.checkNotNull(__typename, "__typename == null");
    this.QRchar = QRchar;
    this.bill_lading_detail_id = bill_lading_detail_id;
    this.bill_package_id = bill_package_id;
    this.capacity = capacity;
    this.create_date = create_date;
    this.create_user = create_user;
    this.from_warehouse_id = from_warehouse_id;
    this.height = height;
    this.id = id;
    this.insurance_name = insurance_name;
    this.item_name = item_name;
    this.length = length;
    this.quantity_plan = quantity_plan;
    this.routing_plan_day_id = routing_plan_day_id;
    this.service_name = service_name;
    this.to_warehouse_id = to_warehouse_id;
    this.total_weight = total_weight;
    this.update_date = update_date;
    this.update_user = update_user;
    this.width = width;
  }
  
  @Override
   public String toString() {
    if ($toString == null) {
      $toString = "BillPackageRoutingPlan{"
        + "__typename=" + __typename + ", "
        + "QRchar=" + QRchar + ", "
        + "bill_lading_detail_id=" + bill_lading_detail_id + ", "
        + "bill_package_id=" + bill_package_id + ", "
        + "capacity=" + capacity + ", "
        + "create_date=" + create_date + ", "
        + "create_user=" + create_user + ", "
        + "from_warehouse_id=" + from_warehouse_id + ", "
        + "height=" + height + ", "
        + "id=" + id + ", "
        + "insurance_name=" + insurance_name + ", "
        + "item_name=" + item_name + ", "
        + "length=" + length + ", "
        + "quantity_plan=" + quantity_plan + ", "
        + "routing_plan_day_id=" + routing_plan_day_id + ", "
        + "service_name=" + service_name + ", "
        + "to_warehouse_id=" + to_warehouse_id + ", "
        + "total_weight=" + total_weight + ", "
        + "update_date=" + update_date + ", "
        + "update_user=" + update_user + ", "
        + "width=" + width + ", "
        + "}";
    }
    
    return $toString;
  }
  
  public BillPackageRoutingPlan() {
    
  }
  
  @Override
   public boolean equals(Object o) {
    if (o == this) {
      return true;
    }
    if (o instanceof BillPackageRoutingPlan) {
      BillPackageRoutingPlan that = (BillPackageRoutingPlan) o;
      return this.__typename.equals(that.__typename) && ((this.QRchar == null) ? (that.QRchar == null) : this.QRchar.equals(that.QRchar)) && ((this.bill_lading_detail_id == null) ? (that.bill_lading_detail_id == null) : this.bill_lading_detail_id.equals(that.bill_lading_detail_id)) && ((this.bill_package_id == null) ? (that.bill_package_id == null) : this.bill_package_id.equals(that.bill_package_id)) && ((this.capacity == null) ? (that.capacity == null) : this.capacity.equals(that.capacity)) && ((this.create_date == null) ? (that.create_date == null) : this.create_date.equals(that.create_date)) && ((this.create_user == null) ? (that.create_user == null) : this.create_user.equals(that.create_user)) && ((this.from_warehouse_id == null) ? (that.from_warehouse_id == null) : this.from_warehouse_id.equals(that.from_warehouse_id)) && ((this.height == null) ? (that.height == null) : this.height.equals(that.height)) && ((this.id == null) ? (that.id == null) : this.id.equals(that.id)) && ((this.insurance_name == null) ? (that.insurance_name == null) : this.insurance_name.equals(that.insurance_name)) && ((this.item_name == null) ? (that.item_name == null) : this.item_name.equals(that.item_name)) && ((this.length == null) ? (that.length == null) : this.length.equals(that.length)) && ((this.quantity_plan == null) ? (that.quantity_plan == null) : this.quantity_plan.equals(that.quantity_plan)) && ((this.routing_plan_day_id == null) ? (that.routing_plan_day_id == null) : this.routing_plan_day_id.equals(that.routing_plan_day_id)) && ((this.service_name == null) ? (that.service_name == null) : this.service_name.equals(that.service_name)) && ((this.to_warehouse_id == null) ? (that.to_warehouse_id == null) : this.to_warehouse_id.equals(that.to_warehouse_id)) && ((this.total_weight == null) ? (that.total_weight == null) : this.total_weight.equals(that.total_weight)) && ((this.update_date == null) ? (that.update_date == null) : this.update_date.equals(that.update_date)) && ((this.update_user == null) ? (that.update_user == null) : this.update_user.equals(that.update_user)) && ((this.width == null) ? (that.width == null) : this.width.equals(that.width));
    }
    
    return false;
  }
  
  @Override
   public int hashCode() {
    if (!$hashCodeMemoized) {
      int h = 1;
      h *= 1000003;
      h ^= __typename.hashCode();
      h *= 1000003;
      h ^= (QRchar == null) ? 0 : QRchar.hashCode();
      h *= 1000003;
      h ^= (bill_lading_detail_id == null) ? 0 : bill_lading_detail_id.hashCode();
      h *= 1000003;
      h ^= (bill_package_id == null) ? 0 : bill_package_id.hashCode();
      h *= 1000003;
      h ^= (capacity == null) ? 0 : capacity.hashCode();
      h *= 1000003;
      h ^= (create_date == null) ? 0 : create_date.hashCode();
      h *= 1000003;
      h ^= (create_user == null) ? 0 : create_user.hashCode();
      h *= 1000003;
      h ^= (from_warehouse_id == null) ? 0 : from_warehouse_id.hashCode();
      h *= 1000003;
      h ^= (height == null) ? 0 : height.hashCode();
      h *= 1000003;
      h ^= (id == null) ? 0 : id.hashCode();
      h *= 1000003;
      h ^= (insurance_name == null) ? 0 : insurance_name.hashCode();
      h *= 1000003;
      h ^= (item_name == null) ? 0 : item_name.hashCode();
      h *= 1000003;
      h ^= (length == null) ? 0 : length.hashCode();
      h *= 1000003;
      h ^= (quantity_plan == null) ? 0 : quantity_plan.hashCode();
      h *= 1000003;
      h ^= (routing_plan_day_id == null) ? 0 : routing_plan_day_id.hashCode();
      h *= 1000003;
      h ^= (service_name == null) ? 0 : service_name.hashCode();
      h *= 1000003;
      h ^= (to_warehouse_id == null) ? 0 : to_warehouse_id.hashCode();
      h *= 1000003;
      h ^= (total_weight == null) ? 0 : total_weight.hashCode();
      h *= 1000003;
      h ^= (update_date == null) ? 0 : update_date.hashCode();
      h *= 1000003;
      h ^= (update_user == null) ? 0 : update_user.hashCode();
      h *= 1000003;
      h ^= (width == null) ? 0 : width.hashCode();
      $hashCode = h;
      $hashCodeMemoized = true;
    }
    
    return $hashCode;
  }
  
  public ResponseFieldMarshaller marshaller() {
    return new ResponseFieldMarshaller() {
      @Override
      public void marshal(ResponseWriter writer) {
        writer.writeString($responseFields[0], __typename);
        writer.writeString($responseFields[1], QRchar != null ? QRchar : null);
        writer.writeInt($responseFields[2], bill_lading_detail_id != null ? bill_lading_detail_id : null);
        writer.writeInt($responseFields[3], bill_package_id != null ? bill_package_id : null);
        writer.writeDouble($responseFields[4], capacity != null ? capacity : null);
        writer.writeCustom((ResponseField.CustomTypeField) $responseFields[5], create_date != null ? create_date : null);
        writer.writeString($responseFields[6], create_user != null ? create_user : null);
        writer.writeInt($responseFields[7], from_warehouse_id != null ? from_warehouse_id : null);
        writer.writeDouble($responseFields[8], height != null ? height : null);
        writer.writeInt($responseFields[9], id != null ? id : null);
        writer.writeString($responseFields[10], insurance_name != null ? insurance_name : null);
        writer.writeString($responseFields[11], item_name != null ? item_name : null);
        writer.writeDouble($responseFields[12], length != null ? length : null);
        writer.writeInt($responseFields[13], quantity_plan != null ? quantity_plan : null);
        writer.writeInt($responseFields[14], routing_plan_day_id != null ? routing_plan_day_id : null);
        writer.writeString($responseFields[15], service_name != null ? service_name : null);
        writer.writeInt($responseFields[16], to_warehouse_id != null ? to_warehouse_id : null);
        writer.writeDouble($responseFields[17], total_weight != null ? total_weight : null);
        writer.writeCustom((ResponseField.CustomTypeField) $responseFields[18], update_date != null ? update_date : null);
        writer.writeString($responseFields[19], update_user != null ? update_user : null);
        writer.writeDouble($responseFields[20], width != null ? width : null);
      }
    };
  }
  public static final class Mapper implements ResponseFieldMapper<BillPackageRoutingPlan> {
    @Override
     public BillPackageRoutingPlan map(ResponseReader reader) {
      final String __typename = reader.readString($responseFields[0]);
      final String QRchar = reader.readString($responseFields[1]);
      final Integer bill_lading_detail_id = reader.readInt($responseFields[2]);
      final Integer bill_package_id = reader.readInt($responseFields[3]);
      final Double capacity = reader.readDouble($responseFields[4]);
      final java.util.Date create_date = reader.readCustomType((ResponseField.CustomTypeField) $responseFields[5]);
      final String create_user = reader.readString($responseFields[6]);
      final Integer from_warehouse_id = reader.readInt($responseFields[7]);
      final Double height = reader.readDouble($responseFields[8]);
      final Integer id = reader.readInt($responseFields[9]);
      final String insurance_name = reader.readString($responseFields[10]);
      final String item_name = reader.readString($responseFields[11]);
      final Double length = reader.readDouble($responseFields[12]);
      final Integer quantity_plan = reader.readInt($responseFields[13]);
      final Integer routing_plan_day_id = reader.readInt($responseFields[14]);
      final String service_name = reader.readString($responseFields[15]);
      final Integer to_warehouse_id = reader.readInt($responseFields[16]);
      final Double total_weight = reader.readDouble($responseFields[17]);
      final java.util.Date update_date = reader.readCustomType((ResponseField.CustomTypeField) $responseFields[18]);
      final String update_user = reader.readString($responseFields[19]);
      final Double width = reader.readDouble($responseFields[20]);
      return new BillPackageRoutingPlan(__typename, QRchar, bill_lading_detail_id, bill_package_id, capacity, create_date, create_user, from_warehouse_id, height, id, insurance_name, item_name, length, quantity_plan, routing_plan_day_id, service_name, to_warehouse_id, total_weight, update_date, update_user, width);
    }
  }
  
}

