package com.nextsolution.graphql.codegen.dto;

import java.util.Arrays;
import com.apollographql.apollo.api.GraphqlFragment;
import java.util.List;
import java.lang.String;
import java.util.Collections;
import java.lang.Override;
import javax.annotation.Generated;
import com.apollographql.apollo.api.ResponseFieldMapper;
import com.apollographql.apollo.api.ResponseField;
import javax.annotation.Nullable;
import java.lang.Integer;
import com.nextsolution.graphql.codegen.types.CustomType;
import javax.annotation.Nonnull;
import com.apollographql.apollo.api.internal.Utils;
import com.apollographql.apollo.api.ResponseReader;
import com.apollographql.apollo.api.ResponseFieldMarshaller;
import com.apollographql.apollo.api.ResponseWriter;

@lombok.Getter
@lombok.Setter
public class Van implements GraphqlFragment {
  private @Nonnull String __typename;
  private @Nullable Integer app_param_value_id;
  private @Nullable Double available_capacity;
  private @Nullable String avatar_url;
  private @Nullable Double axles;
  private @Nullable Double body_length;
  private @Nullable Double body_width;
  private @Nullable Double capacity;
  private @Nullable Double cost;
  private @Nullable Double cost_center;
  private @Nullable java.util.Date create_date;
  private @Nullable String create_user;
  private @Nullable String description;
  private @Nullable String driver;
  private @Nullable Double engine_size;
  private @Nullable Integer fleet_id;
  private @Nullable String fuel_type_id;
  private @Nullable Double gross_weight;
  private @Nullable Integer has_attachment;
  private @Nullable Integer has_image;
  private @Nullable Double height;
  private @Nullable Integer id;
  private @Nullable java.util.Date inspection_due_date;
  private @Nullable Double latitude;
  private @Nullable String licence_plate;
  private @Nullable String location;
  private @Nullable Double longitude;
  private @Nullable Integer maintenance_template_id;
  private @Nullable String name;
  private @Nullable String note;
  private @Nullable Integer parking_point_id;
  private @Nullable Integer status_available;
  private @Nullable Integer status_car;
  private @Nullable Double tire_front_pressure;
  private @Nullable Double tire_front_size;
  private @Nullable Double tire_rear_pressure;
  private @Nullable Double tire_rear_size;
  private @Nullable java.util.Date update_date;
  private @Nullable String update_user;
  private @Nullable String van_inspection;
  private @Nullable Integer van_type_Id;
  private @Nullable String vehicle_registration;
  private @Nullable Double vehicle_tonnage;
  private @Nullable java.util.Date warranty_date1;
  private @Nullable java.util.Date warranty_date2;
  private @Nullable Double warranty_meter1;
  private @Nullable Double warranty_meter2;
  private @Nullable String warranty_name1;
  private @Nullable String warranty_name2;
  private @Nullable Double wheelbase;
  private @Nullable Integer year;
  private volatile String $toString;
  private volatile int $hashCode;
  private volatile boolean $hashCodeMemoized;
  static ResponseField[] $responseFields = {
      ResponseField.forString("__typename", "__typename", null, false, Collections.<ResponseField.Condition>emptyList()),
      ResponseField.forInt("app_param_value_id", "app_param_value_id", null, true, Collections.<ResponseField.Condition>emptyList()),
      ResponseField.forDouble("available_capacity", "available_capacity", null, true, Collections.<ResponseField.Condition>emptyList()),
      ResponseField.forString("avatar_url", "avatar_url", null, true, Collections.<ResponseField.Condition>emptyList()),
      ResponseField.forDouble("axles", "axles", null, true, Collections.<ResponseField.Condition>emptyList()),
      ResponseField.forDouble("body_length", "body_length", null, true, Collections.<ResponseField.Condition>emptyList()),
      ResponseField.forDouble("body_width", "body_width", null, true, Collections.<ResponseField.Condition>emptyList()),
      ResponseField.forDouble("capacity", "capacity", null, true, Collections.<ResponseField.Condition>emptyList()),
      ResponseField.forDouble("cost", "cost", null, true, Collections.<ResponseField.Condition>emptyList()),
      ResponseField.forDouble("cost_center", "cost_center", null, true, Collections.<ResponseField.Condition>emptyList()),
      ResponseField.forCustomType("create_date", "create_date", null, true, CustomType.Date, Collections.<ResponseField.Condition>emptyList()),
      ResponseField.forString("create_user", "create_user", null, true, Collections.<ResponseField.Condition>emptyList()),
      ResponseField.forString("description", "description", null, true, Collections.<ResponseField.Condition>emptyList()),
      ResponseField.forString("driver", "driver", null, true, Collections.<ResponseField.Condition>emptyList()),
      ResponseField.forDouble("engine_size", "engine_size", null, true, Collections.<ResponseField.Condition>emptyList()),
      ResponseField.forInt("fleet_id", "fleet_id", null, true, Collections.<ResponseField.Condition>emptyList()),
      ResponseField.forString("fuel_type_id", "fuel_type_id", null, true, Collections.<ResponseField.Condition>emptyList()),
      ResponseField.forDouble("gross_weight", "gross_weight", null, true, Collections.<ResponseField.Condition>emptyList()),
      ResponseField.forInt("has_attachment", "has_attachment", null, true, Collections.<ResponseField.Condition>emptyList()),
      ResponseField.forInt("has_image", "has_image", null, true, Collections.<ResponseField.Condition>emptyList()),
      ResponseField.forDouble("height", "height", null, true, Collections.<ResponseField.Condition>emptyList()),
      ResponseField.forInt("id", "id", null, true, Collections.<ResponseField.Condition>emptyList()),
      ResponseField.forCustomType("inspection_due_date", "inspection_due_date", null, true, CustomType.Date, Collections.<ResponseField.Condition>emptyList()),
      ResponseField.forDouble("latitude", "latitude", null, true, Collections.<ResponseField.Condition>emptyList()),
      ResponseField.forString("licence_plate", "licence_plate", null, true, Collections.<ResponseField.Condition>emptyList()),
      ResponseField.forString("location", "location", null, true, Collections.<ResponseField.Condition>emptyList()),
      ResponseField.forDouble("longitude", "longitude", null, true, Collections.<ResponseField.Condition>emptyList()),
      ResponseField.forInt("maintenance_template_id", "maintenance_template_id", null, true, Collections.<ResponseField.Condition>emptyList()),
      ResponseField.forString("name", "name", null, true, Collections.<ResponseField.Condition>emptyList()),
      ResponseField.forString("note", "note", null, true, Collections.<ResponseField.Condition>emptyList()),
      ResponseField.forInt("parking_point_id", "parking_point_id", null, true, Collections.<ResponseField.Condition>emptyList()),
      ResponseField.forInt("status_available", "status_available", null, true, Collections.<ResponseField.Condition>emptyList()),
      ResponseField.forInt("status_car", "status_car", null, true, Collections.<ResponseField.Condition>emptyList()),
      ResponseField.forDouble("tire_front_pressure", "tire_front_pressure", null, true, Collections.<ResponseField.Condition>emptyList()),
      ResponseField.forDouble("tire_front_size", "tire_front_size", null, true, Collections.<ResponseField.Condition>emptyList()),
      ResponseField.forDouble("tire_rear_pressure", "tire_rear_pressure", null, true, Collections.<ResponseField.Condition>emptyList()),
      ResponseField.forDouble("tire_rear_size", "tire_rear_size", null, true, Collections.<ResponseField.Condition>emptyList()),
      ResponseField.forCustomType("update_date", "update_date", null, true, CustomType.Date, Collections.<ResponseField.Condition>emptyList()),
      ResponseField.forString("update_user", "update_user", null, true, Collections.<ResponseField.Condition>emptyList()),
      ResponseField.forString("van_inspection", "van_inspection", null, true, Collections.<ResponseField.Condition>emptyList()),
      ResponseField.forInt("van_type_Id", "van_type_Id", null, true, Collections.<ResponseField.Condition>emptyList()),
      ResponseField.forString("vehicle_registration", "vehicle_registration", null, true, Collections.<ResponseField.Condition>emptyList()),
      ResponseField.forDouble("vehicle_tonnage", "vehicle_tonnage", null, true, Collections.<ResponseField.Condition>emptyList()),
      ResponseField.forCustomType("warranty_date1", "warranty_date1", null, true, CustomType.Date, Collections.<ResponseField.Condition>emptyList()),
      ResponseField.forCustomType("warranty_date2", "warranty_date2", null, true, CustomType.Date, Collections.<ResponseField.Condition>emptyList()),
      ResponseField.forDouble("warranty_meter1", "warranty_meter1", null, true, Collections.<ResponseField.Condition>emptyList()),
      ResponseField.forDouble("warranty_meter2", "warranty_meter2", null, true, Collections.<ResponseField.Condition>emptyList()),
      ResponseField.forString("warranty_name1", "warranty_name1", null, true, Collections.<ResponseField.Condition>emptyList()),
      ResponseField.forString("warranty_name2", "warranty_name2", null, true, Collections.<ResponseField.Condition>emptyList()),
      ResponseField.forDouble("wheelbase", "wheelbase", null, true, Collections.<ResponseField.Condition>emptyList()),
      ResponseField.forInt("year", "year", null, true, Collections.<ResponseField.Condition>emptyList())
    };
  public static String FRAGMENT_DEFINITION = "fragment Van on VanDto {   app_param_value_id   available_capacity   avatar_url   axles   body_length   body_width   capacity   cost   cost_center   create_date   create_user   description   driver   engine_size   fleet_id   fuel_type_id   gross_weight   has_attachment   has_image   height   id   inspection_due_date   latitude   licence_plate   location   longitude   maintenance_template_id   name   note   parking_point_id   status_available   status_car   tire_front_pressure   tire_front_size   tire_rear_pressure   tire_rear_size   update_date   update_user   van_inspection   van_type_Id   vehicle_registration   vehicle_tonnage   warranty_date1   warranty_date2   warranty_meter1   warranty_meter2   warranty_name1   warranty_name2   wheelbase   year }";
  public static List<String> POSSIBLE_TYPES = Collections.unmodifiableList(Arrays.asList("VanDto"));
  public Van(@Nonnull String __typename, @Nullable Integer app_param_value_id, @Nullable Double available_capacity, @Nullable String avatar_url, @Nullable Double axles, @Nullable Double body_length, @Nullable Double body_width, @Nullable Double capacity, @Nullable Double cost, @Nullable Double cost_center, @Nullable java.util.Date create_date, @Nullable String create_user, @Nullable String description, @Nullable String driver, @Nullable Double engine_size, @Nullable Integer fleet_id, @Nullable String fuel_type_id, @Nullable Double gross_weight, @Nullable Integer has_attachment, @Nullable Integer has_image, @Nullable Double height, @Nullable Integer id, @Nullable java.util.Date inspection_due_date, @Nullable Double latitude, @Nullable String licence_plate, @Nullable String location, @Nullable Double longitude, @Nullable Integer maintenance_template_id, @Nullable String name, @Nullable String note, @Nullable Integer parking_point_id, @Nullable Integer status_available, @Nullable Integer status_car, @Nullable Double tire_front_pressure, @Nullable Double tire_front_size, @Nullable Double tire_rear_pressure, @Nullable Double tire_rear_size, @Nullable java.util.Date update_date, @Nullable String update_user, @Nullable String van_inspection, @Nullable Integer van_type_Id, @Nullable String vehicle_registration, @Nullable Double vehicle_tonnage, @Nullable java.util.Date warranty_date1, @Nullable java.util.Date warranty_date2, @Nullable Double warranty_meter1, @Nullable Double warranty_meter2, @Nullable String warranty_name1, @Nullable String warranty_name2, @Nullable Double wheelbase, @Nullable Integer year) {
    this.__typename = Utils.checkNotNull(__typename, "__typename == null");
    this.app_param_value_id = app_param_value_id;
    this.available_capacity = available_capacity;
    this.avatar_url = avatar_url;
    this.axles = axles;
    this.body_length = body_length;
    this.body_width = body_width;
    this.capacity = capacity;
    this.cost = cost;
    this.cost_center = cost_center;
    this.create_date = create_date;
    this.create_user = create_user;
    this.description = description;
    this.driver = driver;
    this.engine_size = engine_size;
    this.fleet_id = fleet_id;
    this.fuel_type_id = fuel_type_id;
    this.gross_weight = gross_weight;
    this.has_attachment = has_attachment;
    this.has_image = has_image;
    this.height = height;
    this.id = id;
    this.inspection_due_date = inspection_due_date;
    this.latitude = latitude;
    this.licence_plate = licence_plate;
    this.location = location;
    this.longitude = longitude;
    this.maintenance_template_id = maintenance_template_id;
    this.name = name;
    this.note = note;
    this.parking_point_id = parking_point_id;
    this.status_available = status_available;
    this.status_car = status_car;
    this.tire_front_pressure = tire_front_pressure;
    this.tire_front_size = tire_front_size;
    this.tire_rear_pressure = tire_rear_pressure;
    this.tire_rear_size = tire_rear_size;
    this.update_date = update_date;
    this.update_user = update_user;
    this.van_inspection = van_inspection;
    this.van_type_Id = van_type_Id;
    this.vehicle_registration = vehicle_registration;
    this.vehicle_tonnage = vehicle_tonnage;
    this.warranty_date1 = warranty_date1;
    this.warranty_date2 = warranty_date2;
    this.warranty_meter1 = warranty_meter1;
    this.warranty_meter2 = warranty_meter2;
    this.warranty_name1 = warranty_name1;
    this.warranty_name2 = warranty_name2;
    this.wheelbase = wheelbase;
    this.year = year;
  }
  
  @Override
   public String toString() {
    if ($toString == null) {
      $toString = "Van{"
        + "__typename=" + __typename + ", "
        + "app_param_value_id=" + app_param_value_id + ", "
        + "available_capacity=" + available_capacity + ", "
        + "avatar_url=" + avatar_url + ", "
        + "axles=" + axles + ", "
        + "body_length=" + body_length + ", "
        + "body_width=" + body_width + ", "
        + "capacity=" + capacity + ", "
        + "cost=" + cost + ", "
        + "cost_center=" + cost_center + ", "
        + "create_date=" + create_date + ", "
        + "create_user=" + create_user + ", "
        + "description=" + description + ", "
        + "driver=" + driver + ", "
        + "engine_size=" + engine_size + ", "
        + "fleet_id=" + fleet_id + ", "
        + "fuel_type_id=" + fuel_type_id + ", "
        + "gross_weight=" + gross_weight + ", "
        + "has_attachment=" + has_attachment + ", "
        + "has_image=" + has_image + ", "
        + "height=" + height + ", "
        + "id=" + id + ", "
        + "inspection_due_date=" + inspection_due_date + ", "
        + "latitude=" + latitude + ", "
        + "licence_plate=" + licence_plate + ", "
        + "location=" + location + ", "
        + "longitude=" + longitude + ", "
        + "maintenance_template_id=" + maintenance_template_id + ", "
        + "name=" + name + ", "
        + "note=" + note + ", "
        + "parking_point_id=" + parking_point_id + ", "
        + "status_available=" + status_available + ", "
        + "status_car=" + status_car + ", "
        + "tire_front_pressure=" + tire_front_pressure + ", "
        + "tire_front_size=" + tire_front_size + ", "
        + "tire_rear_pressure=" + tire_rear_pressure + ", "
        + "tire_rear_size=" + tire_rear_size + ", "
        + "update_date=" + update_date + ", "
        + "update_user=" + update_user + ", "
        + "van_inspection=" + van_inspection + ", "
        + "van_type_Id=" + van_type_Id + ", "
        + "vehicle_registration=" + vehicle_registration + ", "
        + "vehicle_tonnage=" + vehicle_tonnage + ", "
        + "warranty_date1=" + warranty_date1 + ", "
        + "warranty_date2=" + warranty_date2 + ", "
        + "warranty_meter1=" + warranty_meter1 + ", "
        + "warranty_meter2=" + warranty_meter2 + ", "
        + "warranty_name1=" + warranty_name1 + ", "
        + "warranty_name2=" + warranty_name2 + ", "
        + "wheelbase=" + wheelbase + ", "
        + "year=" + year + ", "
        + "}";
    }
    
    return $toString;
  }
  
  public Van() {
    
  }
  
  @Override
   public boolean equals(Object o) {
    if (o == this) {
      return true;
    }
    if (o instanceof Van) {
      Van that = (Van) o;
      return this.__typename.equals(that.__typename) && ((this.app_param_value_id == null) ? (that.app_param_value_id == null) : this.app_param_value_id.equals(that.app_param_value_id)) && ((this.available_capacity == null) ? (that.available_capacity == null) : this.available_capacity.equals(that.available_capacity)) && ((this.avatar_url == null) ? (that.avatar_url == null) : this.avatar_url.equals(that.avatar_url)) && ((this.axles == null) ? (that.axles == null) : this.axles.equals(that.axles)) && ((this.body_length == null) ? (that.body_length == null) : this.body_length.equals(that.body_length)) && ((this.body_width == null) ? (that.body_width == null) : this.body_width.equals(that.body_width)) && ((this.capacity == null) ? (that.capacity == null) : this.capacity.equals(that.capacity)) && ((this.cost == null) ? (that.cost == null) : this.cost.equals(that.cost)) && ((this.cost_center == null) ? (that.cost_center == null) : this.cost_center.equals(that.cost_center)) && ((this.create_date == null) ? (that.create_date == null) : this.create_date.equals(that.create_date)) && ((this.create_user == null) ? (that.create_user == null) : this.create_user.equals(that.create_user)) && ((this.description == null) ? (that.description == null) : this.description.equals(that.description)) && ((this.driver == null) ? (that.driver == null) : this.driver.equals(that.driver)) && ((this.engine_size == null) ? (that.engine_size == null) : this.engine_size.equals(that.engine_size)) && ((this.fleet_id == null) ? (that.fleet_id == null) : this.fleet_id.equals(that.fleet_id)) && ((this.fuel_type_id == null) ? (that.fuel_type_id == null) : this.fuel_type_id.equals(that.fuel_type_id)) && ((this.gross_weight == null) ? (that.gross_weight == null) : this.gross_weight.equals(that.gross_weight)) && ((this.has_attachment == null) ? (that.has_attachment == null) : this.has_attachment.equals(that.has_attachment)) && ((this.has_image == null) ? (that.has_image == null) : this.has_image.equals(that.has_image)) && ((this.height == null) ? (that.height == null) : this.height.equals(that.height)) && ((this.id == null) ? (that.id == null) : this.id.equals(that.id)) && ((this.inspection_due_date == null) ? (that.inspection_due_date == null) : this.inspection_due_date.equals(that.inspection_due_date)) && ((this.latitude == null) ? (that.latitude == null) : this.latitude.equals(that.latitude)) && ((this.licence_plate == null) ? (that.licence_plate == null) : this.licence_plate.equals(that.licence_plate)) && ((this.location == null) ? (that.location == null) : this.location.equals(that.location)) && ((this.longitude == null) ? (that.longitude == null) : this.longitude.equals(that.longitude)) && ((this.maintenance_template_id == null) ? (that.maintenance_template_id == null) : this.maintenance_template_id.equals(that.maintenance_template_id)) && ((this.name == null) ? (that.name == null) : this.name.equals(that.name)) && ((this.note == null) ? (that.note == null) : this.note.equals(that.note)) && ((this.parking_point_id == null) ? (that.parking_point_id == null) : this.parking_point_id.equals(that.parking_point_id)) && ((this.status_available == null) ? (that.status_available == null) : this.status_available.equals(that.status_available)) && ((this.status_car == null) ? (that.status_car == null) : this.status_car.equals(that.status_car)) && ((this.tire_front_pressure == null) ? (that.tire_front_pressure == null) : this.tire_front_pressure.equals(that.tire_front_pressure)) && ((this.tire_front_size == null) ? (that.tire_front_size == null) : this.tire_front_size.equals(that.tire_front_size)) && ((this.tire_rear_pressure == null) ? (that.tire_rear_pressure == null) : this.tire_rear_pressure.equals(that.tire_rear_pressure)) && ((this.tire_rear_size == null) ? (that.tire_rear_size == null) : this.tire_rear_size.equals(that.tire_rear_size)) && ((this.update_date == null) ? (that.update_date == null) : this.update_date.equals(that.update_date)) && ((this.update_user == null) ? (that.update_user == null) : this.update_user.equals(that.update_user)) && ((this.van_inspection == null) ? (that.van_inspection == null) : this.van_inspection.equals(that.van_inspection)) && ((this.van_type_Id == null) ? (that.van_type_Id == null) : this.van_type_Id.equals(that.van_type_Id)) && ((this.vehicle_registration == null) ? (that.vehicle_registration == null) : this.vehicle_registration.equals(that.vehicle_registration)) && ((this.vehicle_tonnage == null) ? (that.vehicle_tonnage == null) : this.vehicle_tonnage.equals(that.vehicle_tonnage)) && ((this.warranty_date1 == null) ? (that.warranty_date1 == null) : this.warranty_date1.equals(that.warranty_date1)) && ((this.warranty_date2 == null) ? (that.warranty_date2 == null) : this.warranty_date2.equals(that.warranty_date2)) && ((this.warranty_meter1 == null) ? (that.warranty_meter1 == null) : this.warranty_meter1.equals(that.warranty_meter1)) && ((this.warranty_meter2 == null) ? (that.warranty_meter2 == null) : this.warranty_meter2.equals(that.warranty_meter2)) && ((this.warranty_name1 == null) ? (that.warranty_name1 == null) : this.warranty_name1.equals(that.warranty_name1)) && ((this.warranty_name2 == null) ? (that.warranty_name2 == null) : this.warranty_name2.equals(that.warranty_name2)) && ((this.wheelbase == null) ? (that.wheelbase == null) : this.wheelbase.equals(that.wheelbase)) && ((this.year == null) ? (that.year == null) : this.year.equals(that.year));
    }
    
    return false;
  }
  
  @Override
   public int hashCode() {
    if (!$hashCodeMemoized) {
      int h = 1;
      h *= 1000003;
      h ^= __typename.hashCode();
      h *= 1000003;
      h ^= (app_param_value_id == null) ? 0 : app_param_value_id.hashCode();
      h *= 1000003;
      h ^= (available_capacity == null) ? 0 : available_capacity.hashCode();
      h *= 1000003;
      h ^= (avatar_url == null) ? 0 : avatar_url.hashCode();
      h *= 1000003;
      h ^= (axles == null) ? 0 : axles.hashCode();
      h *= 1000003;
      h ^= (body_length == null) ? 0 : body_length.hashCode();
      h *= 1000003;
      h ^= (body_width == null) ? 0 : body_width.hashCode();
      h *= 1000003;
      h ^= (capacity == null) ? 0 : capacity.hashCode();
      h *= 1000003;
      h ^= (cost == null) ? 0 : cost.hashCode();
      h *= 1000003;
      h ^= (cost_center == null) ? 0 : cost_center.hashCode();
      h *= 1000003;
      h ^= (create_date == null) ? 0 : create_date.hashCode();
      h *= 1000003;
      h ^= (create_user == null) ? 0 : create_user.hashCode();
      h *= 1000003;
      h ^= (description == null) ? 0 : description.hashCode();
      h *= 1000003;
      h ^= (driver == null) ? 0 : driver.hashCode();
      h *= 1000003;
      h ^= (engine_size == null) ? 0 : engine_size.hashCode();
      h *= 1000003;
      h ^= (fleet_id == null) ? 0 : fleet_id.hashCode();
      h *= 1000003;
      h ^= (fuel_type_id == null) ? 0 : fuel_type_id.hashCode();
      h *= 1000003;
      h ^= (gross_weight == null) ? 0 : gross_weight.hashCode();
      h *= 1000003;
      h ^= (has_attachment == null) ? 0 : has_attachment.hashCode();
      h *= 1000003;
      h ^= (has_image == null) ? 0 : has_image.hashCode();
      h *= 1000003;
      h ^= (height == null) ? 0 : height.hashCode();
      h *= 1000003;
      h ^= (id == null) ? 0 : id.hashCode();
      h *= 1000003;
      h ^= (inspection_due_date == null) ? 0 : inspection_due_date.hashCode();
      h *= 1000003;
      h ^= (latitude == null) ? 0 : latitude.hashCode();
      h *= 1000003;
      h ^= (licence_plate == null) ? 0 : licence_plate.hashCode();
      h *= 1000003;
      h ^= (location == null) ? 0 : location.hashCode();
      h *= 1000003;
      h ^= (longitude == null) ? 0 : longitude.hashCode();
      h *= 1000003;
      h ^= (maintenance_template_id == null) ? 0 : maintenance_template_id.hashCode();
      h *= 1000003;
      h ^= (name == null) ? 0 : name.hashCode();
      h *= 1000003;
      h ^= (note == null) ? 0 : note.hashCode();
      h *= 1000003;
      h ^= (parking_point_id == null) ? 0 : parking_point_id.hashCode();
      h *= 1000003;
      h ^= (status_available == null) ? 0 : status_available.hashCode();
      h *= 1000003;
      h ^= (status_car == null) ? 0 : status_car.hashCode();
      h *= 1000003;
      h ^= (tire_front_pressure == null) ? 0 : tire_front_pressure.hashCode();
      h *= 1000003;
      h ^= (tire_front_size == null) ? 0 : tire_front_size.hashCode();
      h *= 1000003;
      h ^= (tire_rear_pressure == null) ? 0 : tire_rear_pressure.hashCode();
      h *= 1000003;
      h ^= (tire_rear_size == null) ? 0 : tire_rear_size.hashCode();
      h *= 1000003;
      h ^= (update_date == null) ? 0 : update_date.hashCode();
      h *= 1000003;
      h ^= (update_user == null) ? 0 : update_user.hashCode();
      h *= 1000003;
      h ^= (van_inspection == null) ? 0 : van_inspection.hashCode();
      h *= 1000003;
      h ^= (van_type_Id == null) ? 0 : van_type_Id.hashCode();
      h *= 1000003;
      h ^= (vehicle_registration == null) ? 0 : vehicle_registration.hashCode();
      h *= 1000003;
      h ^= (vehicle_tonnage == null) ? 0 : vehicle_tonnage.hashCode();
      h *= 1000003;
      h ^= (warranty_date1 == null) ? 0 : warranty_date1.hashCode();
      h *= 1000003;
      h ^= (warranty_date2 == null) ? 0 : warranty_date2.hashCode();
      h *= 1000003;
      h ^= (warranty_meter1 == null) ? 0 : warranty_meter1.hashCode();
      h *= 1000003;
      h ^= (warranty_meter2 == null) ? 0 : warranty_meter2.hashCode();
      h *= 1000003;
      h ^= (warranty_name1 == null) ? 0 : warranty_name1.hashCode();
      h *= 1000003;
      h ^= (warranty_name2 == null) ? 0 : warranty_name2.hashCode();
      h *= 1000003;
      h ^= (wheelbase == null) ? 0 : wheelbase.hashCode();
      h *= 1000003;
      h ^= (year == null) ? 0 : year.hashCode();
      $hashCode = h;
      $hashCodeMemoized = true;
    }
    
    return $hashCode;
  }
  
  public ResponseFieldMarshaller marshaller() {
    return new ResponseFieldMarshaller() {
      @Override
      public void marshal(ResponseWriter writer) {
        writer.writeString($responseFields[0], __typename);
        writer.writeInt($responseFields[1], app_param_value_id != null ? app_param_value_id : null);
        writer.writeDouble($responseFields[2], available_capacity != null ? available_capacity : null);
        writer.writeString($responseFields[3], avatar_url != null ? avatar_url : null);
        writer.writeDouble($responseFields[4], axles != null ? axles : null);
        writer.writeDouble($responseFields[5], body_length != null ? body_length : null);
        writer.writeDouble($responseFields[6], body_width != null ? body_width : null);
        writer.writeDouble($responseFields[7], capacity != null ? capacity : null);
        writer.writeDouble($responseFields[8], cost != null ? cost : null);
        writer.writeDouble($responseFields[9], cost_center != null ? cost_center : null);
        writer.writeCustom((ResponseField.CustomTypeField) $responseFields[10], create_date != null ? create_date : null);
        writer.writeString($responseFields[11], create_user != null ? create_user : null);
        writer.writeString($responseFields[12], description != null ? description : null);
        writer.writeString($responseFields[13], driver != null ? driver : null);
        writer.writeDouble($responseFields[14], engine_size != null ? engine_size : null);
        writer.writeInt($responseFields[15], fleet_id != null ? fleet_id : null);
        writer.writeString($responseFields[16], fuel_type_id != null ? fuel_type_id : null);
        writer.writeDouble($responseFields[17], gross_weight != null ? gross_weight : null);
        writer.writeInt($responseFields[18], has_attachment != null ? has_attachment : null);
        writer.writeInt($responseFields[19], has_image != null ? has_image : null);
        writer.writeDouble($responseFields[20], height != null ? height : null);
        writer.writeInt($responseFields[21], id != null ? id : null);
        writer.writeCustom((ResponseField.CustomTypeField) $responseFields[22], inspection_due_date != null ? inspection_due_date : null);
        writer.writeDouble($responseFields[23], latitude != null ? latitude : null);
        writer.writeString($responseFields[24], licence_plate != null ? licence_plate : null);
        writer.writeString($responseFields[25], location != null ? location : null);
        writer.writeDouble($responseFields[26], longitude != null ? longitude : null);
        writer.writeInt($responseFields[27], maintenance_template_id != null ? maintenance_template_id : null);
        writer.writeString($responseFields[28], name != null ? name : null);
        writer.writeString($responseFields[29], note != null ? note : null);
        writer.writeInt($responseFields[30], parking_point_id != null ? parking_point_id : null);
        writer.writeInt($responseFields[31], status_available != null ? status_available : null);
        writer.writeInt($responseFields[32], status_car != null ? status_car : null);
        writer.writeDouble($responseFields[33], tire_front_pressure != null ? tire_front_pressure : null);
        writer.writeDouble($responseFields[34], tire_front_size != null ? tire_front_size : null);
        writer.writeDouble($responseFields[35], tire_rear_pressure != null ? tire_rear_pressure : null);
        writer.writeDouble($responseFields[36], tire_rear_size != null ? tire_rear_size : null);
        writer.writeCustom((ResponseField.CustomTypeField) $responseFields[37], update_date != null ? update_date : null);
        writer.writeString($responseFields[38], update_user != null ? update_user : null);
        writer.writeString($responseFields[39], van_inspection != null ? van_inspection : null);
        writer.writeInt($responseFields[40], van_type_Id != null ? van_type_Id : null);
        writer.writeString($responseFields[41], vehicle_registration != null ? vehicle_registration : null);
        writer.writeDouble($responseFields[42], vehicle_tonnage != null ? vehicle_tonnage : null);
        writer.writeCustom((ResponseField.CustomTypeField) $responseFields[43], warranty_date1 != null ? warranty_date1 : null);
        writer.writeCustom((ResponseField.CustomTypeField) $responseFields[44], warranty_date2 != null ? warranty_date2 : null);
        writer.writeDouble($responseFields[45], warranty_meter1 != null ? warranty_meter1 : null);
        writer.writeDouble($responseFields[46], warranty_meter2 != null ? warranty_meter2 : null);
        writer.writeString($responseFields[47], warranty_name1 != null ? warranty_name1 : null);
        writer.writeString($responseFields[48], warranty_name2 != null ? warranty_name2 : null);
        writer.writeDouble($responseFields[49], wheelbase != null ? wheelbase : null);
        writer.writeInt($responseFields[50], year != null ? year : null);
      }
    };
  }
  public static final class Mapper implements ResponseFieldMapper<Van> {
    @Override
     public Van map(ResponseReader reader) {
      final String __typename = reader.readString($responseFields[0]);
      final Integer app_param_value_id = reader.readInt($responseFields[1]);
      final Double available_capacity = reader.readDouble($responseFields[2]);
      final String avatar_url = reader.readString($responseFields[3]);
      final Double axles = reader.readDouble($responseFields[4]);
      final Double body_length = reader.readDouble($responseFields[5]);
      final Double body_width = reader.readDouble($responseFields[6]);
      final Double capacity = reader.readDouble($responseFields[7]);
      final Double cost = reader.readDouble($responseFields[8]);
      final Double cost_center = reader.readDouble($responseFields[9]);
      final java.util.Date create_date = reader.readCustomType((ResponseField.CustomTypeField) $responseFields[10]);
      final String create_user = reader.readString($responseFields[11]);
      final String description = reader.readString($responseFields[12]);
      final String driver = reader.readString($responseFields[13]);
      final Double engine_size = reader.readDouble($responseFields[14]);
      final Integer fleet_id = reader.readInt($responseFields[15]);
      final String fuel_type_id = reader.readString($responseFields[16]);
      final Double gross_weight = reader.readDouble($responseFields[17]);
      final Integer has_attachment = reader.readInt($responseFields[18]);
      final Integer has_image = reader.readInt($responseFields[19]);
      final Double height = reader.readDouble($responseFields[20]);
      final Integer id = reader.readInt($responseFields[21]);
      final java.util.Date inspection_due_date = reader.readCustomType((ResponseField.CustomTypeField) $responseFields[22]);
      final Double latitude = reader.readDouble($responseFields[23]);
      final String licence_plate = reader.readString($responseFields[24]);
      final String location = reader.readString($responseFields[25]);
      final Double longitude = reader.readDouble($responseFields[26]);
      final Integer maintenance_template_id = reader.readInt($responseFields[27]);
      final String name = reader.readString($responseFields[28]);
      final String note = reader.readString($responseFields[29]);
      final Integer parking_point_id = reader.readInt($responseFields[30]);
      final Integer status_available = reader.readInt($responseFields[31]);
      final Integer status_car = reader.readInt($responseFields[32]);
      final Double tire_front_pressure = reader.readDouble($responseFields[33]);
      final Double tire_front_size = reader.readDouble($responseFields[34]);
      final Double tire_rear_pressure = reader.readDouble($responseFields[35]);
      final Double tire_rear_size = reader.readDouble($responseFields[36]);
      final java.util.Date update_date = reader.readCustomType((ResponseField.CustomTypeField) $responseFields[37]);
      final String update_user = reader.readString($responseFields[38]);
      final String van_inspection = reader.readString($responseFields[39]);
      final Integer van_type_Id = reader.readInt($responseFields[40]);
      final String vehicle_registration = reader.readString($responseFields[41]);
      final Double vehicle_tonnage = reader.readDouble($responseFields[42]);
      final java.util.Date warranty_date1 = reader.readCustomType((ResponseField.CustomTypeField) $responseFields[43]);
      final java.util.Date warranty_date2 = reader.readCustomType((ResponseField.CustomTypeField) $responseFields[44]);
      final Double warranty_meter1 = reader.readDouble($responseFields[45]);
      final Double warranty_meter2 = reader.readDouble($responseFields[46]);
      final String warranty_name1 = reader.readString($responseFields[47]);
      final String warranty_name2 = reader.readString($responseFields[48]);
      final Double wheelbase = reader.readDouble($responseFields[49]);
      final Integer year = reader.readInt($responseFields[50]);
      return new Van(__typename, app_param_value_id, available_capacity, avatar_url, axles, body_length, body_width, capacity, cost, cost_center, create_date, create_user, description, driver, engine_size, fleet_id, fuel_type_id, gross_weight, has_attachment, has_image, height, id, inspection_due_date, latitude, licence_plate, location, longitude, maintenance_template_id, name, note, parking_point_id, status_available, status_car, tire_front_pressure, tire_front_size, tire_rear_pressure, tire_rear_size, update_date, update_user, van_inspection, van_type_Id, vehicle_registration, vehicle_tonnage, warranty_date1, warranty_date2, warranty_meter1, warranty_meter2, warranty_name1, warranty_name2, wheelbase, year);
    }
  }
  
}

