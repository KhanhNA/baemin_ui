package com.nextsolution.graphql.api;

import com.nextsolution.graphql.codegen.business.GetRoutingDetailQuery;
import com.nextsolution.graphql.codegen.business.GetRoutingPlanDayByCustomerIdQuery;
import com.nextsolution.graphql.codegen.business.GetRoutingPlanDayByIdQuery;
import com.nextsolution.graphql.codegen.business.GetRoutingVanDayQuery;
import com.nextsolution.graphql.codegen.business.PickupMutation;
import com.nextsolution.graphql.codegen.business.UpdateRoutingVanDayMutation;

public class RoutingApolloApi {
    public static void getRoutingPlanDay(Integer vanId, String date, ResponseApollo result, Object... params) {
        GetRoutingVanDayQuery query = GetRoutingVanDayQuery.builder().vanId(vanId).date(date).build();
        AWSClientBase.apolloQuery(query, result, params);
    }
    public static void getRoutingPlanDayById(Integer id, ResponseApollo result, Object... params) {
        GetRoutingPlanDayByIdQuery query = GetRoutingPlanDayByIdQuery.builder().id(id).build();
        AWSClientBase.apolloQuery(query, result, params);
    }
    public static void getRoutingDetail(Integer id, ResponseApollo result, Object... params) {
        GetRoutingDetailQuery query = GetRoutingDetailQuery.builder().id(id).build();
        AWSClientBase.apolloQuery(query, result, params);
    }

    public static void updateVanForNewRoute(Integer routeId,Integer vanId,  ResponseApollo result, Object... params) {
        UpdateRoutingVanDayMutation mutation = UpdateRoutingVanDayMutation.builder().id(routeId).vanId(vanId).build();
        AWSClientBase.apolloMutation(mutation, result, params);
    }
    public static void pickup(Integer routeId,  ResponseApollo result, Object... params) {
        PickupMutation mutation = PickupMutation.builder().routingPlanDayId(routeId).build();
        AWSClientBase.apolloMutation(mutation, result, params);
    }

    public static void getRoutingPlanDayByCustomerId(Integer id, String date, ResponseApollo result, Object... params) {
        GetRoutingPlanDayByCustomerIdQuery query = GetRoutingPlanDayByCustomerIdQuery.builder().customerId(id).date(date).build();
        AWSClientBase.apolloQuery(query, result, params);
    }
}
