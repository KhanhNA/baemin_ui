package com.tsolution.base.Utils;

import android.content.Context;

import com.google.gson.JsonParseException;

import com.ns.tsutils.NetworkUtils;
import com.ns.tsutils.ServerException;
import com.ns.tsutils.ToastUtils;
import com.tsolution.base.dto.EventDTO;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.ConnectException;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;

import io.reactivex.subscribers.DisposableSubscriber;
import retrofit2.HttpException;

/**
 * @author PhamBien
 */
public abstract class RxSubscriber<T> extends DisposableSubscriber<T> {

    Context context;
    public RxSubscriber(Context c) {
        super();
        context = c;
    }

    @Override
    protected void onStart() {
        super.onStart();
        showLoading();
        if (!NetworkUtils.isNetworkConnected(context)) {
            ToastUtils.showToast(context, "No Internet");
            onNoNetWork();
            cancel();
        }
    }


    @Override
    public void onComplete() {

    }

    protected void showLoading() {
//        ToastUtils.showToast("Loading...");
    }

    protected void showProgress() {

    }

    protected void onNoNetWork() {

    }

    @Override
    public void onError(Throwable e) {
        String message = "";
        int code = -1;
        e.printStackTrace();
        if (e instanceof UnknownHostException) {
            message = "UnknownHostException";
        } else if (e instanceof HttpException) {
            try {
                JSONObject jsonObject = new JSONObject(((HttpException) e).response().errorBody().string());
                if (jsonObject.has("message")) {
                    message = jsonObject.getString("message");
                }
            } catch (Exception ex) {
                message = "HTTP 500";
            }
            code = ((HttpException) e).code();
        } else if (e instanceof SocketTimeoutException) {
            message = "SocketTimeoutException";
        } else if (e instanceof JsonParseException
                || e instanceof JSONException) {
            message = "JsonParseError: " + e.getMessage();
        } else if (e instanceof ConnectException) {
            message = "Not Connect Server";
        } else if (e instanceof ServerException) {
            message = ((ServerException) e).message;
            code = ((ServerException) e).code;
        } else {
            message = "Unknown";
        }
        if (code == 401) {
            EventBus.getDefault().post(new EventDTO("token_timeout",code));
        }
        ToastUtils.showToast(context, message);
        onFailure(message, code);
    }

    @Override
    public void onNext(T t) {
        onSuccess(t);
    }

    /**
     * success
     *
     * @param t
     */
    public abstract void onSuccess(T t);

    /**
     * failure
     *
     * @param msg
     */
    public void onFailure(String msg, int code) {
    }


}
