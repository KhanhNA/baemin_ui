package com.tsolution.base;

import android.app.Application;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.IdRes;
import androidx.annotation.LayoutRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.tsolution.base.listener.DefaultFunctionActivity;
import com.workable.errorhandler.ErrorHandler;

public interface BaseFragmentInterface extends DefaultFunctionActivity {
    void setBinding(ViewDataBinding binding);

    void setViewModel(BaseViewModel viewModel);

    void setRecycleView(RecyclerView recycleView);

    default View createView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        BaseViewModel viewModel = null;
        ViewDataBinding binding = null;
        try {
            //super.onCreateView(inflater, container, savedInstanceState);
            binding = DataBindingUtil.inflate(inflater, this.getLayoutRes(), container, false);
            setBinding(binding);
            if (this.getVMClass() != null) {
                this.init(savedInstanceState, binding, this.getLayoutRes(), this.getVMClass(), this.getRecycleResId());
            }
            return binding.getRoot();
        } catch (Throwable e) {
            e.printStackTrace();
            ErrorHandler.create().handle(e);

            return null;
        }

    }

    default BaseViewModel init(@Nullable Bundle savedInstanceState, ViewDataBinding binding, @LayoutRes int layoutId, Class<? extends BaseViewModel> clazz, @IdRes int recyclerViewId) throws Throwable {
        BaseViewModel viewModel = (BaseViewModel) clazz.getDeclaredConstructor(Application.class).newInstance(this.getBaseActivity().getApplication());
        setViewModel(viewModel);
        View view = binding.getRoot();
        binding.setVariable(BR.viewModel, viewModel);
        binding.setVariable(BR.listener, this);

        viewModel.setView(this::processFromVM);
        viewModel.setAlertModel(this.getBaseActivity().getViewModel().getAlertModel());
        if (recyclerViewId != 0) {
            RecyclerView recyclerView = (RecyclerView) view.findViewById(recyclerViewId);
            setRecycleView(recyclerView);
            RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getBaseActivity());
            recyclerView.setLayoutManager(layoutManager);
        }
        return viewModel;

    }

    default BaseViewModel init(@NonNull LayoutInflater inflater, ViewDataBinding binding, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState, @LayoutRes int layoutId, Class<? extends BaseViewModel> clazz, @IdRes int recyclerViewId) throws Exception {
        BaseViewModel viewModel = (BaseViewModel) clazz.getDeclaredConstructor(Application.class).newInstance(this.getBaseActivity().getApplication());
        View view = binding.getRoot();
        binding.setVariable(BR.viewModel, viewModel);
        binding.setVariable(BR.listener, this);
        viewModel.setView(this::processFromVM);
        viewModel.setAlertModel(this.getBaseActivity().getViewModel().getAlertModel());
        if (recyclerViewId != 0) {
            RecyclerView recyclerView = (RecyclerView) view.findViewById(recyclerViewId);
            setRecycleView(recyclerView);
            RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getBaseActivity());
            recyclerView.setLayoutManager(layoutManager);
        }
        return viewModel;
    }
}
