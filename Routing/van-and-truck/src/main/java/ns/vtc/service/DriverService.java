package ns.vtc.service;

import ns.utils.JPAUtility;
import ns.vtc.constants.Constants;
import ns.vtc.entity.FleetDriver;
import ns.vtc.entity.FleetVehicleAssignationLog;
import ns.vtc.enums.StaffType;
import ns.vtc.enums.StatusType;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class DriverService {
    private static DriverService instance;

    public static DriverService getInstance() {
        if (instance == null) {
            instance = new DriverService();
        }
        return instance;
    }
    public List<FleetDriver> getListDriver(String datePlan){
        List<Object> lstParams = new ArrayList<>();
        lstParams.add(StatusType.RUNNING.getValue());
        lstParams.add(StaffType.DRIVER.getValue());
        lstParams.add(StatusType.RUNNING.getValue());
        lstParams.add(datePlan);

        lstParams.add(datePlan);

        // lstParams.add(StatusRouting.NOT_CONFIRM.getValue());
        lstParams.add(datePlan);
        List<FleetDriver> lstDriver = new ArrayList<>();

        try {
            ResultSet re = JPAUtility.getInstance().getJdbcResultSet
                    ("select fd.id, fd.name, fd.display_name, fd.address , " +
                            "                    fd.company_id, fd.driver_code, sdl.\"name\" class_driver, sdl.max_tonnage, " +
                            "                   fd.point,                                                                   " +
                            " (select  count( distinct date_plan) from sharevan_routing_plan_day                            " +
                            " where status = '2' and ship_type = '0' and vehicle_id is not null and driver_id = fd.id) dem  " +
                            "                    from fleet_driver fd " +
                            "                    left join sharevan_driver_license sdl  " +
                            "                    on fd.class_driver = sdl.id " +
                            "  where fd.active = true and fd.status = ? and fd.employee_type = ? " +
                            "     AND NOT EXISTS( SELECT  " +
                            "          1  " +
                            "         FROM  " +
                            "          fleet_vehicle_assignation_log al  " +
                            "         WHERE  " +
                            "          fd.id = al.driver_id " +
                            "           and al.status = ? " +
                            "           and al.driver_status = '1' " +
                            "           and al.date_start = to_date(?,'dd/mm/yyyy'))        " +
                            "     AND NOT EXISTS (SELECT  " +
                            "          1  " +
                            "         FROM  " +
                            "          fleet_request_time_off rto " +
                            "         WHERE  " +
                            "          fd.id = rto.driver_id " +
                            "           and rto.status != '4' " +
                            "           and rto.request_day = to_date(?,'dd/mm/yyyy')) " +
                            "     AND NOT EXISTS (SELECT  " +
                            "          1  " +
                            "         FROM  " +
                            "          sharevan_routing_plan_day rpd " +
                            "         WHERE  " +
                            "          fd.id = rpd.driver_id " +
                            "           and rpd.status in ('0','5') " +
                            "           and rpd.date_plan = to_date(?,'dd/mm/yyyy'))",lstParams,new HashMap());
            while (re.next()){
                System.out.println("id driver: " + re.getInt("id"));
                FleetDriver d = new FleetDriver();
                d.setId(re.getInt("id"));
                d.setName(re.getString("name"));
                d.setDisplayName(re.getString("display_name"));
                d.setCompanyId(re.getInt("company_id"));
                d.setDriverCode(re.getString("driver_code"));
                d.setAddress(re.getString("address"));
                d.setClassDriver(re.getString("class_driver"));
                d.setMaxTonage(re.getDouble("max_tonnage"));
                d.setNumTrip(re.getInt("dem"));
                d.setPoint(re.getInt("point"));
                lstDriver.add(d);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return lstDriver;
    }
    public FleetVehicleAssignationLog createDriverCalendar(FleetVehicleAssignationLog calendar){
        calendar = JPAUtility.getInstance().save(calendar);
        return calendar;
    }
    public List<FleetDriver> getListDriverTest(String datePlan){
        List<Object> lstParams = new ArrayList<>();
        lstParams.add(StatusType.RUNNING.getValue());
        lstParams.add(StaffType.DRIVER.getValue());
        lstParams.add(StatusType.RUNNING.getValue());
        lstParams.add(datePlan);

        lstParams.add(datePlan);

        // lstParams.add(StatusRouting.NOT_CONFIRM.getValue());
        lstParams.add(datePlan);
        lstParams.add(datePlan);
        List<FleetDriver> lstDriver = new ArrayList<>();

        try {
            ResultSet re = JPAUtility.getInstance().getJdbcResultSet
                    ("select fd.id, fd.name, fd.display_name, fd.address , " +
                            "                    fd.company_id, fd.driver_code, sdl.\"name\" class_driver, sdl.max_tonnage, " +
                            "                   fd.point,                                                                   " +
                            " (select  count( distinct date_plan) from sharevan_routing_plan_day                            " +
                            " where status = '2' and ship_type = '0' and vehicle_id is not null and driver_id = fd.id) dem  " +
                            "                    from fleet_driver fd " +
                            "                    left join sharevan_driver_license sdl  " +
                            "                    on fd.class_driver = sdl.id " +
                            "  where fd.active = true and fd.status = ? and fd.employee_type = ? and fd.company_id = 1" +
                            "     AND NOT EXISTS( SELECT  " +
                            "          1  " +
                            "         FROM  " +
                            "          fleet_vehicle_assignation_log al  " +
                            "         WHERE  " +
                            "          fd.id = al.driver_id " +
                            "           and al.status = ? " +
                            "           and al.driver_status = '1' " +
                            "           and al.date_start = to_date(?,'dd/mm/yyyy'))        " +
                            "     AND NOT EXISTS (SELECT  " +
                            "          1  " +
                            "         FROM  " +
                            "          fleet_request_time_off rto " +
                            "         JOIN  fleet_group_request_to grt on rto.group_request_id = grt.id " +
                            "         WHERE  " +
                            "          fd.id = rto.driver_id " +
                            "           and grt.status != '4' " +
                            "           and rto.request_day = to_date(?,'dd/mm/yyyy')) " +
                            "     AND NOT EXISTS (SELECT  " +
                            "          1  " +
                            "         FROM  " +
                            "          sharevan_routing_plan_day rpd " +
                            "         WHERE  " +
                            "          fd.id = rpd.driver_id " +
                            "           and rpd.status in ('0','5') " +
                            "           and rpd.date_plan = to_date(?,'dd/mm/yyyy'))" +
                            "    AND fd.expires_date >= to_date(?,'dd/mm/yyyy')",lstParams,new HashMap()); //bo sung them thong tin con han bang lai xe
            while (re.next()){
                System.out.println("id driver: " + re.getInt("id"));
                FleetDriver d = new FleetDriver();
                d.setId(re.getInt("id"));
                d.setName(re.getString("name"));
                d.setDisplayName(re.getString("display_name"));
                d.setCompanyId(re.getInt("company_id"));
                d.setDriverCode(re.getString("driver_code"));
                d.setAddress(re.getString("address"));
                d.setClassDriver(re.getString("class_driver"));
                d.setMaxTonage(re.getDouble("max_tonnage"));
                d.setPoint(re.getInt("point"));
                d.setNumTrip(re.getInt("dem"));
                lstDriver.add(d);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return lstDriver;
    }
    public List<FleetDriver> getLstDriverOnOutdateLicense(String datePlan){
        HashMap<String,Object> lstParam = new HashMap<>();
        lstParam.put("dateCheck",datePlan);
        lstParam.put("limitDays", Constants.DELTA_DAYS_CHECK_OUTDATE_LICENSE);
        return JPAUtility.getInstance().jpaFindAll("select fd from FleetDriver fd " +
                "where  (expiresDate - to_date(:dateCheck,'dd/mm/yyyy')) between 0 and :limitDays",lstParam);
    }
}
