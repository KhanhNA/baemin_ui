package ns.vtc.entity;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RoutingInfo {
    private Integer routingPlanDayId;
    private String orderType;
    private String customerRating;
    private Integer point;
}
