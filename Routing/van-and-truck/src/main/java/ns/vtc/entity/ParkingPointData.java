package ns.vtc.entity;

import lombok.Data;

import javax.persistence.*;
import java.sql.Time;

@Entity
@Table(name="Parking_Point_Data")
@Data
public class ParkingPointData {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name="id")
  private long id;

  @Column(name="parking_Point_Id")
  private long parkingPointId;

  @Column(name="warehouse_Id")
  private long warehouseId;

  @Column(name="minutes")
  private long minutes;

  @Column(name="cost")
  private long cost;




}
