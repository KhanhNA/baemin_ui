package ns.vtc.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name="fleet_vehicle_assignation_log")
@Getter
@Setter
public class FleetVehicleAssignationLog {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id")
    private Long id;

    @Column(name="assignation_log_code")
    private String assignationLogCode;

    @Column(name = "vehicle_id")
    private Long vehicleId;

    @Column(name = "driver_id")
    private Long driverId;

    @Column(name = "date_start")
    private LocalDateTime startDate;

    @Column(name = "date_end")
    private LocalDateTime endDate;

    @Column(name="status")
    private String status;

    @Column(name = "company_id")
    private Integer companyId;

    @Column(name = "create_Uid")
    private Integer createUid;

    @Column(name = "create_Date")
    private LocalDateTime createDate;

    @Column(name="give_car_back")
    private LocalDateTime giveCarBack;

    @Column(name = "write_Uid")
    private Integer writeUid;

    @Column(name = "write_Date")
    private LocalDateTime writeDate;

    @Column(name = "driver_status")
    private String driverStatus;
}
