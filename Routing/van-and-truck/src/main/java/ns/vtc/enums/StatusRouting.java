package ns.vtc.enums;

public enum StatusRouting {
    NOT_CONFIRM("0"),
    DRIVER_CONFIRM("1"),
    COMPLETED("2"),
    CANCELLED("3"),
    NOT_APPROVED("4"),
    DRAFT("5");
    private String value;

    StatusRouting(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
    public static StatusRouting of(String value) throws Exception {
        if(NOT_CONFIRM.getValue().equals(value)){
            return NOT_CONFIRM;
        }
        if(DRIVER_CONFIRM.getValue().equals(value)){
            return DRIVER_CONFIRM;
        }
        if(COMPLETED.getValue().equals(value)){
            return COMPLETED;
        }
        if(CANCELLED.getValue().equals(value)){
            return CANCELLED;
        }
        if(NOT_APPROVED.getValue().equals(value)){
            return NOT_APPROVED;
        }
        if(DRAFT.getValue().equals(value)){
            return DRAFT;
        }
        throw new Exception ("Wrong value!");
    }
}

