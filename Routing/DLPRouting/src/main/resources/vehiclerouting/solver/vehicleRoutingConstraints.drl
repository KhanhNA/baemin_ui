/*
 * Copyright 2012 Red Hat, Inc. and/or its affiliates.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package vehiclerouting.solver;
    dialect "java"

import org.optaplanner.core.api.score.buildin.hardsoftlong.HardSoftLongScoreHolder;

import vehiclerouting.domain.Depot;
import vehiclerouting.domain.location.Location;
import vehiclerouting.customize.entity.ShipmentStandstill;
import vehiclerouting.domain.VehicleRoutingSolutionV2;
import vehiclerouting.customize.entity.ShipmentPot;
import vehiclerouting.domain.timewindowed.TimeWindowedDepot;
import vehiclerouting.domain.timewindowed.TimeWindowedVehicleRoutingSolution;
import vehiclerouting.domain.timewindowed.TimeWindowedShipmentPot;
import vehiclerouting.domain.VehicleV2;

global HardSoftLongScoreHolder scoreHolder;

// ############################################################################
// Hard constraints
// ############################################################################

rule "vehicleCapacity"
    when
        $vehicle : VehicleV2($capacity : capacity)
        accumulate(
            ShipmentPot(
                vehicle == $vehicle,
                $demand : demand);
            $demandTotal : sum($demand);
            $demandTotal > $capacity
        )
    then
        scoreHolder.addHardConstraintMatch(kcontext, (long)($capacity - $demandTotal));
end

// ############################################################################
// Soft constraints
// ############################################################################

rule "distanceToPreviousStandstill"
    when
        $customer : ShipmentPot(previousStandstill != null, $distanceFromPreviousStandstill : distanceFromPreviousStandstill)
    then
        scoreHolder.addSoftConstraintMatch(kcontext, - ($distanceFromPreviousStandstill).longValue());
end

rule "distanceFromLastCustomerToDepot"
    when
        $customer : ShipmentPot(previousStandstill != null, nextShipmentPot == null)
    then
        VehicleV2 vehicle = $customer.getVehicle();
        scoreHolder.addSoftConstraintMatch(kcontext, - ($customer.getDistanceTo(vehicle)).longValue());
end


// ############################################################################
// TimeWindowed: additional hard constraints
// ############################################################################

rule "arrivalAfterDueTime"
    when
        TimeWindowedShipmentPot(dueTime < arrivalTime, $dueTime : dueTime, $arrivalTime : arrivalTime)
    then
        scoreHolder.addHardConstraintMatch(kcontext, (long)($dueTime - $arrivalTime.longValue()));
end

// Score constraint arrivalAfterDueTimeAtDepot is a built-in hard constraint in VehicleRoutingImporter
