package vehiclerouting.app;

import common.constants.Constansts;
import ns.utils.DateUtils;
import ns.vtc.constants.Constants;
import ns.vtc.entity.FleetVehicleState;
import ns.vtc.entity.SolutionDay;
import ns.vtc.enums.VehicleStatus;
import org.optaplanner.core.api.score.buildin.hardsoftlong.HardSoftLongScore;
import org.optaplanner.core.api.solver.Solver;
import org.optaplanner.core.api.solver.SolverFactory;
import vehiclerouting.customize.StaticData;
import vehiclerouting.customize.entity.ShipmentPot;
import vehiclerouting.domain.VehicleRoutingSolutionV2;
import vehiclerouting.domain.VehicleV2;
import vehiclerouting.solver.score.VehicleRoutingEasyScoreCalculatorV2;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;

public class VehicleRoutingClass {
    public static void main(String[] args) throws Exception {
        SolverFactory<VehicleRoutingSolutionV2> solverFactory = SolverFactory.createFromXmlResource(
                "vehiclerouting/solver/vehicleRoutingSolverConfig.xml");
        Solver<VehicleRoutingSolutionV2> solver = solverFactory.buildSolver();

        // Load a problem with 8 queens
        LocalDateTime ldt = LocalDateTime.now();
    //    LocalDateTime ldt = LocalDateTime.of(2021,01,07,0,0);
     //   StaticData.updateOldAssignationLog(DateUtils.toString(ldt.plusDays(-2), DateUtils.DD_MM_YYYY));
        SolutionDay solutionDay = StaticData.getSolution(DateUtils.toString(ldt, DateUtils.DD_MM_YYYY));
     //   VehicleRoutingSolutionV2 problem = StaticData.loadProblem(solutionDay);
        if(solutionDay == null)
            return;
    //    VehicleRoutingSolutionV2 problem = StaticData.loadProblem(solutionDay);
       VehicleRoutingSolutionV2 problem = StaticData.loadProblemMstore(ldt.toLocalDate());
        // Solve the problem
        VehicleRoutingSolutionV2 problemSolved = solver.solve(problem);
        try {
            if(problemSolved != null && problemSolved.getScore() != null && problemSolved.getScore().getHardScore() == 0){


                StaticData.updateOldAssignationLog(DateUtils.toString(ldt.plusDays(-1), DateUtils.DD_MM_YYYY));
                StaticData.updateBestSolution(problemSolved);
                result(problemSolved);
                System.out.println("Hết lỗi chưa?????");
            }
            else{
                Constants.DEFAULT_MAX_DISTANCE_ROUTING += Constants.DELTA_DISTANCE;
                problemSolved = solver.solve(problem);
                if(problemSolved != null && problemSolved.getScore() != null && problemSolved.getScore().getHardScore() == 0){
                    StaticData.updateOldAssignationLog(DateUtils.toString(ldt.plusDays(-1), DateUtils.DD_MM_YYYY));
                    StaticData.updateBestSolution(problemSolved);
                    result(problemSolved);
                    System.out.println("Check lanf 2");
                }
                else{
                    System.out.println("khong tim duoc phuong an phu hop!");
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    public static void result(VehicleRoutingSolutionV2 solution){
       // List<Customer> customerList = solution.get();

        HardSoftLongScore score = solution.getScore();
      //  if(score.getHardScore() == 0){
            System.out.println("soft: " + score.getSoftScore() + "  hard: " + score.getHardScore());
            List<VehicleV2> vehicleList = solution.getVehicleList();
            System.out.println("kdsjdj: " + vehicleList.size());
            for(VehicleV2 v: vehicleList){
                System.out.println("vehicle: " + v.getId());
                System.out.println("distance vehicle: " + v.getTotalDistance());
                ShipmentPot shipmentPot = v.getNextShipmentPot();
                if(shipmentPot != null){
                    System.out.print(shipmentPot.getId() + "->");
                    while (shipmentPot.getNextShipmentPot() != null){
                        shipmentPot = shipmentPot.getNextShipmentPot();
                        System.out.print(shipmentPot.getId() + "->");
                    }
                    System.out.println();
                    System.out.println("^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^");
                    FleetVehicleState vehicleState = StaticData.getVehicleStateByCode(VehicleStatus.ORDERED.getValue());
                    if(vehicleState != null){
                        try {
                            StaticData.updateStateVehicle(v.getId(),vehicleState.getId());
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }

            }
      //  }

    }

}
