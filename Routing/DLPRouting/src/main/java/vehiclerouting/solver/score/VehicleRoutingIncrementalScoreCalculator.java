/*
 * Copyright 2012 Red Hat, Inc. and/or its affiliates.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package vehiclerouting.solver.score;

import org.optaplanner.core.api.score.buildin.hardsoftlong.HardSoftLongScore;
import org.optaplanner.core.impl.score.director.incremental.AbstractIncrementalScoreCalculator;
import vehiclerouting.customize.entity.ShipmentPot;
import vehiclerouting.customize.entity.ShipmentStandstill;
import vehiclerouting.domain.VehicleRoutingSolutionV2;
import vehiclerouting.domain.VehicleV2;
import vehiclerouting.domain.timewindowed.TimeWindowedShipmentPot;
import vehiclerouting.domain.timewindowed.TimeWindowedVehicleRoutingSolution;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class VehicleRoutingIncrementalScoreCalculator extends AbstractIncrementalScoreCalculator<VehicleRoutingSolutionV2> {

    private boolean timeWindowed;
    private Map<VehicleV2, Double> vehicleDemandMap;

    private long hardScore;
    private long softScore;

    @Override
    public void resetWorkingSolution(VehicleRoutingSolutionV2 solution) {
        timeWindowed = solution instanceof TimeWindowedVehicleRoutingSolution;
        List<VehicleV2> vehicleList = solution.getVehicleList();
        vehicleDemandMap = new HashMap<>(vehicleList.size());
        for (VehicleV2 vehicle : vehicleList) {
            vehicleDemandMap.put(vehicle, 0d);
        }
        hardScore = 0L;
        softScore = 0L;
        for (ShipmentPot shipmentPot : solution.getShipmentPotList()) {
            insertPreviousStandstill(shipmentPot);
            insertVehicle(shipmentPot);
            // Do not do insertNextCustomer(shipmentPot) to avoid counting distanceFromLastCustomerToDepot twice
            if (timeWindowed) {
                insertArrivalTime((TimeWindowedShipmentPot) shipmentPot);
            }
        }
    }

    @Override
    public void beforeEntityAdded(Object entity) {
        // Do nothing
    }

    @Override
    public void afterEntityAdded(Object entity) {
        if (entity instanceof VehicleV2) {
            return;
        }
        insertPreviousStandstill((ShipmentPot) entity);
        insertVehicle((ShipmentPot) entity);
        // Do not do insertNextCustomer(customer) to avoid counting distanceFromLastCustomerToDepot twice
        if (timeWindowed) {
            insertArrivalTime((TimeWindowedShipmentPot) entity);
        }
    }

    @Override
    public void beforeVariableChanged(Object entity, String variableName) {
        if (entity instanceof VehicleV2) {
            return;
        }
        switch (variableName) {
            case "previousStandstill":
                retractPreviousStandstill((ShipmentPot) entity);
                break;
            case "vehicle":
                retractVehicle((ShipmentPot) entity);
                break;
            case "nextCustomer":
                retractNextCustomer((ShipmentPot) entity);
                break;
            case "arrivalTime":
                retractArrivalTime((TimeWindowedShipmentPot) entity);
                break;
            default:
                throw new IllegalArgumentException("Unsupported variableName (" + variableName + ").");
        }
    }

    @Override
    public void afterVariableChanged(Object entity, String variableName) {
        if (entity instanceof VehicleV2) {
            return;
        }
        switch (variableName) {
            case "previousStandstill":
                insertPreviousStandstill((ShipmentPot) entity);
                break;
            case "vehicle":
                insertVehicle((ShipmentPot) entity);
                break;
            case "nextCustomer":
                insertNextCustomer((ShipmentPot) entity);
                break;
            case "arrivalTime":
                insertArrivalTime((TimeWindowedShipmentPot) entity);
                break;
            default:
                throw new IllegalArgumentException("Unsupported variableName (" + variableName + ").");
        }
    }

    @Override
    public void beforeEntityRemoved(Object entity) {
        if (entity instanceof VehicleV2) {
            return;
        }
        retractPreviousStandstill((ShipmentPot) entity);
        retractVehicle((ShipmentPot) entity);
        // Do not do retractNextCustomer(customer) to avoid counting distanceFromLastCustomerToDepot twice
        if (timeWindowed) {
            retractArrivalTime((TimeWindowedShipmentPot) entity);
        }
    }

    @Override
    public void afterEntityRemoved(Object entity) {
        // Do nothing
    }

    private void insertPreviousStandstill(ShipmentPot shipmentPot) {
        ShipmentStandstill previousStandstill = shipmentPot.getPreviousStandstill();
        if (previousStandstill != null) {
            // Score constraint distanceToPreviousStandstill
            softScore -= shipmentPot.getDistanceFromPreviousStandstill();
        }
    }

    private void retractPreviousStandstill(ShipmentPot shipmentPot) {
        ShipmentStandstill previousStandstill = shipmentPot.getPreviousStandstill();
        if (previousStandstill != null) {
            // Score constraint distanceToPreviousStandstill
            softScore += shipmentPot.getDistanceFromPreviousStandstill();
        }
    }

    private void insertVehicle(ShipmentPot shipmentPot) {
        VehicleV2 vehicle = shipmentPot.getVehicle();
        if (vehicle != null) {
            // Score constraint vehicleCapacity
            Double capacity = vehicle.getCapacity();
            Double oldDemand = vehicleDemandMap.get(vehicle);
            Double newDemand = oldDemand + shipmentPot.getDemand();
            hardScore += Math.min(capacity - newDemand, 0) - Math.min(capacity - oldDemand, 0);
            vehicleDemandMap.put(vehicle, newDemand);
            if (shipmentPot.getNextShipmentPot() == null) {
                // Score constraint distanceFromLastCustomerToDepot
                softScore -= shipmentPot.getLocation().getDistanceTo(vehicle.getLocation());
            }
        }
    }

    private void retractVehicle(ShipmentPot shipmentPot) {
        VehicleV2 vehicle = shipmentPot.getVehicle();
        if (vehicle != null) {
            // Score constraint vehicleCapacity
            Double capacity = vehicle.getCapacity();
            Double oldDemand = vehicleDemandMap.get(vehicle);
            Double newDemand = oldDemand - shipmentPot.getDemand();
            hardScore += Math.min(capacity - newDemand, 0) - Math.min(capacity - oldDemand, 0);
            vehicleDemandMap.put(vehicle, newDemand);
            if (shipmentPot.getNextShipmentPot() == null) {
                // Score constraint distanceFromLastCustomerToDepot
                softScore += shipmentPot.getLocation().getDistanceTo(vehicle.getLocation());
            }
        }
    }

    private void insertNextCustomer(ShipmentPot shipmentPot) {
        VehicleV2 vehicle = shipmentPot.getVehicle();
        if (vehicle != null) {
            if (shipmentPot.getNextShipmentPot() == null) {
                // Score constraint distanceFromLastCustomerToDepot
                softScore -= shipmentPot.getLocation().getDistanceTo(vehicle.getLocation());
            }
        }
    }

    private void retractNextCustomer(ShipmentPot customer) {
        VehicleV2 vehicle = customer.getVehicle();
        if (vehicle != null) {
            if (customer.getNextShipmentPot() == null) {
                // Score constraint distanceFromLastCustomerToDepot
                softScore += customer.getLocation().getDistanceTo(vehicle.getLocation());
            }
        }
    }

    private void insertArrivalTime(TimeWindowedShipmentPot windowedShipmentPot) {
        Double arrivalTime = windowedShipmentPot.getArrivalTime();
        if (arrivalTime != null) {
            Double dueTime = windowedShipmentPot.getDueTime();
            if (dueTime < arrivalTime) {
                // Score constraint arrivalAfterDueTime
                hardScore -= (arrivalTime - dueTime);
            }
        }
        // Score constraint arrivalAfterDueTimeAtDepot is a built-in hard constraint in VehicleRoutingImporter
    }

    private void retractArrivalTime(TimeWindowedShipmentPot windowedShipmentPot) {
        Double arrivalTime = windowedShipmentPot.getArrivalTime();
        if (arrivalTime != null) {
            Double dueTime = windowedShipmentPot.getDueTime();
            if (dueTime < arrivalTime) {
                // Score constraint arrivalAfterDueTime
                hardScore += (arrivalTime - dueTime);
            }
        }
    }

    @Override
    public HardSoftLongScore calculateScore() {
        return HardSoftLongScore.of(hardScore, softScore);
    }

}
