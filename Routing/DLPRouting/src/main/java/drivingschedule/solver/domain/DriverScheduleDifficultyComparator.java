package drivingschedule.solver.domain;

import drivingschedule.domain.DrivingSchedule;

import java.util.Collections;
import java.util.Comparator;

public class DriverScheduleDifficultyComparator implements Comparator<DrivingSchedule> {
    private static final Comparator<DrivingSchedule> COMPARATOR = Collections.reverseOrder(Comparator.comparingInt(DrivingSchedule::getListHisDriver))
            .thenComparingLong(DrivingSchedule::getId);
    @Override
    public int compare(DrivingSchedule o1, DrivingSchedule o2) {
        return COMPARATOR.compare(o1, o2);
    }
}
