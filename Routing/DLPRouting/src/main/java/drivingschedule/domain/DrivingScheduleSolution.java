package drivingschedule.domain;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import common.domain.AbstractPersistable;
import org.optaplanner.core.api.domain.solution.PlanningEntityCollectionProperty;
import org.optaplanner.core.api.domain.solution.PlanningScore;
import org.optaplanner.core.api.domain.solution.PlanningSolution;
import org.optaplanner.core.api.domain.solution.ProblemFactCollectionProperty;
import org.optaplanner.core.api.domain.valuerange.ValueRangeProvider;
import org.optaplanner.core.api.score.buildin.hardsoftlong.HardSoftLongScore;

import java.util.List;

@PlanningSolution
@XStreamAlias("DrivingScheduleSolution")
public class DrivingScheduleSolution extends AbstractPersistable {
    private List<Driver> lstDriver;
   // private List<Vehicle> lstVehicle;
    private List<DrivingSchedule> lstSchedule;
    private List<Vehicle> lstVehicle;

    protected HardSoftLongScore score;

    @ProblemFactCollectionProperty
    @ValueRangeProvider(id = "driverRange")
    public List<Driver> getLstDriver() {
        return lstDriver;
    }
    @ProblemFactCollectionProperty
    public List<Vehicle> getLstVehicle() {
        return lstVehicle;
    }

    public void setLstVehicle(List<Vehicle> lstVehicle) {
        this.lstVehicle = lstVehicle;
    }

    public void setLstDriver(List<Driver> lstDriver) {
        this.lstDriver = lstDriver;
    }

    @PlanningEntityCollectionProperty
    public List<DrivingSchedule> getLstSchedule() {
        return lstSchedule;
    }

    public void setLstSchedule(List<DrivingSchedule> lstSchedule) {
        this.lstSchedule = lstSchedule;
    }

    @PlanningScore
    public HardSoftLongScore getScore() {
        return score;
    }

    public void setScore(HardSoftLongScore score) {
        this.score = score;
    }
}
