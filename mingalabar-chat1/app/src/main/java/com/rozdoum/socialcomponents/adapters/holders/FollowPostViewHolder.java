package com.rozdoum.socialcomponents.adapters.holders;

import android.view.View;

import com.rozdoum.socialcomponents.main.base.BaseActivity;
import com.rozdoum.socialcomponents.managers.listeners.OnDataChangedListener;
import com.rozdoum.socialcomponents.managers.listeners.OnPostChangedListener;
import com.rozdoum.socialcomponents.model.FollowingPost;
import com.rozdoum.socialcomponents.model.Post;
import com.rozdoum.socialcomponents.utils.LogUtil;

import java.util.List;

/**
 * Created by Alexey on 22.05.18.
 */
public class FollowPostViewHolder extends PostViewHolder {


    public FollowPostViewHolder(View view, OnClickListener onClickListener, BaseActivity activity) {
        super(view, onClickListener, activity);
    }

    public FollowPostViewHolder(View view, OnClickListener onClickListener, BaseActivity activity, boolean isAuthorNeeded) {
        super(view, onClickListener, activity, isAuthorNeeded);
    }

    public void bindData(FollowingPost followingPost) {

        OnDataChangedListener<Post> onPostsDataChangedListener = new OnDataChangedListener<Post>() {
            @Override
            public void onListChanged(List<Post> list) {
                if(list == null){
                    return;
                }
                for(Post post: list) {
                    bindData(post);
                }
            }
        };
        postManager.getPostsListByUser(onPostsDataChangedListener, followingPost.getProfileId());
//        postManager.getSinglePostValue(followingPost.getProfileId(), new OnPostChangedListener() {
//            @Override
//            public void onObjectChanged(Post obj) {
//                bindData(obj);
//            }
//
//            @Override
//            public void onError(String errorText) {
//                LogUtil.logError(TAG, "bindData", new RuntimeException(errorText));
//            }
//        });
    }

}
