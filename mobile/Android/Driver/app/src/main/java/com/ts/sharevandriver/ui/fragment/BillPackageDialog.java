package com.ts.sharevandriver.ui.fragment;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.DialogFragment;
import androidx.lifecycle.ViewModelProviders;

import com.ts.sharevandriver.R;
import com.ts.sharevandriver.base.Constants;
import com.ts.sharevandriver.databinding.BillPackageDialogBinding;
import com.ts.sharevandriver.model.BillPackage;
import com.ts.sharevandriver.viewmodel.BillPackageDialogVM;

import java.util.ArrayList;
import java.util.List;



public class BillPackageDialog extends DialogFragment {
    BillPackageDialogVM billPackageDialogVM;
    BillPackageDialogBinding mBinding;
    BillPackage billPackage = new BillPackage();
    String type;
    String qrCode;

    public BillPackageDialog(BillPackage billPackages, String type,String qrCode) {
        this.billPackage = billPackages;
        this.type = type;
        this.qrCode=qrCode;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(inflater, R.layout.bill_package_dialog, container, false);
        billPackageDialogVM = ViewModelProviders.of(this).get(BillPackageDialogVM.class);
        mBinding.setViewModel(billPackageDialogVM);
        mBinding.setListener(this);
        if (getDialog() != null && getDialog().getWindow() != null) {
            getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        }
        getData();
        new CountDownTimer(4000, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
            }

            @Override
            public void onFinish() {
                autoDismiss();
            }
        }.start();
        return mBinding.getRoot();
    }
    private void autoDismiss(){
        this.dismiss();
    }


    public void onClick(View v) {
        this.dismiss();
    }

    public void getData() {
        mBinding.lbTitle.setText(qrCode);
        billPackageDialogVM.getBillPackage().set(this.billPackage);
        billPackageDialogVM.getType().set(this.type);
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        return new Dialog(requireContext(), R.style.WideDialog);
    }

}
