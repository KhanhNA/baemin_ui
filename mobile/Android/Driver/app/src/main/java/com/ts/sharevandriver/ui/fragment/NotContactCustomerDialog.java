package com.ts.sharevandriver.ui.fragment;

import android.Manifest;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.FileUtils;
import android.provider.MediaStore;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.DialogFragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.luck.picture.lib.entity.LocalMedia;
import com.ts.sharevandriver.R;
import com.ts.sharevandriver.adapter.GridImageAdapter;
import com.ts.sharevandriver.adapter.ImageBaseAdapter;
import com.ts.sharevandriver.adapter.XBaseAdapter;
import com.ts.sharevandriver.base.AppController;
import com.ts.sharevandriver.databinding.NotContactCustomerDialogBinding;
import com.ts.sharevandriver.utils.FileUltil;
import com.ts.sharevandriver.utils.RotateImage;
import com.ts.sharevandriver.utils.ToastUtils;
import com.ts.sharevandriver.viewmodel.NotContactCustomerDialogVM;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;


public class NotContactCustomerDialog extends DialogFragment implements DatePickerDialog.OnDateSetListener {
    View.OnClickListener onClickListener;
    public NotContactCustomerDialogVM notContactCustomerDialogVM;
    private final int PICK_IMAGE_PERMISSIONS_REQUEST_CODE_CANCEL_DIALOG = 9999;
    public static final int MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE = 123;
    private final int REQUEST_CAMERA = 1100;
    private Uri mCapturedImageURI = null;

    private static final int CAMERA_IMAGE_REQUEST = 101;
    private String imageName;

    NotContactCustomerDialogBinding mBinding;
    Calendar calendar;
    boolean isChooseDate = false;
    CompoundButton.OnCheckedChangeListener onCheckedChangeListener;
    ImageBaseAdapter imageBaseAdapter;

    public NotContactCustomerDialog(View.OnClickListener onClickListener, CompoundButton.OnCheckedChangeListener onCheckedChangeListener) {
        this.onClickListener = onClickListener;
        this.onCheckedChangeListener = onCheckedChangeListener;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        mBinding = DataBindingUtil.inflate(inflater, R.layout.not_contact_customer_dialog, container, false);
        notContactCustomerDialogVM = ViewModelProviders.of(this).get(NotContactCustomerDialogVM.class);
        mBinding.setViewModel(notContactCustomerDialogVM);
        mBinding.setListener(this);
        if (getDialog() != null && getDialog().getWindow() != null) {
            getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        }
        initView();
        setUpRecycleView();
        return mBinding.getRoot();
    }


    public void selectImages() {
        if (checkPermissionREAD_EXTERNAL_STORAGE()) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (getActivity().checkSelfPermission(Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {
                    openCamera();
                } else {
                    requestPermissions(new String[]{Manifest.permission.CAMERA}, PICK_IMAGE_PERMISSIONS_REQUEST_CODE_CANCEL_DIALOG);
                }
            } else {
                openCamera();
            }
        }

    }

    public boolean checkPermissionREAD_EXTERNAL_STORAGE() {
        int currentAPIVersion = Build.VERSION.SDK_INT;
        if (currentAPIVersion >= android.os.Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(getContext(),
                    Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                if (ActivityCompat.shouldShowRequestPermissionRationale(
                        getActivity(),
                        Manifest.permission.READ_EXTERNAL_STORAGE)) {
                    showDialog("External storage", getContext(),
                            Manifest.permission.READ_EXTERNAL_STORAGE);

                } else {
                    ActivityCompat
                            .requestPermissions(
                                    (Activity) getActivity(),
                                    new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                                    MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE);
                }
                return false;
            } else {
                return true;
            }

        } else {
            return true;
        }
    }


    public void showDialog(final String msg, final Context context,
                           final String permission) {
        AlertDialog.Builder alertBuilder = new AlertDialog.Builder(context);
        alertBuilder.setCancelable(true);
        alertBuilder.setTitle("Permission necessary");
        alertBuilder.setMessage(msg + " permission is necessary");
        alertBuilder.setPositiveButton(android.R.string.yes,
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        ActivityCompat.requestPermissions((Activity) context,
                                new String[]{permission},
                                MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE);
                    }
                });
        AlertDialog alert = alertBuilder.create();
        alert.show();
    }

    private void openCamera() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (Build.VERSION.SDK_INT < 29) {
            String fileName = "temp.jpg";
            ContentValues values = new ContentValues();
            values.put(MediaStore.Images.Media.TITLE, fileName);
            mCapturedImageURI = getContext().getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, mCapturedImageURI);
        }
        startActivityForResult(intent, REQUEST_CAMERA);
    }

    public void selectTime() {
        if (getContext() != null) {
            Calendar c = Calendar.getInstance();
            int hour = c.get(Calendar.HOUR_OF_DAY);
            int minute = c.get(Calendar.MINUTE);
            TimePickerDialog timePickerDialog = new TimePickerDialog(getContext(), (view, hour1, minute1) -> {
                calendar.set(Calendar.HOUR_OF_DAY, hour1);
                calendar.set(Calendar.MINUTE, minute1);
                mBinding.txtTime.setText(AppController.formatTime.format(calendar.getTime()));
            }, hour, minute, DateFormat.is24HourFormat(getActivity()));
            timePickerDialog.show();
        }
    }

    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 90, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "IMG_" + Calendar.getInstance().getTime(), null);
        return Uri.parse(path);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CAMERA && resultCode == Activity.RESULT_OK) {
            Integer level_api = Build.VERSION.SDK_INT;
            String path = "";
            if (level_api < 29) {
                String[] projection = {MediaStore.Images.Media.DATA};
                Cursor cursor = getActivity().managedQuery(mCapturedImageURI, projection, null, null, null);
                int column_index_data = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
                cursor.moveToFirst();
                path = cursor.getString(column_index_data);
            } else {
                Bitmap bitmap = (Bitmap) data.getExtras().get("data");
                Uri uri = getImageUri(getContext(), bitmap);
                String pathTemp = FileUltil.getPath(uri, getContext());
                Bitmap bitmapTemp = RotateImage.rotateImage(bitmap, pathTemp);
                Uri uriTemp = getImageUri(getContext(), bitmapTemp);
                path = FileUltil.getPath(uriTemp, getContext());
            }
            notContactCustomerDialogVM.getIsNotEmptyImage().set(true);
            notContactCustomerDialogVM.getListImage().add(path);
            imageBaseAdapter.notifyItemInsert(notContactCustomerDialogVM.getListImage().size() - 1);
        }
    }

    public String getPartImage() {
        ContentValues values = new ContentValues();
        values.put(MediaStore.Images.Media.TITLE, "Title");
        values.put(MediaStore.Images.Media.DESCRIPTION, "From Camera");
        Uri path = getContext().getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
        return path.getPath();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == PICK_IMAGE_PERMISSIONS_REQUEST_CODE_CANCEL_DIALOG) {
            for (int grant : grantResults) {
                if (grant != PackageManager.PERMISSION_GRANTED) {
                    return;
                }
            }
            openCamera();
        }
    }

    public void selectDate() {
        Context scene = getContext();
        if (scene != null) {
            Calendar c = Calendar.getInstance();
            c.setTime(new Date());
            Integer year = c.get(Calendar.YEAR);
            Integer month = c.get(Calendar.MONTH);
            Integer day = c.get(Calendar.DAY_OF_MONTH);
            DatePickerDialog datePickerDialog = new DatePickerDialog(scene, this, year, month, day);
            datePickerDialog.getDatePicker().setMinDate(c.getTimeInMillis());
            datePickerDialog.show();
        }
    }

    private void setUpRecycleView() {
        imageBaseAdapter = new ImageBaseAdapter(getActivity(), R.layout.item_image_rotate, notContactCustomerDialogVM.getListImage());
        mBinding.rcImages.setAdapter(imageBaseAdapter);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity(), RecyclerView.HORIZONTAL, false);
        mBinding.rcImages.setLayoutManager(layoutManager);
    }


    private void initView() {
        mBinding.btnCancel.setOnClickListener(v -> {
            this.dismiss();
        });
        mBinding.btnDismiss.setOnClickListener(v -> {
            this.dismiss();
        });
        mBinding.btnConfirm.setOnClickListener(onClickListener);
        mBinding.cbChooseDate.setOnCheckedChangeListener(onCheckedChangeListener);

        mBinding.btnAddImage.setOnClickListener(v -> selectImages());
        mBinding.txtDate.setOnClickListener(v -> selectDate());
        calendar = Calendar.getInstance();
    }

    public List<String> getListImage() {
        if (notContactCustomerDialogVM.getListImage() != null && notContactCustomerDialogVM.getListImage().size() > 0) {
            return notContactCustomerDialogVM.getListImage();
        }
        ToastUtils.showToast(getActivity(), getString(R.string.NOT_IMAGE), R.drawable.ic_close14);
        return null;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        return new Dialog(requireContext(), R.style.WideDialog);
    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
        calendar.set(year, month, dayOfMonth);
        isChooseDate = true;
        mBinding.txtDate.setText(AppController.formatDate.format(calendar.getTime()));
    }

    public String getDate() {
        return AppController.formatTimeOdooDateTime.format(calendar.getTime());
    }
}
