package com.ts.sharevandriver.api;

import com.ns.odoolib_retrofit.model.OdooResultDto;
import com.ts.sharevandriver.base.IResponse;
import com.ts.sharevandriver.base.StaticData;
import com.ts.sharevandriver.model.ApiResponseModel;
import com.ts.sharevandriver.model.DayOff;
import com.ts.sharevandriver.utils.TsUtils;
import com.workable.errorhandler.ErrorHandler;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.List;

public class DayOffApi extends BaseApi {

    public static void getListDayOff(Integer month, Integer year, IResponse result) {
        JSONObject params;
        try {
            params = new JSONObject();
            params.put("month", month);
            params.put("year", year);
            mOdoo.callRoute("/driver/list_request_time_off", params, new SharingOdooResponse<List<DayOff>>() {

                @Override
                public void onSuccess(List<DayOff> resultDto) {
                    if (TsUtils.isNotNull(resultDto)) {
                        result.onSuccess(resultDto);
                    } else {
                        result.onFail(new Throwable());
                    }
                }

                @Override
                public void onFail(Throwable error) {
                    result.onFail(error);

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
            ErrorHandler.create().handle(e);
        }
    }

    public static void requestDayOff(List<DayOff> lstDayOff, String reason, IResponse result) {
        JSONObject params;
        try {
            params = new JSONObject();
            params.put("driver_id", StaticData.getDriver().getId());
            params.put("request_days", new JSONArray(mGson.toJson(lstDayOff)));
            params.put("reason", reason);
            mOdoo.callRoute("/driver/request_time_off", params, new SharingOdooResponse<String>() {

                @Override
                public void onSuccess(String resultDto) {
                    if (resultDto != null) {
                        result.onSuccess(resultDto);
                    } else {
                        result.onFail(new Throwable());
                    }
                }

                @Override
                public void onFail(Throwable error) {
                    result.onFail(error);

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
            ErrorHandler.create().handle(e);
        }
    }

    public static void cancelDayOff(Long group_request_id, IResponse result) {
        JSONObject params;
        try {
            params = new JSONObject();
            params.put("group_request_id", group_request_id);
            mOdoo.callRoute("/driver/cancel_request_time_off", params, new SharingOdooResponse<OdooResultDto<DayOff>>() {

                @Override
                public void onSuccess(OdooResultDto<DayOff> resultDto) {
                    if(resultDto != null) {
                        result.onSuccess(resultDto);
                    }else {
                        result.onFail(new Throwable());

                    }
                }

                @Override
                public void onFail(Throwable error) {
                    result.onFail(error);

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
            ErrorHandler.create().handle(e);
        }
    }

}
