package com.ts.sharevandriver.model;

import com.tsolution.base.BaseModel;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DriverDistance extends BaseModel {
    private Long id;
    private String code;
    private Float fromLatitude;
    private Float fromLongtitude;
    private Float toLatitude;
    private Float toLongitude;
    private String startAddress;
    private String endAddress;
    private Float minutes;
    private Float cost;//quãng đường.
}
