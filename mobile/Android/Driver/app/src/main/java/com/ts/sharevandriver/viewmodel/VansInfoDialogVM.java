package com.ts.sharevandriver.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.databinding.ObservableField;

import com.ns.odoolib_retrofit.model.OdooResultDto;
import com.ts.sharevandriver.api.CustomerApi;
import com.ts.sharevandriver.base.IResponse;
import com.ts.sharevandriver.base.StaticData;
import com.ts.sharevandriver.model.Driver;
import com.tsolution.base.BaseViewModel;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class VansInfoDialogVM extends BaseViewModel {
    ObservableField<Driver> driverObservable = new ObservableField<>();

    public VansInfoDialogVM(@NonNull Application application) {
        super(application);
    }

    public void getInfoUser() {

        CustomerApi.getDriver(new IResponse<OdooResultDto<Driver>>() {
            @Override
            public void onSuccess(OdooResultDto<Driver> o) {
                if (o != null && o.getRecords().size() > 0) {
                    Driver driver = o.getRecords().get(0);
                    driverObservable.set(driver);
                }
            }

            @Override
            public void onFail(Throwable error) {
            }
        });
    }
}
