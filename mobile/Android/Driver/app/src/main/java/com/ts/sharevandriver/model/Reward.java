package com.ts.sharevandriver.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Reward {
    private Long id;
    private String name;
    private Integer from_point;
    private Integer to_point;
    private String icon;

}
