package com.ts.sharevandriver.enums;

public class SosStatus {
    public static final Integer NOT_SOS = 0;
    public static final Integer CONTINUABLE = 1;
    public static final Integer CRASH = 2;
}
