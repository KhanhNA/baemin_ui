package com.ts.sharevandriver.model;

import com.tsolution.base.BaseModel;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SOSNumber extends BaseModel {
    String name;
    String phone;
}
