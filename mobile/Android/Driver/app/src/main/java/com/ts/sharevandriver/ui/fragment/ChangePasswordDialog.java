package com.ts.sharevandriver.ui.fragment;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.DialogFragment;
import androidx.lifecycle.ViewModelProviders;

import com.ts.sharevandriver.R;
import com.ts.sharevandriver.base.Constants;
import com.ts.sharevandriver.databinding.ChangePasswordDialogBinding;
import com.ts.sharevandriver.viewmodel.ChangePasswordVM;


public class ChangePasswordDialog extends DialogFragment {

    ChangePasswordVM changePasswordVM;
    ChangePasswordDialogBinding binding;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.change_password_dialog, container, false);
        changePasswordVM = ViewModelProviders.of(this).get(ChangePasswordVM.class);
        binding.setViewHoler2(changePasswordVM);
        changePasswordVM.setContext(getContext());
        if (getDialog() != null && getDialog().getWindow() != null) {
            getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        }

        binding.btnCancel.setOnClickListener(v -> dismiss());
        binding.btnDismiss.setOnClickListener(v -> dismiss());
        changePasswordVM.clearErro("oldPass");
        changePasswordVM.clearErro("newPass");
        changePasswordVM.clearErro("confirmPass");

        binding.btnConfirm.setOnClickListener(v -> {
            changePasswordVM.changePassword(this::runUi);
        });


        initView();

        return binding.getRoot();
    }

    private void initView() {
        binding.oldPass.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() > 0) {
                    changePasswordVM.clearErro("oldPass");
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        binding.newPass.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() > 0) {
                    changePasswordVM.clearErro("newPass");
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        binding.newConfirmPass.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() > 0) {
                    changePasswordVM.clearErro("confirmPass");
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    private void runUi(Object[] objects) {
        String action = (String) objects[0];
        if (action.equals(Constants.SUCCESS_API)) {
            getResult();
            dismiss();
        }
    }


    public void getResult() {
        Intent intent = new Intent();
        intent.putExtra(Constants.SUCCESS_API, Constants.SUCCESS_API);
        getTargetFragment().onActivityResult(
                getTargetRequestCode(), Constants.RESULT_OK, intent);
        this.dismiss();
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        return new Dialog(requireContext(), R.style.WideDialog);
    }

}
