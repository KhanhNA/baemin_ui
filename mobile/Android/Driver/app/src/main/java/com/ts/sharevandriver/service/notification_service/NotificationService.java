package com.ts.sharevandriver.service.notification_service;


import com.tsolution.base.BaseModel;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.util.Map;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class NotificationService extends BaseModel {
    private Long id;
    private String title;
    private Integer android_channel_id;
    private String body;
    private String token;
    private String topic;
    private String senderName;
    private String customData;
    private Long senderId;
    private String item_id;
    private String sound;
    private String appId;
    private String click_action;
    private String mess_object;
    private String badge;


    public static NotificationService of(Map<String, String> data)  {
        final ObjectMapper mapper = new ObjectMapper();
        final NotificationService notificationService = mapper.convertValue(data, NotificationService.class);

        return notificationService;
    }
}
