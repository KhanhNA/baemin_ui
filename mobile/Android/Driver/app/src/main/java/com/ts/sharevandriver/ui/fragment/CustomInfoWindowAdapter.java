package com.ts.sharevandriver.ui.fragment;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.Marker;
import com.ts.sharevandriver.R;
import com.ts.sharevandriver.base.AppController;
import com.ts.sharevandriver.base.ICallBack;
import com.ts.sharevandriver.base.StaticData;
import com.ts.sharevandriver.model.section.RootNode;


public class CustomInfoWindowAdapter implements GoogleMap.InfoWindowAdapter {

    private TextView txtPhone;
    private TextView txtAddress;
    private TextView txtName;
    private TextView txtStatus;
    private final Context context;
    ICallBack iCallBack;

    public CustomInfoWindowAdapter(Context context) {
        this.context = context;
    }

    public CustomInfoWindowAdapter(Context context, ICallBack iCallBack) {
        this.context = context;
        this.iCallBack = iCallBack;
    }

    @Override
    public View getInfoWindow(Marker marker) {
        View view;
        if (marker.getTag() instanceof RootNode) {
            view = ((Activity) context).getLayoutInflater()
                    .inflate(R.layout.map_window_properties, null);
            initMarkerWarehouse(view, marker);
        } else {
            view = ((Activity) context).getLayoutInflater()
                    .inflate(R.layout.map_window_vans, null);
            initMarkerVans(view);
        }
        return view;
    }

    @SuppressLint("SetTextI18n")
    private void render(Marker marker) {
        RootNode routingPlan = (RootNode) marker.getTag();
        if (routingPlan != null) {
            txtName.setText(routingPlan.getWarehouseName());
            txtAddress.setText(routingPlan.getWarehouseAddress());
            txtPhone.setText(routingPlan.getWarehousePhone());
            if (routingPlan.getStatus()){
                txtStatus.setTextColor(AppController.getInstance().getResources().getColor(R.color.primaryColor));
                txtStatus.setText(AppController.getInstance().getText(R.string.COMPLETE));
            }
            else{
                txtStatus.setText(AppController.getInstance().getText(R.string.Unfinished));
            }
            iCallBack.callBack();
        }
    }

    private void initMarkerWarehouse(View view, Marker marker) {
        view.setLayoutParams(new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT));
        txtAddress = view.findViewById(R.id.txtAddress);
        txtPhone = view.findViewById(R.id.txtPhoneNumber);
        txtName = view.findViewById(R.id.txtName);
        txtStatus = view.findViewById(R.id.txtStatus);
        RootNode routingPlan = (RootNode) marker.getTag();
        Drawable icWarehouse;
        if(routingPlan.getStatus()){
            icWarehouse = context.getResources().getDrawable(R.drawable.ic_warehouse_complete);
        }else{
            icWarehouse = context.getResources().getDrawable(R.drawable.ic_warehouse);
        }
        txtName.setCompoundDrawablesWithIntrinsicBounds(icWarehouse, null, null, null);

        Drawable iconAddress = context.getResources().getDrawable(R.drawable.ic_location);
        txtAddress.setCompoundDrawablesWithIntrinsicBounds(iconAddress, null, null, null);

        Drawable iconPhone = context.getResources().getDrawable(R.drawable.icon_call_green);
        txtPhone.setCompoundDrawablesWithIntrinsicBounds(iconPhone, null, null, null);


        render(marker);

    }

//    private void initMarkerWarehouse(View view, Marker marker) {
//        view.setLayoutParams(new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT));
//        txtName = view.findViewById(R.id.txtName);
//        txtName.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
//        render(marker);
//
//    }

    private void initMarkerVans(View view) {
        view.setLayoutParams(new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT));
        TextView txtAddress = view.findViewById(R.id.txtAddress);
        txtName = view.findViewById(R.id.txtName);

        Drawable iconAddress = context.getResources().getDrawable(R.drawable.ic_location);
        txtAddress.setCompoundDrawablesWithIntrinsicBounds(iconAddress, null, null, null);

        Drawable iconVan = context.getResources().getDrawable(R.drawable.ic_vans_green);
        txtName.setCompoundDrawablesWithIntrinsicBounds(iconVan, null, null, null);

        txtAddress.setText(StaticData.getDriver().getVehicle().getParking_point().getAddress());
        txtName.setText(StaticData.getDriver().getVehicle().getName());

    }

    @Override
    public View getInfoContents(final Marker marker) {
        return null;
    }


}