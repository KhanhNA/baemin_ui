package com.ts.sharevandriver.ui.fragment;

import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;

import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.ts.sharevandriver.R;
import com.ts.sharevandriver.databinding.EditBillPackageBinding;
import com.ts.sharevandriver.model.BillPackage;
import com.ts.sharevandriver.utils.KeyboardUtil;
import com.ts.sharevandriver.utils.ToastUtils;
import com.ts.sharevandriver.viewmodel.EditBillPackageVM;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProviders;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class EditBillPackageFragment extends BottomSheetDialogFragment {
    private OnEditItem onEditItem;
    private BillPackage billPackage;
    private EditBillPackageBinding mBinding;
    private EditBillPackageVM viewModel;
    private String type;
    int originQuantity;
    public EditBillPackageFragment(BillPackage billPackage, int originQuantity, String type, OnEditItem onEditItem) {
        this.onEditItem = onEditItem;
        this.billPackage = (BillPackage) billPackage.clone();
        this.type = type;
        this.originQuantity = originQuantity;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(inflater, R.layout.edit_bill_package, container, false);
        viewModel = ViewModelProviders.of(this).get(EditBillPackageVM.class);
        mBinding.setViewModel(viewModel);
        viewModel.originQuantityImport = this.originQuantity;

        if (getDialog() != null && getDialog().getWindow() != null) {
            getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        }
        if (billPackage != null) {
            viewModel.billPackage.set(billPackage);
            viewModel.type = this.type;
        } else {
            ToastUtils.showToast(getString(R.string.INVALID_FIELD));
            dismiss();
        }

        DisplayMetrics displayMetrics = new DisplayMetrics();
        requireActivity().getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int height = displayMetrics.heightPixels;
        mBinding.itemRoot.setMinimumHeight(height);

        new KeyboardUtil(requireActivity(), mBinding.getRoot());
        mBinding.etQuantityImport.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (charSequence.length() > 0) {
                    int quantity = Integer.parseInt(charSequence.toString());
                    if (quantity > originQuantity) {
                        mBinding.btnAdd.setEnabled(false);
                    } else {
                        mBinding.btnAdd.setEnabled(true);
                    }
                } else {
                    mBinding.btnAdd.setEnabled(false);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });


        mBinding.btnAdd.setOnClickListener(v -> {
            if (viewModel.isValid()) {
                viewModel.billPackage.get().setQuantity_import(viewModel.billPackage.get().getQuantityQrChecked());
                onEditItem.onEditItem(viewModel.billPackage.get());
                dismiss();
            }
        });

        mBinding.btnDismiss.setOnClickListener(v -> dismiss());

        return mBinding.getRoot();

    }


    public interface OnEditItem {
        void onEditItem(BillPackage selected);
    }

}
