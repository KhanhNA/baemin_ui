package com.ts.sharevandriver.model;

import com.ns.odoolib_retrofit.utils.OdooDate;
import com.tsolution.base.BaseModel;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Vehicle extends BaseModel {
    private Integer id;
    private String name;

    private String image_logo;
    private String vehicle_image;
    private List<String> list_image;


    private String license_plate;
    private String color;
    private Integer model_year;
    private String fuel_type;
    private Double body_length;
    private Double body_width;
    private Double height;
    private Double capacity;
    private Double available_capacity;

    private String vehicle_type;
    private String fleet_type_name;
    private Float engine_size;//


    private OdooDate acquisition_date_;//ngày đăng ký
    private OdooDate inspection_due_date;//hạn đăng kiểm

    private String status;
    private ParkingPoint parking_point;

    private List<Equipment> equipments;
    private String uniqueid;
    private Boolean iot_type;
    private String vin_sn;

    private Integer sos_status;//tình trạng sos của xe

    public String getVehicle_image() {
        if (list_image == null || list_image.size() == 0) {
            return "";
        }
        return list_image.get(0);
    }


}
