package com.ts.sharevandriver.api;

import com.google.android.gms.maps.model.LatLng;
import com.google.gson.Gson;
import com.luck.picture.lib.entity.LocalMedia;
import com.ns.odoolib_retrofit.model.OdooResultDto;
import com.ts.sharevandriver.base.IResponse;
import com.ts.sharevandriver.base.SOService;
import com.ts.sharevandriver.base.StaticData;
import com.ts.sharevandriver.model.SOSNumber;
import com.ts.sharevandriver.model.SOSType;
import com.workable.errorhandler.ErrorHandler;

import org.json.JSONObject;

import java.io.File;
import java.util.List;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SOSApi extends BaseApi {

    public static void getSOSNumber(IResponse result) {
        JSONObject params;
        try {
            params = new JSONObject();

            mOdoo.callRoute("/sos/number", params, new SharingOdooResponse<OdooResultDto<SOSNumber>>() {

                @Override
                public void onSuccess(OdooResultDto<SOSNumber> resultDto) {
                    if(resultDto != null){
                        result.onSuccess(resultDto.getRecords());
                    }else {
                        result.onFail(new Throwable());
                    }
                }

                @Override
                public void onFail(Throwable error) {
                    result.onFail(error);

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
            ErrorHandler.create().handle(e);
        }
    }

    public static void getSOS(IResponse result) {
        JSONObject params;
        try {
            params = new JSONObject();

            mOdoo.callRoute("/sos/type", params, new SharingOdooResponse<OdooResultDto<SOSType>>() {
                @Override
                public void onSuccess(OdooResultDto<SOSType> obj) {
                    if(obj != null){
                        result.onSuccess(obj.getRecords());
                    }else {
                        result.onFail(new Throwable());
                    }
                }

                @Override
                public void onFail(Throwable error) {
                    result.onFail(error);

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
            ErrorHandler.create().handle(e);
        }
    }

    public static void updateSosStatus(IResponse result) {
        JSONObject params;
        try {
            params = new JSONObject();
            params.put("vehicle_id", StaticData.getDriver().getVehicle().getId());

            mOdoo.callRoute("/driver/update_vehicle_sos_status", params, new SharingOdooResponse<String>() {
                @Override
                public void onSuccess(String obj) {
                    if(obj != null){
                        result.onSuccess(obj);
                    }else {
                        result.onFail(new Throwable());
                    }
                }

                @Override
                public void onFail(Throwable error) {
                    result.onFail(error);

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
            ErrorHandler.create().handle(e);
        }
    }

    public static void createSOS(SOSType type, int routing_plan_id, List<LocalMedia> lstFileSelected,
                                 LatLng latLng, boolean continuable, int delayTime, String address, String orderNumber, IResponse result) {
        SOService soService = mOdoo.getClient().createService(SOService.class);
        MultipartBody.Part[] parts = null;

        String json = "{\"vehicle_id\":" + StaticData.getDriver().getVehicle().getId()
                + ", \"warning_type_id\":" + type.getId()
                + ", \"routing_plan_day_id\": " + routing_plan_id
                + ", \"latitude\": " + latLng.latitude
                + ", \"longitude\": " + latLng.longitude
                + ", \"note\":\"" + type.getNote() +"\""
                + ", \"continue_check\":" + continuable
                + ", \"address\":\"" + address + "\""
                + ",\"delay_time\":" + delayTime + "}";
        RequestBody bodyJson = RequestBody.create(MediaType.parse("application/json"), json);
        if (lstFileSelected != null) {
            parts = new MultipartBody.Part[lstFileSelected.size()];
            for (int i = 0; i < lstFileSelected.size(); i++) {
                File file = new File(lstFileSelected.get(i).getCompressPath());
                RequestBody fileReqBody = RequestBody.create(MediaType.parse("image/*"), file);
                MultipartBody.Part part = MultipartBody.Part.createFormData("files", file.getName(), fileReqBody);
                parts[i] = part;
            }
        }

        Call<ResponseBody> call = soService.createSOS(mOdoo.getSessionCokie()
                ,bodyJson, orderNumber, parts);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if(response.body() != null){
                    result.onSuccess(response.body());
                }else {
                    result.onFail(new Throwable());

                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                result.onFail(t);
            }
        });
    }
}
