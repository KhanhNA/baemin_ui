package com.nextsolution.db.api;


import com.luck.picture.lib.entity.LocalMedia;
import com.nextsolution.db.dto.AddressDTO;
import com.nextsolution.db.dto.CareerDTO;
import com.nextsolution.db.dto.Customer;
import com.nextsolution.db.dto.HistoryRatingPointDTO;
import com.nextsolution.db.dto.MyRankingCustomer;
import com.nextsolution.db.dto.Partner;
import com.nextsolution.vancustomer.base.AppController;
import com.nextsolution.vancustomer.base.StaticData;
import com.nextsolution.vancustomer.enums.CustomerType;
import com.nextsolution.vancustomer.network.SOService;
import com.ns.odoolib_retrofit.model.OdooResultDto;
import com.ns.odoolib_retrofit.wrapper.ODomain;
import com.workable.errorhandler.ErrorHandler;

import org.json.JSONObject;

import java.io.File;
import java.util.List;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CustomerApi extends BaseApi {
    public static String GET_INFO_CUSTOMER = "/share_van_order/get_customer_information";
    public static String UPDATE_INFO_CUSTOMER = "/share_van_order/edit_customer_information";
    public static String CHANGE_PASSWORD = "/server/change_password";
    public static String GET_RANKING_CUSTOMER = "/sharevan/get_rating_title_award";
    public static String GET_HISTORY_RATING_POINT = "/sharevan/get_history_rating_point";
    public static String GET_AREA = "/market_place/get_area";
    public static String GET_CAREER = "/company/get_career";

    public static void createAccount(Customer customer, List<LocalMedia> lstFileSelected, IResponse result) {
        SOService soService = mOdoo.getClient().createService(SOService.class);

        MultipartBody.Part[] parts = null;
        String phone = customer.getPhone().replace("+", "");
        String json = "{\"phone\":\"" + phone + "\""
                + ",\"name\":\"" + customer.getName() + "\""
                + ",\"birthday\":\"" + customer.getBirthday() + "\""
                + ",\"gender\":\"" + customer.getGender() + "\""
                + ",\"address\":\"" + customer.getAddress() + "\""
                + ",\"province_id\":" + customer.getProvince_id()
                + ",\"district_id\":" + customer.getDistrict_id()
                + " , \"career_id\":" + customer.getCareer_id() + "}";
        RequestBody bodyJson = RequestBody.create(MediaType.parse("application/json"), json);
        if (lstFileSelected != null) {
            parts = new MultipartBody.Part[lstFileSelected.size()];
            for (int i = 0; i < lstFileSelected.size(); i++) {
                File file = new File(lstFileSelected.get(i).getCompressPath());
                RequestBody fileReqBody = RequestBody.create(MediaType.parse("image/*"), file);
                MultipartBody.Part part = MultipartBody.Part.createFormData("files", file.getName(), fileReqBody);
                parts[i] = part;
            }
        }
        Call<ResponseBody> call = soService.createAccount(bodyJson, AppController.CLIENT_SECRET, parts);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                result.onSuccess(response);
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                result.onSuccess(null);
            }
        });
    }

    public static void editAvatar(LocalMedia avatar, IResponse result) {
        SOService soService = mOdoo.getClient().createService(SOService.class);
        File file = new File(avatar.getCompressPath());
        RequestBody fileReqBody = RequestBody.create(MediaType.parse("image/*"), file);
        MultipartBody.Part part = MultipartBody.Part.createFormData("files", file.getName(), fileReqBody);
        Call<ResponseBody> call = soService.editAvatar(mOdoo.getSessionCokie(),part);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                result.onSuccess(response.body());
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                result.onSuccess(null);
            }
        });
    }

    public static void getCareer(IResponse result) {
        JSONObject params = null;
        try {
            params = new JSONObject();

            mOdoo.callRoute(GET_CAREER, params, new SharingOdooResponse<OdooResultDto<CareerDTO>>() {
                @Override
                public void onSuccess(OdooResultDto<CareerDTO> resultDto) {
                    result.onSuccess(resultDto);
                }

                @Override
                public void onFail(Throwable error) {

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
            ErrorHandler.create().handle(e);
        }
    }

    public static void getArea(Integer parent_id, String location_type, IResponse result) {
        JSONObject params;
        try {
            params = new JSONObject();
            params.put("location_type", location_type);
            if (parent_id == null) {
                params.put("parent_id", "");
            } else {
                params.put("parent_id", parent_id);
            }

            mOdoo.callRoute(GET_AREA, params, new SharingOdooResponse<OdooResultDto<AddressDTO>>() {
                @Override
                public void onSuccess(OdooResultDto<AddressDTO> resultDto) {
                    result.onSuccess(resultDto);
                }

                @Override
                public void onFail(Throwable error) {

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
            ErrorHandler.create().handle(e);
        }
    }

    public static void getHistoryRatingPoint(int type, Integer page, IResponse result) {
        JSONObject params = null;
        try {
            params = new JSONObject();
            params.put("page", page);
            params.put("limit", 10);
            params.put("type", type);

            mOdoo.callRoute(GET_HISTORY_RATING_POINT, params, new SharingOdooResponse<OdooResultDto<HistoryRatingPointDTO>>() {
                @Override
                public void onSuccess(OdooResultDto<HistoryRatingPointDTO> resultDto) {
                    result.onSuccess(resultDto);
                }

                @Override
                public void onFail(Throwable error) {

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
            ErrorHandler.create().handle(e);
        }
    }

    public static void getRankingCustomer(IResponse result) {
        JSONObject params = null;
        try {
            params = new JSONObject();
            mOdoo.callRoute(GET_RANKING_CUSTOMER, params, new SharingOdooResponse<MyRankingCustomer>() {
                @Override
                public void onSuccess(MyRankingCustomer myRankingCustomer) {
                    if (myRankingCustomer != null) {
                        result.onSuccess(myRankingCustomer);
                    } else {
                        result.onSuccess(null);
                    }
                }

                @Override
                public void onFail(Throwable error) {

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
            ErrorHandler.create().handle(e);
        }
    }

    public static void updateInfoCustomer(Partner editInfo, IResponse result) {
        JSONObject params = null;
        try {
            params = new JSONObject();
            editInfo.setPhone(null);
            editInfo.setUri_path(null);
            editInfo.setDisplay_name(null);
            if(!CustomerType.FREE_CUSTOMER.equals(StaticData.getPartner().getCustomer_type())){
                editInfo.setEmail(null);
            }
            params.put("customerInfo", new JSONObject(mGson.toJson(editInfo)));
            mOdoo.callRoute(UPDATE_INFO_CUSTOMER, params, new SharingOdooResponse<OdooResultDto<Partner>>() {
                @Override
                public void onSuccess(OdooResultDto<Partner> resultDto) {
                    if (resultDto != null && resultDto.getRecords() != null) {
                        StaticData.setPartner(resultDto.getRecords().get(0));
                        result.onSuccess(true);
                    } else {
                        result.onSuccess(false);
                    }
                }

                @Override
                public void onFail(Throwable error) {

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
            ErrorHandler.create().handle(e);
        }
    }

    public static void changePassword(String oldPassword, String newPassword, IResponse result) {
        JSONObject params;
        try {
            params = new JSONObject();
            params.put("old_password", oldPassword);
            params.put("new_password", newPassword);
            mOdoo.callRoute(CHANGE_PASSWORD, params, new SharingOdooResponse<Boolean>() {
                @Override
                public void onSuccess(Boolean aBoolean) {
                    result.onSuccess(aBoolean);
                }

                @Override
                public void onFail(Throwable error) {

                }

            });
        } catch (Exception e) {
            e.printStackTrace();
            ErrorHandler.create().handle(e);
        }
    }

    public static void getCustomer(Integer partnerId, IResponse result) {
        try {

            ODomain domain = new ODomain();
            domain.add("id", "=", partnerId);

            mOdoo.searchRead("res.partner", null, domain, 0, 0, "", new SharingOdooResponse<OdooResultDto<Partner>>() {
                @Override
                public void onSuccess(OdooResultDto<Partner> o) {
                    result.onSuccess(o);
                    StaticData.sessionCookie = mOdoo.getSessionCokie();
                }

                @Override
                public void onFail(Throwable error) {

                }
            });


        } catch (Exception e) {
            e.printStackTrace();
        }


//        GetCustomerByUsernameQuery query = GetCustomerByUsernameQuery.builder().username(username).build();
//        AWSClientBase.apolloQuery(query, result, params);
    }

    public static void getInfoCustomer(IResponse result) {
        JSONObject params = null;
        try {

            params = new JSONObject();
            mOdoo.callRoute(GET_INFO_CUSTOMER, params, new SharingOdooResponse<OdooResultDto<Partner>>() {
                @Override
                public void onSuccess(OdooResultDto<Partner> partnerOdooResultDto) {
                    result.onSuccess(partnerOdooResultDto);
                    StaticData.sessionCookie = mOdoo.getSessionCokie();
                }

                @Override
                public void onFail(Throwable error) {

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
            ErrorHandler.create().handle(e);
        }
    }

    public static void updateUser(String userName, String fcmToken, IResponse result, Object... params) {
//        UpdateUserMutation userMutation = UpdateUserMutation.builder().username(userName).fcm_device_token(fcmToken).build();
//        AWSClientBase.apolloMutation(userMutation, result, params);
    }

}
