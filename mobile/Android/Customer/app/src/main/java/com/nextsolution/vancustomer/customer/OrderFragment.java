package com.nextsolution.vancustomer.customer;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.chad.library.adapter.base.viewholder.BaseDataBindingHolder;
import com.haibin.calendarview.Calendar;
import com.haibin.calendarview.CalendarView;
import com.nextsolution.db.dto.EnumUserRole;
import com.nextsolution.db.dto.RoutingDay;
import com.nextsolution.db.dto.Warehouse;
import com.nextsolution.vancustomer.BR;
import com.nextsolution.vancustomer.R;
import com.nextsolution.vancustomer.adapter.BaseAdapterV3;
import com.nextsolution.vancustomer.base.StaticData;
import com.nextsolution.vancustomer.confirm_change.ConfirmEditBillLadingActivity;
import com.nextsolution.vancustomer.customer.vm.OrderVM;
import com.nextsolution.vancustomer.databinding.FragmentOrderBinding;
import com.nextsolution.vancustomer.enums.TroubleType;
import com.nextsolution.vancustomer.network.NetworkManager;
import com.nextsolution.vancustomer.ui.MainActivity;
import com.nextsolution.vancustomer.enums.Constants;
import com.nextsolution.vancustomer.util.RoutingDayStatus;
import com.nextsolution.vancustomer.widget.CustomLoadMoreView;
import com.tsolution.base.BaseFragment;
import com.tsolution.base.BaseModel;
import com.tsolution.base.BaseViewModel;
import com.tsolution.base.CommonActivity;
import com.tsolution.base.listener.AdapterListener;


import java.util.Date;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.ViewDataBinding;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;


public class OrderFragment extends BaseFragment implements CalendarView.OnCalendarSelectListener,
        CalendarView.OnYearChangeListener, NetworkManager.NetworkHandler {

    MainActivity mActivity;

    Date date = new Date();
    StatusDialogFragment dialogFragment;
    ChooseWarehouseDialog chooseWarehouseDialog;
    private BaseAdapterV3<RoutingDay> adapterV3;
    private Integer positionStatus = 0;
    private Integer positionWarehouse = 0;
    private OrderVM orderVM;
    private Integer indexItem;
    FragmentOrderBinding mBinding;
    private int mYear;
    IReloadRoutingDay iReloadRoutingDay;

    boolean isOnline;
    private NetworkManager networkManager;

    final RecyclerView.OnScrollListener mScrollListener = new RecyclerView.OnScrollListener() {
        @Override
        public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
            if (mActivity == null) return;
            mActivity.animationFAB(getTag(), (0 == dy));
        }
    };

    public OrderFragment(MainActivity activity, IReloadRoutingDay iReloadRoutingDay) {
        this.mActivity = activity;
        this.iReloadRoutingDay = iReloadRoutingDay;
    }

    public void listenerFromMainActivity(String routing_plan_day_code) {
        int index = 0;
        for (RoutingDay item : orderVM.getListRoutingDay()) {
            if (item.getRouting_plan_day_code().equals(routing_plan_day_code)) {
                orderVM.getListRoutingDay().get(index).setStatus(RoutingDayStatus.COMPLETED);
                adapterV3.notifyItemChanged(index);
                index++;
                return;
            }
        }
    }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);
        orderVM = (OrderVM) viewModel;
        mBinding = (FragmentOrderBinding) binding;
        initToolBar();
        initView();
        initBillView();
        java.util.Calendar c = java.util.Calendar.getInstance();
        c.set(java.util.Calendar.DAY_OF_MONTH, c.getActualMaximum(java.util.Calendar.DAY_OF_MONTH));
        Date toDate = c.getTime();
        c.set(java.util.Calendar.MONTH, c.get(java.util.Calendar.MONTH) - 1);
        c.set(java.util.Calendar.DAY_OF_MONTH, 20);
        Date fromDate = c.getTime();
        orderVM.getListDate(fromDate, toDate, this::runUi);
        orderVM.getBillByDate(new Date(), false, this::runUi);
        initLoadMore();
        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        networkManager = new NetworkManager(getContext(), this);
        networkManager.start();
        isOnline = networkManager.isOnline();
    }

    @Override
    public void onStop() {
        super.onStop();
        networkManager.stop();
    }

    public void reFresh() {
        orderVM.getBillByDate(date, false, this::runUi);
    }

    @Override
    public void onResume() {
        super.onResume();
        adapterV3.notifyDataSetChanged();
    }

    public void chooseWarehouse() {
        FragmentManager fm = mActivity.getSupportFragmentManager();
        chooseWarehouseDialog = new ChooseWarehouseDialog(new AdapterListener() {
            @Override
            public void onItemClick(View view, Object o) {
                Warehouse warehouse = (Warehouse) o;
                positionWarehouse = warehouse.index - 1;
                if (positionWarehouse == 0) {
                    mBinding.txtWarehouse.setText(getString(R.string.ALL));
                    orderVM.setWarehouse_code(null);
                } else {
                    if (warehouse.getName().length() < 25) {
                        mBinding.txtWarehouse.setText(warehouse.getName());
                    } else {
                        mBinding.txtWarehouse.setText(warehouse.getName().toString().substring(0, 25) + "...");
                    }

                    orderVM.setWarehouse_code(warehouse.getWarehouse_code());
                }
                getBillByDate();
                chooseWarehouseDialog.dismiss();
            }

            @Override
            public void onItemLongClick(View view, Object o) {

            }
        });
        Bundle bundle = new Bundle();
        bundle.putInt(Constants.DATA, positionWarehouse);
        chooseWarehouseDialog.setArguments(bundle);
        chooseWarehouseDialog.setTargetFragment(this, Constants.CHOOSE_WAREHOUSE_DIALOG);
        chooseWarehouseDialog.show(fm, "ABC");
    }

    private void getBillByDate() {
        orderVM.getBillByDate(this.date, false, this::runUi);
    }

    public void chooseStatus() {
        FragmentManager fm = mActivity.getSupportFragmentManager();
        dialogFragment = new StatusDialogFragment(this.date, positionStatus, new StatusDialogFragment.IGetRadioChecked() {

            @Override
            public void getRadioCheck(int position, String txt) {
                handleStatusDialogResult(position, txt);
                dialogFragment.dismiss();
            }
        });
        dialogFragment.setTargetFragment(this, Constants.REQUEST_CODE);
        dialogFragment.show(fm, "ABC");
    }

    private void handleStatusDialogResult(int positionStatus, String strStatus) {
        this.positionStatus = positionStatus;
        if (positionStatus == 0) {
            orderVM.setStatus(null);
        } else {
            orderVM.setStatus((positionStatus - 1) + "");
        }
        mBinding.txtStatus.setText(strStatus);
        orderVM.getListRoutingDay().clear();
        orderVM.getBillByDate(this.date, false, this::runUi);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (requestCode == Constants.REQUEST_ACTIVITY_RESULT && resultCode == Constants.RESULT_OK) {
            resetData();
            orderVM.getBillByDate(date, false, this::runUi);
            iReloadRoutingDay.reloadRoutingDay();
        }
    }


    @SuppressLint("SetTextI18n")
    private void initToolBar() {
        mBinding.btnBack.setOnClickListener(v -> {
            mBinding.calendar.closeYearSelectLayout();
        });
        mBinding.tvMonthDay.setOnClickListener(v -> {
            if (!mBinding.calendarLayout.isExpand()) {
                mBinding.calendarLayout.expand();
                return;
            }
            mBinding.calendar.showYearSelectLayout(mYear);
            mBinding.tvYear.setVisibility(View.GONE);
            mBinding.tvMonthDay.setText(String.valueOf(mYear));
        });
        mBinding.flCurrent.setOnClickListener(v -> mBinding.calendar.scrollToCurrent());
        mBinding.tvYear.setText(String.valueOf(mBinding.calendar.getCurYear()));
        mYear = mBinding.calendar.getCurYear();
        String monthStr = mBinding.calendar.getCurMonth() + "";
        if (monthStr.length() < 2) {
            monthStr = "0" + monthStr;
        }
        String dayStr = mBinding.calendar.getCurDay() + "";
        if (dayStr.length() < 2) {
            dayStr = "0" + dayStr;
        }
        mBinding.tvMonthDay.setText(dayStr + " / " + monthStr);
        mBinding.tvCurrentDay.setText(String.valueOf(mBinding.calendar.getCurDay()));
    }


    public void scrollToCurrent() {
        mBinding.calendar.scrollToCurrent();
    }
//    public void

    private void runUi(Object[] params) {
        String action = (String) params[0];
        switch (action) {
            case "getBillByDate":
                adapterV3.getLoadMoreModule().loadMoreComplete();
                adapterV3.notifyDataSetChanged();
                orderVM.getEmptyData().set(false);
                orderVM.getIsLoading().set(false);
                break;
            case "getListDate":
                mBinding.calendar.update();
                break;
            case Constants.GET_DATA_FAIL:
                orderVM.getEmptyData().set(true);
                break;
            case "noMore":
                adapterV3.getLoadMoreModule().loadMoreEnd();
                break;
        }

    }

    private void initBillView() {
        adapterV3 = new BaseAdapterV3(R.layout.order_fragment_item, orderVM.getListRoutingDay(), orderVM, this);
        mBinding.rcBillByDay.setAdapter(adapterV3);
    }

    private void initLoadMore() {
        adapterV3.getLoadMoreModule().setOnLoadMoreListener(() -> {
            orderVM.loadMore(this.date, this::runUi);
        });
        adapterV3.getLoadMoreModule().setLoadMoreView(new CustomLoadMoreView());
        adapterV3.getLoadMoreModule().setAutoLoadMore(true);
    }


    @SuppressLint("SetTextI18n")
    private void initView() {
        mBinding.calendar.setOnCalendarSelectListener(this);
        mBinding.calendar.setOnYearChangeListener(this);
        mBinding.calendar.setSchemeDate(orderVM.getMScheme());
        mBinding.calendarLayout.setModeOnlyWeekView();
        mBinding.calendar.setSelectDefaultMode();
        mBinding.txtStatus.setText(getString(R.string.ALL));
        mBinding.txtWarehouse.setText(getString(R.string.ALL));
        mBinding.swRefresh.setOnRefreshListener(() -> {
            getBillByDate();
        });
        mBinding.calendar.setOnMonthChangeListener((year, month) -> {
            getListDateByMonth(month);

        });
        mBinding.calendar.setOnYearViewChangeListener(isClose -> {
            if (isClose) {
                mBinding.btnBack.setVisibility(View.GONE);
            } else {
                mBinding.btnBack.setVisibility(View.VISIBLE);
            }
        });
        mBinding.calendar.setOnViewChangeListener(isMonthView -> {
            if (isMonthView) {
                getListDateByMonth(mBinding.calendar.getCurMonth());
            }
        });

        mBinding.rcBillByDay.addOnScrollListener(mScrollListener);

        mBinding.tvMonthDay.setOnClickListener(v -> {
            mBinding.calendar.showYearSelectLayout(mYear);
            mBinding.tvYear.setVisibility(View.GONE);
            mBinding.tvMonthDay.setText(String.valueOf(mYear));
        });
//        mBinding.lbStatus.setEndIconOnClickListener(v -> {
//            positionStatus = 0;
//            orderVM.setStatus(null);
//            orderVM.getBillByDate(this.date, false, this::runUi);
//            mBinding.txtStatus.setText(getString(R.string.ALL));
//        });
//        mBinding.lbWareHouse.setEndIconOnClickListener(v -> {
//            positionWarehouse = 0;
//            mBinding.txtWarehouse.setText(getString(R.string.ALL));
//            orderVM.setWarehouse_code(null);
//            orderVM.getBillByDate(this.date, false, this::runUi);
//        });

    }

    private void getListDateByMonth(int month) {
        java.util.Calendar c = java.util.Calendar.getInstance();
        c.set(java.util.Calendar.DAY_OF_MONTH, 20);
        c.set(java.util.Calendar.MONTH, month - 2);
        Date fromDate = c.getTime();
        c.set(java.util.Calendar.DAY_OF_MONTH, 10);
        c.set(java.util.Calendar.MONTH, month);
        Date toDate = c.getTime();
        orderVM.getListDate(fromDate, toDate, OrderFragment.this::runUi);
    }

    @Override
    public void onItemClick(View v, Object o) {
        RoutingDay routingDay = (RoutingDay) o;
        indexItem = ((RoutingDay) o).index - 1;
        if (v.getId() == R.id.itemBillOfLadingDate) {
            Intent intent;
            Bundle bundle = new Bundle();
            if (routingDay.getStatus().equals(RoutingDayStatus.IN_CLAIM) && !TroubleType.WAITING_CONFIRM.equals(routingDay.getTrouble_type()) && EnumUserRole.CUSTOMER_MANAGER.equals(StaticData.getPartner().getStaff_type_name())) {
                intent = new Intent(getBaseActivity(), ConfirmEditBillLadingActivity.class);
                bundle.putString(Constants.ITEM_ID, routingDay.getChange_bill_lading_detail_id() + "");
            } else {
                intent = new Intent(getBaseActivity(), ListRoutingVanDayActivity.class);
                bundle.putString(Constants.ITEM_ID, routingDay.getRouting_plan_day_code());
            }
            intent.putExtras(bundle);
            startActivityForResult(intent, Constants.REQUEST_ACTIVITY_RESULT);
        } else if (v.getId() == R.id.showMap) {
            Intent intent = new Intent(getContext(), CommonActivity.class);
            Bundle bundle = new Bundle();
            bundle.putSerializable(Constants.ROUTING_CODE, routingDay.getRouting_plan_day_code());
            bundle.putSerializable(Constants.FRAGMENT, MapsFragment.class);
            bundle.putSerializable(Constants.FROM_FRAGMENT, "ORDER_FRAGMENT");
            intent.putExtras(bundle);
            startActivity(intent);
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        mBinding.rcBillByDay.removeOnScrollListener(mScrollListener);

    }

    @Override
    public int getLayoutRes() {
        return R.layout.fragment_order;
    }

    @Override
    public Class<? extends BaseViewModel> getVMClass() {
        return OrderVM.class;
    }

    @Override
    public int getRecycleResId() {
        return R.id.rcBillByDay;
    }


    @Override
    public void onCalendarOutOfRange(Calendar calendar) {

    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onCalendarSelect(Calendar calendar, boolean isClick) {
        mBinding.tvYear.setVisibility(View.VISIBLE);
        java.util.Calendar c = java.util.Calendar.getInstance();
        c.set(java.util.Calendar.DAY_OF_MONTH, c.get(java.util.Calendar.DAY_OF_MONTH) + 1);
        String monthStr = calendar.getMonth() + "";
        String dayStr = calendar.getDay() + "";
        if (monthStr.length() < 2) {
            monthStr = "0" + monthStr;
        }
        if (dayStr.length() < 2) {
            dayStr = "0" + dayStr;
        }
        mBinding.tvMonthDay.setText(dayStr + " / " + monthStr);
        mBinding.tvYear.setText(String.valueOf(calendar.getYear()));
        mYear = calendar.getYear();
        if (calendar.getTimeInMillis() < c.getTimeInMillis()) {
            if (isClick) {
                resetData();
                this.date.setTime(calendar.getTimeInMillis());
                if (date.getDate() != (new Date()).getDate()) {
                    orderVM.getIsConfirm().set(false);
                } else {
                    orderVM.getIsConfirm().set(true);
                }
                orderVM.getBillByDate(date, false, this::runUi);
            }
        } else {
            orderVM.getEmptyData().set(true);
            orderVM.getIsLoading().set(false);
        }
    }

    public void resetData() {
        positionStatus = 0;
        positionWarehouse = 0;
        orderVM.setStatus(null);
        mBinding.txtStatus.setText(getString(R.string.ALL));
        mBinding.txtWarehouse.setText(getString(R.string.ALL));
        orderVM.setWarehouse_code(null);
    }

    @Override
    public void onYearChange(int year) {
        mBinding.tvMonthDay.setText(String.valueOf(year));

    }

    @Override
    public void onNetworkUpdate(boolean isOnline) {
        this.isOnline = isOnline;
        if (!isOnline) {
            mBinding.progressLayout.setVisibility(View.VISIBLE);
            mBinding.progressLayout.setOnClickListener(v -> {
                Log.d("progress_click", "click");
            });
        } else {
            mBinding.progressLayout.setVisibility(View.GONE);
        }
    }

    public interface IReloadRoutingDay {
        void reloadRoutingDay();
    }
}