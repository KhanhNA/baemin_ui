package com.nextsolution.db.dto;


import com.tsolution.base.BaseModel;

import androidx.annotation.Nullable;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BillService extends BaseModel {
    private Integer id;
    private String name;
    private String display_name;
    private String service_code;
    private String type;
    private Double price;
    private String description;
    private String status;
    private String partner_name;


    @Override
    public boolean equals(@Nullable Object obj) {
        if (obj == null)
            return false;
        return this.id.equals(((BillService) obj).id);
    }
}
