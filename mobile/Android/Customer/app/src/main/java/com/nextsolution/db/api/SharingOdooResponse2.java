package com.nextsolution.db.api;

import com.android.volley.VolleyError;

import com.ns.odoolib_retrofit.listener.IOdooResponse;
import com.workable.errorhandler.ErrorHandler;

public abstract class SharingOdooResponse2<T> implements IResponse<T>{

    public IOdooResponse getResponse(Class<T> clazz){
        return new IOdooResponse<T>() {
            @Override
            public void onResponse(T o, Throwable volleyError)  {
                if(volleyError != null){
                    volleyError.printStackTrace();
                    ErrorHandler.create().handle(volleyError);
                    return;
                }
                onSuccess(o);
            }
        };
    }

}
