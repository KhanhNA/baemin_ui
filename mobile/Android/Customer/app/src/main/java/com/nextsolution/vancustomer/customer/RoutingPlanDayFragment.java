package com.nextsolution.vancustomer.customer;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.ObservableField;

import com.king.zxing.Intents;
import com.nextsolution.vancustomer.R;
import com.nextsolution.vancustomer.adapter.XBaseAdapter;
import com.nextsolution.vancustomer.customer.vm.RoutingPlanDayVM;
import com.nextsolution.vancustomer.databinding.BillOfLadingDateFragmentBinding;
import com.nextsolution.vancustomer.enums.Constants;
import com.nextsolution.vancustomer.scan.QrScannerActivity;
import com.tsolution.base.BaseFragment;
import com.tsolution.base.BaseViewModel;

public class RoutingPlanDayFragment extends BaseFragment {
    private RoutingPlanDayVM routingPlanDayVM;
    private BillOfLadingDateFragmentBinding mBinding;
    private XBaseAdapter adapter;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        View view= super.onCreateView(inflater, container, savedInstanceState);
        routingPlanDayVM = (RoutingPlanDayVM) viewModel;
        mBinding = (BillOfLadingDateFragmentBinding) binding;
        mBinding.toolbar.setTitle(R.string.bill_of_lading_date);
        AppCompatActivity appCompatActivity = ((AppCompatActivity) getActivity());
        if (appCompatActivity.getSupportActionBar() != null) {
            appCompatActivity.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            appCompatActivity.getSupportActionBar().setDisplayShowHomeEnabled(true);
        }
        setUpRecycleview();

        return view;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            getBaseActivity().onBackPressed();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void runUi(Object[] objects) {
        String action = (String) objects[0];
        if (action.equals(Constants.GET_DATA_SUCCESS)) {
            adapter.notifyDataSetChanged();
        }

    }
    private void setUpRecycleview(){
        adapter = new XBaseAdapter(R.layout.order_fragment_item, routingPlanDayVM.getListRoutingDay(), this);
        mBinding.billOfLadingDate.setAdapter(adapter);
    }

    public void onClick(View v){
        if(v.getId() == R.id.toolbar){
            getBaseActivity().onBackPressed();
        }else if(v.getId() == R.id.search){
            routingPlanDayVM.getData(this::runUi);
        }else if(v.getId()==R.id.qrCode){
            try {
                Intent qrScan = new Intent(getContext(), QrScannerActivity.class);
                startActivityForResult(qrScan, Constants.REQUEST_CODE_SCAN);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (isScanCodeAndResultOk(requestCode, resultCode) && data != null && data.hasExtra(Intents.Scan.RESULT)) {
            routingPlanDayVM.setResultQrCode(new ObservableField<>(data.getStringExtra(Intents.Scan.RESULT)));
            Log.d("result_qr_code", routingPlanDayVM.getResultQrCode().get());
        }
        closeProcess();
    }
    private boolean isScanCodeAndResultOk(int requestCode, int resultCode) {
        return requestCode == Constants.REQUEST_CODE_SCAN && resultCode == Activity.RESULT_OK;
    }

    @Override
    public void onItemClick(View v, Object o) {
        if(v.getId()==R.id.itemBillOfLadingDate){
//            Intent intent = new Intent(getBaseActivity(), CommonActivity.class);
//            Bundle bundle= new Bundle();
//            bundle.putSerializable(Constants.MODEL,(ShareVanRoutingPlan)o);
//            bundle.putSerializable("FRAGMENT", ListRoutingVanDayFragment.class);
//            intent.putExtras(bundle);
//            startActivity(intent);
        }
    }

    @Override
    public int getLayoutRes() {
        return R.layout.bill_of_lading_date_fragment;
    }

    @Override
    public Class<? extends BaseViewModel> getVMClass() {
        return RoutingPlanDayVM.class;
    }

    @Override
    public int getRecycleResId() {
        return R.id.billOfLadingDate;
    }
}
