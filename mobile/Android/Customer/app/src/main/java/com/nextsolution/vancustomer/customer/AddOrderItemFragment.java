package com.nextsolution.vancustomer.customer;

import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;

import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.nextsolution.db.dto.BillPackage;
import com.nextsolution.db.dto.ProductType;
import com.nextsolution.vancustomer.R;
import com.nextsolution.vancustomer.adapter.XBaseAdapter;
import com.nextsolution.vancustomer.customer.vm.AddOrderItemVM;
import com.nextsolution.vancustomer.databinding.AddOrderItemBinding;
import com.nextsolution.vancustomer.util.KeyboardUtil;
import com.nextsolution.vancustomer.util.SetErrorEditText;
import com.nextsolution.vancustomer.util.StringUtils;
import com.nextsolution.vancustomer.util.ToastUtils;
import com.nextsolution.vancustomer.util.TsUtils;
import com.tsolution.base.BaseModel;
import com.tsolution.base.listener.AdapterListener;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.GridLayoutManager;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AddOrderItemFragment extends BottomSheetDialogFragment {
    protected AddOrderItemVM viewModel;
    private AddOrderItemBinding dialogBinding;
    private OnAddItem onAddItem;
    private BillPackage billPackage;
    private XBaseAdapter adapter;
    private List<ProductType> listProductType;
    private ProductType selectedType;
    //position category selected
    private int preCate = -1;
    private int quantityPackage = 0;
    boolean type_edit = false;

    public AddOrderItemFragment(Integer quantityPackage, BillPackage billPackage, ProductType selectedType, List<ProductType> listProductType, OnAddItem onAddItem) {
        this.onAddItem = onAddItem;
        this.selectedType = selectedType;
        this.listProductType = listProductType;
        if (billPackage != null) {
            type_edit = true;
            this.billPackage = (BillPackage) billPackage.clone();
        }
        if (quantityPackage != null) {
            this.quantityPackage = quantityPackage;
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        dialogBinding = DataBindingUtil.inflate(inflater, R.layout.add_order_item, container, false);
        viewModel = ViewModelProviders.of(this).get(AddOrderItemVM.class);
        dialogBinding.setViewModel(viewModel);
        if (getDialog() != null && getDialog().getWindow() != null) {
            getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        }
        if (billPackage != null) {
            viewModel.getBillPackage().set(billPackage);
            dialogBinding.btnAdd.setText(getString(R.string.update));
            dialogBinding.txtTitle.setText(getString(R.string.edit_item));
        }

        DisplayMetrics displayMetrics = new DisplayMetrics();
        requireActivity().getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int height = displayMetrics.heightPixels;
        dialogBinding.itemRoot.setMinimumHeight(height);

        new KeyboardUtil(requireActivity(), dialogBinding.getRoot());

        adapter = new XBaseAdapter(R.layout.item_cate_product, viewModel.getListProductType(), new AdapterListener() {
            @Override
            public void onItemClick(View view, Object o) {
                dialogBinding.lbItemType.setError(null);
                int position = ((BaseModel) o).index - 1;
                dialogBinding.rcItemType.smoothScrollToPosition(position);
                viewModel.getListProductType().get(position).checked = true;
                adapter.notifyItemChanged(position);
                if (preCate >= 0) {
                    viewModel.getListProductType().get(preCate).checked = false;
                    adapter.notifyItemChanged(preCate);
                }
                preCate = position;
            }

            @Override
            public void onItemLongClick(View view, Object o) {

            }
        });
        GridLayoutManager layoutManager =
                new GridLayoutManager(getContext(), 1, GridLayoutManager.HORIZONTAL, false);

        dialogBinding.rcItemType.setLayoutManager(layoutManager);
        dialogBinding.rcItemType.setAdapter(adapter);


        dialogBinding.btnAdd.setOnClickListener(v -> {
            if (isValid()) {
                if (this.selectedType == null || (quantityPackage == 1 && type_edit)) {
                    addBillPackage();
                } else {
                    viewModel.compareProductType(selectedType, this::runUi);
                }
            }
        });

        dialogBinding.btnDismiss.setOnClickListener(v -> dismiss());

        if (TsUtils.isNotNull(listProductType)) {
            for (ProductType productType : listProductType) {
                productType.checked = false;
            }
            viewModel.setListProductType(this.listProductType);
            runUi(new Object[]{"getListType"});
        } else {
            viewModel.getListType(this::runUi);
        }
        initView();
        return dialogBinding.getRoot();

    }

    public void initView() {
        SetErrorEditText.setInputTextHandle(dialogBinding.itemName, dialogBinding.lbItemName);
        SetErrorEditText.setInputTextHandle(dialogBinding.itemQuantity, dialogBinding.lbItemQuantity);
        SetErrorEditText.setInputTextHandle(dialogBinding.itemLong, dialogBinding.lbItemLong);
        SetErrorEditText.setInputTextHandle(dialogBinding.itemWeight, dialogBinding.lbWeight);
        SetErrorEditText.setInputTextHandle(dialogBinding.itemWidth, dialogBinding.lbWidth);
        SetErrorEditText.setInputTextHandle(dialogBinding.itemHeight, dialogBinding.lbHeight);
    }


    public boolean isValid() {
        boolean isValid = true;
        boolean isSelectedType = false;
        BillPackage model = viewModel.getBillPackage().get();
        if (model != null) {
            if (StringUtils.isNullOrEmpty(model.getItem_name())) {
                isValid = false;
                dialogBinding.itemName.setError(getResources().getString(R.string.INVALID_FIELD));
            }
            for (ProductType type : viewModel.getListProductType()) {
                if (type.checked != null && type.checked) {
                    viewModel.getBillPackage().get().setProduct_type_name(type.getName());
                    viewModel.getBillPackage().get().setProductType(type);
                    isSelectedType = true;
                    break;
                }
            }

            if (!isSelectedType) {
                isValid = false;
                viewModel.addError("itemType", R.string.INVALID_FIELD, true);
            } else {
                viewModel.clearErro("itemType");
            }

            if (model.getQuantity_package() == null || model.getQuantity_package() <= 0) {
                isValid = false;
                dialogBinding.itemQuantity.setError(getResources().getString(R.string.INVALID_FIELD));
            }


            if (model.getLength() == null || model.getLength() <= 0) {
                isValid = false;
                dialogBinding.itemLong.setError(getResources().getString(R.string.INVALID_FIELD));
            } else if (model.getLength() < 0.01) {
                isValid = false;
                dialogBinding.itemLong.setError(getResources().getString(R.string.GREATER_THAN_001M));
            }

            if (model.getWidth() == null || model.getWidth() <= 0) {
                isValid = false;
                dialogBinding.itemWidth.setError(getResources().getString(R.string.INVALID_FIELD));
            } else if (model.getWidth() < 0.01) {
                isValid = false;
                dialogBinding.itemWidth.setError(getResources().getString(R.string.GREATER_THAN_001M));
            }

            if (model.getHeight() == null || model.getHeight() <= 0) {
                isValid = false;
                dialogBinding.itemHeight.setError(getResources().getString(R.string.INVALID_FIELD));
            } else if (model.getHeight() < 0.01) {
                isValid = false;
                dialogBinding.itemHeight.setError(getResources().getString(R.string.GREATER_THAN_001M));
            }

            if (model.getWeight() == null || model.getWeight() <= 0) {
                isValid = false;
                dialogBinding.itemWeight.setError(getResources().getString(R.string.INVALID_FIELD));
            } else if (model.getWeight() < 0.1) {
                isValid = false;
                dialogBinding.itemWeight.setError(getResources().getString(R.string.GREATER_THAN_001KG));
            }
        }
        return isValid;
    }

    private void addBillPackage() {
        BillPackage billPackage = viewModel.getBillPackage().get();
        billPackage.setNet_weight(
                Math.max(billPackage.calculateNetWeight(billPackage.getLength(), billPackage.getWidth(), billPackage.getHeight()),
                        billPackage.getWeight()));
        onAddItem.getItemAdded(viewModel.getBillPackage().get());
        dismiss();
    }

    private void runUi(Object[] objects) {
        String action = (String) objects[0];
        switch (action) {
            case "getListTypeSuccess":
                this.listProductType.clear();
                this.listProductType.addAll(viewModel.getListProductType());//giữ lại vùng nhớ để lưu ở OrderVM.
            case "getListType":
                if (billPackage != null) {
                    for (int i = 0; i < viewModel.getListProductType().size(); i++) {
                        ProductType type = viewModel.getListProductType().get(i);
                        if (billPackage.getProductType().getId().equals(type.getId())) {
                            type.checked = true;
                            preCate = i;
                        }
                    }
                }
                adapter.notifyDataSetChanged(listProductType);
                break;
            case "getListTypeFail":
                ToastUtils.showToast(getString(R.string.can_not_get_product_types));
                break;
            case "compareSuccess":
                addBillPackage();
                break;
            case "compareFail":
                ToastUtils.showToast(getString(R.string.product_type_not_compatible));
                break;
        }
    }


    public interface OnAddItem {
        void getItemAdded(BillPackage selected);
    }

    @Override
    public void onDismiss(@NonNull DialogInterface dialog) {
        super.onDismiss(dialog);

    }
}
