package com.nextsolution.vancustomer.customer;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.DatePicker;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.ViewModel;
import androidx.recyclerview.widget.RecyclerView;

import com.nextsolution.db.dto.BillLading;
import com.nextsolution.vancustomer.R;
import com.nextsolution.vancustomer.adapter.XBaseAdapter;
import com.nextsolution.vancustomer.customer.vm.CustomerOrdersChildVM;
import com.nextsolution.vancustomer.databinding.CustomerOrdersChildFragmentBinding;
import com.nextsolution.vancustomer.enums.Constants;
import com.nextsolution.vancustomer.util.MyDateUtils;
import com.tsolution.base.BaseFragment;
import com.tsolution.base.BaseViewModel;
import com.tsolution.base.CommonActivity;

import java.util.Calendar;

public class CustomerOrdersChildFragment extends BaseFragment implements DatePickerDialog.OnDateSetListener {
    CustomerOrdersChildVM customerOrdersChildVM;


    private CustomerOrdersChildFragmentBinding mBinding;
    private boolean isFromDate = false;
    private RecyclerView mRecyclerView;
    private XBaseAdapter adapter;
    private String status;

    public CustomerOrdersChildFragment(String status) {
        this.status = status;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        View view = super.onCreateView(inflater, container, savedInstanceState);
        customerOrdersChildVM = (CustomerOrdersChildVM) viewModel;

        mBinding = (CustomerOrdersChildFragmentBinding) binding;
        mBinding.orderCodeSearch.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    getData();
                    return true;
                }
                return false;
            }
        });
        setUpRecycleView();
        init();
        customerOrdersChildVM.init(this::runUi);
        return view;
    }

    public void init() {
        customerOrdersChildVM.setStatus(this.status);
    }

    private void runUi(Object[] objects) {
        String action = (String) objects[0];
        if (action.equals(Constants.GET_DATA_SUCCESS)) {
            adapter.notifyDataSetChanged();
            customerOrdersChildVM.getEmptyData().set(false);
        } else if (action.equals(Constants.GET_DATA_FAIL)) {
            customerOrdersChildVM.getEmptyData().set(true);
        }
    }

    private void setUpRecycleView() {
        adapter = new XBaseAdapter(R.layout.bill_lading_history_item, customerOrdersChildVM.getListBillByDay(), this);
        mBinding.orderHistory.setAdapter(adapter);
    }

    @Override
    public void onItemClick(View v, Object o) {
        if (v.getId() == R.id.itemOrderHistory) {
            Intent intent = new Intent(getContext(), BillLadingInfoActivity.class);
            BillLading billLading = (BillLading) o;
            Bundle bundle = new Bundle();
            bundle.putSerializable(Constants.MODEL, billLading);
            intent.putExtras(bundle);
            startActivity(intent);
        }
    }

    public void getData() {
        customerOrdersChildVM.getData(this::runUi, status);
    }

    public void onClick(View v, ViewModel o) {
        if (v.getId() == R.id.toolbar) {
            getBaseActivity().onBackPressed();
        } else if (v.getId() == R.id.search) {
            getData();
        } else if (R.id.fDate == v.getId()) {
            isFromDate = true;
            showDialogDatePicker(null, null);
        } else if (R.id.tDate == v.getId()) {
            isFromDate = false;
            showDialogDatePicker(null, null);
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            getBaseActivity().onBackPressed();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void showDialogDatePicker(Long minDate, Long maxDate) {
        Context scene = getContext();
        if (scene != null) {
            Calendar c = Calendar.getInstance();
            Integer year = c.get(Calendar.YEAR);
            Integer month = c.get(Calendar.MONTH);
            Integer day = c.get(Calendar.DAY_OF_MONTH);
            DatePickerDialog datePickerDialog = new DatePickerDialog(scene, this, year, month, day);
            if (maxDate != null) {
                datePickerDialog.getDatePicker().setMaxDate(maxDate);
            }
            if (minDate != null) {
                datePickerDialog.getDatePicker().setMinDate(minDate);
            }
            datePickerDialog.show();
        }
    }


    @Override
    public int getLayoutRes() {
        return R.layout.customer_orders_child_fragment;
    }

    @Override
    public Class<? extends BaseViewModel> getVMClass() {
        return CustomerOrdersChildVM.class;
    }

    @Override
    public int getRecycleResId() {
        return R.id.order_history;
    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
        if (isFromDate) {
            customerOrdersChildVM.getFromDate().set(MyDateUtils.convertDateToString(MyDateUtils.convertDayMonthYearToDate(dayOfMonth, month, year), MyDateUtils.DATE_FORMAT_YYYY_MM_DD));
        } else {
            customerOrdersChildVM.getToDate().set(MyDateUtils.convertDateToString(MyDateUtils.convertDayMonthYearToDate(dayOfMonth, month, year), MyDateUtils.DATE_FORMAT_YYYY_MM_DD));
        }

    }
}
