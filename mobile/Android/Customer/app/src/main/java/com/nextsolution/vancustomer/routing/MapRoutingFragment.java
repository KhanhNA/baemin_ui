package com.nextsolution.vancustomer.routing;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.widget.PopupMenu;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.Marker;
import com.nextsolution.db.dto.RoutingVanDay;
import com.nextsolution.vancustomer.R;
import com.nextsolution.vancustomer.base.AppController;
import com.nextsolution.vancustomer.base.StaticData;
import com.nextsolution.vancustomer.routing.maps.DirectionView;
import com.nextsolution.vancustomer.routing.maps.RouteMarkers;
import com.nextsolution.vancustomer.ui.LoginActivityV2;
import com.nextsolution.vancustomer.ui.fragment.DialogConfirm;
import com.nextsolution.vancustomer.enums.Constants;
import com.tsolution.base.BaseFragment;
import com.tsolution.base.BaseViewModel;
import com.tsolution.base.CommonActivity;
import com.tsolution.base.listener.ActionsListener;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.io.IOException;
import java.util.Map;

public class MapRoutingFragment extends BaseFragment implements OnMapReadyCallback,
        ActionsListener, PopupMenu.OnMenuItemClickListener,
RouteBriefInfoInterface{


    private RoutingVM routingVM;

    private DirectionView directionView;
    private RouteMarkers routeMarkers;
    private GoogleMap map;
    private static final String TAG = "LocationActivity";
    private static final long INTERVAL = 1000 * 10;
    private static final long FASTEST_INTERVAL = 1000 * 5;


    private Marker markerGoogle;
    private Marker mSelectedMarker;
    private LinearLayout tv;
    @Override
    public void onDestroy() {
        super.onDestroy();
//        EventBus.getDefault().unregister(this);
    }


    public MapRoutingFragment() {
//        getMapAsync(this);

        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager()
                .findFragmentById(R.id.map);

        mapFragment.getMapAsync(this);
        routingVM = (RoutingVM) viewModel;

//         Find the toolbar view inside the activity layout
//        Toolbar toolbar = (Toolbar) binding.getRoot().findViewById(R.id.toolbar);
        // Sets the Toolbar to act as the ActionBar for this Activity window.
        // Make sure the toolbar exists in the activity and is not null
//        toolbar.inflateMenu(R.menu.main);
//        ((AppCompatActivity)getBaseActivity()).setSupportActionBar(toolbar);
//        setHasOptionsMenu(true);
        return binding.getRoot();
    }

//    @Override
//    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
//        inflater.inflate(R.menu.main, menu);
//    }

//    @Override
//    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
//        switch(item.getItemId()) {
//            case R.id.list:
//                viewAllRoutes();
//                return true;
//            case R.id.setting:
//
//                return true;
//            case R.id.logout:
//                showDialogLogout();
//                return true;
//            default:
//                return super.onOptionsItemSelected(item);
//        }
//    }

    private void showDialogLogout() {
        DialogConfirm dialogConfirm = new DialogConfirm(getString(R.string.message_logout), "",
                v->{
                    // Delete user & pass
                    AppController.getInstance().getSharePre().edit().putString(Constants.USER_NAME, "").apply();
                    AppController.getInstance().getSharePre().edit().putString(Constants.MK, "").apply();
                    // Delete cache merchant
                    AppController.getInstance().clearCache();
                    // Delete local db
                    // Intent loginActivity
                    Intent intent = new Intent(getBaseActivity(), LoginActivityV2.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);

                    //cập nhật lại số đơn hàng khi đăng nhập lại.
                    startActivity(intent);
                });
        dialogConfirm.show(getChildFragmentManager(), dialogConfirm.getTag());
    }

    @Override
    public void onMapReady(final GoogleMap gmap) {
        this.map = gmap;
        boolean success = map.setMapStyle(
                MapStyleOptions.loadRawResourceStyle(
                        this.getBaseActivity(), R.raw.style_json));

        map.getUiSettings().setMapToolbarEnabled(false);
        // reder button default recent location
        map.setMyLocationEnabled(false);


        routingVM.getRoutingPlanDay("27/04/2020", this::runUi);
        routingVM.fabOkVisible.set(false);

    }

    public Animation getAnimation(){
        Animation shake = AnimationUtils.loadAnimation(getBaseActivity(), R.anim.shake);
        shake.setRepeatCount(Animation.INFINITE);
        return shake;
    }



    public void runUi(Object... param) {
        getActivity().runOnUiThread(new Runnable() {
            public void run() {
                try {
                    direct();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });

    }

    public void direct() throws IOException {
        directionView = new DirectionView(this.getBaseActivity(), routingVM, map);
        routeMarkers = new RouteMarkers(this.getBaseActivity(), routingVM, map);

//        Van van = StaticData.getVan();
//        if (van == null) {
//            return;
//        }
        routeMarkers.addRouteMarkerForCustomer();
        RoutingVanDay aRoute = StaticData.getCurrentRouteProcessed();
        if(aRoute != null && aRoute.getVan() != null) {
            map.animateCamera(CameraUpdateFactory.newCameraPosition(new CameraPosition.Builder().target(aRoute.getVan().getLatLng())
                    .zoom(16).build()));
        }


    }



    @Override
    public int getLayoutRes() {
        return R.layout.fragment_map;
    }

    @Override
    public Class<? extends BaseViewModel> getVMClass() {
        return RoutingVM.class;
    }

    @Override
    public int getRecycleResId() {
        return 0;
    }


    @Subscribe
    public void onEvent(Object obj) {
        Map routing = (Map<String, String>) obj;
        System.out.println("aaaaaaaa");
        String id = routing.get("id").toString();
        routingVM.getRoutingPlanDayById(Integer.parseInt(id), this::updateNewRouting);
//        RoutingApi.getRoutingPlanDayById();
    }

    private void updateNewRouting(Object... objects) {
        RoutingVanDay aRoute = (RoutingVanDay) objects[0];
        try {
            routeMarkers.addMarker(aRoute);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void showMenu(View v) {
        PopupMenu popup = new PopupMenu(getBaseActivity(), v);
        popup.setOnMenuItemClickListener(this);// to implement on click event on items of menu
        MenuInflater inflater = popup.getMenuInflater();
        inflater.inflate(R.menu.main, popup.getMenu());
        popup.show();
    }



    @Override
    public void action(Object... objects) {

    }

    public void viewAllRoutes(Object... objects) {
        System.out.println("tesssssssssssssssssst");
        Intent intent = new Intent(getBaseActivity(), CommonActivity.class);
        intent.putExtra("FRAGMENT", RoutingsFragment.class);
        startActivity(intent);

    }



//    public void registerNewRoute(Object... objects) {
//        if (mSelectedMarker != null) {
//            RoutingVanDay aRoute = (RoutingVanDay) mSelectedMarker.getTag();
//            routingVM.updateVanForNewRoute(aRoute.getId(), this::runUi);
//        }
//
//    }

    @Override
    public boolean onMenuItemClick(MenuItem item) {
        System.out.println("item: " + item.getTitle());
        return true;
    }

    @Override
    public void onItemChildClick() {

    }

    @Override
    public void onRegisterNewRoute() {


        if (mSelectedMarker != null) {
            RoutingVanDay aRoute = (RoutingVanDay) mSelectedMarker.getTag();
            routingVM.updateVanForNewRoute(aRoute.getId(), this::runUi);
        }

    }

    @Override
    public void onCancelNewRoute() {

    }

}