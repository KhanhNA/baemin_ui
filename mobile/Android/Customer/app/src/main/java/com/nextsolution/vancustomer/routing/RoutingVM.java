package com.nextsolution.vancustomer.routing;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.databinding.ObservableBoolean;
import androidx.databinding.ObservableField;

import com.google.android.gms.maps.model.LatLng;
import com.nextsolution.db.api.RoutingApi;
import com.nextsolution.db.api.SharingOdooResponse;
import com.nextsolution.db.dto.RoutingVanDay;
import com.nextsolution.db.dto.ShipmentStandStill;
import com.nextsolution.vancustomer.base.RunUi;
import com.nextsolution.vancustomer.base.StaticData;
import com.tsolution.base.BaseViewModel;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

/**
 */
@Getter
@Setter
public class RoutingVM extends BaseViewModel {

    ObservableField<ShipmentStandStill> standStill;
    ObservableBoolean isRouting;

    ObservableBoolean fabOkVisible = new ObservableBoolean(false);
    ObservableBoolean fabNewVisible = new ObservableBoolean(true);
    ObservableField<String> timeRemain = new ObservableField<>();

    public RoutingVM(@NonNull Application application) {
        super(application);
        standStill = new ObservableField<>();
        isRouting = new ObservableBoolean(true);
    }
    public void getRoutingPlanDay(String fdate, RunUi runUi){

        RoutingApi.getRoutingPlanDayByCustomerId(StaticData.getPartner().getId(), fdate,
                new SharingOdooResponse() {
                    @Override
                    public void onSuccess(Object o) {
                        responseRouting(o,runUi);
                    }

                    @Override
                    public void onFail(Throwable error) {

                    }
                }
        );
    }



    public void getRoutingPlanDayById(Integer id, RunUi runUi){

        RoutingApi.getRoutingPlanDayById(id, new SharingOdooResponse() {
            @Override
            public void onSuccess(Object o) {
                responseRoutingById(o, runUi);
            }

            @Override
            public void onFail(Throwable error) {

            }
        });
    }
    public void updateVanForNewRoute(Integer routeId, RunUi runUi){
        Integer vanId = StaticData.getVanId();
        RoutingApi.updateVanForNewRoute(routeId, vanId, new SharingOdooResponse() {
            @Override
            public void onSuccess(Object o) {
                responseUpdateVanForNewRoute(routeId, runUi);
            }

            @Override
            public void onFail(Throwable error) {

            }
        }, runUi);
    }

    private void responseUpdateVanForNewRoute(Object response, RunUi runUi) {
        System.out.println("result" + response);
//        StaticData.clearRoutes();
//        StaticData.setRoutes( ((GetRoutingVanDayQuery.Data)response.data()).getRoutingVanDay());
//
//        runUi.run(response, throwable);
    }

    public void responseRouting(Object responses, Object... params) {
        System.out.println("result" + responses);
        StaticData.clearRoutes();

        StaticData.setRoutesOfCustomer( (List<RoutingVanDay>)responses);
        standStill.set(StaticData.getCurrentRouteProcessed());
//        RunUi runUi = (RunUi)params[0];
//        runUi.run(lst, var4);


    }

    public void responseRoutingById(Object obj, Object... params) {
        System.out.println("result" + obj);
//        StaticData.clearRoutingVanDayAdded();
        RoutingVanDay aRoute = (RoutingVanDay) obj;
//        StaticData.addNewRoute( aRoute);

        RunUi runUi = (RunUi)params[0];
        runUi.run(aRoute);
    }


    public LatLng getLatLng(int index){
        RoutingVanDay org = StaticData.getRoutingVanDays().get(index);
//        if (org.getLatitude() != null && org.getLongitude() != null) {
            LatLng orgLatLng = new LatLng(org.getLatitude(), org.getLongitude());
            return orgLatLng;
    }





}
