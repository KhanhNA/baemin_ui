package com.nextsolution.vancustomer.customer;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.nextsolution.vancustomer.R;
import com.nextsolution.vancustomer.adapter.BaseAdapterV3;
import com.nextsolution.vancustomer.customer.vm.HistoryPointChildVM;
import com.nextsolution.vancustomer.databinding.HistoryPointChildFragmentBinding;
import com.nextsolution.vancustomer.widget.CustomLoadMoreView;
import com.tsolution.base.BaseFragment;
import com.tsolution.base.BaseViewModel;

import static com.nextsolution.vancustomer.enums.Constants.GET_DATA_SUCCESS;

public class HistoryPointChildFragment extends BaseFragment {
    BaseAdapterV3 adapter;
    HistoryPointChildFragmentBinding mBinding;
    HistoryPointChildVM historyPointChildVM;
    int type;

    public HistoryPointChildFragment(int type) {
        this.type = type;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view= super.onCreateView(inflater, container, savedInstanceState);
        mBinding = (HistoryPointChildFragmentBinding) binding;
        historyPointChildVM = (HistoryPointChildVM) viewModel;
        setUpRecycleView();
        initView();
        getData();
        initLoadMore();
        return view;
    }
    private void initView(){
        mBinding.swRefresh.setOnRefreshListener(() -> {
            onRefresh();
        });
    }
    public void onRefresh(){
        historyPointChildVM.getIsRefresh().set(true);
        historyPointChildVM.getData(type,false,this::runUi);
    }
    private void setUpRecycleView(){
        adapter = new BaseAdapterV3(R.layout.history_point_child_item,historyPointChildVM.getHistoryRatingPointDTOS(),this);
        mBinding.rcContent.setAdapter(adapter);
    }
    private void getData(){

        historyPointChildVM.getData(type,false,this::runUi);
    }
    private void runUi(Object[] objects) {
        String action = (String) objects[0];
        switch (action){
            case GET_DATA_SUCCESS :
                adapter.getLoadMoreModule().loadMoreComplete();
                adapter.notifyDataSetChanged();
                historyPointChildVM.getEmptyData().set(false);
                break;
            case "noMore":
                adapter.getLoadMoreModule().loadMoreEnd();
                adapter.notifyDataSetChanged();
                historyPointChildVM.getEmptyData().set(false);
                break;
        }
    }
    private void initLoadMore() {
        adapter.getLoadMoreModule().setOnLoadMoreListener(() -> {
            historyPointChildVM.loadMore(type,this::runUi);
        });
        adapter.getLoadMoreModule().setLoadMoreView(new CustomLoadMoreView());
        adapter.getLoadMoreModule().setAutoLoadMore(true);
    }

    @Override
    public int getLayoutRes() {
        return R.layout.history_point_child_fragment;
    }

    @Override
    public Class<? extends BaseViewModel> getVMClass() {
        return HistoryPointChildVM.class;
    }

    @Override
    public int getRecycleResId() {
        return R.id.rcContent;
    }
}
