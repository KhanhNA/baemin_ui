package com.nextsolution.vancustomer.customer;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.DialogFragment;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProviders;

import com.nextsolution.vancustomer.R;
import com.nextsolution.vancustomer.customer.vm.ChangePasswordVM;
import com.nextsolution.vancustomer.databinding.ChangePasswordDialogBinding;
import com.nextsolution.vancustomer.enums.Constants;
import com.nextsolution.vancustomer.model.ChangingPassword;

import static com.nextsolution.vancustomer.util.StringUtils.isNullOrEmpty;


public class ChangePasswordDialog extends DialogFragment {

    ChangePasswordVM changePasswordVM;
    ChangePasswordDialogBinding binding;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.change_password_dialog, container, false);
        changePasswordVM = ViewModelProviders.of(this).get(ChangePasswordVM.class);
        binding.setViewHoler2(changePasswordVM);
        changePasswordVM.setContext(getContext());
        if (getDialog() != null && getDialog().getWindow() != null) {
            getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        }

        binding.btnCancel.setOnClickListener(v -> dismiss());
        binding.btnDismiss.setOnClickListener(v -> dismiss());

        binding.btnConfirm.setOnClickListener(v -> {
            if (isValid())
                changePasswordVM.changePassword(this::runUi);
        });


        initView();

        return binding.getRoot();
    }

    private void initView() {
        binding.oldPass.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() > 0) {
                    binding.txtOldPassword.setError(null);
                }
            }
        });
        binding.newPass.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() > 0) {
                    binding.txtNewPassword.setError(null);
                }
            }
        });
        binding.newConfirmPass.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() > 0) {
                    binding.txtNewConfirmPassword.setError(null);
                }
            }
        });
    }

    private void runUi(Object[] objects) {
        String action = (String) objects[0];
        if (action.equals(Constants.SUCCESS)) {
            getResult();
            dismiss();
        }
        else if (action.equals(Constants.FAIL)) {
            binding.txtOldPassword.setError(getString(R.string.INCORRECT_CURRENT_PASSWORD));
        }
    }

    private boolean isValid() {
        ChangingPassword changingPassword = changePasswordVM.getChangingPassword().get();
        boolean isValid = true;
        if (changingPassword != null) {
            if (isNullOrEmpty(changingPassword.getOldPassword())) {
                isValid = false;
                binding.txtOldPassword.setError(getString(R.string.please_enter_password));
            }
            if (isNullOrEmpty(changingPassword.getNewPassword())) {
                isValid = false;
                binding.txtNewPassword.setError(getString(R.string.please_enter_new_password));
            } else if (changingPassword.getNewPassword().length() < 6) {
                isValid = false;
                binding.txtNewPassword.setError(getString(R.string.PASSWORD_TOO_SHORT));
            } else if (changingPassword.getOldPassword().equals(changingPassword.getNewPassword())) {
                isValid = false;
                binding.txtNewPassword.setError(getString(R.string.SAME_PASSWORD));
            }
            if (isNullOrEmpty(changingPassword.getNewConfirmPassword())) {
                isValid = false;
                binding.txtNewConfirmPassword.setError(getString(R.string.please_enter_new_confirm_password));
            } else if (!changingPassword.getNewPassword().equals(changingPassword.getNewConfirmPassword())) {
                isValid = false;
                binding.txtNewConfirmPassword.setError(getString(R.string.incorrect_password));
            }
        }
        return isValid;
    }


    public void getResult() {
        Intent intent = new Intent();
        intent.putExtra(Constants.SUCCESS, Constants.SUCCESS);
        getTargetFragment().onActivityResult(
                getTargetRequestCode(), Constants.RESULT_OK, intent);
        this.dismiss();
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        return new Dialog(requireContext(), R.style.WideDialog);
    }

}
