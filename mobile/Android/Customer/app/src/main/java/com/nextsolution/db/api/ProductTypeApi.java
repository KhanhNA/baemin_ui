package com.nextsolution.db.api;

import com.nextsolution.db.dto.ProductType;
import com.nextsolution.vancustomer.enums.StatusType;
import com.ns.odoolib_retrofit.model.OdooResultDto;
import com.workable.errorhandler.ErrorHandler;

import org.json.JSONObject;

public class ProductTypeApi extends BaseApi {
    private static final String ROUTE_LIST_DATE = "/share_van_order/bill/list_date/";
    private static final String ROUTE_LIST_ACTIVE = "/share_van_order/product_type/list_active/";

    public static void getProductType(IResponse result) {
        JSONObject params;
        String status_service = StatusType.RUNNING;
        try {
            params = new JSONObject();
            params.put("status", status_service);
            mOdoo.callRoute(ROUTE_LIST_ACTIVE, params, new SharingOdooResponse<OdooResultDto<ProductType>>() {
                @Override
                public void onSuccess(OdooResultDto<ProductType> productTypes) {
                    result.onSuccess(productTypes);
                }

                @Override
                public void onFail(Throwable error) {

                }
            });
        } catch (Exception e) {
            result.onSuccess(null);
            e.printStackTrace();
            ErrorHandler.create().handle(e);
        }
    }

    public static void checkProductType(ProductType selectedType, ProductType currentType, IResponse result) {
        JSONObject params;
        try {
            params = new JSONObject();
            params.put("currentProductType", selectedType.getName_seq());
            params.put("newProductType", currentType.getName_seq());
            mOdoo.callRoute("/sharevan/check_product_type", params, new SharingOdooResponse<Boolean>() {
                @Override
                public void onSuccess(Boolean productTypes) {
                    result.onSuccess(productTypes);
                }

                @Override
                public void onFail(Throwable error) {

                }
            });
        } catch (Exception e) {
            result.onSuccess(false);
            e.printStackTrace();
            ErrorHandler.create().handle(e);
        }
    }

}
