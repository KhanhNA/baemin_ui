package com.nextsolution.vancustomer.customer.vm;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.databinding.ObservableArrayList;
import androidx.databinding.ObservableBoolean;
import androidx.databinding.ObservableList;

import com.nextsolution.db.api.CustomerApi;
import com.nextsolution.db.api.SharingOdooResponse;
import com.nextsolution.db.dto.HistoryRatingPointDTO;
import com.nextsolution.vancustomer.base.RunUi;
import com.nextsolution.vancustomer.enums.Constants;
import com.ns.odoolib_retrofit.model.OdooResultDto;
import com.tsolution.base.BaseViewModel;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class HistoryPointChildVM extends BaseViewModel {
    int page=0;
    int totalRecord=0;
    ObservableBoolean emptyData= new ObservableBoolean();
    ObservableBoolean isRefresh= new ObservableBoolean();
    ObservableList<HistoryRatingPointDTO> historyRatingPointDTOS = new ObservableArrayList<>();
    public HistoryPointChildVM(@NonNull Application application) {
        super(application);
        isRefresh.set(false);
        emptyData.set(true);
    }

    //Handle data before call api
    public void getData(int type,Boolean loadMore,RunUi runUi){
        if(!loadMore){
            historyRatingPointDTOS.clear();
            page=0;
            totalRecord=0;
        }
        CustomerApi.getHistoryRatingPoint(type,page, new SharingOdooResponse() {
            @Override
            public void onSuccess(Object o) {
                isRefresh.set(false);
                getDataSuccess(o,runUi);
            }

            @Override
            public void onFail(Throwable error) {

            }
        });
    }
    private void getDataSuccess(Object o,RunUi runUi){
        OdooResultDto<HistoryRatingPointDTO> resultDto = (OdooResultDto<HistoryRatingPointDTO>) o;
        if(resultDto!=null && resultDto.getRecords() !=null &&  resultDto.getRecords().size()>0){
            totalRecord = resultDto.getTotal_record();
            historyRatingPointDTOS.addAll(resultDto.getRecords());
            runUi.run(Constants.GET_DATA_SUCCESS);
        }else{
            runUi.run(Constants.GET_DATA_FAIL);
        }
    }
    public void loadMore(int type,RunUi runUi){
        page += 1;
        if (page*10<totalRecord) {
            getData(type,true,runUi);
        } else {
            runUi.run("noMore");
        }
    }
}
