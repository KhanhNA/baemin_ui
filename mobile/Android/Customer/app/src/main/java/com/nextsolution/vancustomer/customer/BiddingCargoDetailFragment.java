package com.nextsolution.vancustomer.customer;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.nextsolution.db.dto.BiddingVehicle;
import com.nextsolution.db.dto.CargoTypes;
import com.nextsolution.vancustomer.R;
import com.nextsolution.vancustomer.adapter.XBaseAdapter;
import com.nextsolution.vancustomer.customer.vm.BiddingCargoDetailVM;
import com.nextsolution.vancustomer.databinding.BiddingCargoDetailFragmentBinding;
import com.nextsolution.vancustomer.enums.Constants;
import com.nextsolution.vancustomer.util.ToastUtils;
import com.nextsolution.vancustomer.widget.SnapOnScrollListener;
import com.tsolution.base.BaseFragment;
import com.tsolution.base.BaseViewModel;
import com.tsolution.base.CommonActivity;
import com.tsolution.base.listener.AdapterListener;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.PagerSnapHelper;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.SnapHelper;

public class BiddingCargoDetailFragment extends BaseFragment {
    BiddingCargoDetailVM biddingCargoDetailVM;
    BiddingCargoDetailFragmentBinding mBinding;
    XBaseAdapter adapterVehicle;
    XBaseAdapter adapterTitle;
    XBaseAdapter adapterContent;
    int bidding_order_id;
    int indexItemCLick;

    int prePosition = 0;// vị trí đang chọn của cargoType

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        View view = super.onCreateView(inflater, container, savedInstanceState);
        biddingCargoDetailVM = (BiddingCargoDetailVM) viewModel;
        mBinding = (BiddingCargoDetailFragmentBinding) binding;
        setUpRecycleView();
        initToolbar();
        getData();
        return view;
    }

    private void initToolbar() {
        getBaseActivity().setSupportActionBar(mBinding.toolbar);
        AppCompatActivity appCompatActivity = ((AppCompatActivity) getActivity());
        if (appCompatActivity.getSupportActionBar() != null) {
            appCompatActivity.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            appCompatActivity.getSupportActionBar().setDisplayShowHomeEnabled(true);
        }
    }

    private void setUpRecycleView() {
        adapterVehicle = new XBaseAdapter(R.layout.bidding_cargo_detail_item, biddingCargoDetailVM.getBiddingVehicles(), this);
        mBinding.rcListCargo.setAdapter(adapterVehicle);

        //========================================recycleView Title==================================================
        adapterTitle = new XBaseAdapter(R.layout.item_title_bidding_cargo_detail, biddingCargoDetailVM.getCargoTypes(), new AdapterListener() {
            @Override
            public void onItemClick(View view, Object o) {
                int position = ((CargoTypes) o).index - 1;

                mBinding.rcCargoType.smoothScrollToPosition(position);
                mBinding.rcCargoTypeContent.smoothScrollToPosition(position);

            }

            @Override
            public void onItemLongClick(View view, Object o) {
                onItemClick(view, o);
            }
        });
        mBinding.rcCargoType.setAdapter(adapterTitle);
        mBinding.rcCargoType.setLayoutManager(new LinearLayoutManager(getContext(), RecyclerView.HORIZONTAL, false));

        //=========================================recycleView Content===============================================
        adapterContent = new XBaseAdapter(R.layout.item_content_bidding_cargo_detail, biddingCargoDetailVM.getCargoTypes(), this);
        mBinding.rcCargoTypeContent.setAdapter(adapterContent);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext(), RecyclerView.HORIZONTAL, false);
        mBinding.rcCargoTypeContent.setLayoutManager(linearLayoutManager);
        SnapHelper helper = new PagerSnapHelper();
        helper.attachToRecyclerView(mBinding.rcCargoTypeContent);

        mBinding.rcCargoTypeContent.addOnScrollListener(new SnapOnScrollListener(helper, position -> {
            if (position != prePosition) {
                biddingCargoDetailVM.getCargoTypes().get(position).checked = true;
                biddingCargoDetailVM.getCargoTypes().get(prePosition).checked = false;
                adapterTitle.notifyItemChanged(position);
                adapterTitle.notifyItemChanged(prePosition);
                mBinding.rcCargoType.smoothScrollToPosition(position);
                prePosition = position;
            }
        }));

    }

    private void runUi(Object[] objects) {
        String action = (String) objects[0];
        switch (action) {
            case Constants.GET_DATA_SUCCESS:
                if (biddingCargoDetailVM.getCargoTypes().size() > 0) {
                    biddingCargoDetailVM.getCargoTypes().get(0).checked = true;
                }
                adapterVehicle.notifyDataSetChanged();
                adapterTitle.notifyDataSetChanged();
                adapterContent.notifyDataSetChanged();
                break;
            case Constants.GET_DATA_FAIL:
                ToastUtils.showToast(getActivity(), getString(R.string.some_thing_wrong), getResources().getDrawable(R.drawable.ic_warning));
                break;
        }
    }

    private void getData() {
        Intent intent = getActivity().getIntent();
        try {
            if (intent.hasExtra(Constants.DATA)) {
                bidding_order_id = intent.getIntExtra(Constants.DATA, -1);
                if (bidding_order_id != -1) {
                    biddingCargoDetailVM.getBiddingCargoDetail(bidding_order_id, this::runUi);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            getBaseActivity().onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onItemClick(View v, Object o) {
        if (v.getId() == R.id.item_bidding_vehicle) {
            BiddingVehicle biddingVehicle = (BiddingVehicle) o;
            indexItemCLick = ((BiddingVehicle) o).index - 1;
            Intent intent = new Intent(getActivity(), CommonActivity.class);
            Bundle bundle = new Bundle();
            bundle.putSerializable(Constants.MODEL, biddingVehicle);
            bundle.putSerializable(Constants.DATA, biddingCargoDetailVM.getBiddingOrder().get());
            bundle.putSerializable(Constants.FRAGMENT, BiddingCargoVehicleDetailFragment.class);
            intent.putExtras(bundle);
            startActivityForResult(intent, Constants.REQUEST_FRAGMENT_RESULT);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (requestCode == Constants.REQUEST_FRAGMENT_RESULT && resultCode == Constants.RESULT_OK) {
            biddingCargoDetailVM.getBiddingVehicles().get(indexItemCLick).getBidding_order_return().setStatus("2");
            if(checkComplete()){
                setDataResult();
                getActivity().onBackPressed();
            }else{
                adapterVehicle.notifyItemChanged(indexItemCLick);
            }
        }
    }

    private boolean checkComplete() {
        for (BiddingVehicle item : biddingCargoDetailVM.getBiddingVehicles()) {
            if(!"2".equals(item.getBidding_order_return().getStatus())){
                return false;
            }
        }
        return true;
    }
    private void setDataResult(){
        getActivity().setResult(Constants.RESULT_OK);
    }

    @Override
    public int getLayoutRes() {
        return R.layout.bidding_cargo_detail_fragment;
    }

    @Override
    public Class<? extends BaseViewModel> getVMClass() {
        return BiddingCargoDetailVM.class;
    }

    @Override
    public int getRecycleResId() {
        return R.id.rcListCargo;
    }
}
