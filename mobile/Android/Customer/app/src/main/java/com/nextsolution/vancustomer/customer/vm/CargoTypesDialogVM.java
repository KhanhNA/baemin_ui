package com.nextsolution.vancustomer.customer.vm;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.databinding.ObservableField;

import com.nextsolution.db.dto.CargoBillPackage;
import com.nextsolution.db.dto.CargoTypes;
import com.tsolution.base.BaseViewModel;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CargoTypesDialogVM extends BaseViewModel {
    ObservableField<CargoTypes> cargoTypes =  new ObservableField<>();
    ObservableField<String> qr_code= new ObservableField<>();
    public CargoTypesDialogVM(@NonNull Application application) {
        super(application);
    }
}
