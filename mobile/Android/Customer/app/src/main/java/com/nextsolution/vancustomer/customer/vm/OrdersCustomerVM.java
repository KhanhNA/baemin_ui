package com.nextsolution.vancustomer.customer.vm;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.databinding.ObservableArrayList;
import androidx.databinding.ObservableList;

import com.nextsolution.db.dto.BillPackage;
import com.nextsolution.db.dto.Warehouse;
import com.nextsolution.vancustomer.model.MySubscriber;
import com.tsolution.base.BaseViewModel;

public class OrdersCustomerVM extends BaseViewModel {
    private ObservableList<BillPackage> billPackageRoutingPlans = new ObservableArrayList<>();
    private Warehouse selectedWarehouse;
    private MySubscriber selectedSubscriber;

    public OrdersCustomerVM(@NonNull Application application) {
        super(application);
    }
}
