package com.nextsolution.vancustomer.routing;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.king.zxing.Intents;
import com.nextsolution.db.dto.BillPackageRoutingPlan;
import com.nextsolution.db.dto.RoutingVanDay;
import com.nextsolution.vancustomer.R;
import com.nextsolution.vancustomer.adapter.BaseAdapterV3;
import com.nextsolution.vancustomer.base.AppController;
import com.nextsolution.vancustomer.base.StaticData;
import com.nextsolution.vancustomer.scan.QRCodeScannerFragment;
import com.tsolution.base.BaseFragment;
import com.tsolution.base.BaseViewModel;
import com.tsolution.base.listener.ActionsListener;

public class RoutingDetailFragment extends BaseFragment implements ActionsListener {


    private RoutingDetailVM routingDetailVM;
    private BaseAdapterV3<BillPackageRoutingPlan> adapter;
    public static int MY_INTENT_REQUEST_CODE = 1;

    public RoutingDetailFragment() {
//        getMapAsync(this);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        routingDetailVM = (RoutingDetailVM) viewModel;
        adapter = new BaseAdapterV3(R.layout.routing_detail_item, routingDetailVM.billPackageRoutingPlanList, this);
        recyclerView.setAdapter(adapter);

//        adapter.addChildClickViewIds(R.id.itemRoot);
//        adapter.setOnItemChildClickListener(this);
        extractRoute();
        return binding.getRoot();

    }

    private void extractRoute() {
        if (getBaseActivity().getIntent() != null && getBaseActivity().getIntent().hasExtra(StaticData.ROUTE_DETAIL)) {
            RoutingVanDay aRoute = (RoutingVanDay) getBaseActivity().getIntent().getSerializableExtra(StaticData.ROUTE_DETAIL);
            routingDetailVM.getRoutingDetail(aRoute, this::afterFetchDetail);
        }
    }

    private void afterFetchDetail(Object... objects) {
        getActivity().runOnUiThread(new Runnable() {
            public void run() {
                try {
                    adapter.notifyDataSetChanged();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    @Override
    public int getLayoutRes() {
        return R.layout.fragment_route_detail;
    }

    @Override
    public Class<? extends BaseViewModel> getVMClass() {
        return RoutingDetailVM.class;
    }

    @Override
    public int getRecycleResId() {
        return R.id.packages;
    }


    @Override
    public void action(Object... objects) {

    }
    public void pickup() {
        routingDetailVM.pickup();

    }
    public void scan() {
        System.out.println("okkkkkkkkkkkkkkkkkk");
        AppController.getInstance().newActivity(this, QRCodeScannerFragment.class, true, MY_INTENT_REQUEST_CODE);


    }

    public void onItemChildClick(BillPackageRoutingPlan billPackageRoutingPlan) {
        System.out.println("okkkkkkkkkkkkkkkkkk");
        if(billPackageRoutingPlan == null){
            return;
        }
        routingDetailVM.putPackageScan(billPackageRoutingPlan);
        AppController.getInstance().newActivity(this, QRCodeScannerFragment.class, true, billPackageRoutingPlan.getId());


    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        if(intent == null){
            return;
        }
        routingDetailVM.updateBillPackage(intent.getStringExtra(Intents.Scan.RESULT));
    }
}