package com.nextsolution.vancustomer.customer.vm;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.databinding.ObservableArrayList;
import androidx.databinding.ObservableField;

import com.nextsolution.db.dto.BillLadingDetail;
import com.nextsolution.vancustomer.base.RunUi;
import com.tsolution.base.BaseViewModel;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BillLadingDetailVM extends BaseViewModel {
    ObservableField<BillLadingDetail> billLadingDetail;
    Integer position;
    ObservableArrayList<String> list_warehouse_name;
    public BillLadingDetailVM(@NonNull Application application) {
        super(application);
        billLadingDetail= new ObservableField<>();
        list_warehouse_name = new ObservableArrayList<>();
    }

}
