package com.nextsolution.vancustomer.model;


import com.nextsolution.db.dto.Driver;
import com.tsolution.base.BaseModel;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Vehicle extends BaseModel {
    private Integer id;
    private String name;
    private String license_plate;
    private String color;
    private Integer model_year;
    private String fuel_type;
    private Integer body_length;
    private Integer body_width;
    private Integer height;
    private String vehicle_type;
    private Float engine_size;//
    private Double latitude;
    private Double longitude;//
    private Driver driver;

    private Integer vehicle_status;

}
