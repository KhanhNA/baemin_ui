package com.nextsolution.db.api;

import android.content.Context;

import androidx.databinding.ObservableField;

import com.google.gson.Gson;
import com.nextsolution.db.dto.BillLading;
import com.nextsolution.db.dto.BillLadingDetail;
import com.nextsolution.db.dto.Driver;
import com.nextsolution.db.dto.DriverRating;
import com.nextsolution.db.dto.RatingBadges;
import com.nextsolution.db.dto.RoutingDay;
import com.nextsolution.db.dto.RoutingDetail;
import com.nextsolution.vancustomer.base.AppController;
import com.nextsolution.vancustomer.model.ApiResponseModel;
import com.nextsolution.vancustomer.network.SOBillingService;
import com.nextsolution.vancustomer.base.StaticData;
import com.nextsolution.vancustomer.enums.Constants;
import com.nextsolution.vancustomer.network.SOService;
import com.nextsolution.vancustomer.util.BillLadingHistoryStatus;
import com.nextsolution.vancustomer.util.MyDateUtils;
import com.ns.odoolib_retrofit.model.OdooResultDto;
import com.ns.odoolib_retrofit.utils.OdooDate;
import com.ns.odoolib_retrofit.wrapper.BaseClient;
import com.workable.errorhandler.ErrorHandler;

import org.json.JSONObject;

import java.io.File;
import java.util.Calendar;
import java.util.Date;
import java.util.List;


import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class OrderApi extends BaseApi {
    private static final String ROUTE_CREATE_ORDER = "/share_van_order/create_order/";
    private static final String ROUTE_LIST_DATE = "/share_van_order/routing_plan/list_date";
    private static final String ROUTE_LIST_BY_DATE = "/share_van_order/routing_plan_day_by_employeeid";
    private static final String ROUTE_LIST_ORDER_HISTORY = "/share_van_order/bill_lading_history_ad";
    private static final String ROUTE_BILL_LADING_DETAIL = "/share_van_order/get_bill_lading_details";
    private static final String ROUTE_SHARE_VAN_LIST_DAY = "/share_van_order/routing_plan/list_date";
    private static final String ROUTE_PLAN_DAY_DETAIL = "/share_van_order/routing_plan_day/detail";
    private static final String GET_DRIVER_LOACTION = "/routing/driver_location";
    private static final String ROUTE_DETAIL_ROUTING_PLAN_CUSTOMER = "/share_van_order/get_detail_routing_plan_customer";
    private static final String ROUTE_LIST_RATING_BADGES = "/share_van_order/get_list_rating_badges";
    private static final String ROUTE_DRIVING_RATING = "/share_van_order/driver_rating";
    private static final String GET_QUANTITY_TYPE_STATUS_ROUTING_DAY = "/share_van_order/get_total_resutl_routing_plan";

    public static void getQuantityTypeStatusRoutingDay(Date date, IResponse result) {
        JSONObject params;
        try {
            params = new JSONObject();
            params.put("date", MyDateUtils.convertDateToString(date, MyDateUtils.DATE_FORMAT_YYYY_MM_DD));

            mOdoo.callRoute(GET_QUANTITY_TYPE_STATUS_ROUTING_DAY, params, new SharingOdooResponse<OdooResultDto<Integer>>() {

                @Override
                public void onSuccess(OdooResultDto<Integer> integers) {
                    result.onSuccess(integers.getRecords());
                }

                @Override
                public void onFail(Throwable error) {

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
            ErrorHandler.create().handle(e);
        }
    }

    public static void drivingRating(DriverRating rating, IResponse result) {
        JSONObject params;
        try {
            params = new JSONObject();
            params.put("rating", new JSONObject(mGson.toJson(rating)));

            mOdoo.callRoute(ROUTE_DRIVING_RATING, params, new SharingOdooResponse<Driver>() {
                @Override
                public void onSuccess(Driver driver) {
                    if (driver != null && driver.getId() != null)
                        result.onSuccess(true);
                    else
                        result.onSuccess(false);
                }

                @Override
                public void onFail(Throwable error) {

                }

            });
        } catch (Exception e) {
            e.printStackTrace();
            ErrorHandler.create().handle(e);
        }
    }

    public static void createOrder(BillLading billLading, IResponse result) {
        JSONObject params;
        try {
            params = new JSONObject();
            params.put("billLading", new JSONObject(mGson.toJson(billLading)));

            mOdoo.callRoute(ROUTE_CREATE_ORDER, params, new SharingOdooResponse2<Double>() {
                @Override
                public void onSuccess(Double obj) {
                    result.onSuccess(obj);
                }

                @Override
                public void onFail(Throwable error) {

                }
            }.getResponse(Double.class));
        } catch (Exception e) {
            e.printStackTrace();
            ErrorHandler.create().handle(e);
        }
    }

    public static void updateOder(BillLading billLading, boolean isRouting, IResponse result) {
        JSONObject params;
        try {
            params = new JSONObject();
            params.put("billLading", new JSONObject(mGson.toJson(billLading)));
            params.put("routing", isRouting);
            mOdoo.callRoute("/share_van_order/update_order", params, new SharingOdooResponse<ApiResponseModel>() {
                @Override
                public void onSuccess(ApiResponseModel obj) {
                    result.onSuccess(obj);
                }

                @Override
                public void onFail(Throwable error) {

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
            ErrorHandler.create().handle(e);
        }
    }

    public static void cancelOrder(Integer originBillLadingDetailId, String reason, IResponse result) {
        SOService soService = mOdoo.getClient().createService(SOService.class);

        Call<ApiResponseModel> call = soService.cancelRoutingPlanDay(mOdoo.getSessionCokie(), originBillLadingDetailId, reason);
        call.enqueue(new Callback<ApiResponseModel>() {
            @Override
            public void onResponse(Call<ApiResponseModel> call, Response<ApiResponseModel> response) {
                result.onSuccess(response.body());
            }

            @Override
            public void onFailure(Call<ApiResponseModel> call, Throwable t) {
                result.onSuccess(null);
            }

        });
    }

    public static void updateNotificationAction(Integer notification_id) {

    }

    public static void getDriverLocation(Integer id, IResponse result) {
        JSONObject params;
        try {
            params = new JSONObject();
            params.put("routing_plan_day_id", id);
            mOdoo.callRoute(GET_DRIVER_LOACTION, params, new SharingOdooResponse<OdooResultDto<RoutingDay>>() {
                @Override
                public void onSuccess(OdooResultDto<RoutingDay> routingDayOdooResultDto) {
                    result.onSuccess(routingDayOdooResultDto);
                }

                @Override
                public void onFail(Throwable error) {

                }

            });
        } catch (Exception e) {
            e.printStackTrace();
            ErrorHandler.create().handle(e);
        }
    }

    public static void getListRatingBadges(IResponse result) {
        JSONObject params;
        try {
            params = new JSONObject();
            params.put("type", "DRIVER");
            mOdoo.callRoute(ROUTE_LIST_RATING_BADGES, params, new SharingOdooResponse<OdooResultDto<RatingBadges>>() {
                @Override
                public void onSuccess(OdooResultDto<RatingBadges> ratingBadgesOdooResultDto) {
                    result.onSuccess(ratingBadgesOdooResultDto);
                }

                @Override
                public void onFail(Throwable error) {

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
            ErrorHandler.create().handle(e);
        }
    }

    public static void getListDate(Date fromDate, Date toDate, Boolean type, IResponse result) {

        JSONObject params = null;
        try {
            OdooDate fDate = new OdooDate(fromDate.getTime());
            OdooDate tDate = new OdooDate(toDate.getTime());
            params = new JSONObject();
            params.put("from_date", MyDateUtils.convertDateToString(fDate, MyDateUtils.DATE_FORMAT_YYYY_MM_DD));
            params.put("to_date", MyDateUtils.convertDateToString(tDate, MyDateUtils.DATE_FORMAT_YYYY_MM_DD));
            params.put("type", type);

            mOdoo.callRoute(ROUTE_LIST_DATE, params, new SharingOdooResponse<OdooResultDto<OdooDate>>() {
                @Override
                public void onSuccess(OdooResultDto<OdooDate> dates) {
                    result.onSuccess(dates);
                }

                @Override
                public void onFail(Throwable error) {

                }


            });
        } catch (Exception e) {
            e.printStackTrace();
            ErrorHandler.create().handle(e);
        }


    }

    public static void getListByDate(Date date, String warehouse_code, String status, Integer offset, IResponse result) {

        JSONObject params = null;
        try {
            OdooDate fDate = new OdooDate(date.getTime());

            params = new JSONObject();
            params.put("date", MyDateUtils.convertDateToString(fDate, MyDateUtils.DATE_FORMAT_YYYY_MM_DD));
            params.put("warehouse_code", warehouse_code);
            params.put("status", status);
            params.put("offset", offset);
            params.put("limit", Constants.LIMIT);
            mOdoo.callRoute(ROUTE_LIST_BY_DATE, params, new SharingOdooResponse<OdooResultDto<RoutingDay>>() {


                @Override
                public void onSuccess(OdooResultDto<RoutingDay> odooResultDto) {
                    result.onSuccess(odooResultDto);
                }

                @Override
                public void onFail(Throwable error) {

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
            ErrorHandler.create().handle(e);
        }


    }

    public static void getListOrderHistory(Date fromDate, Date toDate, String orderCode, String status, Integer offset, IResponse result) {
        JSONObject params = null;
        try {
            params = new JSONObject();
            String bill_state = "running";
            Calendar c = Calendar.getInstance();
            c.setTime(toDate);
            c.add(Calendar.DATE, 1);
            toDate = c.getTime();
            String tDate = MyDateUtils.convertDateToString(toDate, MyDateUtils.DATE_FORMAT_YYYY_MM_DD);
            String fDate = MyDateUtils.convertDateToString(fromDate, MyDateUtils.DATE_FORMAT_YYYY_MM_DD);
            if (fDate.isEmpty()) {
                fDate = null;
            }
            if (tDate.isEmpty()) {
                tDate = null;
            }

            if (BillLadingHistoryStatus.deleted.toString().equals(status)) {
                bill_state = null;
            } else if (BillLadingHistoryStatus.all.toString().equals(status)) {
                bill_state = null;
                status = null;
            } else if (BillLadingHistoryStatus.finished.toString().equals(status)) {
                bill_state = "finished";
                status = "running";
            }

            params.put("start_date", fDate);
            params.put("end_date", tDate);
            params.put("company_id", StaticData.getOdooSessionDto().getCompany_id());
            params.put("status", status);
            params.put("bill_state", bill_state);
            params.put("offset", offset);
            params.put("limit", 10);
            if (orderCode != null && orderCode.isEmpty()) {
                orderCode = null;
            }
            params.put("text_search", orderCode);
            mOdoo.callRoute(ROUTE_LIST_ORDER_HISTORY, params, new SharingOdooResponse<OdooResultDto<BillLading>>() {
                @Override
                public void onSuccess(OdooResultDto<BillLading> billLadings) {
                    result.onSuccess(billLadings);
                }

                @Override
                public void onFail(Throwable error) {

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
            ErrorHandler.create().handle(e);
        }
    }

    public static void getListDate(Date fromDate, Date toDate, Integer uId, IResponse result) {
        JSONObject params;
        try {
            OdooDate fDate = new OdooDate(fromDate.getTime());
            OdooDate tDate = new OdooDate(toDate.getTime());
            params = new JSONObject();
            params.put("from_date", MyDateUtils.convertDateToString(fDate, MyDateUtils.DATE_FORMAT_YYYY_MM_DD));
            params.put("to_date", MyDateUtils.convertDateToString(tDate, MyDateUtils.DATE_FORMAT_YYYY_MM_DD));
            params.put("uID", StaticData.getOdooSessionDto().getUid());

            mOdoo.callRoute(ROUTE_SHARE_VAN_LIST_DAY, params, new SharingOdooResponse<OdooResultDto<RoutingDay>>() {


                @Override
                public void onSuccess(OdooResultDto<RoutingDay> odooResultDto) {
                    result.onSuccess(odooResultDto);
                }

                @Override
                public void onFail(Throwable error) {

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
            ErrorHandler.create().handle(e);
        }
    }

    public static void getRoutingDayPlanDetail(String routing_plan_day_code, IResponse result) {

        JSONObject params;
        try {
            params = new JSONObject();
            params.put(Constants.ROUTING_CODE, routing_plan_day_code);

            mOdoo.callRoute(ROUTE_PLAN_DAY_DETAIL, params, new SharingOdooResponse<OdooResultDto<RoutingDay>>() {

                public void onSuccess(OdooResultDto<RoutingDay> routingDaylOdooResultDto) {
                    result.onSuccess(routingDaylOdooResultDto);
                }

                @Override
                public void onFail(Throwable error) {

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
            ErrorHandler.create().handle(e);
        }


    }

    public static void getBillLadingDetail(Integer bill_ladding_id, IResponse result) {
        JSONObject params = null;
        try {

            params = new JSONObject();
            params.put("bill_ladding_id", mGson.toJson(bill_ladding_id));
            mOdoo.callRoute(ROUTE_BILL_LADING_DETAIL, params, new SharingOdooResponse<OdooResultDto<BillLading>>() {
                @Override
                public void onSuccess(OdooResultDto<BillLading> billLadingOdooResultDto) {
                    result.onSuccess(billLadingOdooResultDto);
                }

                @Override
                public void onFail(Throwable error) {
                    error.getStackTrace();
                }

            });
        } catch (Exception e) {
            e.printStackTrace();
            ErrorHandler.create().handle(e);
        }
    }

    public static void getDetailRoutingPlanCustomer(Integer routing_plan_day_detail_id, IResponse result) {
        JSONObject params = null;
        try {
            params = new JSONObject();
            params.put("routing_plan_day_detail_id", routing_plan_day_detail_id);

            mOdoo.callRoute(ROUTE_DETAIL_ROUTING_PLAN_CUSTOMER, params, new SharingOdooResponse<OdooResultDto<RoutingDetail>>() {


                @Override
                public void onSuccess(OdooResultDto<RoutingDetail> routingDetailResult) {
                    result.onSuccess(routingDetailResult);
                }

                @Override
                public void onFail(Throwable error) {

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
            ErrorHandler.create().handle(e);
        }
    }

    public static void getPrice(Context context, BillLading billLading, IResponse result) {
        BaseClient<SOBillingService> soService = new BaseClient(context, AppController.BILLING_URL, SOBillingService.class);
        String json = mGson.toJson(billLading);
        RequestBody body = RequestBody.create(MediaType.parse("application/json"), json);
        soService.getServices().priceCalculate(body,"Bearer "+StaticData.getOdooSessionDto().getAccess_token()).enqueue(new MyCallBack<>(result));
    }
}
