package com.nextsolution.vancustomer.customer.vm;

import android.app.Application;
import android.content.Context;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.databinding.ObservableBoolean;
import androidx.databinding.ObservableField;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.maps.model.LatLng;
import com.nextsolution.db.api.IResponse;
import com.nextsolution.db.api.OrderApi;
import com.nextsolution.db.api.OrderPackageApi;
import com.nextsolution.db.api.RoutingApi;
import com.nextsolution.db.api.SharingOdooResponse;
import com.nextsolution.db.api.WarehouseApi;
import com.nextsolution.db.dto.Area;
import com.nextsolution.db.dto.BillLading;
import com.nextsolution.db.dto.BillLadingDetail;
import com.nextsolution.db.dto.BillPackage;
import com.nextsolution.db.dto.DistanceDTO;
import com.nextsolution.db.dto.ProductType;
import com.nextsolution.db.dto.Warehouse;
import com.nextsolution.vancustomer.base.RunUi;
import com.nextsolution.vancustomer.base.StaticData;
import com.nextsolution.vancustomer.model.OrderPackage;

import com.ns.odoolib_retrofit.model.OdooResultDto;
import com.tsolution.base.BaseModel;
import com.tsolution.base.BaseViewModel;


import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class OrderVMV2 extends BaseViewModel {
    private ObservableField<BillLading> billLadingField;
    private HashMap<Integer, Warehouse> selectedWarehouses = new HashMap<>();//key : warehouse id
    private String fromWareHouseCode;
    private ObservableBoolean isLoading = new ObservableBoolean();
    private List<ProductType> listProductType;
    private List<BaseModel> orderPackages;
    private DistanceDTO distanceDTO;
    private ObservableBoolean isWa = new ObservableBoolean();

    public OrderVMV2(@NonNull Application application) {
        super(application);
        fromWareHouseCode = new Date().getTime() + "";
        listProductType = new ArrayList<>(0);
        orderPackages = new ArrayList<>(0);
        isWa.set(false);

    }

    public void initBillLading() {
        billLadingField = new ObservableField<>();
        BillLading temp;
        temp = new BillLading();
        if (StaticData.getPartner().getCompany_id() != null) {
            temp.setCompany_id(StaticData.getPartner().getCompany_id().getId());
        }
        temp.setArrBillLadingDetail(new ArrayList<>());
        temp.getArrBillLadingDetail().add(new BillLadingDetail());
        billLadingField.set(temp);

    }

    public void checkIsExistWareHouse(Warehouse warehouse, RunUi runUi) {
        isLoading.set(true);
        WarehouseApi.checkIsExistWarehouse(warehouse.getWjson_address(), new SharingOdooResponse() {
            @Override
            public void onSuccess(Object o) {
                List<Area> list = (List<Area>) o;
                if (list != null && list.size() > 0) {
                    warehouse.setAreaInfo(list.get(0));
                    runUi.run("checkWarehouseSuccess", warehouse);
                } else {
                    runUi.run("checkWarehouseFail");
                    isLoading.set(false);
                }
            }

            @Override
            public void onFail(Throwable error) {

            }
        });
    }

    public void getOrderPackage(RunUi runUi) {
        OrderPackageApi.getOrderPackages(new SharingOdooResponse() {
            @Override
            public void onSuccess(Object o) {
                if (o != null) {
                    orderPackages.clear();
                    orderPackages.addAll(((OdooResultDto<OrderPackage>) o).getRecords());
                    runUi.run("getOrderPackage");
                }
            }

            @Override
            public void onFail(Throwable error) {

            }
        });
    }

    public void createOrder(RunUi runUi) {
        isLoading.set(true);
        System.out.println("create order");
        BillLading billLading = billLadingField.get();
        billLading.setCompany_id(StaticData.getPartner().getCompany_id().getId());
        OrderApi.createOrder(billLadingField.get(), new SharingOdooResponse() {
            @Override
            public void onSuccess(Object o) {
                isLoading.set(false);
                if (o != null) {
                    runUi.run("createOrderSuccess");
                } else {
                    runUi.run("createOrderFail");

                }
            }

            @Override
            public void onFail(Throwable error) {

            }
        });
    }

    public void priceCalculate() {
        isLoading.set(true);
        OrderApi.getPrice(getApplication().getApplicationContext(), billLadingField.get(), new SharingOdooResponse() {
            @Override
            public void onSuccess(Object o) {
                isLoading.set(false);
                billLadingField.get().setTotal_amount(((BillLading) o).getTotal_amount());
                billLadingField.get().setPrice_not_discount(((BillLading) o).getPrice_not_discount());
                billLadingField.get().setInsurance_price(((BillLading) o).getInsurance_price());
                billLadingField.get().setService_price(((BillLading) o).getService_price());
                billLadingField.notifyChange();
            }

            @Override
            public void onFail(Throwable error) {

            }
        });

    }

    /**
     * @return 0: đúng
     * 1: chưa chia hết
     * 2: Kho không có bất kỳ gói hàng nào.
     * kiểm tra số lượng sản phẩm từ kho lấy có bằng số lượng tại các kho trả hay ko
     */
    public int isValidReceiptWarehouse() {
        List<BillLadingDetail> arrBillLadingDetail = billLadingField.get().getArrBillLadingDetail();
        if (arrBillLadingDetail != null && arrBillLadingDetail.size() > 1) {

            for (BillLadingDetail billRoutingDetail : arrBillLadingDetail) {
                boolean isEmpty = true;
                for (BillPackage billPackage : billRoutingDetail.getBillPackages()) {
                    if (billPackage.getQuantity_package() > 0) {
                        isEmpty = false;
                        break;
                    }
                }
                if (isEmpty) return 2;
            }

            List<BillPackage> listPickup = arrBillLadingDetail.get(0).getBillPackages();
            for (int i = 0; i < listPickup.size(); i++) {
                int total = 0;
                for (int j = 1; j < arrBillLadingDetail.size(); j++) {
                    total += arrBillLadingDetail.get(j).getBillPackages().get(i).getQuantity_package();
                }
                if (total < listPickup.get(i).getQuantity_package() || total > listPickup.get(i).getQuantity_package()) {
                    return 1;
                }
            }
            return 0;
        }
        return 1;
    }

    public void getDistance(LatLng start, LatLng end, Integer indexBillLadingDetail) {
        distanceDTO = new DistanceDTO();
        RoutingApi.getDistanceByLatAndLong(start, end, new SharingOdooResponse() {
            @Override
            public void onSuccess(Object o) {
                if (o != null) {
                    DistanceDTO distanceDTO = (DistanceDTO) o;
                    Double cost = distanceDTO.getCost() / 1000;
                    billLadingField.get().getArrBillLadingDetail().get(indexBillLadingDetail).setExpress_distance(Math.ceil(cost * 1000) / 1000);
                }
            }

            @Override
            public void onFail(Throwable error) {

            }
        });
    }


    /**
     * Tính toán khoảng cách từ kho -> hub của kho đó hoặc ngươc lại.
     *
     * @param selectedWareHouse kho cần tính toán.
     * @param isWarehouseToHub  tính từ kho -> hub hay từ hub -> kho.
     * @param runUi             listener thay đổi giao diện.
     */
    public void getDestination(Context context, Warehouse selectedWareHouse, boolean isWarehouseToHub, RunUi runUi) {
        isLoading.set(true);
        LatLng start;
        LatLng end;

        if (selectedWareHouse.getAreaInfo() == null) {
            runUi.run("warehouseInfoInvalid", selectedWareHouse.getId());
            return;
        }
        if (selectedWareHouse.getAreaInfo().getHubInfo() == null) {
            runUi.run("warehouseInfoInvalid", selectedWareHouse.getId());
            return;
        }
        ;
        if (selectedWareHouse.getAreaInfo().getHubInfo().getLatitude() == null || selectedWareHouse.getAreaInfo().getHubInfo().getLongitude() == null) {
            runUi.run("warehouseInfoInvalid", selectedWareHouse.getId());
            return;
        }


        if (isWarehouseToHub) {
            start = new LatLng(selectedWareHouse.getLatitude(), selectedWareHouse.getLongitude());
            end = new LatLng(selectedWareHouse.getAreaInfo().getHubInfo().getLatitude(), selectedWareHouse.getAreaInfo().getHubInfo().getLongitude());
        } else {
            end = new LatLng(selectedWareHouse.getLatitude(), selectedWareHouse.getLongitude());
            start = new LatLng(selectedWareHouse.getAreaInfo().getHubInfo().getLatitude(), selectedWareHouse.getAreaInfo().getHubInfo().getLongitude());
        }
//        RoutingApi.getDistance(context, start, end, new SharingOdooResponse() {
//            @Override
//            public void onSuccess(Object o) {
//                isLoading.set(false);
//                if (o != null) {
//                    runUi.run("getDistanceSuccess", selectedWareHouse, (DistanceDTO) o);
//                } else {
//                    runUi.run("getDistanceFail");
//                }
//            }
//
//            @Override
//            public void onFail(Throwable error) {
//                runUi.run("getDistanceFail");
//            }
//        });
        RoutingApi.getDistanceByLatAndLong(start, end, new SharingOdooResponse() {
            @Override
            public void onSuccess(Object o) {
                isLoading.set(false);
                if (o != null) {
                    runUi.run("getDistanceSuccess", selectedWareHouse, (DistanceDTO) o);
                } else {
                    runUi.run("getDistanceFail");
                }
            }

            @Override
            public void onFail(Throwable error) {
                isLoading.set(false);
                runUi.run("getDistanceFail");
            }
        });

    }


    private List<LatLng> decodePoly(String encoded) {
        List<LatLng> poly = new ArrayList<>();
        int index = 0, len = encoded.length();
        int lat = 0, lng = 0;

        while (index < len) {
            int b, shift = 0, result = 0;
            do {
                b = encoded.charAt(index++) - 63;
                result |= (b & 0x1f) << shift;
                shift += 5;
            } while (b >= 0x20);
            int dlat = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
            lat += dlat;

            shift = 0;
            result = 0;
            do {
                b = encoded.charAt(index++) - 63;
                result |= (b & 0x1f) << shift;
                shift += 5;
            } while (b >= 0x20);
            int dlng = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
            lng += dlng;

            LatLng p = new LatLng((((double) lat / 1E5)),
                    (((double) lng / 1E5)));
            poly.add(p);
        }

        return poly;
    }


    public void setCapacity(BillLadingDetail billLadingDetail, boolean isCapacityBill) {
        double total_weight = 0;
        int totalParcel = 0;
        double totalVol = 0;
        List<BillPackage> billPackages = billLadingDetail.getBillPackages();
        for (BillPackage billPackage : billPackages) {
            total_weight += billPackage.getNet_weight() * billPackage.getQuantity_package();
            totalParcel += billPackage.getQuantity_package();
            totalVol += billPackage.getTotalVol();
        }
        billLadingDetail.setTotal_weight(total_weight);
        billLadingDetail.setTotal_parcel(totalParcel);
        billLadingDetail.setArea_id(billLadingDetail.getWarehouse().getAreaInfo().getId());
        billLadingDetail.setTotal_volume(totalVol);

        if (isCapacityBill) {
            billLadingField.get().setTotal_weight(total_weight);
            billLadingField.get().setTotal_volume(totalVol);
            billLadingField.get().setTotal_parcel(totalParcel);
        }
    }

    /**
     * Hàm kiểm tra 1 billPackage có trùng thông tin vói các billPackage đã được thêm trước đó
     *
     * @param bill billPackage muốn compare
     * @return true | false
     */
    public int compareBillPackage(BillPackage bill) {
        List<BillPackage> billPackages = billLadingField.get().getArrBillLadingDetail().get(0).getBillPackages();
        for (int i = 0; i < billPackages.size(); i++) {
            BillPackage billPackage = billPackages.get(i);
            if (billPackage.mEquals(bill)) return i;
        }
        return -1;
    }

    public void setAddressWarehouse() {
        for (int i = 0; i < billLadingField.get().getArrBillLadingDetail().size(); i++) {
            BillLadingDetail item = billLadingField.get().getArrBillLadingDetail().get(i);
            item.setAddress(item.getWarehouse().getFullAddress());
        }
    }
}
