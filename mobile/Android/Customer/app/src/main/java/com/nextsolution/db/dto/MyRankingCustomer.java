package com.nextsolution.db.dto;

import com.nextsolution.vancustomer.util.MyDateUtils;
import com.ns.odoolib_retrofit.utils.OdooDateTime;
import com.tsolution.base.BaseModel;

import java.util.Date;
import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MyRankingCustomer extends BaseModel {
    Long id;
    Integer point = 0;
    Integer total_point;
    OdooDateTime expired_date;
    List<RankingDTO> list_rank;

    public String expired_dateStr() {
        if (expired_date == null)
            return null;
        return MyDateUtils.convertDateToString(this.expired_date, MyDateUtils.DATE_FORMAT_DD_MM_YYYY);

    }
}
