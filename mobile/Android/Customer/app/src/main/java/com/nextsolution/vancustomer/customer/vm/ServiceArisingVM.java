package com.nextsolution.vancustomer.customer.vm;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.databinding.ObservableBoolean;
import androidx.databinding.ObservableField;

import com.luck.picture.lib.entity.LocalMedia;
import com.tsolution.base.BaseViewModel;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ServiceArisingVM extends BaseViewModel {
    ObservableField<Boolean> emptyData= new ObservableField<>();
    ObservableBoolean isNotEmptyImage = new ObservableBoolean(false);
    private List<LocalMedia> lstFileSelected;
    public ServiceArisingVM(@NonNull Application application) {
        super(application);
        emptyData.set(true);
    }

    public void setDataFileChoose(List<LocalMedia> albumFiles) {
        lstFileSelected = albumFiles;
        if (albumFiles != null && albumFiles.size() > 0) {
            isNotEmptyImage.set(true);
        } else {
            isNotEmptyImage.set(false);
        }
    }
}
