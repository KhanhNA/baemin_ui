package com.nextsolution.vancustomer.viewmodel;

import android.app.Application;
import android.util.Log;

import com.google.gson.Gson;
import com.nextsolution.db.api.SharingOdooResponse;
import com.nextsolution.db.api.WarehouseApi;
import com.nextsolution.db.dto.Warehouse;
import com.nextsolution.vancustomer.base.RunUi;
import com.nextsolution.vancustomer.enums.Constants;
import com.nextsolution.vancustomer.enums.ErrorSaveWarehouse;
import com.nextsolution.vancustomer.model.ErrorBodyDTO;
import com.nextsolution.vancustomer.util.StringUtils;
import com.ns.odoolib_retrofit.model.OdooResultDto;
import com.tsolution.base.BaseViewModel;

import java.util.List;
import java.util.Objects;

import androidx.annotation.NonNull;
import androidx.databinding.ObservableBoolean;

import lombok.Getter;
import lombok.Setter;
import okhttp3.RequestBody;
import retrofit2.Response;

@Getter
@Setter
public class WarehouseInfoVM extends BaseViewModel<Warehouse> {
    private ObservableBoolean isLoading = new ObservableBoolean();

    private String messagePleaseEnterFullName = "";
    private String messagePleaseEnterPhoneNumber = "";
    private String messageRequiteProvince = "";
    private String messagePleaseEnterAddress = "";
    private Warehouse warehouse;

    public WarehouseInfoVM(@NonNull Application application) {
        super(application);
        model.set(new Warehouse());

    }

    public void saveWarehouseInfo(RunUi runUi) {
        isLoading.set(true);
//        WarehouseApi.saveWarehouseInfo(model.get(), false, new SharingOdooResponse() {
        WarehouseApi.editWarehouseInfo(model.get(), false, new SharingOdooResponse() {
            @Override
            public void onSuccess(Object o) {
                isLoading.set(false);
                Response<Warehouse> response = (Response<Warehouse>) o;
                if (response == null) {
                    runUi.run("saveAddressFalse");
                    return;
                } else if (response.body() == null) {
                    try {

                        ErrorBodyDTO errorBodyDTO = new Gson().fromJson(response.errorBody().string(), ErrorBodyDTO.class);
                        runUi.run(Constants.SAVE_WAREHOUSE_FAIL, errorBodyDTO.getStatus());
//                        handleSaveWarehouseFail(runUi, errorBodyDTO);
                    } catch (Exception e) {
                        e.printStackTrace();
                        runUi.run("saveAddressFalse");
                    }
                    return;
                }
                List<Warehouse> warehouseList = (List<Warehouse>) response.body();
                if (warehouseList.size() == 0) {
                    runUi.run("saveAddressFalse");
                    return;
                }
                warehouse = warehouseList.get(0);

                warehouse.setState_id(getModelE().getState_id());
                warehouse.setDistrict(getModelE().getDistrict());
                warehouse.setWard(getModelE().getWard());

                warehouse.setProvince_name(getModelE().getProvince_name());
                warehouse.setDistrict_name(getModelE().getDistrict_name());
                warehouse.setWard_name(getModelE().getWard_name());
                runUi.run("saveAddressSuccess");
            }

            @Override
            public void onFail(Throwable error) {
                runUi.run("saveAddressFalse");
                isLoading.set(false);
            }
        });
    }


    public boolean isValid(Warehouse merchantAddressDto) {
        boolean isValid = true;
        if (StringUtils.isNullOrEmpty(merchantAddressDto.getName())) {
            isValid = false;
            addError("name", messagePleaseEnterFullName, true);
        } else {
            clearErro("name");
        }
        if (StringUtils.isNullOrEmpty(merchantAddressDto.getPhone())) {
            isValid = false;
            addError("phone", messagePleaseEnterPhoneNumber, true);
        } else {
            clearErro("phone");
        }

        if (StringUtils.isNullOrEmpty(merchantAddressDto.getProvince_name())) {
            isValid = false;
            addError("province_name", messageRequiteProvince, true);
        } else {
            clearErro("province_name");
        }

        if (StringUtils.isNullOrEmpty(merchantAddressDto.getAddress())) {
            isValid = false;
            addError("address", messagePleaseEnterAddress, true);
        } else {
            clearErro("address");
        }

        return isValid;
    }


    public void deleteWarehouse(RunUi runUi) {
        isLoading.set(true);
//        WarehouseApi.saveWarehouseInfo(model.get(), true, new SharingOdooResponse() {
        WarehouseApi.editWarehouseInfo(model.get(), true, new SharingOdooResponse() {
            @Override
            public void onSuccess(Object o) {
                isLoading.set(false);
                runUi.run("deleteAddressSuccess");
            }

            @Override
            public void onFail(Throwable error) {
                isLoading.set(false);
                runUi.run("deleteAddressFail");
            }
        });
    }

}
