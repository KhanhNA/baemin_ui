package com.nextsolution.vancustomer.customer;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.viewpager.widget.ViewPager;

import com.king.zxing.Intents;
import com.nextsolution.vancustomer.R;
import com.nextsolution.vancustomer.adapter.PagerAdapter;
import com.nextsolution.vancustomer.customer.vm.CargoViewPagerVM;
import com.nextsolution.vancustomer.databinding.CargoViewPagerFragmentBinding;
import com.nextsolution.vancustomer.enums.Constants;
import com.nextsolution.vancustomer.scan.QrScannerActivity;
import com.tsolution.base.BaseFragment;
import com.tsolution.base.BaseViewModel;

public class CargoViewPagerFragment extends BaseFragment {
    CargoViewPagerFragmentBinding mBinding;
    private MenuItem preItem;
    CargoViewPagerVM cargoViewPagerVM;
    ListCargoFragment cargoWaitingFragment;
    ListCargoFragment cargoSuccessFragment;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = super.onCreateView(inflater, container, savedInstanceState);
        mBinding = (CargoViewPagerFragmentBinding) binding;
        cargoViewPagerVM = (CargoViewPagerVM) viewModel;
        navigationView();
        return v;
    }

    @Override
    public void action(View view, BaseViewModel baseViewModel) {
        if (view.getId() == R.id.toolbar) {
            getActivity().onBackPressed();
        } else if (view.getId() == R.id.qrCode) {
            Intent qrScan = new Intent(getContext(), QrScannerActivity.class);
            startActivityForResult(qrScan, Constants.REQUEST_CODE_SCAN);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (isScanCodeAndResultOk(requestCode, resultCode) && data != null && data.hasExtra(Intents.Scan.RESULT)) {
            cargoSuccessFragment.setTextSearch(data.getStringExtra(Intents.Scan.RESULT));
            cargoWaitingFragment.setTextSearch(data.getStringExtra(Intents.Scan.RESULT));
        }
    }

    private boolean isScanCodeAndResultOk(int requestCode, int resultCode) {
        return requestCode == Constants.REQUEST_CODE_SCAN && resultCode == Activity.RESULT_OK;
    }

    private void navigationView() {
        PagerAdapter myPagerAdapter = new PagerAdapter(getChildFragmentManager());
        cargoWaitingFragment = new ListCargoFragment(Constants.CARGO_STATUS.values()[0].toString());
        cargoSuccessFragment = new ListCargoFragment(Constants.CARGO_STATUS.values()[1].toString());

        myPagerAdapter.addFragment(cargoWaitingFragment);
        myPagerAdapter.addFragment(cargoSuccessFragment);

        mBinding.frameContainer.setAdapter(myPagerAdapter);
        mBinding.navigation.setOnNavigationItemSelectedListener(item -> {
            switch (item.getItemId()) {
                case R.id.nvCargoWaitingPacking:
                    mBinding.frameContainer.setCurrentItem(0);
                    break;
                case R.id.nvCargoPacked:
                    mBinding.frameContainer.setCurrentItem(1);
                    break;
            }
            return false;
        });
        mBinding.frameContainer.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageSelected(int position) {
                if (preItem != null) {
                    preItem.setChecked(false);
                } else {
                    mBinding.navigation.getMenu().getItem(0).setChecked(false);
                }
                mBinding.navigation.getMenu().getItem(position).setChecked(true);
                preItem = mBinding.navigation.getMenu().getItem(position);
            }

            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {
                // Code goes here
            }
        });
    }

    @Override
    public int getLayoutRes() {
        return R.layout.cargo_view_pager_fragment;
    }

    @Override
    public Class<? extends BaseViewModel> getVMClass() {
        return CargoViewPagerVM.class;
    }

    @Override
    public int getRecycleResId() {
        return 0;
    }
}
