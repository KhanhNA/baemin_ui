package com.nextsolution.db.dto;


import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;

import java.io.Serializable;
import java.net.URL;

public interface ShipmentStandStill extends Serializable {
//    transient Marker marker = null;
//    protected Integer id;

    abstract public LatLng getLatLng();

    abstract public String getAvatarStr();

    abstract public URL getAvatarUrl() throws Exception;
    void setMarker(Marker marker1);
    Marker getMarker();
//    public void setMarker(Marker marker1) {
//        marker = marker1;
//    }
//
//    public Marker getMarker() {
//        return marker;
//    }
//
//    public Integer getId() {
//        return id;
//    }

    String getServiceTime();

    String getName();

    String getDescription();

    Double getCapacity();

    String getArriveTime();

}