package com.nextsolution.vancustomer.model;

import com.tsolution.base.BaseModel;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class OrderPackage extends BaseModel {
    private Integer id;
    private String name;
    private String type;
    private String description;

}
