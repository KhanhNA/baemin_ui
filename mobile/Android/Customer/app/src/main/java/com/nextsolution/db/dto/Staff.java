package com.nextsolution.db.dto;

@lombok.Getter
@lombok.Setter
public class Staff  {
  private String __typename;
  private Integer SSN;
  private String address;
  private Integer app_param_value_id;
  private String avatar_url;
  private Integer billing_rate;
  private java.util.Date birth_date;
  private String country;
  private java.util.Date create_date;
  private String create_user;
  private String description;
  private String email;
  private String first_name;
  private Integer fleet_id;
  private String full_name;
  private Integer gender;
  private java.util.Date hire_date;
  private Integer id;
  private String id_no;
  private String imei;
  private Integer labor_rate;
  private String last_name;
  private Double latitude;
  private java.util.Date leave_date;
  private String license_note;
  private Double longitude;
  private String nation;
  private Integer object_type;
  private String phone;
  private Integer point;
  private String province;
  private String staff_code;
  private String state_province;
  private Integer status;
  private java.util.Date update_date;
  private String update_user;
  private Integer user_id;
  private String user_name;

}

