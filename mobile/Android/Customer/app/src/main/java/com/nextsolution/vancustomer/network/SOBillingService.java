package com.nextsolution.vancustomer.network;

import com.nextsolution.db.dto.BillLading;

import java.util.logging.Handler;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;

public interface SOBillingService {

    @POST("/app/v1/price/calc/bill")
    Call<BillLading> priceCalculate(@Body RequestBody billLadingDetails
            ,@Header("Authorization") String token);

}
