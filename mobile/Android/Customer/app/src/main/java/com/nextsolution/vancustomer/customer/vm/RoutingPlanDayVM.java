package com.nextsolution.vancustomer.customer.vm;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.databinding.ObservableField;

import com.nextsolution.db.api.RoutingApi;
import com.nextsolution.db.api.SharingOdooResponse;
import com.nextsolution.db.dto.RoutingDay;
import com.nextsolution.vancustomer.base.RunUi;
import com.nextsolution.vancustomer.base.StaticData;
import com.nextsolution.vancustomer.enums.Constants;
import com.ns.odoolib_retrofit.model.OdooResultDto;
import com.tsolution.base.BaseViewModel;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RoutingPlanDayVM extends BaseViewModel {

    private List<RoutingDay> listRoutingDay;
    private Integer positionStatus;
    private ObservableField<String> resultQrCode;

    public RoutingPlanDayVM(@NonNull Application application) {
        super(application);
        listRoutingDay = new ArrayList<>();
        resultQrCode= new ObservableField<>();
    }

    public void getData(RunUi runUi){
        Date date = new Date();
        RoutingApi.getRoutingDay(date, StaticData.getOdooSessionDto().getUid(), resultQrCode.get(), date, date, new SharingOdooResponse() {
            @Override
            public void onSuccess(Object o) {
                getListData(o, runUi);
            }

            @Override
            public void onFail(Throwable error) {

            }
        });

    }
    public void getListData(Object o,RunUi runUi){
        this.listRoutingDay.clear();
        OdooResultDto<RoutingDay> resultDto = (OdooResultDto<RoutingDay>) o;
        try{
            listRoutingDay.addAll(resultDto.getRecords());
            runUi.run(Constants.GET_DATA_SUCCESS);
        }catch (Exception e){
            e.printStackTrace();
        }

    }
}
