package com.nextsolution.vancustomer.confirm_change;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.LinearLayout;

import com.google.android.gms.maps.model.LatLng;
import com.google.android.material.tabs.TabLayout;
import com.nextsolution.db.dto.Area;
import com.nextsolution.db.dto.AreaDistance;
import com.nextsolution.db.dto.BillLadingDetail;
import com.nextsolution.db.dto.DistanceDTO;
import com.nextsolution.vancustomer.BR;
import com.nextsolution.vancustomer.R;
import com.nextsolution.vancustomer.databinding.ConfirmEditLadingActivityBinding;
import com.nextsolution.vancustomer.enums.Constants;
import com.nextsolution.vancustomer.widget.NonSwipeableViewPager;
import com.nextsolution.vancustomer.widget.TabAdapter;
import com.tsolution.base.BaseActivity;
import com.tsolution.base.BaseViewModel;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;

import androidx.lifecycle.ViewModelProviders;

public class ConfirmEditBillLadingActivity extends BaseActivity<ConfirmEditLadingActivityBinding> {
    ConfirmChangeVM confirmChangeVM;
    int tabPosition = 0;
    DetailExportChangeFragment detailExport;
    DetailImportsFragment detailImport;
    NonSwipeableViewPager viewPager;

    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        viewModel = ViewModelProviders.of(getBaseActivity()).get(ConfirmChangeVM.class);
        confirmChangeVM = (ConfirmChangeVM) viewModel;
        binding.setVariable(BR.viewModel, viewModel);
        binding.toolbar.setTitle(R.string.confirm_change);
        getBaseActivity().setSupportActionBar(binding.toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        initView();
        Intent intent = getIntent();
        String billDetailId = intent.getStringExtra(Constants.ITEM_ID);
        if(billDetailId != null) {
            confirmChangeVM.getBillLadingUpdate(billDetailId, this::runUi);
        }

    }
    @SuppressLint("ClickableViewAccessibility")
    private void runUi(Object[] objects) {
        String action = (String) objects[0];
        switch (action) {
            case "getBillLadingSuccess":
                detailExport.notifyBillPackageAdapter();
                detailExport.notifyImageAdapter();
                detailImport.notifyDataSetChanged();

                setAreaDistance();

                viewPager.setAllowedSwipeDirection(NonSwipeableViewPager.SwipeDirection.ALL);
                break;

            case "getDistanceSuccess":
                boolean isPickup = (boolean) objects[3];
                if(isPickup) {
                    distancePickupWarehouse((BillLadingDetail) objects[1], (DistanceDTO) objects[2]);
                }else {
                    distanceDropWarehouse((BillLadingDetail) objects[1], (DistanceDTO) objects[2]);
                }
                break;
        }

    }

    private void setAreaDistance() {
        List<BillLadingDetail> arrBillLadingDetail = confirmChangeVM.getModelE().getArrBillLadingDetail();
        for (int i = 1; i < arrBillLadingDetail.size(); i++) {
            BillLadingDetail billLadingDetail = arrBillLadingDetail.get(i);
            confirmChangeVM.getDestination(billLadingDetail, false, this::runUi);
        }
    }


    /**
     * @param billLadingDetail kho trả hàng - drop
     * @param distanceDTO            thông tin quãng đường từ Hub đến warehouse
     */
    private void distanceDropWarehouse(BillLadingDetail billLadingDetail, DistanceDTO distanceDTO) {
        List<AreaDistance> distanceList = new ArrayList<>();

        //lấy area của pickup warehouse - phần tử đầu tiên.
        Area areaPickup = confirmChangeVM.getModelE().getArrBillLadingDetail().get(0).getWarehouse().getAreaInfo();

        //nếu kho nhận khác hub với kho trả
        if (!areaPickup.getHubInfo().getName_seq().equals(billLadingDetail.getWarehouse().getAreaInfo().getHubInfo().getName_seq())) {
            //nếu kho nhận cùng zone với kho trả : hub -> hub
            if (areaPickup.getZoneInfo().getName_seq().equals(billLadingDetail.getWarehouse().getAreaInfo().getZoneInfo().getName_seq())) {
                distanceList.add(
                        confirmChangeVM.getAreaDistance(areaPickup.getHubInfo()
                                , billLadingDetail.getWarehouse().getAreaInfo().getHubInfo(), 2)
                );
                //nếu kho nhận khác zone với kho trả : hub -> depot -> depot -> hub
            } else {
                //hub -> depot
                distanceList.add(
                        confirmChangeVM.getAreaDistance(areaPickup.getHubInfo(), areaPickup.getZoneInfo().getDepotInfo(), 3)
                );
                //depot -> depot
                distanceList.add(
                        confirmChangeVM.getAreaDistance(areaPickup.getZoneInfo().getDepotInfo()
                                , billLadingDetail.getWarehouse().getAreaInfo().getZoneInfo().getDepotInfo(), 4)
                );
                //depot -> hub
                distanceList.add(
                        confirmChangeVM.getAreaDistance(billLadingDetail.getWarehouse().getAreaInfo().getZoneInfo().getDepotInfo()
                                , billLadingDetail.getWarehouse().getAreaInfo().getHubInfo(), 5)
                );
            }
        }

        //hub -> kho
        AreaDistance hubToWarehouse = new AreaDistance();
        hubToWarehouse.setType(6);
        hubToWarehouse.setDistance(distanceDTO.getCost());
        hubToWarehouse.setDuration(distanceDTO.getMinutes());
        hubToWarehouse.setFrom_name_seq(billLadingDetail.getWarehouse().getAreaInfo().getHubInfo().getName_seq());
        hubToWarehouse.setTo_name_seq(billLadingDetail.getWarehouse().getName_seq());
        hubToWarehouse.setFrom_warehouse_name(billLadingDetail.getWarehouse().getAreaInfo().getHubInfo().getName());
        hubToWarehouse.setTo_warehouse_name(billLadingDetail.getWarehouse().getName());
        hubToWarehouse.setFromLocation(new LatLng(billLadingDetail.getWarehouse().getAreaInfo().getHubInfo().getLatitude(),
                billLadingDetail.getWarehouse().getAreaInfo().getHubInfo().getLongitude()));

        hubToWarehouse.setToLocation(new LatLng(billLadingDetail.getWarehouse().getLatitude(),
                billLadingDetail.getWarehouse().getLongitude()));

        distanceList.add(hubToWarehouse);


        billLadingDetail.getWarehouse().setFromWareHouseCode(confirmChangeVM.getFromWareHouseCode());
        billLadingDetail.setAreaDistances(distanceList);
    }

    /**
     *
     * @param billLadingDetail kho lấy hàng - pickup
     * @param distanceDTO thông tin quãng đường từ warehouse to hub
     */
    public void distancePickupWarehouse(BillLadingDetail billLadingDetail, DistanceDTO distanceDTO){
        List<AreaDistance> distanceList = new ArrayList<>();

        AreaDistance distance = new AreaDistance();
        distance.setType(1);
        distance.setDistance(distanceDTO.getCost());
        distance.setDuration(distanceDTO.getMinutes());
        distance.setFrom_name_seq(billLadingDetail.getWarehouse().getName_seq());
        distance.setTo_name_seq(billLadingDetail.getWarehouse().getAreaInfo().getHubInfo().getName_seq());
        distance.setFrom_warehouse_name(billLadingDetail.getWarehouse().getName());
        distance.setTo_warehouse_name(billLadingDetail.getWarehouse().getAreaInfo().getHubInfo().getName());
        distance.setFromLocation(new LatLng(billLadingDetail.getWarehouse().getLatitude(),
                billLadingDetail.getWarehouse().getLongitude()));
        distance.setToLocation(new LatLng(billLadingDetail.getWarehouse().getAreaInfo().getHubInfo().getLatitude(),
                billLadingDetail.getWarehouse().getAreaInfo().getHubInfo().getLongitude()));

        distanceList.add(distance);

        BillLadingDetail billDetail = confirmChangeVM.getModelE().getArrBillLadingDetail().get(0);

        billDetail.setAreaDistances(distanceList);

        billLadingDetail.getWarehouse().setWarehouse_code(confirmChangeVM.getFromWareHouseCode());
        billLadingDetail.getWarehouse().setFromWareHouseCode("0");

    }
    @SuppressLint("ClickableViewAccessibility")
    private void initView() {
        TabLayout tabLayout = binding.tabs;
        viewPager = binding.viewPager;
        viewPager.setAllowedSwipeDirection(NonSwipeableViewPager.SwipeDirection.NONE);

        TabAdapter adapter = new TabAdapter(getSupportFragmentManager());

        detailExport = new DetailExportChangeFragment();
        detailImport = new DetailImportsFragment(() -> {
            setResult(Constants.RESULT_OK);
            onBackPressed();
        });

        adapter.addFragment(detailExport, getString(R.string.WAREHOUSE_EXPORT));
        adapter.addFragment(detailImport, getString(R.string.WAREHOUSE_IMPORT));

        viewPager.setAdapter(adapter);
        viewPager.setOffscreenPageLimit(2);
        tabLayout.setupWithViewPager(viewPager);
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                tabPosition = tab.getPosition();
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

        LinearLayout tabStrip = ((LinearLayout) tabLayout.getChildAt(0));

        for (int i = 0; i < tabStrip.getChildCount(); i++) {
            tabStrip.getChildAt(i).setOnTouchListener((v, event) -> !confirmChangeVM.getIsLoading().get());
        }

    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(Integer pageSelected) {
        if (pageSelected != null) {
            switch (pageSelected) {
                case 0:
                    viewPager.setCurrentItem(0);
                    break;
                case 1:
                    viewPager.setCurrentItem(1);
                    break;
            }
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            getBaseActivity().onBackPressed();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public int getLayoutRes() {
        return R.layout.confirm_edit_lading_activity;
    }

    @Override
    public Class<? extends BaseViewModel> getVMClass() {
        return ConfirmChangeVM.class;
    }

    @Override
    public int getRecycleResId() {
        return 0;
    }
}
