package com.nextsolution.vancustomer.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.databinding.ObservableField;

import com.nextsolution.vancustomer.base.AppController;
import com.tsolution.base.BaseViewModel;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MainVM extends BaseViewModel {
    public MainVM(@NonNull Application application) {
        super(application);
    }
}
