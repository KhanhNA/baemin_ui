package com.nextsolution.vancustomer.customer;

import android.Manifest;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.GridLayoutManager;

import com.luck.picture.lib.PictureSelector;
import com.luck.picture.lib.animators.AnimationType;
import com.luck.picture.lib.config.PictureConfig;
import com.luck.picture.lib.config.PictureMimeType;
import com.luck.picture.lib.decoration.GridSpacingItemDecoration;
import com.luck.picture.lib.entity.LocalMedia;
import com.luck.picture.lib.listener.OnResultCallbackListener;
import com.luck.picture.lib.tools.ScreenUtils;
import com.nextsolution.vancustomer.R;
import com.nextsolution.vancustomer.adapter.GridImageAdapter;
import com.nextsolution.vancustomer.customer.vm.ListRoutingVanDayVM;
import com.nextsolution.vancustomer.customer.vm.ServiceArisingVM;
import com.nextsolution.vancustomer.databinding.ServiceArisingFragmentBinding;
import com.nextsolution.vancustomer.widget.FullyGridLayoutManager;
import com.nextsolution.vancustomer.widget.GlideEngine;
import com.tsolution.base.BaseFragment;
import com.tsolution.base.BaseViewModel;

import java.lang.ref.WeakReference;
import java.util.List;

public class ServiceArisingFragment extends BaseFragment {
    ServiceArisingVM serviceArisingVM;
    ServiceArisingFragmentBinding mBinding;
    private GridImageAdapter imageSelectAdapter;
    final int PICK_IMAGE_PERMISSIONS_REQUEST_CODE = 999;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view= super.onCreateView(inflater, container, savedInstanceState);
        serviceArisingVM = (ServiceArisingVM) viewModel;
        mBinding = (ServiceArisingFragmentBinding) binding;
        return view;
    }

    private void pickImage() {
        PictureSelector.create(this)
                .openGallery(PictureMimeType.ofImage())
                .loadImageEngine(GlideEngine.createGlideEngine())
                .theme(R.style.picture_WeChat_style)
                .isWeChatStyle(true)
                .setRecyclerAnimationMode(AnimationType.SLIDE_IN_BOTTOM_ANIMATION)
                .synOrAsy(true)
                .compress(true)
                .compressQuality(80)
                .selectionMode(PictureConfig.MULTIPLE)
                .selectionMedia(imageSelectAdapter.getData())
                .forResult(new MyResultCallback(imageSelectAdapter, serviceArisingVM));
    }

    public void selectImages() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (getActivity().checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED
                    && getActivity().checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED
                    && getActivity().checkSelfPermission(Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {
                pickImage();
            } else {
                requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA}, PICK_IMAGE_PERMISSIONS_REQUEST_CODE);
            }
        } else {
            pickImage();
        }

    }
    private void initAdapterImage(Bundle savedInstanceState) {
        initLayoutManager();
        imageSelectAdapter = new GridImageAdapter(getContext(), this::pickImage);
        if (savedInstanceState != null && savedInstanceState.getParcelableArrayList("selectorList") != null) {
            imageSelectAdapter.setList(savedInstanceState.getParcelableArrayList("selectorList"));
        }
        imageSelectAdapter.setOnClear(() -> {
            serviceArisingVM.getIsNotEmptyImage().set(false);
        });
        mBinding.rcImages1.setAdapter(imageSelectAdapter);
        imageSelectAdapter.setOnItemClickListener((v, position) -> {
            openOptionImage(position);
        });
    }

    private void openOptionImage(int position) {
        List<LocalMedia> selectList = imageSelectAdapter.getData();
        if (selectList.size() > 0) {
            LocalMedia media = selectList.get(position);
            String mimeType = media.getMimeType();
            int mediaType = PictureMimeType.getMimeType(mimeType);
            if (mediaType == PictureConfig.TYPE_VIDEO) {
                PictureSelector.create(this)
                        .themeStyle(R.style.picture_default_style)
                        .externalPictureVideo(TextUtils.isEmpty(media.getAndroidQToPath()) ? media.getPath() : media.getAndroidQToPath());
            } else {
                PictureSelector.create(this)
                        .themeStyle(R.style.picture_default_style)
                        .setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED)
                        .isNotPreviewDownload(true)
                        .openExternalPreview(position, selectList);
            }
        }
    }

    private void initLayoutManager() {
        FullyGridLayoutManager manager = new FullyGridLayoutManager(getContext(),
                4, GridLayoutManager.VERTICAL, false);
        mBinding.rcImages1.setLayoutManager(manager);

        mBinding.rcImages1.addItemDecoration(new GridSpacingItemDecoration(4,
                ScreenUtils.dip2px(getContext(), 8), false));
    }
    @Override
    public int getLayoutRes() {
        return R.layout.service_arising_fragment;
    }

    @Override
    public Class<? extends BaseViewModel> getVMClass() {
        return ServiceArisingVM.class;
    }

    @Override
    public int getRecycleResId() {
        return 0;
    }


    private static class MyResultCallback implements OnResultCallbackListener<LocalMedia> {
        private WeakReference<GridImageAdapter> mAdapterWeakReference;
        private ServiceArisingVM serviceArisingVM;

        public MyResultCallback(GridImageAdapter adapter, ServiceArisingVM serviceArisingVM) {
            super();
            this.mAdapterWeakReference = new WeakReference<>(adapter);
            this.serviceArisingVM = serviceArisingVM;
        }

        @Override
        public void onResult(List<LocalMedia> result) {
            serviceArisingVM.setDataFileChoose(result);
            if (mAdapterWeakReference.get() != null) {
                mAdapterWeakReference.get().setList(result);
                mAdapterWeakReference.get().notifyDataSetChanged();
            }
        }

        @Override
        public void onCancel() {
        }
    }
}
