package com.nextsolution.db.dto;

import com.tsolution.base.BaseModel;

@lombok.Getter
@lombok.Setter
public class Customer extends BaseModel {
    private String __typename;
    private String address;
    private String address1;
    private String address2;
    private Integer app_param_value_id;
    private String avatar_url;
    private String city;
    private String code;
    private String contact_name;
    private java.util.Date create_date;
    private String create_user;
    private Double discount;
    private String discount_type;
    private String email;
    private String fax;
    private Integer has_attachment;
    private Integer has_image;
    private Integer id;
    private String id_no;
    private String is_vip;
    private String mobile_phone;
    private String name;
    private java.util.Date pay_by_day;
    private String payment_type;
    private String phone;
    private String phone1;
    private String phone2;
    private String postal_code;
    private String state_province;
    private Integer status;
    private String tax_code;
    private java.util.Date update_date;
    private String update_user;
    private String wallet_account;
    private Integer wallet_id;
    private String website;
    private String user_name;
    private String walletAcount;
    private Long walletId;//client id
    private String gender;
    private String birthday;
    private Integer province_id;
    private Integer district_id;
    private Integer country_id;
    private Integer career_id;

}

