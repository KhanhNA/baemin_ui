package com.nextsolution.vancustomer.viewmodel;

import android.app.Application;
import android.util.Log;

import com.nextsolution.db.api.NotificationAPI;
import com.nextsolution.db.api.SharingOdooResponse;
import com.nextsolution.db.dto.NotificationDTO;
import com.nextsolution.vancustomer.base.RunUi;
import com.nextsolution.vancustomer.enums.Constants;
import com.ns.odoolib_retrofit.model.OdooResultDto;
import com.tsolution.base.BaseViewModel;

import androidx.annotation.NonNull;
import androidx.databinding.ObservableArrayList;
import androidx.databinding.ObservableField;
import androidx.databinding.ObservableList;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class NotificationVM extends BaseViewModel {
    Integer offset=Constants.FIRST_PAGE;
    Integer totalRecord=0;
    ObservableField<Integer> indexNavigation= new ObservableField<>();
    ObservableField<Boolean> emptyData= new ObservableField<>();
    ObservableField<Boolean> isLoading= new ObservableField<>(false);

    private ObservableList<NotificationDTO> listNotification;

    public NotificationVM(@NonNull Application application) {
        super(application);
        listNotification = new ObservableArrayList<>();
        emptyData.set(true);
        indexNavigation.set(0);
    }


    public void seenNotification(Integer notification_id) {
        NotificationAPI.seenNotification(notification_id, new SharingOdooResponse() {
            @Override
            public void onSuccess(Object o) {
                Boolean aBoolean = (Boolean) o;
                Log.d("Seen_notification", notification_id+" - "+ aBoolean);
            }

            @Override
            public void onFail(Throwable error) {

            }
        });
    }
    public void loadMore(String type,RunUi runUi){
        offset += 10;
        if (offset>totalRecord) {
            getNotification(type,true,runUi);
        } else {
            runUi.run("noMore");
        }
    }


    public void getNotification(String type,Boolean loadMore, RunUi runUi) {
        NotificationAPI.getNotification(offset,type,null, new SharingOdooResponse() {
            @Override
            public void onSuccess(Object o) {
                if(!loadMore){
                    listNotification.clear();
                    offset=0;
                }
                getDataSuccess(o, runUi);
            }

            @Override
            public void onFail(Throwable error) {

            }
        });
    }

    public void getDataSuccess(Object o, RunUi runUi) {
        OdooResultDto<NotificationDTO> resultDto = (OdooResultDto<NotificationDTO>) o;
        try{
            if (resultDto != null && resultDto.getRecords() != null && resultDto.getRecords().size() > 0) {
                listNotification.addAll(resultDto.getRecords());
                if(offset+10>resultDto.getTotal_record()){
                    runUi.run("noMore");
                }else{
                    runUi.run(Constants.GET_DATA_SUCCESS);
                }
                emptyData.set(false);
            } else {
                emptyData.set(true);
                runUi.run(Constants.GET_DATA_FAIL);
            }
            isLoading.set(false);
        }catch (Exception e){
            e.printStackTrace();
        }

    }


}
