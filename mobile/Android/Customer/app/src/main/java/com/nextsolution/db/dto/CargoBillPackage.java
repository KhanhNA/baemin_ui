package com.nextsolution.db.dto;

import android.os.Parcel;
import android.os.Parcelable;

import com.tsolution.base.BaseModel;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CargoBillPackage extends BaseModel implements Parcelable {
    Integer id;
    String item_name;
    Integer bill_lading_detail_id;
    Double net_weight;
    Double length;
    Double height;
    Double width;
    Integer capacity;
    Integer quantity_package;
    String description;
    Integer product_type_id;
    String status;
    Integer from_bill_package_id;
    CargoBillPackageRoutingPlan bill_package_routing_plan;
    transient String qr_char;

    protected CargoBillPackage(Parcel in) {
        if (in.readByte() == 0) {
            id = null;
        } else {
            id = in.readInt();
        }
        item_name = in.readString();
        if (in.readByte() == 0) {
            bill_lading_detail_id = null;
        } else {
            bill_lading_detail_id = in.readInt();
        }
        if (in.readByte() == 0) {
            net_weight = null;
        } else {
            net_weight = in.readDouble();
        }
        if (in.readByte() == 0) {
            length = null;
        } else {
            length = in.readDouble();
        }
        if (in.readByte() == 0) {
            height = null;
        } else {
            height = in.readDouble();
        }
        if (in.readByte() == 0) {
            width = null;
        } else {
            width = in.readDouble();
        }
        if (in.readByte() == 0) {
            capacity = null;
        } else {
            capacity = in.readInt();
        }
        if (in.readByte() == 0) {
            quantity_package = null;
        } else {
            quantity_package = in.readInt();
        }
        description = in.readString();
        if (in.readByte() == 0) {
            product_type_id = null;
        } else {
            product_type_id = in.readInt();
        }
        status = in.readString();
        if (in.readByte() == 0) {
            from_bill_package_id = null;
        } else {
            from_bill_package_id = in.readInt();
        }
        bill_package_routing_plan = in.readParcelable(CargoBillPackageRoutingPlan.class.getClassLoader());
    }

    public static final Creator<CargoBillPackage> CREATOR = new Creator<CargoBillPackage>() {
        @Override
        public CargoBillPackage createFromParcel(Parcel in) {
            return new CargoBillPackage(in);
        }

        @Override
        public CargoBillPackage[] newArray(int size) {
            return new CargoBillPackage[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        if (id == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(id);
        }
        dest.writeString(item_name);
        if (bill_lading_detail_id == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(bill_lading_detail_id);
        }
        if (net_weight == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeDouble(net_weight);
        }
        if (length == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeDouble(length);
        }
        if (height == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeDouble(height);
        }
        if (width == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeDouble(width);
        }
        if (capacity == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(capacity);
        }
        if (quantity_package == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(quantity_package);
        }
        dest.writeString(description);
        if (product_type_id == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(product_type_id);
        }
        dest.writeString(status);
        if (from_bill_package_id == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(from_bill_package_id);
        }
        dest.writeParcelable(bill_package_routing_plan, flags);
    }

    public void setQr_char(){
        this.qr_char= bill_package_routing_plan.qr_char;
    }
    public String getQr_char() {
        return bill_package_routing_plan.qr_char;
    }



}
