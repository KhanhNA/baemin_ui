package com.nextsolution.vancustomer.enums;

public class Constants {
    public static final String EMPTY_STRING = "";
    public static final String USER_NAME = "USER_NAME";
    public static final String DATA = "DATA";
    public static final String MK = "MK";
    public static final String SUCCESS = "SUCCESS";
    public static final String GOM_CARGO_SUCCESS = "GOM_CARGO_SUCCESS";
    public static final String GOM_CARGO_FAIL = "GOM_CARGO_FAIL";
    public static final String DATA_RESULT = "DATA_RESULT";
    public static final String DRIVER = "DRIVER";
    public static final String WAREHOUSE = "WAREHOUSE";
    public static final Integer LIMIT =10;
    public static final Integer FIRST_PAGE =0;
    public static final String FAIL = "FAIL";
    public static final String FROM_DATE = "FROM_DATE";
    public static final String QR_CODE = "QR_CODE";
    public static final String QUANTITY_QR_CHECKED="QUANTITY_QR_CHECKED";
    public static final Integer CHANGE_PASSWORD=201;
    public static final Integer EXPORT_QUANTITY=202;
    public static final Integer REQUEST_CODE = 199;
    public static final Integer CHOOSE_WAREHOUSE_DIALOG = 198;
    public static final Integer RESULT_OK = 200;
    public static final Integer EDIT_INFO_SUCCESS = 203;
    public static final Integer REQUEST_CODE_SCAN = 1111;
    public static final Integer REQUEST_ACTIVITY_RESULT=203;
    public static final Integer REQUEST_FRAGMENT_RESULT=204;
    public static final String RESULT_SCAN = "RESULT_SCAN";
    public static final String TO_DATE = "TO_DATE";
    public static final String MODEL="MODEL";
    public static final String ID="ID";
    public static final String FRAGMENT="FRAGMENT";
    public static final String QR_DATA="QR_DATA";
    public static final String IS_NOTIFICATION="isNotification";
    public static final String BIDDING_CARGO_STATUS="BIDDING_CARGO_STATUS";
    public static final String ROUTING_CODE = "routing_plan_day_code";
    public static final String BIDDING_CARGO_VEHICLES = "BIDDING_CARGO_VEHICLES";

    public static final String POSITION_SELECTED="POSITION_SELECTED";
    public static final String GET_DATA_SUCCESS="getDataSuccess";
    public static final String GET_NOTIFICATION_ROUTE="GET_NOTIFICATION_ROUTE";
    public static final String GET_NOTIFICATION_SYSTEM="GET_NOTIFICATION_SYSTEM";
    public static final String NOTIFY_CHANGE_DATA="NOTIFY_CHANGE_DATA";
    public static final String GET_DATA_FAIL="No Data";
    public static final String GET_DRIVER_LOCATION_SUCCESS="GET_DRIVER_LOCATION_SUCCESS";
    public static final String chatAppId = "nextsolutions.chat";

    public static final String TYPE_NOTIFICATION_ROUTING = "routing";
    public static final String TYPE_NOTIFICATION_SYSTEM = "system";
    public static final String ITEM_ID = "item_id";
    public static final String OBJECT_STATUS = "OBJECT_STATUS";
    public static final String TITLE = "TITLE";
    public static final String SCAN_MULTI_QR_CODE = "SCAN_MULTI_QR_CODE";
    public static final String FROM_FRAGMENT = "FROM_FRAGMENT";
    public static final String FROM_ACTIVITY = "FROM_ACTIVITY";
    public static final String DEPOT_TYPE = "DEPOT_TYPE";
    public static final String ORDER_FRAGMENT = "ORDER_FRAGMENT";
    public static final String BIDDING_CARGO_CHILD_FRAGMENT="BIDDING_CARGO_CHILD_FRAGMENT";
    public static final String TYPE_IN_ZONE = "0";
    public static final String TYPE_OUT_ZONE = "1";
    public static final Integer RESULT_FAIL = 2020;
    public static final String IS_CONFIRM = "IS_CONFIRM";
    public static final String SAVE_WAREHOUSE_FAIL = "saveAddressFalse";
    public static final String DISCONNECTED_NETWORK = "DISCONNECTED_NETWORK";
    public static final int REQUEST_PHONE_CALL = 101;

    public static final int ADMINISTRATIVE_ACTION_PROVINCE = 1;
    public static final int ADMINISTRATIVE_ACTION_DISTRICT = 2;
    public static final int ADMINISTRATIVE_ACTION_VILLAGE = 3;
    public static final int ADMINISTRATIVE_ID_PROVINCE = -1;

    public enum CARGO_STATUS{
        waiting,
        success
    }

    public enum CODE {
        SUCCESS(200), NOT_FOUND(404), ERROR_INTERNAL(500), AUTHEN_FAIL(401), USER_DEFIEND(-200), FATAL_ERROR(-1);

        private Integer code;

        CODE(Integer i) {
            code = i;
        }

        public Integer getCode() {
            return code;
        }
    }

    public final static String FIRST_USE = "FIRST_USE";
//    public static final int CODE_SUCCESS =200;
//    public static final int CODE_NOT_FOUND =404;
//    public static final int CODE_ERROR_INTERNAL =500;
//    public static final int CODE_AUTHEN_FAIL =401;

    public final static int METHOD_WALLET = 1;//tra bang vi
    public final static int METHOD_CARD = 2; // tra bang the
    public final static int METHOD_DIRECTORY = 3; //tra truc tiep

    public final static String SPACE = " ";

    public enum ORDER {
        PENDING(0), APPROVE(1), REJECT(2);
        private Integer code;

        ORDER(Integer i) {
            code = i;
        }

        public Integer getCode() {
            return code;
        }
    }

    //    public static final int ORDER_PENDING = 0;
//    public static final int ORDER_APPROVE = 1;
//    public static final int ORDER_REJECT = 2;
//    public static final int LANG_VI = 1;
    public static final int LANG_EN = 2;
    public static final long MERCHANT_ID_FAKE = 1;
    public static final int INCENTIVE = 1;
    public static final int SALE = 0;
    public static final int GET_DATA_MANUFACTURE = 1001;
    public static final int GET_DATA_CATEGORIES = 1002;

    public static final int GET_DATA_PRODUCT = 1003;
    public static final int GET_PRODUCT_DETAIL = 1004;
    public static final int GET_DATA_LIST_ORDER = 1005;
    public static final int GET_DATA_LIST_PRODUCT_ORDER_CART = 1020;
    public static final int INTENT_GET_RECEPTION_ADDRESS = 1021;
    public static final int INTENT_GET_METHOD_PAYMENT = 1022;

    public static final int GET_DATE_ORDER_DETAIL = 1006;
    public static final int REGISTER_MERCHANT = 1007;
    public static final int GET_DATA_MERCHANT = 1008;
    public static final int CONNECT_TIMEOUT = 30;
    public static final int READ_TIMEOUT = 30;
    public static final int WRITE_TIMEOUT = 30;

    public static boolean IS_REFRESH_ORDERED = false;
    public static boolean IS_REFRESH_ORDER_PENDING = false;
    public static boolean IS_REFRESH_ORDER_REJECT = false;
}
