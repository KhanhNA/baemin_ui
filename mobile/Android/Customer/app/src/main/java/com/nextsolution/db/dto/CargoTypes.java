package com.nextsolution.db.dto;

import com.nextsolution.vancustomer.base.AppController;
import com.tsolution.base.BaseModel;
import com.tsolution.base.BaseViewModel;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CargoTypes extends BaseModel {
    transient String qrChecked="";
    private Integer id;
    private Double length;
    private Double width;
    private Double height;
    private String long_unit_moved0;
    private Integer weight_unit_moved0;
    private String type;
    private Integer from_weight;
    private Integer to_weight;
    private String size_standard_seq;
    private Integer cargo_quantity;
    private Integer total_weight;
    private List<Cargo> cargos;
    public String getLengthStr() {
        return this.length != null ? AppController.getInstance().formatNumber(this.length) : "";
    }

    public String getWidthStr() {
        return this.width != null ? AppController.getInstance().formatNumber(width) : "";
    }

    public String getHeightStr() {
        return this.height != null ? AppController.getInstance().formatNumber(height) : "";
    }
}
