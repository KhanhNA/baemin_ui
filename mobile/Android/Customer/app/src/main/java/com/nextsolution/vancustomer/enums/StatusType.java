package com.nextsolution.vancustomer.enums;

public class StatusType {
    public static final String DELETED = "deleted";
    public static final String RUNNING = "running";
    public static final String DRAFT = "draft";
}
