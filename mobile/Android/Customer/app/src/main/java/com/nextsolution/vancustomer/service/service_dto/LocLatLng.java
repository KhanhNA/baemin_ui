package com.nextsolution.vancustomer.service.service_dto;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class LocLatLng implements Serializable {
    public double latitude;
    public double longitude;
    public String time;
    public float direction;

    public LocLatLng() {
    }

    public LocLatLng(double latitude, double longitude, String time) {
        this.latitude = latitude;
        this.longitude = longitude;
        this.time = time;
    }

}
