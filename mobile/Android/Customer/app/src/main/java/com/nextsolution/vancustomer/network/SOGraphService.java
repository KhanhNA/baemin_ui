package com.nextsolution.vancustomer.network;

import com.google.gson.JsonElement;
import com.nextsolution.db.dto.BillLadingDetail;
import com.nextsolution.vancustomer.model.PlacesPOJO;
import com.ns.odoolib_retrofit.service.OdooServices;

import java.util.List;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Query;

public interface SOGraphService {
    @GET("place/textsearch/json?")
    Call<PlacesPOJO.Root> doPlaces(@Query(value = "query", encoded = true) String name
            , @Query(value = "key", encoded = true) String key);

    @GET("place/details/json?")
    Call<JsonElement> getPlaceDetail(@Query(value = "place_id", encoded = true) String place_id
            , @Query(value = "fields", encoded = true) String fields
            , @Query(value = "key", encoded = true) String key);



}
