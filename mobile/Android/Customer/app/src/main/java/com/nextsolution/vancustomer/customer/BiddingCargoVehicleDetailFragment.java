package com.nextsolution.vancustomer.customer;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.PagerSnapHelper;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.SnapHelper;

import com.nextsolution.db.dto.BiddingOrder;
import com.nextsolution.db.dto.BiddingVehicle;
import com.nextsolution.db.dto.Cargo;
import com.nextsolution.db.dto.CargoTypes;
import com.nextsolution.vancustomer.R;
import com.nextsolution.vancustomer.adapter.XBaseAdapter;
import com.nextsolution.vancustomer.customer.vm.BiddingCargoVehicleDetailVM;
import com.nextsolution.vancustomer.databinding.BiddingCargoVehicleDetailFragmentBinding;
import com.nextsolution.vancustomer.enums.Constants;
import com.nextsolution.vancustomer.scan.QrScannerActivity;
import com.nextsolution.vancustomer.util.ToastUtils;
import com.nextsolution.vancustomer.widget.SnapOnScrollListener;
import com.tsolution.base.BaseFragment;
import com.tsolution.base.BaseViewModel;
import com.tsolution.base.listener.AdapterListener;

import pub.devrel.easypermissions.EasyPermissions;

public class BiddingCargoVehicleDetailFragment extends BaseFragment {
    private final int REQUEST_PERMISSION_CAMERA = 1001;
    BiddingCargoVehicleDetailFragmentBinding mBinding;
    BiddingCargoVehicleDetailVM biddingCargoVehicleDetailVM;
    XBaseAdapter adapterTitle;
    XBaseAdapter adapterContent;
    int prePosition = 0;
    int quantityQrChecked = 0;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);
        biddingCargoVehicleDetailVM = (BiddingCargoVehicleDetailVM) viewModel;
        mBinding = (BiddingCargoVehicleDetailFragmentBinding) binding;
        setUpRecycleView();
        getData();
        return view;
    }

    private void getData() {
        try {
            Intent intent = getActivity().getIntent();
            biddingCargoVehicleDetailVM.getBiddingVehicle().set((BiddingVehicle) intent.getSerializableExtra(Constants.MODEL));
            biddingCargoVehicleDetailVM.getCargoTypes().addAll(biddingCargoVehicleDetailVM.getBiddingVehicle().get().getCargo_types());
            biddingCargoVehicleDetailVM.getBiddingOrder().set((BiddingOrder) intent.getSerializableExtra(Constants.DATA));
            biddingCargoVehicleDetailVM.getCargoTypes().get(0).checked = true;
            adapterTitle.notifyDataSetChanged();
            adapterContent.notifyDataSetChanged();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void runUi(Object[] objects) {
        String action = (String) objects[0];
        if (action.equals(Constants.GET_DATA_SUCCESS)) {
            ToastUtils.showToast(getActivity(), getString(R.string.CONFIRMED), getResources().getDrawable(R.drawable.ic_access));
            setResultData();
        } else if (action.equals(Constants.GET_DATA_FAIL)) {
            ToastUtils.showToast(getActivity(), getString(R.string.CONFIRM_FAIL), getResources().getDrawable(R.drawable.ic_fail));
        }
    }

    private void setResultData() {
        getActivity().setResult(Constants.RESULT_OK, new Intent());
        getActivity().onBackPressed();
    }

    private void setUpRecycleView() {
        //========================================recycleView Title==================================================
        adapterTitle = new XBaseAdapter(R.layout.item_title_bidding_cargo_detail, biddingCargoVehicleDetailVM.getCargoTypes(), new AdapterListener() {
            @Override
            public void onItemClick(View view, Object o) {
                int position = ((CargoTypes) o).index - 1;

                mBinding.rcCargoType.smoothScrollToPosition(position);
                mBinding.rcCargoTypeContent.smoothScrollToPosition(position);

            }

            @Override
            public void onItemLongClick(View view, Object o) {
                onItemClick(view, o);
            }
        });
        mBinding.rcCargoType.setAdapter(adapterTitle);
        mBinding.rcCargoType.setLayoutManager(new LinearLayoutManager(getContext(), RecyclerView.HORIZONTAL, false));
        //=========================================recycleView Content===============================================
        adapterContent = new XBaseAdapter(R.layout.item_content_bidding_cargo_detail, biddingCargoVehicleDetailVM.getCargoTypes(), this);
        mBinding.rcCargoTypeContent.setAdapter(adapterContent);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext(), RecyclerView.HORIZONTAL, false);
        mBinding.rcCargoTypeContent.setLayoutManager(linearLayoutManager);
        SnapHelper helper = new PagerSnapHelper();
        helper.attachToRecyclerView(mBinding.rcCargoTypeContent);

        mBinding.rcCargoTypeContent.addOnScrollListener(new SnapOnScrollListener(helper, position -> {
            if (position != prePosition) {
                biddingCargoVehicleDetailVM.getCargoTypes().get(position).checked = true;
                biddingCargoVehicleDetailVM.getCargoTypes().get(prePosition).checked = false;
                adapterTitle.notifyItemChanged(position);
                adapterTitle.notifyItemChanged(prePosition);
                mBinding.rcCargoType.smoothScrollToPosition(position);
                adapterContent.notifyItemChanged(position);
                prePosition = position;
            }
        }));
    }

    @Override
    public void onItemClick(View v, Object o) {
        if (v.getId() == R.id.item_title_cargo) {
            String description = getResources().getString(R.string.description_uncheck_qr_code) + " " + ((Cargo) o).getQr_code() + " ?";
            ConfirmDialog confirmDialog = new ConfirmDialog(R.string.title_uncheck_qr_code, description);
            confirmDialog.setTargetFragment(this, Constants.REQUEST_CODE);
            confirmDialog.show(getChildFragmentManager(), "ABC");
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        try {
            if (isScanCodeAndResultOk(requestCode, resultCode) && data != null && data.hasExtra(Constants.DATA_RESULT)) {
                biddingCargoVehicleDetailVM.getBiddingVehicle().set((BiddingVehicle) data.getSerializableExtra(Constants.DATA_RESULT));
                biddingCargoVehicleDetailVM.getCargoTypes().clear();
                biddingCargoVehicleDetailVM.getCargoTypes().addAll(biddingCargoVehicleDetailVM.getBiddingVehicle().get().getCargo_types());
                quantityQrChecked = data.getIntExtra(Constants.QUANTITY_QR_CHECKED, 0);
                adapterContent.notifyDataSetChanged();
                adapterTitle.notifyDataSetChanged();
                biddingCargoVehicleDetailVM.getIsSuccess().set(data.getBooleanExtra(Constants.SUCCESS, false));
                if(biddingCargoVehicleDetailVM.getIsSuccess().get()){
                    ToastUtils.showToast(getActivity(), getResources().getString(R.string.SUCCESS), getResources().getDrawable(R.drawable.ic_access));
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private boolean isScanCodeAndResultOk(int requestCode, int resultCode) {
        return requestCode == Constants.REQUEST_CODE_SCAN && resultCode == Constants.RESULT_OK;
    }

    @Override
    public void action(View view, BaseViewModel baseViewModel) {
        if (view.getId() == R.id.qrCode && !biddingCargoVehicleDetailVM.getIsSuccess().get()) {
            String[] perms = {Manifest.permission.CAMERA};
            if (EasyPermissions.hasPermissions(getActivity(), perms)) {
                gotoQrScanActivity();
            } else {
                // Do not have permissions, request them now
                EasyPermissions.requestPermissions(this, getString(R.string.permissions_not_granted),
                        REQUEST_PERMISSION_CAMERA, perms);
            }
        } else if (view.getId() == R.id.btnBack) {
            getActivity().onBackPressed();
        } else if (view.getId() == R.id.btnReceive) {
            biddingCargoVehicleDetailVM.confirmBiddingCargoVehicle(this::runUi);
        }
    }

    private void gotoQrScanActivity() {
            Intent qrScan = new Intent(getContext(), QrScannerActivity.class);
            Bundle bundle = new Bundle();
            bundle.putString(Constants.FROM_FRAGMENT, "BIDDING_CARGO_VEHICLE_DETAIL_FRAGMENT");
            bundle.putBoolean(Constants.SCAN_MULTI_QR_CODE, true);
            bundle.putSerializable(Constants.MODEL, biddingCargoVehicleDetailVM.getBiddingVehicle().get());
            bundle.putInt(Constants.QUANTITY_QR_CHECKED, quantityQrChecked);
            qrScan.putExtras(bundle);
            startActivityForResult(qrScan, Constants.REQUEST_CODE_SCAN);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == REQUEST_PERMISSION_CAMERA) {
            for (int item : grantResults) {
                if (item != PackageManager.PERMISSION_GRANTED) {
                    return;
                }
            }
            gotoQrScanActivity();
        }
    }

    @Override
    public int getLayoutRes() {
        return R.layout.bidding_cargo_vehicle_detail_fragment;
    }

    @Override
    public Class<? extends BaseViewModel> getVMClass() {
        return BiddingCargoVehicleDetailVM.class;
    }

    @Override
    public int getRecycleResId() {
        return 0;
    }
}
