import 'package:driver/base/ui/get/password_field.dart';
import 'package:driver/base/ui/text/text_ui.dart';
import 'package:driver/pages/login/select_language_view.dart';
import 'package:driver/pages/main/navigation_page.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'login_controller.dart';

class LoginPage extends GetView<LoginController> {
  // final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  final LoginController controller1 = Get.put(LoginController());

  @override
  Widget build(BuildContext context) {
    // double heightScreen = Get.height;
    double widthScreen = Get.width;
    // double paddingBottom = 0;

    return new Scaffold(
        appBar: new AppBar(
          title: Container(
            color: Theme.of(context).primaryColor,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: <Widget>[
                CustomSelectLanguageWidget(),
                // Container()
              ],
            ),
          ),
        ),
        body: Center(
          child: SingleChildScrollView(
            child: Padding(
              padding: EdgeInsets.only(left: 30, right: 30),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  context.myTextFormField(
                      obs: controller1.username, preIcon: Icon(Icons.person)),
                  _buildWidgetSizedBox(16.0),
                  _buildStatefulWidget(
                      PasswordField(
                        obs: controller1.password,
                      ),
                      48,
                      widthScreen),
                  _buildWidgetSizedBox(16.0),
                  _buildWidgetButtonSignin(context),
                  Padding(
                    padding: EdgeInsets.fromLTRB(8, 8, 8, 0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        InkWell(
                          child: Text(
                            'Create Account',
                            style: TextStyle(
                              color: Colors.blue,
                              fontSize: 14,
                            ),
                          ),
                          // onTap: () => Get.to(CreateAccountPage()),
                        ),
                        Text(
                          'Forget password ?',
                          style: TextStyle(color: Colors.blue, fontSize: 14),
                        ),
                      ],
                    ),
                  )
                ],
              ),
            ),
          ),
        ));
  }

  void loginTapped() {}

  Widget _buildWidgetButtonSignin(BuildContext context) {
    return SizedBox(
      width: double.infinity,
      child: ElevatedButton(
        child: Text(
          'Signin'.tr,
        ),
        style: ButtonStyle(
          // backgroundColor: MaterialStateProperty.all(Theme.of(context).buttonTheme.colorScheme.secondary),
          minimumSize: MaterialStateProperty.all(Size(20, 40)),
        ),
        onPressed: () async {
          bool isLogin = await controller1.login();
          if (isLogin) {
            Get.to(() => NavigationPage());
          }
        },
        // color: Theme.of(context).primaryColor,
      ),
    );
  }

  Widget _buildWidgetSizedBox(double height) => SizedBox(height: height);

  Widget _buildStatefulWidget(
      Widget widget, double heightScreen, double width) {
    return Container(
        height: heightScreen,
        alignment: Alignment.centerRight,
        child: Container(child: widget, width: width));
  }
}
