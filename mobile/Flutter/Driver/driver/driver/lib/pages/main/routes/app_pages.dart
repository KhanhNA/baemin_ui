import 'package:driver/pages/login/login_page.dart';
import 'package:driver/pages/main/splash/splash_page.dart';
import 'package:get/get.dart';

import '../navigation_page.dart';

part './app_routes.dart';

abstract class AppPages {
  static final pages = [
    GetPage(name: Routes.INITIAL, page:()=> SplashPage(),),
    GetPage(name: Routes.HOME, page:()=> NavigationPage(),),
    GetPage(name: Routes.LOGIN, page:()=> LoginPage(),),
  ];

}