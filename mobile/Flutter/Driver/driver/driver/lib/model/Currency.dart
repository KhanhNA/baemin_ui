import 'package:json_annotation/json_annotation.dart';

import 'base.dart';
part 'Currency.g.dart';
@JsonSerializable(explicitToJson: true)
class Currency extends _Currency {
  String error;
  Currency.withError(this.error);
  Currency();

  factory Currency.fromJson(Map<String, dynamic> js)=> Base().fromJs(js, (js) => _$CurrencyFromJson(js));

  Map<String, dynamic> toJson() => _$CurrencyToJson(this);
}
class _Currency extends Base {
    String code;
    String name;
    bool support;



    String getCode() {
        return code;
    }

    void setCode(String code) {
        this.code = code;
    }

    String getName() {
        return name;
    }

    void setName(String name) {
        this.name = name;
    }

    bool getSupport() {
        return support;
    }

    void setSupport(bool support) {
        this.support = support;
    }
}
