// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'Principal.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Principal _$PrincipalFromJson(Map<String, dynamic> json) {
  return Principal()
    ..checked = json['checked'] as bool
    ..index = json['index'] as int
    ..transactionErrCode = json['transactionErrCode'] as String
    ..id = json['id'] as int
    ..uuId = json['uuId'] as String
    ..createDate = json['createDate'] == null
        ? null
        : DateTime.parse(json['createDate'] as String)
    ..createUser = json['createUser'] as String
    ..updateDate = json['updateDate'] == null
        ? null
        : DateTime.parse(json['updateDate'] as String)
    ..updateUser = json['updateUser'] as String
    ..fId = json['@id'] as String
    ..refId = json['@ref'] as String
    ..firstName = json['firstName'] as String
    ..lastName = json['lastName'] as String
    ..mustChangePassword = json['mustChangePassword'] as bool
    ..roles = (json['roles'] as List)
        ?.map(
            (e) => e == null ? null : Role.fromJson(e as Map<String, dynamic>))
        ?.toList()
    ..menus = (json['menus'] as List)
        ?.map(
            (e) => e == null ? null : Menu.fromJson(e as Map<String, dynamic>))
        ?.toList()
    ..error = json['error'] as String;
}

Map<String, dynamic> _$PrincipalToJson(Principal instance) => <String, dynamic>{
      'checked': instance.checked,
      'index': instance.index,
      'transactionErrCode': instance.transactionErrCode,
      'id': instance.id,
      'uuId': instance.uuId,
      'createDate': instance.createDate?.toIso8601String(),
      'createUser': instance.createUser,
      'updateDate': instance.updateDate?.toIso8601String(),
      'updateUser': instance.updateUser,
      '@id': instance.fId,
      '@ref': instance.refId,
      'firstName': instance.firstName,
      'lastName': instance.lastName,
      'mustChangePassword': instance.mustChangePassword,
      'roles': instance.roles?.map((e) => e?.toJson())?.toList(),
      'menus': instance.menus?.map((e) => e?.toJson())?.toList(),
      'error': instance.error,
    };
