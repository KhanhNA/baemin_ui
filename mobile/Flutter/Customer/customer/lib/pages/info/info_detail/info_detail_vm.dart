
import 'package:customer/base/controller/BaseViewModel.dart';
import 'package:customer/base/util/AppUtils.dart';
import 'package:customer/model/login/PartnerDto.dart';
import 'package:get/get.dart';

class InfoDetailVM extends BaseVM{
  Rx<PartnerDto> partnerDto= Rx(PartnerDto());
  InfoDetailVM(){
    partnerDto.value = AppUtils.partnerDto;
  }
}