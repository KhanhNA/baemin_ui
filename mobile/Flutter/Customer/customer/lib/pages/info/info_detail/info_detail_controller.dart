import 'package:customer/base/ui/get/base_get_controller.dart';
import 'package:customer/model/login/PartnerDto.dart';
import 'package:customer/pages/info/info_detail/info_detail_vm.dart';
import 'package:customer/model/login/PartnerDto.dart';
import 'package:get/get.dart';
import 'api/info_detail_repository.dart';

class InfoDetailController extends BaseController<InfoDetailVM>{
  InfoDetailController(InfoDetailVM viewModel) : super(viewModel);
  final InfoDetailRepository repository = InfoDetailRepository();
  updateInfoCustomer(){
    Map<String,dynamic> params={

    };
    repository.updateInfoCustomer(params);
  }
}