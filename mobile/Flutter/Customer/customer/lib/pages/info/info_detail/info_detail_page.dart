import 'dart:ui';

import 'package:customer/base/ui/common_ui.dart';
import 'package:customer/base/ui/get/base_get_view.dart';
import 'package:customer/base/ui/get/my_text_form_field.dart';
import 'package:customer/base/ui/list/list_ui.dart';
import 'package:customer/pages/info/info_detail/info_detail_controller.dart';
import 'package:customer/pages/info/info_detail/info_detail_vm.dart';
import 'package:customer/theme/app_theme.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:get/get.dart';

import 'package:flutter/widgets.dart';

class InfoDetailPage extends BaseView<InfoDetailVM, InfoDetailController> {
  final _formEmailKey = GlobalKey<FormState>();
  final _formAddressKey = GlobalKey<FormState>();
  final _formZipKey = GlobalKey<FormState>();
  final _formPhoneKey = GlobalKey<FormState>();

  @override
  AppBar appBar(BuildContext context) {
    // TODO: implement appBar
    return AppBar(
      title: Container(
          child: Text(
        'menu.Info'.tr,
        style: TextStyle(
            color: Colors.black, fontWeight: FontWeight.bold, fontSize: 14),
      )),
      actions: <Widget>[
        IconButton(
          icon: Icon(Icons.settings),
          onPressed: () {
            // update info
            _showMaterialDialog(context);
          },
        ),
      ],
    );
  }

  _showMaterialDialog(BuildContext context) {
    showDialog(
        context: context,
        builder: (_) => new AlertDialog(
              title: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Text('Edit info',style: TextStyle(fontSize: 14, color: Colors.black,fontWeight: FontWeight.normal),),
                  IconButton(icon: Icon(Icons.clear), onPressed: ()=>{Navigator.of(context).pop()})
                ],
              ),
              content: Container(
                  height: MediaQuery.of(context).size.height * 0.4,
                  child: Column(
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.fromLTRB(0, 16, 0, 0),
                        child: Container(
                          width: MediaQuery.of(context).size.width * 0.6,
                          height: 40,
                          child: Form(
                              key: _formEmailKey,
                              child: TextFormField(
                                decoration: InputDecoration(labelText: 'Email'),
                                onChanged: (text) => {},
                                validator: (value) {
                                  if (value.isEmpty) {
                                    return 'Not empty';
                                  }
                                  return null;
                                },
                              )),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.fromLTRB(0, 16, 0, 0),
                        child: Container(
                          width: MediaQuery.of(context).size.width * 0.6,
                          height: 40,
                          child: Form(
                              key: _formPhoneKey,
                              child: TextFormField(
                                decoration: InputDecoration(labelText: 'Phone'),
                                onChanged: (text) => {},
                                validator: (value) {
                                  if (value.isEmpty) {
                                    return 'Not empty';
                                  }
                                  return null;
                                },
                              )),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.fromLTRB(0, 16, 0, 0),
                        child: Container(
                          width: MediaQuery.of(context).size.width * 0.6,
                          height: 40,
                          child: Form(
                              key: _formAddressKey,
                              child: TextFormField(
                                decoration:
                                    InputDecoration(labelText: 'Address'),
                                onChanged: (text) => {},
                                validator: (value) {
                                  if (value.isEmpty) {
                                    return 'Not empty';
                                  }
                                  return null;
                                },
                              )),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.fromLTRB(0, 16, 0, 0),
                        child: Container(
                          width: MediaQuery.of(context).size.width * 0.6,
                          height: 40,
                          child: Form(
                              key: _formZipKey,
                              child: TextFormField(
                                decoration: InputDecoration(labelText: 'Zip'),
                                onChanged: (text) => {},
                                validator: (value) {
                                  if (value.isEmpty) {
                                    return 'Not empty';
                                  }
                                  return null;
                                },
                              )),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.fromLTRB(0, 16, 0, 0),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                          //   ElevatedButton(
                          //     onPressed: () => {Navigator.of(context).pop()},
                          //     child: Container(
                          //       child: Center(
                          //           child: Text(
                          //         'No',
                          //         style: TextStyle(
                          //             color: Colors.black,
                          //             fontSize: 14,
                          //             fontWeight: FontWeight.normal),
                          //       )),
                          //       width: MediaQuery.of(context).size.width * 0.25,
                          //       height: 40,
                          //     ),
                          //   ),
                            Padding(
                              padding: const EdgeInsets.fromLTRB(8.0, 0, 0, 0),
                              child: ElevatedButton(
                                onPressed: () => {Navigator.of(context).pop()},
                                child: Container(
                                  child: Center(
                                      child: Text(
                                    'Yes',
                                    style: TextStyle(
                                        color: Colors.black,
                                        fontSize: 14,
                                        fontWeight: FontWeight.normal),
                                  )),
                                  width:
                                      MediaQuery.of(context).size.width * 0.3,
                                  height: 40,
                                ),
                              ),
                            ),
                          ],
                        ),
                      )
                    ],
                  )),
            ));
  }


  @override
  Widget body(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(8, 0, 8, 0),
      child: Container(
        child: Column(
          children: <Widget>[
            _componentInfo(),
            _contentInfo(context),
            context.divider(),
          ],
        ),
      ),
    );
  }

  Widget _contentInfo(BuildContext context) {
    return Container(
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Column(
          children: <Widget>[
            context.divider(),
            // context.listItemView("", notificationDto.title),
          ],
        ),
      ),
    );
  }

  Widget _componentInfo() {
    return Container(
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Row(
          children: <Widget>[
            Container(
              height: 100,
              width: 100,
              decoration: BoxDecoration(
                shape: BoxShape.circle,
              ),
              child: viewModel?.partnerDto?.value?.uri_path != null
                  ? Image.network(
                      viewModel?.partnerDto?.value?.uri_path,
                      fit: BoxFit.fill,
                    )
                  : Image.asset("assets/icons/no_image.png"),
            ),
            Container(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  Text(
                    viewModel?.partnerDto?.value?.name ?? '',
                    textAlign: TextAlign.left,
                    style: TextStyle(
                      color: Colors.black,
                      fontSize: 14,
                      fontWeight: FontWeight.normal,
                    ),
                  ),
                  Text(
                    viewModel?.partnerDto?.value?.street ?? '',
                    textAlign: TextAlign.left,
                    style: TextStyle(
                      color: Colors.black,
                      fontSize: 14,
                      fontWeight: FontWeight.normal,
                    ),
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }

  @override
  InfoDetailController putController() {
    return InfoDetailController(InfoDetailVM());
  }
}
