import 'package:customer/base/auth/api_auth_provider.dart';
import 'package:customer/base/util/AppUtils.dart';
import 'package:customer/pages/login/login_page.dart';
import 'package:customer/pages/main/routes/app_pages.dart';
import 'package:get/get.dart';
import 'package:customer/base/ui/get/base_get_controller.dart';

import 'infoVM.dart';


class InfoController extends BaseController<InfoVM>{
  InfoController(InfoVM viewModel) : super(viewModel);
  ApiAuthProvider api = ApiAuthProvider();
  logout() async {
    final bool = await api.logout();
    if(bool){
      AppUtils.partnerDto=null;
      AppUtils.odooSessionDto=null;
      Get.to(LoginPage());
    }
  }
}