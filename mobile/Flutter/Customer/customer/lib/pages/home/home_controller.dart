import 'package:customer/base/controller/BaseViewModel.dart';
import 'package:customer/base/ui/get/base_get_controller.dart';
import 'package:customer/enum/notification_type.dart';
import 'package:customer/model/list/PagedListData.dart';
import 'package:customer/model/notification/NotificationDto.dart';
import 'package:customer/pages/common_list/list_controller.dart';
import 'package:customer/pages/home/home_vm.dart';

class HomeController extends BaseController<HomeVM>{
  HomeController(BaseVM vm) : super(vm){
    viewModel.listPromotion = ListController(loadFunc: this.getNotifications);
  }
  Future<PagedListData<NotificationDto>> getNotifications(int page) async {
    Map<String, dynamic> params = {
      "type": NotificationType.SYSTEM,
      "offset": (page-1)*10,
      "limit":10
    };
    final data=await viewModel.repository.getNotifications(params);
    return data;
  }
  details(notification){

  }

}