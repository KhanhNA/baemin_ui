import 'package:customer/base/ui/list/list_ui.dart';
import 'package:customer/model/notification/NotificationDto.dart';
import 'package:customer/pages/home/home_controller.dart';
import 'package:flutter/material.dart';

class HomeItemBuilder {
  HomeController controller;

  HomeItemBuilder(this.controller);

  Widget buildItem(
      BuildContext context, NotificationDto notificationDto, int index) {
    double itemWidth = MediaQuery.of(context).size.width* 0.4;
    // double imageWidth = deviceWidth * 0.4;
    return Padding(
      padding: const EdgeInsets.only(left: 10.0, right: 10,top: 5),
      child: Container(
          child: Column(
        children: [
          Container(
              width: itemWidth,
              height: itemWidth,
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(12), color: Colors.white),
              child: notificationDto.image_256==null? Image.asset('assets/images/wallet.png', fit: BoxFit.fill) : Image.network(
                notificationDto.image_256,
                fit: BoxFit.fill,
              )),
          Padding(
            padding: const EdgeInsets.only(top:3.0),
            child: Container(
              width: itemWidth,
              child: context.normalTextOneLine(notificationDto.title),
            ),
          ),
        ],
      )
          // child: _build(deviceWidth),
          ),
    );
  }
}
