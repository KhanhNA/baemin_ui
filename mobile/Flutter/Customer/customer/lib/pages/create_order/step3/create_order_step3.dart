import 'package:customer/base/ui/common_ui.dart';
import 'package:customer/base/ui/get/base_get_view.dart';
import 'package:customer/base/ui/list/list_ui.dart';
import 'package:customer/model/insurance/InsuranceDto.dart';
import 'package:customer/model/insurance/RecurrentModel.dart';
import 'package:customer/model/insurance/SubscribeDto.dart';
import 'package:customer/model/order_package/OrderPackageDto.dart';
import 'package:customer/pages/create_order/create_order_controller.dart';
import 'package:customer/pages/create_order/create_order_vm.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter_autolink_text/flutter_autolink_text.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:get/get.dart';

class CreateOrderStep3 extends BaseView<CreateOrderVM, CreateOrderController> {
  @override
  Widget body(BuildContext context) {
    List<Widget> listWidgets =
        viewModel.billLadingDto.value.arrBillLadingDetail.map((e) {
      return e.warehouse_type == '0'
          ? Container()
          : Padding(
              padding: const EdgeInsets.only(left: 16, top: 4.0),
              child: Container(
                width: MediaQuery.of(context).size.width * 0.85,
                decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(8),
                    border: Border.all(color: Colors.black12)),
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        context
                            .titleText(e.warehouse?.name ?? 'Export warehouse'),
                        context.normalText(e.warehouse?.address ?? 'Address'),
                        AutolinkText(
                          text: e.warehouse?.phone ?? "0123456789",
                          textStyle: TextStyle(color: Colors.green),
                          linkStyle: TextStyle(color: Colors.green),
                          onPhoneTap: (link) => print('Clicked: ' + link),
                        )
                      ]),
                ),
              ),
            );
    }).toList();
    return SingleChildScrollView(
      child: Container(
          child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          context.normalText('Kho xuất hàng'),
          Padding(
            padding: const EdgeInsets.only(top: 8.0, left: 16),
            child: Container(
                width: MediaQuery.of(context).size.width * 0.85,
                decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(8),
                    border: Border.all(color: Colors.black12)),
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        context.titleText(viewModel.exportBillLadingDetail.value
                                ?.warehouse?.name ??
                            'Export warehouse'),
                        context.normalText(viewModel.exportBillLadingDetail
                                .value?.warehouse?.address ??
                            'Address'),
                        AutolinkText(
                          text: viewModel.exportBillLadingDetail.value
                                  ?.warehouse?.phone ??
                              "0123456789",
                          textStyle: TextStyle(color: Colors.green),
                          linkStyle: TextStyle(color: Colors.green),
                          onPhoneTap: (link) => print('Clicked: ' + link),
                        )
                      ]),
                )),
          ),
          context.normalText('Kho nhập hàng'),
          Wrap(
            children: listWidgets,
          ),
          context.divider(),
          Padding(
            padding: const EdgeInsets.only(top: 8.0),
            child: Row(
              children: <Widget>[
                context.normalText('Bảo hiểm'),
                Padding(
                  padding: const EdgeInsets.only(left:8.0),
                  child: InkWell(
                    onTap: () => _selectInsurance(context),
                    child: Obx(()=>Text(
                      viewModel.billLadingDto?.value?.insurance?.name??
                      '[Chọn loại bảo hiểm]',
                      style: TextStyle(
                          color: Colors.green,
                          fontSize: 13,
                          fontWeight: FontWeight.bold),
                    ),)
                  ),
                )
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 8.0),
            child: Row(
              children: <Widget>[
                context.normalText('Ngày bắt đầu'),
                Padding(
                  padding: const EdgeInsets.only(left: 8.0),
                  child: InkWell(
                    onTap: () => _selectDate(context),
                    child: Row(
                      children: [
                        Obx(
                          () => Text(
                            CommonUI.timeFormatDate(
                                viewModel.billLadingDto.value.start_date ??
                                    DateTime.now()),
                            style: TextStyle(
                                color: Colors.green,
                                fontSize: 13,
                                fontWeight: FontWeight.bold),
                          ),
                        ),
                        Icon(Icons.calendar_today_outlined)
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 8.0),
            child: Row(
              children: <Widget>[
                context.normalText('Ngày kết thúc'),
                Padding(
                  padding: const EdgeInsets.only(left: 8.0),
                  child: Row(
                    children: [
                      Obx(
                        () => Text(
                          CommonUI.timeFormatDate(
                              viewModel.billLadingDto.value.end_date ??
                                  DateTime.now()),
                          style: TextStyle(
                              color: Colors.black,
                              fontSize: 13,
                              fontWeight: FontWeight.bold),
                        ),
                      ),
                      Icon(Icons.calendar_today_outlined)
                    ],
                  ),
                ),
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: InkWell(
              onTap: () => _selectOrderPackage(context),
              child: Container(
                height: 40,
                width: double.infinity,
                decoration: BoxDecoration(
                    color: Colors.white,
                    border: Border.all(color: Colors.black12),
                    borderRadius: BorderRadius.circular(8)),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.only(left: 8.0),
                      child: Obx(() => context.normalText(
                          viewModel.billLadingDto.value.order_package?.name ??
                              'Chọn gói vận chuyển')),
                    ),
                    Icon(
                      Icons.keyboard_arrow_down,
                      size: 24,
                    )
                  ],
                ),
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: InkWell(
              onTap: () => _selectSubscribe(context),
              child: Container(
                height: 40,
                width: double.infinity,
                decoration: BoxDecoration(
                    color: Colors.white,
                    border: Border.all(color: Colors.black12),
                    borderRadius: BorderRadius.circular(8)),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.only(left: 8.0),
                      child: Obx(() => context.normalText(
                          viewModel.billLadingDto.value.subscribe?.name ??
                              'Chọn thuê bao vận chuyển')),
                    ),
                    Icon(
                      Icons.keyboard_arrow_down,
                      size: 24,
                    )
                  ],
                ),
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: InkWell(
              onTap: () => _selectFrequency(context),
              child: Container(
                height: 40,
                width: double.infinity,
                decoration: BoxDecoration(
                    color: Colors.white,
                    border: Border.all(color: Colors.black12),
                    borderRadius: BorderRadius.circular(8)),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.only(left: 8.0),
                      child: Obx(() => context.normalText(
                          viewModel.recurrentModel.value?.recurrentModelStr()??
                              'Chọn tần suất vận chuyển')),
                    ),
                    Icon(
                      Icons.keyboard_arrow_down,
                      size: 24,
                    )
                  ],
                ),
              ),

            ),
          ),
          context.divider(),
          context.listItemView('Số lượng gói hàng',
              viewModel.quantity_package.value.toString() ?? '0.0'),
          context.listItemView('Thể tích ước tính',
              viewModel.billLadingDto.value.total_volume?.toString() ?? '0.0'),
          context.listItemView('Cân nặng ước tính',
              viewModel.billLadingDto.value.total_weight?.toString() ?? '0.0'),
          context.divider(),
          context.listItemView(
              'Giá chưa chiết khấu',
              viewModel.billLadingDto.value.price_not_discount?.toString() ??
                  '0.0'),
          context.listItemView(
              'Phí bảo hiểm',
              viewModel.billLadingDto.value.insurance_price?.toString() ??
                  '0.0'),
          context.listItemView('Phí dịch vụ',
              viewModel.billLadingDto.value.service_price?.toString() ?? '0.0'),
          context.divider(),
          context.listItemView('Tổng tiền',
              viewModel.billLadingDto.value.total_amount?.toString() ?? '0.0'),
          ElevatedButton(
              onPressed: ()async{
                await controller.createOrder();
              }, child: Center(child: Text('Confirm')))
        ],
      )),
    );
  }

  @override
  CreateOrderController putController() {
    return CreateOrderController(CreateOrderVM());
  }

  _selectSubscribe(BuildContext context) {
    context.showBottomSheet<SubscribeDto>(
        Padding(
          padding: const EdgeInsets.only(left: 8.0),
          child: Text(('Subscribe').tr),
        ), (context, item, index) {
      return Padding(
        padding: const EdgeInsets.fromLTRB(8, 8, 8, 0),
        child: Container(
            child: InkWell(
                onTap: () => {
                      viewModel.billLadingDto.value.setEndDate(item),
                      viewModel.billLadingDto.value.subscribe = item,
                      viewModel.billLadingDto.refresh(),
                      if (viewModel.recurrentModel.value == null)
                        {viewModel.recurrentModel.value = new RecurrentModel()},
                      viewModel.recurrentModel.value.frequency = null,
                      Navigator.pop(context)
                    },
                child: Padding(
                  padding: const EdgeInsets.fromLTRB(8, 4, 8, 0),
                  child: Column(
                    children: [
                      Container(
                        height: 40,
                        width: double.infinity,
                        child: Container(
                            child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: <Widget>[
                              context.normalText(item.name),
                            ])),
                      ),
                      context.divider()
                    ],
                  ),
                ))),
      );
    }, viewModel.subscribeListController);
  }
  _selectInsurance(BuildContext context) {
    context.showBottomSheet<InsuranceDto>(
        Padding(
          padding: const EdgeInsets.only(left: 8.0),
          child: Text(('Choose Insurance').tr),
        ), (context, item, index) {
      return Padding(
        padding: const EdgeInsets.fromLTRB(8, 8, 8, 0),
        child: Container(
            child: InkWell(
                onTap: () => {
                      viewModel.billLadingDto.value.insurance= item,
                      viewModel.billLadingDto.refresh(),
                      Navigator.pop(context)
                    },
                child: Padding(
                  padding: const EdgeInsets.fromLTRB(8, 4, 8, 0),
                  child: Column(
                    children: [
                      Container(
                        height: 40,
                        width: double.infinity,
                        child: Container(
                            child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: <Widget>[
                              context.normalText(item.name),
                            ])),
                      ),
                      context.divider()
                    ],
                  ),
                ))),
      );
    }, viewModel.insuranceListController);
  }

  _selectFrequency(BuildContext context) {
    viewModel.recurrentModel.value.day_of_week=0;
    viewModel.recurrentModel.value.day_of_month=0;
    viewModel.recurrentModel.value.frequency=0;
    context.showBottomSheetV2<SubscribeDto>(Container(
        child: Column(
      children: <Widget>[
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  context.titleText('Choose Frequency'),
                  IconButton(
                      icon: Icon(Icons.clear),
                      onPressed: () => {
                        handleSelectSub(),
                        Navigator.of(context).pop()
                      })
                ],
              ),
              Padding(
                padding: const EdgeInsets.only(bottom: 16.0),
                child: Container(
                  child: Obx(() => Column(
                        children: <Widget>[
                          Container(
                            height: 36,
                            child: RadioListTile(
                              title: Text(viewModel.listFrequency[0]),
                              value: viewModel.listFrequency[0],
                              groupValue: viewModel.frequencyObs.value,
                              onChanged: (val) {
                                viewModel.selectedPeriod = val;
                                viewModel.recurrentModel.value.frequency=0;
                                viewModel.recurrentModel.refresh();
                                handleSelectSub();
                                Navigator.pop(context);
                              },
                            ),
                          ),
                          Container(
                            height: 36,
                            child: RadioListTile(
                              title: Text(viewModel.listFrequency[1]),
                              value: viewModel.listFrequency[1],
                              groupValue: viewModel.frequencyObs.value,
                              onChanged: (val) {
                                viewModel.frequencyObs.value = val;
                                viewModel.recurrentModel.value.frequency=1;
                                viewModel.recurrentModel.refresh();
                              },
                            ),
                          ),
                          Container(
                            height: 36,
                            child: RadioListTile(
                              title: Text(viewModel.listFrequency[2]),
                              value: viewModel.listFrequency[2],
                              groupValue: viewModel.frequencyObs.value,
                              onChanged: (val) {
                                viewModel.frequencyObs.value = val;
                                viewModel.recurrentModel.value.frequency=2;
                                viewModel.recurrentModel.refresh();
                              },
                            ),
                          ),
                        ],
                      )),
                ),
              ),
              Obx(() => viewModel.recurrentModel.value.frequency==1
                  ? Padding(
                      padding: const EdgeInsets.only(left: 16.0, right: 16),
                      child: Container(
                        height: 46,
                        child: GridView.count(
                            controller: ScrollController(),
                            crossAxisCount: 7,
                            children: List.generate(viewModel.listDay.length,
                                (index) {
                              return InkWell(
                                onTap: () =>
                                    {viewModel.recurrentModel.value.day_of_week = index,
                                      viewModel.recurrentModel.refresh(),
                                      handleSelectSub(),
                                      Navigator.pop(context)
                                    },
                                child: Center(
                                  child: Padding(
                                    padding: const EdgeInsets.only(
                                        right: 4.0, left: 4),
                                    child: Container(
                                        height: 30,
                                        decoration: BoxDecoration(
                                            borderRadius:
                                                BorderRadius.circular(4),
                                            color: viewModel.recurrentModel.value.day_of_week ==
                                                    index
                                                ? Colors.green
                                                : Colors.white,
                                            border: Border.all(
                                                color: Colors.black12)),
                                        child: Padding(
                                          padding: const EdgeInsets.all(4),
                                          child: Center(
                                              child: context.normalText(
                                                  viewModel
                                                      .listDay.value[index],
                                                  color: viewModel.recurrentModel.value.day_of_week ==
                                                          index
                                                      ? Colors.white
                                                      : Colors.black)),
                                        )),
                                  ),
                                ),
                              );
                            })),
                      ),
                    )
                  : Container()),
              Obx(() => viewModel.recurrentModel.value.frequency==2
                  ? Padding(
                      padding: const EdgeInsets.only(left: 16.0, right: 16),
                      child: Container(
                        height: 200,
                        child: GridView.count(
                            controller: ScrollController(),
                            crossAxisCount: 8,
                            children: List.generate(viewModel.listDate.length,
                                (index) {
                              return InkWell(
                                onTap: () =>
                                    {viewModel.recurrentModel.value.day_of_month = index,
                                      viewModel.recurrentModel.refresh(),
                                      handleSelectSub(),
                                      Navigator.pop(context)
                                    },
                                child: Center(
                                  child: Padding(
                                    padding: const EdgeInsets.only(
                                        right: 4.0, left: 4),
                                    child: Container(
                                        height: 36,
                                        decoration: BoxDecoration(
                                          shape: BoxShape.circle,
                                          color: viewModel.recurrentModel.value.day_of_month ==
                                                  index
                                              ? Colors.green
                                              : Colors.white,
                                          // border: Border.all(
                                          //     color: Colors.black12)
                                        ),
                                        child: Padding(
                                          padding: const EdgeInsets.all(4),
                                          child: Center(
                                              child: context.normalText(
                                                  viewModel
                                                      .listDate.value[index]
                                                      .toString(),
                                                  color: viewModel.recurrentModel.value.day_of_month ==index
                                                      ? Colors.white
                                                      : Colors.black)),
                                        )),
                                  ),
                                ),
                              );
                            })),
                      ),
                    )
                  : Container()),
      ],
    )));
  }
  handleSelectSub(){
    viewModel.recurrentModel.refresh();
    viewModel.billLadingDto.value.frequency = viewModel.recurrentModel.value.frequency;
    viewModel.billLadingDto.value.day_of_week = viewModel.recurrentModel.value.day_of_week;
    viewModel.billLadingDto.value.day_of_month = viewModel.recurrentModel.value.day_of_month;
    controller.calculatorPrice();
  }

  _selectOrderPackage(BuildContext context) {
    context.showBottomSheet<OrderPackageDto>(
        Padding(
          padding: const EdgeInsets.only(left: 8.0),
          child: Text(('Choose Order package').tr),
        ), (context, item, index) {
      return Padding(
        padding: const EdgeInsets.fromLTRB(8, 8, 8, 0),
        child: Container(
            child: InkWell(
                onTap: () => {
                      viewModel.billLadingDto.value.order_package = item,
                      viewModel.billLadingDto.refresh(),
                      controller.calculatorPrice(),
                      Navigator.pop(context)
                    },
                child: Padding(
                  padding: const EdgeInsets.fromLTRB(8, 4, 8, 0),
                  child: Column(
                    children: [
                      Container(
                        height: 40,
                        width: double.infinity,
                        child: Container(
                            child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: <Widget>[
                              context.normalText(item.name ?? 'Name'),
                            ])),
                      ),
                      context.divider()
                    ],
                  ),
                ))),
      );
    }, viewModel.orderPackageListController);
  }

  _selectDate(BuildContext context) async {
    final DateTime picked = await showDatePicker(
      context: context,
      initialDate: viewModel.billLadingDto.value.start_date ?? DateTime.now(),
      firstDate: DateTime.now(),
      lastDate: DateTime(2030),
    );
    if (picked != null) {
      // picked.
      viewModel.picked.value = picked;
      viewModel.billLadingDto.value.start_date = picked;
      viewModel.billLadingDto.value
          .setEndDate(viewModel.billLadingDto.value.subscribe);
      viewModel.billLadingDto.refresh();
    }
  }
}
