import 'package:customer/model/AreaDistanceDto.dart';
import 'package:customer/model/bill_lading/BillLadingDetailDto.dart';
import 'package:customer/model/bill_lading/BillLadingDto.dart';
import 'package:customer/model/bill_package/BillServiceDto.dart';
import 'package:customer/model/insurance/InsuranceDto.dart';
import 'package:customer/model/insurance/SubscribeDto.dart';
import 'package:customer/model/list/PagedListData.dart';
import 'package:customer/model/order_package/OrderPackageDto.dart';
import 'package:customer/model/product_type/ProductTypeDto.dart';
import 'package:customer/model/warehouse/WarehouseDto.dart';
import 'package:customer/pages/create_order/api/create_order_provider.dart';

class CreateOrderRepository{
    CreateOrderProvider api= CreateOrderProvider();
    Future<PagedListData<WarehouseDto>> getWarehouse(Map<String, dynamic> params) => api.getWarehouse(params);
    Future<PagedListData<BillServiceDto>> getBillService(Map<String, dynamic> params) => api.getBillService(params);
    Future<PagedListData<AreaDistanceDto>> getAreaDistances(Map<String, dynamic> params) => api.getAreaDistances(params);
    Future<PagedListData<SubscribeDto>> getSubscribe(Map<String, dynamic> params) => api.getSubscribe(params);
    Future<PagedListData<InsuranceDto>> getInsurance(Map<String, dynamic> params) => api.getInsurance(params);
    Future<PagedListData<OrderPackageDto>> getOrderPackage(Map<String, dynamic> params) => api.getOrderPackage(params);
    Future<BillLadingDto> calculatorPrice(BillLadingDto billLadingDto) => api.calculatorPrice(billLadingDto);
    Future<PagedListData<ProductTypeDto>> getProductType(Map<String, dynamic> params) => api.getProductType(params);
    Future<bool> checkProductType(Map<String, dynamic> params) => api.checkProductType(params);
    Future<double> createOrder(BillLadingDto dto) => api.createOrder(dto);
}