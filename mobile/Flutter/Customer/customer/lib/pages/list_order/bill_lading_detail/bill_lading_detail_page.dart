import 'package:customer/base/ui/common_ui.dart';
import 'package:customer/base/ui/get/base_get_view.dart';
import 'package:customer/model/bill_lading/BillLadingDto.dart';
import 'package:customer/pages/list_order/bill_lading_detail/bill_lading_detail_vm.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:get/get.dart';
import 'package:customer/base/ui/list/list_ui.dart';

import 'bill_lading_detail_item.dart';
import 'bill_lading_detail_controller.dart';

class BillLadingDetailPage
    extends BaseView<BillLadingDetailVM, BillLadingDetailController> {
  BillLadingDetailPage(int billLadingId) {
    controller.initData(billLadingId);
  }

  @override
  AppBar appBar(BuildContext context) {
    return AppBar(
        centerTitle: true,
        title: Obx(() => Text(
              (viewModel?.billLadingDto?.value?.name ?? 'Bill lading detail'),
              style: TextStyle(
                  color: Colors.black,
                  fontSize: 16,
                  fontWeight: FontWeight.bold),
            )));
  }

  @override
  Widget body(BuildContext context) {
    return SliverView(
      toolbarHeight: 200.0,
      floatBar: Obx(() => Column(
            // mainAxisSize: MainAxisSize.max,
            children: <Widget>[
              context.listItemView('Bill_lading.bill_lading_code'.tr,
                  viewModel?.billLadingDto?.value?.name),
              context.listItemView('Bill_lading.order_package'.tr,
                  viewModel?.billLadingDto?.value?.order_package?.name),
              context.listItemView('Bill_lading.start_time'.tr,
                  CommonUI.timeFormatDate(viewModel?.billLadingDto?.value?.start_date)),
              context.listItemView('Bill_lading.end_time'.tr,
                CommonUI.timeFormatDate(viewModel?.billLadingDto?.value?.end_date)),
              context.listItemView('Bill_lading.total_volume'.tr,
                  viewModel?.billLadingDto?.value?.total_volume?.toString()),
              context.listItemView('Bill_lading.total_weight'.tr,
                  viewModel?.billLadingDto?.value?.total_weight?.toString()),
              context.listItemView('Bill_lading.number_package'.tr,
                  viewModel?.billLadingDto?.value?.total_parcel?.toString()),
            ],
          )),
      listController: viewModel.listController,
      itemBuilder: BillLadingDetailItem(controller).buildItem,
    );
  }

  @override
  BillLadingDetailController putController() {
    return BillLadingDetailController(BillLadingDetailVM());
  }
}
