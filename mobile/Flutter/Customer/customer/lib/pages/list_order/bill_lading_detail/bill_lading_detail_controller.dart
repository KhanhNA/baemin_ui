import 'package:customer/base/controller/BaseZone.dart';
import 'package:customer/base/ui/get/base_get_controller.dart';
import 'package:customer/model/bill_lading/BillLadingDetailDto.dart';
import 'package:customer/model/bill_lading/BillLadingDto.dart';
import 'package:customer/model/list/PagedListData.dart';
import 'package:customer/pages/common_list/list_controller.dart';
import 'package:customer/pages/list_order/bill_lading_detail/api/bill_lading_detail_repository.dart';
import 'package:customer/pages/list_order/bill_lading_detail/bill_lading_detail_vm.dart';
import 'package:customer/pages/list_order/order_follow_warehouse/order_follow_warehouse_page.dart';
import 'package:customer/pages/notification/notification_detail_page.dart';
import 'package:get/get.dart';

class BillLadingDetailController extends BaseController<BillLadingDetailVM> {
  BillLadingDetailController(BillLadingDetailVM vm) : super(vm) {
    viewModel.listController = ListController(
        loadFunc: (page) async => PagedListData.withContent(
            viewModel.billLadingDto?.value?.arrBillLadingDetail));
  }

  initData(int billLadingId) async {
    viewModel.billLadingDto.value =
        await this.getBillLadingDetail(billLadingId);
    viewModel.listController.refresh();
  }

  final repository = BillLadingDetailRepository();

  Future<BillLadingDto> getBillLadingDetail(int bill_ladding_id) async {
    Map<String, dynamic> params = {'bill_ladding_id': bill_ladding_id};
    final data = await repository.getBillLadings(params);
    return data;
  }

  details(billLadingDetailDto) {
    BaseZone.run(
            () => Get.to(()=>OrderFollowWarehousePage(billLadingDetailDto)));
  }
}
