import 'package:customer/base/ui/get/base_get_view.dart';
import 'package:customer/base/ui/list/list_ui.dart';
import 'package:customer/enum/warehouse_type.dart';
import 'package:customer/model/bill_lading/BillLadingDetailDto.dart';
import 'package:customer/pages/list_order/order_follow_warehouse/order_follow_warehouse_controller.dart';
import 'package:customer/pages/list_order/order_follow_warehouse/order_follow_warehouse_vm.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:get/get.dart';

import 'order_follow_warehouse_item.dart';

class OrderFollowWarehousePage extends BaseView<OrderFollowWarehouseVM,OrderFollowWarehouseController>{

  OrderFollowWarehousePage(BillLadingDetailDto billLadingDetailDto){
    viewModel.billLadingDetailDto.value =billLadingDetailDto;
  }
  @override
  AppBar appBar(BuildContext context) {
    return AppBar(
      title: Text('Bill_lading.order_follow_warehouse_title'.tr),
      centerTitle: true,
    );
  }

  @override
  Widget body(BuildContext context) {
    return SliverView(
      toolbarHeight: 200.0,
      floatBar: Obx(() => Column(
        // mainAxisSize: MainAxisSize.max,
        children: <Widget>[
          context.listItemView(viewModel?.billLadingDetailDto?.value?.name,
              ''),
          context.listItemView(viewModel?.billLadingDetailDto?.value?.warehouse?.name,
              viewModel?.billLadingDetailDto?.value?.warehouse_type==WarehouseType().Export? 'Bill_lading.warehouse_export'.tr : 'Bill_lading.warehouse_import'.tr),
          context.listItemView('Bill_lading.adress'.tr,
              viewModel?.billLadingDetailDto?.value?.address),
        ],
      )),
      listController: viewModel.listController,
      itemBuilder: OrderFollowWarehouseItem(controller).buildItem,
    );
  }

  @override
  OrderFollowWarehouseController putController() {
    return OrderFollowWarehouseController(OrderFollowWarehouseVM());
  }
}