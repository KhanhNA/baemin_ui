import 'package:customer/model/bill_lading/BillLadingDto.dart';
import 'package:customer/model/list/PagedListData.dart';
import 'list_order_provider.dart';

class ListOrderTapViewRepository{
  final ListOrderTapViewProvider _api = ListOrderTapViewProvider();

  Future<PagedListData<BillLadingDto>> getBillLadings(Map<String,dynamic> params) async =>
      _api.getBillLadings(params);
}