import 'package:customer/base/ui/list/list_ui.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'list_order_tap_view_controller.dart';

class SearchBar extends GetView<ListOrderTapViewController> {
  final ListOrderTapViewController controller;
  SearchBar(this.controller);

  @override
  Widget build(BuildContext context)  {
    return  Column(
      children: [
        context.textSearch(
            onchange: (text) {
              controller.viewModel.searchKeyword = text;
            },
            hintText: 'import.search.hintText'.tr,
            initValue: controller.viewModel.searchKeyword
        ),
      ],
    );
  }
}
