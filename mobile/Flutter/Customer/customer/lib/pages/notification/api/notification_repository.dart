

import 'package:customer/model/list/PagedListData.dart';
import 'package:customer/model/notification/NotificationDto.dart';

import 'notification_provider.dart';

class NotificationRepository{

  final NotificationProvider _api = NotificationProvider();

  Future<PagedListData<NotificationDto>> getNotifications(Map<String, dynamic> params) =>
      _api.getNotifications(params);
  Future<PagedListData<NotificationDto>> seenNotification(Map<String, dynamic> params) =>
      _api.seenNotification(params);

}