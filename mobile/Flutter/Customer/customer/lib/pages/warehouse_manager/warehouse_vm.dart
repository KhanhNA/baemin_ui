import 'package:customer/base/controller/BaseViewModel.dart';
import 'package:customer/model/warehouse/AdministrativeDto.dart';
import 'package:customer/model/warehouse/WarehouseDto.dart';
import 'package:customer/pages/common_list/list_controller.dart';
import 'package:get/get.dart';

import 'api/warehouse_repository.dart';

class WarehouseVM extends BaseVM {
  ListController<WarehouseDto> listController;
  ListController<AdministrativeDto> listAddressAreaController;
  final repository = WarehouseRepository();
  Rx<WarehouseDto> warehouseDto = Rx(WarehouseDto());

  // RxInt area_parent_id=0.obs;
  int area_parent_id=0;
  RxInt index=0.obs;
  RxString title_choose_area_address='province'.obs;
  WarehouseDto warehouseTemp=WarehouseDto();
}
