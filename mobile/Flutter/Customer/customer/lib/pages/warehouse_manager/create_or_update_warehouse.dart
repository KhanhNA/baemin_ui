import 'package:customer/base/ui/get/base_get_view.dart';
import 'package:customer/base/ui/list/list_ui.dart';
import 'package:customer/model/warehouse/AdministrativeDto.dart';
import 'package:customer/model/warehouse/WarehouseDto.dart';
import 'package:customer/pages/warehouse_manager/warehouse_controller.dart';
import 'package:customer/pages/warehouse_manager/warehouse_vm.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:get/get.dart';

class CreateOrUpdateWarehouse
    extends BaseView<WarehouseVM, WarehouseController> {
  final _formWarehouseNameKey = GlobalKey<FormState>();
  final _formPhoneNumberKey = GlobalKey<FormState>();
  final _formStreetKey = GlobalKey<FormState>();

  @override
  AppBar appBar(BuildContext context) {
    // TODO: implement appBar
    return AppBar(title: Text('Warehouse manager'));
  }

  @override
  Widget body(BuildContext context) {
    return Container(
      child: Padding(
        padding: const EdgeInsets.only(top: 8.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            context.titleText('Liên lạc'),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Container(
                height: 40,
                child: Form(
                    key: _formWarehouseNameKey,
                    child: TextFormField(
                      controller: TextEditingController(
                          text: viewModel?.warehouseDto?.value?.name ?? ''),
                      decoration: InputDecoration(labelText: 'Warehouse name'),
                      onChanged: (text) => {},
                      validator: (value) {
                        if (value.isEmpty) {
                          return 'Not empty';
                        }
                        return null;
                      },
                    )),
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Container(
                height: 40,
                child: Form(
                    key: _formPhoneNumberKey,
                    child: TextFormField(
                      keyboardType: TextInputType.number,
                      controller: TextEditingController(
                          text: viewModel?.warehouseDto?.value?.phone ?? ''),
                      decoration: InputDecoration(labelText: 'Phone number'),
                      onChanged: (text) => {},
                      validator: (value) {
                        if (value.isEmpty) {
                          return 'Not empty';
                        }
                        return null;
                      },
                    )),
              ),
            ),
            context.divider(),
            context.titleText('Địa chỉ'),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: InkWell(
                onTap: () =>
                    {print('select address'), _selectAddressArea(context,0)},
                child: Container(
                  height: 40,
                  decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(8),
                      border: Border.all(color: Colors.black12)),
                  child: Padding(
                    padding: const EdgeInsets.only(left:10.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Obx(()=>context.normalText(
                            viewModel.warehouseDto?.value?.getAdministrative() ??
                                'Chọn tỉnh/thành phố')),
                        Icon(
                          Icons.keyboard_arrow_down,
                          size: 24,
                        )
                      ],
                    ),
                  ),
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Container(
                height: 40,
                child: Form(
                    key: _formStreetKey,
                    child: TextFormField(
                      controller: TextEditingController(
                          text: viewModel?.warehouseDto?.value?.address ?? ''),
                      decoration: InputDecoration(labelText: 'Address'),
                      onChanged: (text) => {},
                      validator: (value) {
                        if (value.isEmpty) {
                          return 'Not empty';
                        }
                        return null;
                      },
                    )),
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: ElevatedButton(
                  onPressed: () async {final bool= await controller.createWarehouse();},
                  child: Container(
                    height: 40,
                    width: double.infinity,
                    child: Center(child: context.normalText('Save')),
                  )),
            )
          ],
        ),
      ),
    );
  }

  @override
  putController() {
    return WarehouseController(WarehouseVM());
  }

  _selectAddressArea(BuildContext context,int index) {
    if(index!=null){
      viewModel.warehouseTemp= WarehouseDto();
      viewModel.index.value=0;
      viewModel.title_choose_area_address.value = 'province';
      viewModel.area_parent_id=0;
    }
    viewModel.listAddressAreaController.refresh();
    context.showBottomSheet<AdministrativeDto>(
        Padding(
          padding: const EdgeInsets.only(left: 8.0),
          child: Obx(()=>context.normalText((viewModel.title_choose_area_address.value).tr,size: 16.0),)
        ), (context, item, index) {
      return InkWell(
        onTap: ()async{
          if(viewModel.index.value==2){
            viewModel.warehouseTemp.ward_name =item.name;
            viewModel.warehouseTemp.ward = item.id;
            viewModel.warehouseDto.value= viewModel.warehouseTemp;
            Navigator.pop(context);
          }else{

            if(viewModel.index.value==0){
              viewModel.title_choose_area_address.value = item.name;
              viewModel.warehouseTemp.province_name = item.name;
              viewModel.warehouseTemp.state_id = item.id;
            }else{
              viewModel.title_choose_area_address.value += ' > ' +item.name;
              viewModel.warehouseTemp.district_name = item.name;
              viewModel.warehouseTemp.district = item.id;
            }
            viewModel.index.value+=1;
            viewModel.area_parent_id=item.id;
            Navigator.pop(context);
            _selectAddressArea(context,null);
          }
        },
          child: Container(
            child: Padding(
              padding: const EdgeInsets.only(top:8.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Padding(
                    padding: const EdgeInsets.only(left: 8.0),
                    child: context.normalText(item.name),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 8.0),
                    child: context.divider(),
                  )
                ],
              ),
            ),
        ),
      );
    }, viewModel.listAddressAreaController);
  }
}
