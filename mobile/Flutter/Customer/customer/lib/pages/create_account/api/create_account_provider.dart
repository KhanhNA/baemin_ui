import 'dart:convert';

import 'package:customer/base/auth/api_auth_provider.dart';
import 'package:customer/model/area/AddressDto.dart';
import 'package:customer/model/list/PagedListData.dart';
import 'package:dio/dio.dart';
import 'package:multi_image_picker/multi_image_picker.dart';

class CreateAccountProvider{
  final String URI_GET_CAREER ='company/get_career';
  final String URI_GET_AREA ='market_place/get_area';
   ApiAuthProvider api = ApiAuthProvider();
  Future<PagedListData<AddressDto>> getCareer(Map<String, dynamic> params) async {
    final response = await api.getListJsonParamsNoSession<AddressDto>(
        URI_GET_CAREER,
        params,
            (js) => AddressDto.fromJson(js));
    return response;
  }
  Future<PagedListData<AddressDto>> getArea(Map<String, dynamic> params) async {
    final response = await api.getListJsonParamsNoSession<AddressDto>(
        URI_GET_AREA,
        params,
            (js) => AddressDto.fromJson(js));
    return response;
  }
  Future<ResponseBody> createAccount(Map<String, dynamic> params,List<Asset> listAsset) async {
    final response = await api.createAccount(params, listAsset);
    return response;
  }
}