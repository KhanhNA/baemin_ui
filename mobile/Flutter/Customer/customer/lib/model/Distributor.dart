import 'package:json_annotation/json_annotation.dart';
import 'base.dart';

part 'Distributor.g.dart';

@JsonSerializable(explicitToJson: true)
class Distributor extends _Distributor {
  Distributor();

  factory Distributor.fromJson(Map<String, dynamic> js) =>
      Base().fromJs(js, (js) => _$DistributorFromJson(js));

  Map<String, dynamic> toJson() => _$DistributorToJson(this);
  @override
  String get display => name;
}

class _Distributor extends Base {
  String aboutUs;
  String address;
  String code;
  String email;
  bool enabled;
  String name;
  String phoneNumber;
}
