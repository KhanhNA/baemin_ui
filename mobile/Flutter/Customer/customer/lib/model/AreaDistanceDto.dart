import 'package:customer/base/json/datetime_converter.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'base.dart';
import 'package:json_annotation/json_annotation.dart';
part 'AreaDistanceDto.g.dart';

@JsonSerializable(explicitToJson: true)
@CustomDateTimeConverter()
@CustomConverter()
class AreaDistanceDto extends _AreaDistanceDto{

  factory AreaDistanceDto.fromJson(Map<String, dynamic> js) => _$AreaDistanceDtoFromJson(js);

  Map<String, dynamic> toJson() => _$AreaDistanceDtoToJson(this);

  AreaDistanceDto();
}
class _AreaDistanceDto extends Base{
   // LatLng fromLocation;
   // LatLng toLocation;
   String from_name_seq;
   String to_name_seq;
   String from_warehouse_name;
   String to_warehouse_name;
  /**
   * type
   * 1.kho -> hub
   * 2.hub -> hub
   * 3.hub -> depot
   * 4.depot -> depot
   * 5.depot -> hub
   * 6.hub -> kho
   */
   int type;
   double distance;//đơn vị m
   double duration;//đơn vị s
   double price;
}