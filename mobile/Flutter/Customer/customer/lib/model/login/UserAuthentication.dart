import 'package:json_annotation/json_annotation.dart';
import 'package:customer/model/base.dart';

import 'Authority.dart';
import 'Principal.dart';

part 'UserAuthentication.g.dart';

@JsonSerializable(explicitToJson: true)
class UserAuthentication extends _UserAuthentication {
  String error;
  @JsonKey(ignore: true)
  Map<String, bool> authens;
  UserAuthentication.withError(this.error);

  UserAuthentication();

  factory UserAuthentication.fromJson(Map<String, dynamic> js) => Base().fromJs<UserAuthentication>(js, (js) => _$UserAuthenticationFromJson(js));

  Map<String, dynamic> toJson() => _$UserAuthenticationToJson(this);
  void updateMap(){
    authens = Map.fromIterable(authorities, key: (e) => e.authority, value: (e) => true);
  }
  bool hasAuth(String uri) {
    // if (AppUtils.me == null) {
    //   return false;
    // }
    // AppUtils.me.authorities.forEach((authority) {
    //   if (authority.authority == uri) {
    //     return true;
    //   }
    // });
    return authens.containsKey(uri);
  }
}

class _UserAuthentication extends Base {
  Principal principal;
  List<Authority> authorities;
}
