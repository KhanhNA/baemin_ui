// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'BillLadingDto.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

BillLadingDto _$BillLadingDtoFromJson(Map<String, dynamic> json) {
  return BillLadingDto()
    ..uuId = json['uuId'] as String
    ..createDate =
        const CustomDateTimeConverter().fromJson(json['createDate'] as String)
    ..createUser = json['createUser'] as String
    ..updateDate =
        const CustomDateTimeConverter().fromJson(json['updateDate'] as String)
    ..updateUser = json['updateUser'] as String
    ..fId = json['@id'] as String
    ..refId = json['@ref'] as String
    ..id = json['id'] as int
    ..from_bill_lading_id = json['from_bill_lading_id'] as int
    ..origin_bill_lading_detail_id = json['origin_bill_lading_detail_id'] as int
    ..insurance_name = json['insurance_name'] as String
    ..subscribe_name = json['subscribe_name'] as String
    ..company_id = json['company_id'] as int
    ..total_weight = (json['total_weight'] as num)?.toDouble()
    ..total_parcel = json['total_parcel'] as int
    ..total_amount = (json['total_amount'] as num)?.toDouble()
    ..total_volume = (json['total_volume'] as num)?.toDouble()
    ..vat = (json['vat'] as num)?.toDouble()
    ..arrBillLadingDetail = (json['arrBillLadingDetail'] as List)
        ?.map((e) => e == null
            ? null
            : BillLadingDetailDto.fromJson(e as Map<String, dynamic>))
        ?.toList()
    ..name_seq = json['name_seq'] as String
    ..name = json['name'] as String
    ..code = json['code'] as String
    ..bill_state = json['bill_state'] as String
    ..status = _$enumDecodeNullable(_$BillLadingStatusEnumMap, json['status'])
    ..status_routing = json['status_routing'] as String
    ..create_date = json['create_date'] as String
    ..web = json['web'] as bool
    ..start_date =
        const CustomDateTimeConverter().fromJson(json['start_date'] as String)
    ..end_date =
        const CustomDateTimeConverter().fromJson(json['end_date'] as String)
    ..startDateStr = json['startDateStr'] as String
    ..image_urls =
        (json['image_urls'] as List)?.map((e) => e as String)?.toList()
    ..insurance = json['insurance'] == null
        ? null
        : InsuranceDto.fromJson(json['insurance'] as Map<String, dynamic>)
    ..frequency = json['frequency'] as int
    ..day_of_week = json['day_of_week'] as int
    ..day_of_month = json['day_of_month'] as int
    ..subscribe = json['subscribe'] == null
        ? null
        : SubscribeDto.fromJson(json['subscribe'] as Map<String, dynamic>)
    ..order_package = json['order_package'] == null
        ? null
        : OrderPackageDto.fromJson(
            json['order_package'] as Map<String, dynamic>)
    ..price_not_discount = (json['price_not_discount'] as num)?.toDouble()
    ..insurance_price = (json['insurance_price'] as num)?.toDouble()
    ..service_price = (json['service_price'] as num)?.toDouble();
}

Map<String, dynamic> _$BillLadingDtoToJson(BillLadingDto instance) =>
    <String, dynamic>{
      'uuId': instance.uuId,
      'createDate': const CustomDateTimeConverter().toJson(instance.createDate),
      'createUser': instance.createUser,
      'updateDate': const CustomDateTimeConverter().toJson(instance.updateDate),
      'updateUser': instance.updateUser,
      '@id': instance.fId,
      '@ref': instance.refId,
      'id': instance.id,
      'from_bill_lading_id': instance.from_bill_lading_id,
      'origin_bill_lading_detail_id': instance.origin_bill_lading_detail_id,
      'insurance_name': instance.insurance_name,
      'subscribe_name': instance.subscribe_name,
      'company_id': instance.company_id,
      'total_weight': instance.total_weight,
      'total_parcel': instance.total_parcel,
      'total_amount': instance.total_amount,
      'total_volume': instance.total_volume,
      'vat': instance.vat,
      'arrBillLadingDetail':
          instance.arrBillLadingDetail?.map((e) => e?.toJson())?.toList(),
      'name_seq': instance.name_seq,
      'name': instance.name,
      'code': instance.code,
      'bill_state': instance.bill_state,
      'status': _$BillLadingStatusEnumMap[instance.status],
      'status_routing': instance.status_routing,
      'create_date': instance.create_date,
      'web': instance.web,
      'start_date': const CustomDateTimeConverter().toJson(instance.start_date),
      'end_date': const CustomDateTimeConverter().toJson(instance.end_date),
      'startDateStr': instance.startDateStr,
      'image_urls': instance.image_urls,
      'insurance': instance.insurance?.toJson(),
      'frequency': instance.frequency,
      'day_of_week': instance.day_of_week,
      'day_of_month': instance.day_of_month,
      'subscribe': instance.subscribe?.toJson(),
      'order_package': instance.order_package?.toJson(),
      'price_not_discount': instance.price_not_discount,
      'insurance_price': instance.insurance_price,
      'service_price': instance.service_price,
    };

T _$enumDecode<T>(
  Map<T, dynamic> enumValues,
  dynamic source, {
  T unknownValue,
}) {
  if (source == null) {
    throw ArgumentError('A value must be provided. Supported values: '
        '${enumValues.values.join(', ')}');
  }

  final value = enumValues.entries
      .singleWhere((e) => e.value == source, orElse: () => null)
      ?.key;

  if (value == null && unknownValue == null) {
    throw ArgumentError('`$source` is not one of the supported values: '
        '${enumValues.values.join(', ')}');
  }
  return value ?? unknownValue;
}

T _$enumDecodeNullable<T>(
  Map<T, dynamic> enumValues,
  dynamic source, {
  T unknownValue,
}) {
  if (source == null) {
    return null;
  }
  return _$enumDecode<T>(enumValues, source, unknownValue: unknownValue);
}

const _$BillLadingStatusEnumMap = {
  BillLadingStatus.RUNNING: 'running',
  BillLadingStatus.FINISHED: 'finished',
  BillLadingStatus.DELETED: 'deleted',
};
