import 'package:customer/base/json/datetime_converter.dart';

import '../base.dart';
import 'SubscribeDto.dart';
import 'package:json_annotation/json_annotation.dart';
part 'RecurrentModel.g.dart';

@JsonSerializable(explicitToJson: true)
@CustomDateTimeConverter()
@CustomConverter()
class RecurrentModel extends _RecurrentModel{

  factory RecurrentModel.fromJson(Map<String, dynamic> js) => _$RecurrentModelFromJson(js);

  Map<String, dynamic> toJson() => _$RecurrentModelToJson(this);

  RecurrentModel();
}

class _RecurrentModel extends Base{
   int frequency;// 1 daily, 2 weekly, 3 monthly, 4 express, 5 one time.
   int day_of_week; //if frequency = 2 : 0- sunday, 1- monday,2 tuesday,...
   int day_of_month;// if frequency = 3.
   SubscribeDto subscribe;//lặp lại trong 1 tuần, trong 1 tháng, trong 6 tháng,  trong 1 năm...

    String recurrentModelStr(){
      if(frequency==0){
        return 'Hàng ngày';
      }else if(frequency==1){
        if(day_of_week==0){
          return 'Hàng tháng - Ngày chủ nhật hàng tuần';
        }else{
          return 'Hàng tháng - Ngày thứ '+(day_of_week+1).toString()+' hàng tuần';
        }
      }else if(frequency==2){
        return 'Hàng năm - Ngày '+(day_of_month+1).toString()+' hàng tháng';
      }
      return null;
    }
}