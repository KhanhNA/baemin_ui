import 'package:customer/base/json/datetime_converter.dart';
import 'package:customer/model/vendor/VendorDto.dart';
import 'package:json_annotation/json_annotation.dart';

import '../base.dart';
part 'SubscribeDto.g.dart';

@JsonSerializable(explicitToJson: true)
@CustomDateTimeConverter()
@CustomConverter()
class SubscribeDto extends _SubscribeDto{

  factory SubscribeDto.fromJson(Map<String, dynamic> js) => _$SubscribeDtoFromJson(js);

  Map<String, dynamic> toJson() => _$SubscribeDtoToJson(this);

  SubscribeDto();
}

class _SubscribeDto extends Base{
   int id;
   String name;
   String description;
   String subscribe_code;

  //tính theo ngày
   int value;
}