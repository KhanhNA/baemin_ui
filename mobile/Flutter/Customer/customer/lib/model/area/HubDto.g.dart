// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'HubDto.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

HubDto _$HubDtoFromJson(Map<String, dynamic> json) {
  return HubDto()
    ..uuId = json['uuId'] as String
    ..createDate =
        const CustomDateTimeConverter().fromJson(json['createDate'] as String)
    ..createUser = json['createUser'] as String
    ..updateDate =
        const CustomDateTimeConverter().fromJson(json['updateDate'] as String)
    ..updateUser = json['updateUser'] as String
    ..fId = json['@id'] as String
    ..refId = json['@ref'] as String
    ..id = json['id'] as int
    ..latitude = (json['latitude'] as num)?.toDouble()
    ..longitude = (json['longitude'] as num)?.toDouble()
    ..name = json['name'] as String
    ..address = json['address'] as String
    ..status = json['status'] as String
    ..name_seq = json['name_seq'] as String;
}

Map<String, dynamic> _$HubDtoToJson(HubDto instance) => <String, dynamic>{
      'uuId': instance.uuId,
      'createDate': const CustomDateTimeConverter().toJson(instance.createDate),
      'createUser': instance.createUser,
      'updateDate': const CustomDateTimeConverter().toJson(instance.updateDate),
      'updateUser': instance.updateUser,
      '@id': instance.fId,
      '@ref': instance.refId,
      'id': instance.id,
      'latitude': instance.latitude,
      'longitude': instance.longitude,
      'name': instance.name,
      'address': instance.address,
      'status': instance.status,
      'name_seq': instance.name_seq,
    };
