import 'package:customer/base/json/datetime_converter.dart';
import 'package:json_annotation/json_annotation.dart';

import '../base.dart';
part 'AddressDto.g.dart';

@JsonSerializable(explicitToJson: true)
@CustomDateTimeConverter()
@CustomConverter()
class AddressDto extends _AddressDto{

  factory AddressDto.fromJson(Map<String, dynamic> js) => _$AddressDtoFromJson(js);

  Map<String, dynamic> toJson() => _$AddressDtoToJson(this);

  AddressDto();
}
class _AddressDto extends Base{
  int id;
  String name;
  String code;
  int parent_id;
}