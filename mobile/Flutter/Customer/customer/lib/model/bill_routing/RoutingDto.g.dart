// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'RoutingDto.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

RoutingDto _$RoutingDtoFromJson(Map<String, dynamic> json) {
  return RoutingDto()
    ..uuId = json['uuId'] as String
    ..createDate =
        const CustomDateTimeConverter().fromJson(json['createDate'] as String)
    ..createUser = json['createUser'] as String
    ..updateDate =
        const CustomDateTimeConverter().fromJson(json['updateDate'] as String)
    ..updateUser = json['updateUser'] as String
    ..fId = json['@id'] as String
    ..refId = json['@ref'] as String
    ..id = json['id'] as int
    ..status = json['status'] as String
    ..driver_id = json['driver_id'] as int
    ..vehicle_id = json['vehicle_id'] as int
    ..latitude = (json['latitude'] as num)?.toDouble()
    ..longitude = (json['longitude'] as num)?.toDouble()
    ..warehouse_name = json['warehouse_name'] as String
    ..from_routing_plan_day_id = json['from_routing_plan_day_id'] as int
    ..ship_type = json['ship_type'] as String
    ..driver_name = json['driver_name'] as String
    ..driver_phone = json['driver_phone'] as String
    ..license_plate = json['license_plate'] as String
    ..vehicle_name = json['vehicle_name'] as String
    ..accept_time = json['accept_time'] as String
    ..type = json['type'] as String
    ..routing_plan_day_code = json['routing_plan_day_code'] as String;
}

Map<String, dynamic> _$RoutingDtoToJson(RoutingDto instance) =>
    <String, dynamic>{
      'uuId': instance.uuId,
      'createDate': const CustomDateTimeConverter().toJson(instance.createDate),
      'createUser': instance.createUser,
      'updateDate': const CustomDateTimeConverter().toJson(instance.updateDate),
      'updateUser': instance.updateUser,
      '@id': instance.fId,
      '@ref': instance.refId,
      'id': instance.id,
      'status': instance.status,
      'driver_id': instance.driver_id,
      'vehicle_id': instance.vehicle_id,
      'latitude': instance.latitude,
      'longitude': instance.longitude,
      'warehouse_name': instance.warehouse_name,
      'from_routing_plan_day_id': instance.from_routing_plan_day_id,
      'ship_type': instance.ship_type,
      'driver_name': instance.driver_name,
      'driver_phone': instance.driver_phone,
      'license_plate': instance.license_plate,
      'vehicle_name': instance.vehicle_name,
      'accept_time': instance.accept_time,
      'type': instance.type,
      'routing_plan_day_code': instance.routing_plan_day_code,
    };
