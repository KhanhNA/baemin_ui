import 'package:customer/base/json/datetime_converter.dart';
import 'package:customer/model/bill_package/BillPackageDto.dart';
import 'package:customer/model/bill_package/BillServiceDto.dart';
import 'package:customer/model/bill_routing/RoutingDto.dart';
import 'package:customer/model/warehouse/WarehouseDto.dart';
import 'package:json_annotation/json_annotation.dart';

import '../base.dart';
part 'BillRoutingDetailDto.g.dart';

@JsonSerializable(explicitToJson: true)
@CustomDateTimeConverter()
@CustomConverter()
class BillRoutingDetailDto extends _BillRoutingDetailDto{

  factory BillRoutingDetailDto.fromJson(Map<String, dynamic> js) => _$BillRoutingDetailDtoFromJson(js);

  Map<String, dynamic> toJson() => _$BillRoutingDetailDtoToJson(this);

  BillRoutingDetailDto();
}
class _BillRoutingDetailDto extends Base{
   double latitude;
   int area_id;
   double longitude;
   String approved_type;
   String status;
   String name;
   int id;
   double max_price;
   double min_price;
   double total_weight;

   WarehouseDto warehouse;
   String address;
   String warehouse_name;
   String phone;
   String warehouse_type;//0. nhan, 1. tra
   String description;
   List<String> image_urls;

   String order_type;//0: trong zone, 1: ngoài zone  - so với kho xuất hàng.
   List<BillServiceDto> billService;
   int bill_lading_id;

   List<BillPackageDto> billPackages;

   List<RoutingDto> routing;
}