import 'package:customer/model/driver_vehicle/DriverDto.dart';

import '../base.dart';

import 'package:customer/base/json/datetime_converter.dart';
import 'package:json_annotation/json_annotation.dart';

part 'VehicleDto.g.dart';

@JsonSerializable(explicitToJson: true)
@CustomDateTimeConverter()
@CustomConverter()
class VehicleDto extends _VehicleDto{

  factory VehicleDto.fromJson(Map<String, dynamic> js) => _$VehicleDtoFromJson(js);

  Map<String, dynamic> toJson() => _$VehicleDtoToJson(this);

  VehicleDto();
}

class _VehicleDto extends Base{
   int id;
   String name;
   String license_plate;
   String color;
   int model_year;
   String fuel_type;
   int body_length;
   int body_width;
   int height;
   String vehicle_type;
   double engine_size;//
   double latitude;
   double longitude;//
   DriverDto driver;

   int vehicle_status;
}