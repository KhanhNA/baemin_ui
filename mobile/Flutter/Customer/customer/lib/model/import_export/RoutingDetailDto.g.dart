// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'RoutingDetailDto.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

RoutingDetailDto _$RoutingDetailDtoFromJson(Map<String, dynamic> json) {
  return RoutingDetailDto()
    ..uuId = json['uuId'] as String
    ..createDate =
        const CustomDateTimeConverter().fromJson(json['createDate'] as String)
    ..createUser = json['createUser'] as String
    ..updateDate =
        const CustomDateTimeConverter().fromJson(json['updateDate'] as String)
    ..updateUser = json['updateUser'] as String
    ..fId = json['@id'] as String
    ..refId = json['@ref'] as String
    ..id = json['id'] as int
    ..type = json['type'] as String
    ..order_type = json['order_type'] as String
    ..insurance_name = json['insurance_name'] as String
    ..service_name = (json['service_name'] as List)
        ?.map((e) => e == null
            ? null
            : BillServiceDto.fromJson(e as Map<String, dynamic>))
        ?.toList()
    ..routing_plan_detail_code = json['routing_plan_detail_code'] as String
    ..status = json['status'] as int
    ..date_plan = json['date_plan'] as String
    ..expected_from_time = json['expected_from_time'] as String
    ..expected_to_time = json['expected_to_time'] as String
    ..vehicle = json['vehicle'] == null
        ? null
        : VehicleDto.fromJson(json['vehicle'] as Map<String, dynamic>)
    ..latitude = (json['latitude'] as num)?.toDouble()
    ..longitude = (json['longitude'] as num)?.toDouble()
    ..address = json['address'] as String
    ..phone = json['phone'] as String
    ..warehouse_name = json['warehouse_name'] as String
    ..list_bill_package_import = (json['list_bill_package_import'] as List)
        ?.map((e) => e == null
            ? null
            : BillPackageDto.fromJson(e as Map<String, dynamic>))
        ?.toList()
    ..list_bill_package_export = (json['list_bill_package_export'] as List)
        ?.map((e) => e == null
            ? null
            : BillPackageDto.fromJson(e as Map<String, dynamic>))
        ?.toList()
    ..routing_plan_day_code = json['routing_plan_day_code'] as String;
}

Map<String, dynamic> _$RoutingDetailDtoToJson(RoutingDetailDto instance) =>
    <String, dynamic>{
      'uuId': instance.uuId,
      'createDate': const CustomDateTimeConverter().toJson(instance.createDate),
      'createUser': instance.createUser,
      'updateDate': const CustomDateTimeConverter().toJson(instance.updateDate),
      'updateUser': instance.updateUser,
      '@id': instance.fId,
      '@ref': instance.refId,
      'id': instance.id,
      'type': instance.type,
      'order_type': instance.order_type,
      'insurance_name': instance.insurance_name,
      'service_name': instance.service_name?.map((e) => e?.toJson())?.toList(),
      'routing_plan_detail_code': instance.routing_plan_detail_code,
      'status': instance.status,
      'date_plan': instance.date_plan,
      'expected_from_time': instance.expected_from_time,
      'expected_to_time': instance.expected_to_time,
      'vehicle': instance.vehicle?.toJson(),
      'latitude': instance.latitude,
      'longitude': instance.longitude,
      'address': instance.address,
      'phone': instance.phone,
      'warehouse_name': instance.warehouse_name,
      'list_bill_package_import':
          instance.list_bill_package_import?.map((e) => e?.toJson())?.toList(),
      'list_bill_package_export':
          instance.list_bill_package_export?.map((e) => e?.toJson())?.toList(),
      'routing_plan_day_code': instance.routing_plan_day_code,
    };
