// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'RoutingDayDto.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

RoutingDayDto _$RoutingDayDtoFromJson(Map<String, dynamic> json) {
  return RoutingDayDto()
    ..uuId = json['uuId'] as String
    ..createDate =
        const CustomDateTimeConverter().fromJson(json['createDate'] as String)
    ..createUser = json['createUser'] as String
    ..updateDate =
        const CustomDateTimeConverter().fromJson(json['updateDate'] as String)
    ..updateUser = json['updateUser'] as String
    ..fId = json['@id'] as String
    ..refId = json['@ref'] as String
    ..id = json['id'] as int
    ..routing_plan_day_id = json['routing_plan_day_id'] as int
    ..bill_routing_id = json['bill_routing_id'] as int
    ..bill_routing_name = json['bill_routing_name'] as String
    ..routing_plan_day_code = json['routing_plan_day_code'] as String
    ..bill_lading_detail_code = json['bill_lading_detail_code'] as String
    ..actual_time = json['actual_time'] as String
    ..expected_from_time = json['expected_from_time'] as String
    ..expected_to_time = json['expected_to_time'] as String
    ..name = json['name'] as String
    ..license_plate = json['license_plate'] as String
    ..type = json['type'] as String
    ..status = json['status'] as String
    ..trouble_type = json['trouble_type'] as String
    ..date_plan = json['date_plan'] as String
    ..driver = json['driver'] == null
        ? null
        : DriverDto.fromJson(json['driver'] as Map<String, dynamic>)
    ..change_bill_lading_detail_id = json['change_bill_lading_detail_id'] as int
    ..services = (json['services'] as List)
        ?.map((e) => e == null
            ? null
            : BillServiceDto.fromJson(e as Map<String, dynamic>))
        ?.toList()
    ..vehicle = json['vehicle'] == null
        ? null
        : RoutingDayDto.fromJson(json['vehicle'] as Map<String, dynamic>)
    ..latitude = (json['latitude'] as num)?.toDouble()
    ..longitude = (json['longitude'] as num)?.toDouble()
    ..arrived_check = json['arrived_check'] as bool
    ..address = json['address'] as String
    ..phone = json['phone'] as String
    ..booked_employee = json['booked_employee'] as String
    ..insurance_name = json['insurance_name'] as String
    ..warehouse_name = json['warehouse_name'] as String
    ..image_urls =
        (json['image_urls'] as List)?.map((e) => e as String)?.toList()
    ..routing_plan_day_detail = (json['routing_plan_day_detail'] as List)
        ?.map((e) => e == null
            ? null
            : RoutingDetailDto.fromJson(e as Map<String, dynamic>))
        ?.toList()
    ..list_bill_package = (json['list_bill_package'] as List)
        ?.map((e) => e == null
            ? null
            : BillPackageDto.fromJson(e as Map<String, dynamic>))
        ?.toList()
    ..sos_ids = json['sos_ids'] as bool
    ..rating_drivers = json['rating_drivers'] == null
        ? null
        : RatingDriverDto.fromJson(
            json['rating_drivers'] as Map<String, dynamic>)
    ..driver_name = json['driver_name'] as String
    ..driver_phone = json['driver_phone'] as String
    ..accept_time = json['accept_time'] as String
    ..confirm_time = json['confirm_time'] as String
    ..so_type = json['so_type'] as bool;
}

Map<String, dynamic> _$RoutingDayDtoToJson(RoutingDayDto instance) =>
    <String, dynamic>{
      'uuId': instance.uuId,
      'createDate': const CustomDateTimeConverter().toJson(instance.createDate),
      'createUser': instance.createUser,
      'updateDate': const CustomDateTimeConverter().toJson(instance.updateDate),
      'updateUser': instance.updateUser,
      '@id': instance.fId,
      '@ref': instance.refId,
      'id': instance.id,
      'routing_plan_day_id': instance.routing_plan_day_id,
      'bill_routing_id': instance.bill_routing_id,
      'bill_routing_name': instance.bill_routing_name,
      'routing_plan_day_code': instance.routing_plan_day_code,
      'bill_lading_detail_code': instance.bill_lading_detail_code,
      'actual_time': instance.actual_time,
      'expected_from_time': instance.expected_from_time,
      'expected_to_time': instance.expected_to_time,
      'name': instance.name,
      'license_plate': instance.license_plate,
      'type': instance.type,
      'status': instance.status,
      'trouble_type': instance.trouble_type,
      'date_plan': instance.date_plan,
      'driver': instance.driver?.toJson(),
      'change_bill_lading_detail_id': instance.change_bill_lading_detail_id,
      'services': instance.services?.map((e) => e?.toJson())?.toList(),
      'vehicle': instance.vehicle?.toJson(),
      'latitude': instance.latitude,
      'longitude': instance.longitude,
      'arrived_check': instance.arrived_check,
      'address': instance.address,
      'phone': instance.phone,
      'booked_employee': instance.booked_employee,
      'insurance_name': instance.insurance_name,
      'warehouse_name': instance.warehouse_name,
      'image_urls': instance.image_urls,
      'routing_plan_day_detail':
          instance.routing_plan_day_detail?.map((e) => e?.toJson())?.toList(),
      'list_bill_package':
          instance.list_bill_package?.map((e) => e?.toJson())?.toList(),
      'sos_ids': instance.sos_ids,
      'rating_drivers': instance.rating_drivers?.toJson(),
      'driver_name': instance.driver_name,
      'driver_phone': instance.driver_phone,
      'accept_time': instance.accept_time,
      'confirm_time': instance.confirm_time,
      'so_type': instance.so_type,
    };
