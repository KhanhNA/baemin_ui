import 'dart:convert';

import 'dart:typed_data';

import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get/get_connect/http/src/status/http_status.dart';
import 'package:customer/icon/custom_icons.dart';
import 'package:customer/theme/app_theme.dart';

class Utils {
  static String stringPH(String input, String placeHolder) {
    return input == null ? placeHolder : input;
  }

  static Uint8List decodeBase64(input) {
    try {
      return base64.decode(input);
    } catch (e) {
      return null;
    }
  }

  static snackBarSuccess(String title, String message) {
    Get.snackbar(
      title, message,
      backgroundColor: AppTheme.alertSuccess,
      colorText: AppTheme.alertText,
      borderColor: AppTheme.alertSuccessBorder,
      borderWidth: 1,
      borderRadius: 6,
      icon: Icon(CustomIcons.fromName('FontAwesome.0xf058')),
    );
  }

  static snackBarWarning(String title, String message) {
    Get.snackbar(
      title, message,
      backgroundColor: AppTheme.alertWarning,
      colorText: AppTheme.alertText,
      borderColor: AppTheme.alertWarningBorder,
      borderWidth: 1,
      borderRadius: 6,
      icon: Icon(CustomIcons.fromName('FontAwesome.0xf071')),
    );
  }

  static snackBarError(String title, String message) {
    Get.snackbar(
      title, message,
      backgroundColor: AppTheme.alertError,
      colorText: AppTheme.alertText,
      borderColor: AppTheme.alertErrorBorder,
      borderWidth: 1,
      borderRadius: 6,
      icon: Icon(CustomIcons.fromName('FontAwesome.0xf06a')),
    );
  }

  static snackBarInfo(String title, String message) {
    Get.snackbar(
      title, message,
      backgroundColor: AppTheme.alertInfo,
      colorText: AppTheme.alertText,
      borderColor: AppTheme.alertInfoBorder,
      borderWidth: 1,
      borderRadius: 6,
      icon: Icon(CustomIcons.fromName('FontAwesome.0xf05a')),
    );
  }

  static String getHttpErrorMessage(dynamic error){
    String ret = "";
    if (error is DioError) {
      DioError e = error;
      if (e.response.statusCode == HttpStatus.forbidden){
        return "Bạn không có quyền thực thi";
      } else if (e.response?.data != null && e.response.data is Map) {
        Map errorDesc = (e.response.data as Map);
        ret = errorDesc["error_description"];
      }
    }
    if (ret == null || ret.isEmpty) {
      ret = "$error";
    }
    return ret;
  }
}
