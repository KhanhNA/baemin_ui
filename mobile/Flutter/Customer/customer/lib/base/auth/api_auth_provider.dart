import 'dart:convert';
import 'dart:typed_data';
import 'package:customer/model/bill_lading/BillLadingDetailDto.dart';
import 'package:customer/model/bill_lading/BillLadingDto.dart';
import 'package:customer/model/session/OdooSessionDto.dart';
import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'package:customer/base/auth/token.dart';
import 'package:customer/base/util/AppUtils.dart';
import 'package:customer/model/base.dart';
import 'package:customer/model/base_model.dart';
import 'package:customer/model/list/PagedListData.dart';
import 'package:customer/model/login/UserAuthentication.dart';
import 'package:multi_image_picker/multi_image_picker.dart';
import 'package:uuid/uuid.dart';

import 'dio_logging_interceptor.dart';
import 'refresh_token_body.dart';


import 'package:flutter/services.dart';
import 'package:http_parser/http_parser.dart';

class ApiAuthProvider {
  static final ApiAuthProvider _singleton = ApiAuthProvider._internal();
  final Dio _dioAuth = new Dio();
  final Dio _dioBussiness = new Dio();
  final Dio _dioBilling = new Dio();
  final String CLIENT_SECRET = "1qazXSW@3edcVFR\$5tgbNHY^7ujm<KI*9ol.?:P)";

  // final String _baseAuthUrl = 'https://demo.aggregatoricapaci.com:8070/web/';

  final String _baseBusinessUrl = 'http://192.168.1.44:8070/';

  final String _baseAuthUrl = 'https://mfunctions.com:9999/';
  final String _billing_url = 'http://192.168.1.44:3004';

  //
  // final String _baseBusinessUrl = 'https://mfunctions.com:8888/';

  String get imgUrl => _baseBusinessUrl;

  // static String token;

  // final String _baseAuthUrl = 'http://192.168.1.69:9999/';
  // final String _baseBusinessUrl = 'http://192.168.1.69:8888/';

  // final String _baseBusinessUrl = 'http://192.168.1.99:8888/';
  final String clientId = 'Logistics';
  final String clientSecret = 'XY7kmzoNzl100';

  ApiAuthProvider._internal() {
    _dioAuth.options.baseUrl = _baseAuthUrl;
    _dioAuth.interceptors.add(DioLoggingInterceptors(_dioAuth));

    _dioBilling.options.baseUrl =_billing_url;
    _dioBilling.interceptors.add(DioLoggingInterceptors(_dioBilling));

    _dioBussiness.options.baseUrl = _baseBusinessUrl;
    _dioBussiness.interceptors.add(DioLoggingInterceptors(_dioBussiness));
  }

  Dio getDioBussiness() {
    return _dioBussiness;
  }

  factory ApiAuthProvider() {
    return _singleton;
  }

  Future<OdooSessionDto> loginUser(BaseModel loginBody) async {
    final params = loginBody.toJson();
    Map data = {'jsonrpc': '2.0', 'params': params};
    final response = await _dioBussiness.post(
      'web/session/authenticate',
      data: data,
      options: Options(),
    );
    OdooSessionDto odooSessionDto =
        OdooSessionDto.fromJson(response.data['result']);
    String session_id = response.headers['set-cookie'].toString();
    session_id.substring(1, session_id.length - 1);
    odooSessionDto.session_id = session_id.substring(1, session_id.length - 1);
    return odooSessionDto;
  }
  Future<double> createOrder<double>(String url,BillLadingDto dto) async {
    final params = dto.toJson();
    Map data = {'jsonrpc': '2.0', 'params': params};
    final response = await _dioBussiness.post(
      url,
      data: data,
      options: Options(),
    );
    final result = response.data['result'];
    return result;
  }
  Future<bool> logout() async {
    Map<String, dynamic> queryParameters={
      'user_id': AppUtils.odooSessionDto.uid
    };
    final response = await _dioBussiness.post(
      'mobile/logout',
      queryParameters: queryParameters,
      options: Options(
        headers: {
          'Cookie': AppUtils.odooSessionDto.session_id
        }
      ),
    );

    bool isLogoutSuccess = false;
    if(response.statusCode==200){
      isLogoutSuccess=true;
    }
    return isLogoutSuccess;
  }

  Future<T> getOne<T>(String uri, Map<String, dynamic> params,
      ConvertFunc<T> convertFunc) async {
    Map data = {'jsonrpc': '2.0', 'params': params};
    print('CALL_API ---- ' + uri + ': ' + data.toString());
    final response = await _dioBussiness.post(
      uri,
      data: data,
      options: Options(
        headers: {
          'Cookie': AppUtils.odooSessionDto.session_id,
        },
      ),
    );
    T result = convertFunc(response.data['result']);
    return result;
  }


  Future<BillLadingDto> calculatorPrice(BillLadingDto billLadingDto) async {
    final params = billLadingDto.toJson();
    // Map data = {'jsonrpc': '2.0', 'params': params};
    print('CALL_API ---- app/v1/price/calc/bill' + ': ' + params.toString());
    final response = await _dioBilling.post(
      '/app/v1/price/calc/bill',
      data: params,
      options: Options(
        headers: {
          'Authorization': 'Bearer '+ AppUtils.odooSessionDto.access_token,
        },
      ),
    );
    billLadingDto.total_amount = response.data['total_amount'];
    billLadingDto.price_not_discount = response.data['price_not_discount'];
    billLadingDto.insurance_price = response.data['insurance_price'];
    billLadingDto.service_price = response.data['service_price'];
    return billLadingDto;
  }
  Future<T> httpJson<T>(String url,Base dto,ConvertFunc<T> convertFunc) async {
    final params = dto.toJson();
    // Map data = {'jsonrpc': '2.0', 'params': params};
    print('CALL_API ----' + url+' :' + params.toString());
    final response = await _dioBilling.post(
      url,
      data: params,
      options: Options(
        headers: {
          'Cookie': AppUtils.odooSessionDto.session_id,
        },
      ),
    );
    T result = convertFunc(response.data['result']);
    return result;
  }
  Future<bool> jsonCheck(String uri, Map<String, dynamic> params) async {
    Map data = {'jsonrpc': '2.0', 'params': params};
    print('CALL_API ---- ' + uri + ': ' + data.toString());
    final response = await _dioBussiness.post(
      uri,
      data: data,
      options: Options(
        headers: {
          'Cookie': AppUtils.odooSessionDto.session_id,
        },
      ),
    );
    bool result = response.data['result'];
    return result;
  }

  Future<UserAuthentication> getUserMe() async {
    final response = await _dioAuth.get(
      'user/me',
      options: Options(
        headers: {
          'Cookie': AppUtils.odooSessionDto.session_id,
          'Authorization': 'bearer ' + AppUtils.odooSessionDto.access_token
        },
      ),
    );

    String uuid = Uuid().v4();
    response.data[Base.UUID] = uuid;
    Base.step[uuid] = 0;
    UserAuthentication ret = UserAuthentication.fromJson(response.data);
    Base.step[uuid] = 1;
    ret = UserAuthentication.fromJson(response.data);
    ret.updateMap();
    // Base.referenceObj.clear();
    // Base.referenceData.clear();
    Base.reference.remove(uuid + '-' + Base.OBJ);
    Base.reference.remove(uuid + '-' + Base.DATA);
    return ret;
  }

  Future<Token> refreshAuth(RefreshTokenBody refreshTokenBody) async {
    final response = await _dioAuth.post(
      'oauth/token',
      data: FormData.fromMap(refreshTokenBody.toJson()),
      options: Options(
        headers: {
          'Authorization': 'Basic ${base64Encode(
            utf8.encode('$clientId:$clientSecret'),
          )}',
        },
      ),
    );
    return Token.fromJson(response.data);
  }

  Future<T> postAndConvert<T>(
      String uri, Base data, ConvertFunc<T> convertFunc) async {
    final response = await _dioBussiness.post(
      uri,
      data: data?.toJson(),
      options: Options(
        headers: {
          'requirestoken': true,
        },
      ),
    );

    String uuid = Uuid().v4();
    response.data[Base.UUID] = uuid;
    Base.step[uuid] = 0;
    T ret = convertFunc(response.data);
    Base.step[uuid] = 1;
    ret = convertFunc(response.data);
    // Base.referenceObj.clear();
    // Base.referenceData.clear();
    Base.reference.remove(uuid + '-' + Base.OBJ);
    Base.reference.remove(uuid + '-' + Base.DATA);
    return ret;
  }

  Future<Object> post(String uri, Base data) async {
    final response = await _dioBussiness.post(
      uri,
      data: FormData.fromMap(data.toJson()),
      options: Options(
        headers: {
          'requirestoken': true,
        },
      ),
    );
    return response.data;
  }

  Future<Object> postJson(String uri, String js) async {
    final response = await _dioBussiness.post(
      uri,
      data: js,
      options: Options(
        headers: {
          'requirestoken': true,
        },
      ),
    );
    return response.data;
  }

  Future<T> patchOne<T>(
      String uri, Base data, ConvertFunc<T> convertFunc) async {
    Map json = data.toJson();
    print(jsonEncode(json));
    _dioBussiness.options.contentType = Headers.jsonContentType;
    final response = await _dioBussiness.patch(
      uri,
      data: json,
      options: Options(
        contentType: Headers.jsonContentType,
        headers: {
          'requirestoken': true,
        },
      ),
    );
    String uuid = Uuid().v4();
    response.data[Base.UUID] = uuid;
    Base.step[uuid] = 0;
    T ret = convertFunc(response.data);
    Base.step[uuid] = 1;
    ret = convertFunc(response.data);
    // Base.referenceObj.clear();
    // Base.referenceData.clear();
    Base.reference.remove(uuid + '-' + Base.OBJ);
    Base.reference.remove(uuid + '-' + Base.DATA);
    return ret;
  }

  Future<T> patch<T>(String uri, Base data, ConvertFunc<T> convertFunc) async {
    Map json = data == null ? {} : data.toJson();
    _dioBussiness.options.contentType = Headers.jsonContentType;
    final response = await _dioBussiness.patch(
      uri,
      data: json,
      options: Options(
        contentType: Headers.jsonContentType,
        headers: {
          'requirestoken': true,
        },
      ),
    );
    String uuid = Uuid().v4();
    response.data[Base.UUID] = uuid;
    Base.step[uuid] = 0;
    T ret = convertFunc(response.data);
    Base.step[uuid] = 1;
    ret = convertFunc(response.data);
    // Base.referenceObj.clear();
    // Base.referenceData.clear();
    Base.reference.remove(uuid + '-' + Base.OBJ);
    Base.reference.remove(uuid + '-' + Base.DATA);
    return ret;
  }

  Future<T> uploadMultiFiles<T>(
      String uri, FormData formData, ConvertFunc<T> convertFunc) async {
    final response = await _dioBussiness.patch(
      uri,
      data: formData,
      options: Options(
        contentType: Headers.jsonContentType,
        headers: {
          'requirestoken': true,
        },
      ),
    );
    if (!(response.data is Map)) {
      return null;
    }
    String uuid = Uuid().v4();
    response.data[Base.UUID] = uuid;
    Base.step[uuid] = 0;
    T ret = convertFunc(response.data);
    Base.step[uuid] = 1;
    ret = convertFunc(response.data);
    // Base.referenceObj.clear();
    // Base.referenceData.clear();
    Base.reference.remove(uuid + '-' + Base.OBJ);
    Base.reference.remove(uuid + '-' + Base.DATA);
    return ret;
    // return Claim() as T;
  }

  Future<Object> get(String uri, Map<String, dynamic> queryParameters) async {
    final response = await _dioBussiness.get(
      uri,
      queryParameters: queryParameters,
      options: Options(
        headers: {
          'requirestoken': true,
        },
      ),
    );
    // response.request.

    return response.data;
  }

  Future<PagedListData<T>> getList<T>(String uri,
      Map<String, dynamic> queryParameters, ConvertFunc<T> convertFunc) async {
    final response = await _dioBussiness.get(
      uri,
      queryParameters: queryParameters,
      options: Options(
        headers: {
          'requirestoken': true,
        },
      ),
    );
    String uuid = Uuid().v4();
    Base.step[uuid] = 0;
    response.data.map((e) {
      e[Base.UUID] = uuid;
      return convertFunc(e);
    }).toList();
    Base.step[uuid] = 1;
    List<T> list = response.data.map<T>((e) {
      e[Base.UUID] = uuid;
      return convertFunc(e);
    }).toList();
    Base.reference.remove(uuid + '-' + Base.OBJ);
    Base.reference.remove(uuid + '-' + Base.DATA);
    return PagedListData.withContent(list);
  }


  Future<ResponseBody> createAccount<ResponseBody>(
      Map<String, dynamic> params,
      List<Asset> listAsset) async {
    String model=json.encode(params);
    _dioBussiness.options.contentType = 'multipart/form-data';
    FormData formData = FormData();
    formData.fields.add(MapEntry('secret_key', CLIENT_SECRET));
    listAsset.forEach((element) async {
      ByteData byteData = await element.getByteData();
      List<int> imageData = byteData.buffer.asUint8List();
      var multipartFile = MultipartFile.fromBytes(imageData,
          filename: element.name,
          contentType: MediaType('image', '*'));
      formData.files.add(MapEntry('files', multipartFile));
    });


    final response = await _dioBussiness.patch(
      'share_van_order/create_individual_customer',
      data: formData,
      // queryParameters: params,
      options: Options(
          // headers: {
          //   'requirestoken': true,
          // },
          ),
    );
    // response.request.

    return response.data;
  }

  Future<PagedListData<T>> getListJsonParams<T>(String uri,
      Map<String, dynamic> params, ConvertFunc<T> convertFunc) async {
    Map data;
    if (uri == 'share_van_order/get_customer_information') {
      data = {'jsonrpc': '2.0', 'params': params};
    } else {
      data = {'jsonrpc': '2.0', 'method': 'call', 'params': params};
    }
    print('CALL_API ---- ' + uri + ': ' + data.toString());
    final response = await _dioBussiness.post(
      uri,
      data: data,
      options: Options(
        headers: {
          'Cookie': AppUtils.odooSessionDto.session_id,
        },
      ),
    );
    PagedListData<T> pagedListData =
        new PagedListData<T>((i) => convertFunc(i));
    pagedListData.fromJson(response.data['result']);
    return pagedListData;
  }

  Future<PagedListData<T>> getListJsonParamsNoSession<T>(String uri,
      Map<String, dynamic> params, ConvertFunc<T> convertFunc) async {
    Map data;
    data = {'jsonrpc': '2.0', 'method': 'call', 'params': params};
    print('CALL_API ---- ' + uri + ': ' + data.toString());
    final response = await _dioBussiness.post(
      uri,
      data: data,
      options: Options(
          // headers: {
          //   'Cookie': AppUtils.odooSessionDto.session_id,
          // },
          ),
    );
    PagedListData<T> pagedListData =
        new PagedListData<T>((i) => convertFunc(i));
    pagedListData.fromJson(response.data['result']);
    return pagedListData;
  }

  Image getImage(String url, {double width: 200, double height: 200}) {
    return Image.network(
      imgUrl + 'files/' + url,
      headers: {'Authorization': 'Bearer ${AppUtils.token}'},
      fit: BoxFit.fill,
      width: width,
      height: height,
    );
  }

  void printError(error, StackTrace stacktrace) {
    debugPrint('error: $error & stacktrace: $stacktrace');
  }
}

typedef S ItemCreator<S>();
