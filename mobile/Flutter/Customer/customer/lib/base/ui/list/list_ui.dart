import 'package:customer/pages/bottomsheet/list/BottomSheetList.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:loading_more_list/loading_more_list.dart';
import 'package:customer/base/controller/BaseZone.dart';
import 'package:customer/base/ui/common_ui.dart';
import 'package:customer/base/ui/qr/ScanQrWidget.dart';
import 'package:customer/icon/custom_icons.dart';
import 'package:customer/theme/app_theme.dart';

extension ListUi on BuildContext {
  Widget listItemView(String lbl, String value,
      {padding: AppTheme.cardFieldPadding,
      background: Colors.transparent,
      styleLb,
      styleVl}) {
    return this.listItemViewWidget(
        lbl,
        Text(
          value ?? '',
          style: styleVl ?? Theme.of(this).textTheme.bodyText2,
          textAlign: TextAlign.right,
        ),
        padding: padding,
        background: background,
        styleLb: styleLb);
  }

  Widget scanQrCode(
      {TextEditingController textController,
      bool Function(String qrCode) onQrData,
      String initValue}) {
    textController ??= TextEditingController(text: initValue);
    return ScanQrWidget(
        oneTime: false,
        onData: (String qrCode) {
          if (onQrData != null) {
            if (qrCode != textController.text) {
              textController.text = qrCode;
              return onQrData(qrCode);
            } else {
              return true;
            }
          } else {
            return false;
          }
        },
        child: IgnorePointer(
          child: IconButton(
            icon: Icon(CustomIcons.fromName('CustomIcons.0xe80f')),
            iconSize: 30,
            color: Color(0xFF919395),
            onPressed: () {},
          ),
        ));
  }

  Widget listItemViewV2(String lbl, String value) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(4,2,4,2),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Expanded(
              flex: 2,
              child: Text(
                lbl,
                style: TextStyle(
                    color: Colors.black,
                    fontWeight: FontWeight.normal,
                    fontSize: 12),
              )),
          Expanded(
              flex: 3,
              child: Text(
                value,
                textAlign: TextAlign.right,
                style: TextStyle(

                    color: Colors.black,
                    // fontWeight: FontWeight.normal,
                    fontSize: 13),
              ))
        ],
      ),
    );
  }

  Widget itemView(
    String value,
  ) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(8, 0, 8, 0),
      child: Text(
        value,
        style: TextStyle(
            color: Colors.black, fontSize: 12, fontWeight: FontWeight.normal),
        textAlign: TextAlign.left,
      ),
    );
  }

  Widget itemTitleView(
    String value,
  ) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(8, 0, 8, 0),
      child: Text(
        value,
        style: TextStyle(
            color: Colors.black, fontSize: 13, fontWeight: FontWeight.bold),
        textAlign: TextAlign.left,
      ),
    );
  }

  Widget titleText(String value) {
    return Padding(
      padding: const EdgeInsets.only(left:8.0),
      child: Container(
        width: 200,
        child: Text(
          value,
          maxLines: 1,
          overflow: TextOverflow.ellipsis,
          style: TextStyle(
              color: Colors.black, fontSize: 13, fontWeight: FontWeight.bold),
        ),
      ),
    );
  }

  Widget listItemViewWidget(String lbl, Widget value,
      {padding: AppTheme.cardFieldPadding,
      background: AppTheme.white,
      styleLb}) {
    return Container(
        color: background,
        padding: padding,
        child:
            Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
          FittedBox(
              fit: BoxFit.contain,
              child: Text(
                GetUtils.isNullOrBlank(lbl) ? ' ' : lbl,
                style: styleLb ?? Theme.of(this).textTheme.caption,
              )),
          Expanded(
              child: Container(
            child: value,
            alignment: Alignment.centerRight,
          ))
        ]));
  }

  Widget listItemSubViewStatusWidget(String lbl, String status, Color color,
      {padding: AppTheme.cardFieldPadding,
      background: Colors.transparent,
      styleLb,
      styleVl}) {
    return listItemViewStatusWidget(lbl, status, color,
        padding: padding,
        background: background,
        styleLb: styleLb,
        styleVl: Theme.of(this).textTheme.bodyText1);
  }

  Widget listItemViewStatusWidget(String lbl, String status, Color color,
      {padding: AppTheme.cardHeaderPadding,
      background: Colors.transparent,
      styleLb,
      styleVl}) {
    return Container(
      padding: padding,
      child: Row(
        children: [
          Expanded(
              child: Text(lbl,
                  style: styleVl ?? Theme.of(this).textTheme.headline6)),
          Text(status ?? ' '),
          Padding(
              padding: EdgeInsets.only(left: 3),
              child: Icon(
                Icons.circle,
                size: 10,
                color: color,
              ))
        ],
      ),
    );
  }

  Widget listItemViewWidgetFixWidth(String lbl, Widget value,
      {width: 100.0, lblStyle, paddingStyle: AppTheme.cardFieldPadding}) {
    return Padding(
        padding: paddingStyle,
        child:
            Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
          SizedBox(
              width: width,
              child: Text(
                lbl == null ? '' : lbl,
                style: lblStyle == null
                    ? Theme.of(this).textTheme.bodyText1
                    : lblStyle,
                maxLines: 3,
              )),
          Expanded(
              child: Container(
            child: value,
            alignment: Alignment.centerRight,
          ))
        ]));
  }

  Widget actionWidgetExpanded(Widget child,
      {visible: true,
      onTap,
      showSuccess = false,
      String successMessage = 'success',
      showError = true,
      String errorMessage,
      ButtonStyle style,
      showConfirmDlg = false,
      String titleConfirm,
      String messageConfirm,
      String ok,
      String cancel}) {
    return actionWidget(child,
        visible: visible,
        onTap: onTap,
        showSuccess: showSuccess,
        successMessage: successMessage,
        showError: showError,
        errorMessage: errorMessage,
        style: style,
        isExpanded: true,
        showConfirmDlg: showConfirmDlg,
        titleConfirm: titleConfirm,
        messageConfirm: messageConfirm,
        ok: ok,
        cancel: cancel);
  }

  Widget actionWidget(Widget child,
      {visible: true,
      onTap,
      showSuccess = false,
      String successMessage = 'successMessage',
      showError = true,
      String errorMessage,
      ButtonStyle style,
      isExpanded = false,
      showConfirmDlg = false,
      String titleConfirm,
      String messageConfirm,
      String ok,
      String cancel}) {
    void action() {
      if (showConfirmDlg) {
        CommonUI.showConfirmDialog(
                title: titleConfirm,
                message: messageConfirm,
                ok: ok,
                cancel: cancel)
            .then((value) {
          if (value == ButtonActionType.OK) {
            BaseZone.run(() => onTap(),
                showError: showError,
                errorMessage: errorMessage,
                showSuccess: showSuccess,
                successMessage: successMessage);
          }
        });
      } else {
        BaseZone.run(onTap,
            showError: showError,
            errorMessage: errorMessage,
            showSuccess: showSuccess,
            successMessage: successMessage);
      }
    }

    final btn = OutlinedButton(
        child: child,
        style: style ??
            OutlinedButton.styleFrom(
                backgroundColor: Theme.of(this).primaryColor,
                textStyle: Theme.of(this).primaryTextTheme.bodyText2),
        onPressed: onTap == null ? null : action);
    return Visibility(
        visible: visible, child: isExpanded ? Expanded(child: btn) : btn);
  }

  Widget listItemSubHeader(String lbl, Widget value,
      {lblStyle: AppTheme.normalBold,
      visible: true,
      onTap,
      divider = true,
      showSuccess = false,
      String successMessage = 'success',
      showError = true,
      String errorMessage,
      ButtonStyle btnStyle,
      padding: AppTheme.cardFieldPadding}) {
    return listItemHeader(lbl, value,
        lblStyle: lblStyle,
        visible: visible,
        onTap: onTap,
        divider: divider,
        showSuccess: showSuccess,
        successMessage: successMessage,
        showError: showError,
        errorMessage: errorMessage,
        btnStyle: btnStyle,
        padding: padding);
  }

  Widget listItemHeader(String lbl, Widget value,
      {lblStyle,
      visible: true,
      onTap,
      divider = true,
      showSuccess = false,
      String successMessage = 'success',
      showError = true,
      String errorMessage,
      ButtonStyle btnStyle,
      padding: AppTheme.cardHeaderPadding}) {
    return Container(
        padding: padding,
        child: Column(children: [
          Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
            Expanded(
                child: Text(
              lbl == null ? '' : lbl,
              style: lblStyle == null
                  ? Theme.of(this).textTheme.headline6
                  : lblStyle,
            )),
            value == null
                ? Container()
                : this.actionWidget(value,
                    visible: visible,
                    onTap: onTap,
                    showError: showError,
                    showSuccess: showSuccess,
                    successMessage: successMessage,
                    errorMessage: errorMessage,
                    style: btnStyle ??
                        TextButton.styleFrom(
                            side: BorderSide.none,
                            minimumSize: Size(1, 1),
                            padding: EdgeInsets.zero,
                            textStyle:
                                Theme.of(this).primaryTextTheme.bodyText1,
                            tapTargetSize: MaterialTapTargetSize.shrinkWrap))
          ]),
        ]));
  }

  Widget textOnTap(String lbl, {onTap}) {
    return InkWell(
      onTap: onTap,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.fromLTRB(16, 0, 0, 0),
            child: Text(
              lbl,
              style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 14,
                  color: Colors.black),
            ),
          ),
          Icon(Icons.navigate_next)
        ],
      ),
    );
  }

  Widget titleContainer(String lbl, Icon icon) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Row(
        children: <Widget>[
          icon,
          Padding(
            padding: const EdgeInsets.fromLTRB(5, 0, 0, 0),
            child: Text(
              lbl,
              style: TextStyle(
                  color: Colors.black,
                  fontWeight: FontWeight.normal,
                  fontSize: 12),
            ),
          )
        ],
      ),
    );
  }

  Widget datePicker(
      {DateTime initialDate,
      DateTime firstDate,
      DateTime lastDate,
      void Function(DateTime) onValue,
      String fieldLabelText,
      String hintText}) {
    return Row(children: [
      Expanded(
          child: InputDatePickerFormField(
        initialDate: initialDate,
        firstDate: (firstDate ?? initialDate),
        lastDate: lastDate ?? DateTime.now(),
        fieldLabelText: fieldLabelText,
        fieldHintText: hintText,
      )),
      FittedBox(
          fit: BoxFit.contain,
          child: InkWell(
              child: Icon(
                Icons.calendar_today_outlined,
                color: AppTheme.grey,
              ),
              onTap: () {
                showDatePicker(
                        context: this,
                        initialDate: initialDate,
                        firstDate: firstDate ?? initialDate,
                        lastDate: lastDate ?? DateTime.now())
                    .then((value) {
                  if (onValue != null) onValue(value);
                });
              }))
    ]);
  }

  Widget textSearchAndQrCode(
      {@required void Function(String text) onchange,
      String hintText,
      TextEditingController textController,
      bool Function(String qrCode) onQrData,
      String initValue}) {
    return Container(
        padding: EdgeInsets.only(right: 15, top: 8),
        child: Row(
          children: [
            ScanQrWidget(
                onData: (String qrCode) {
                  if (onQrData != null) {
                    return onQrData(qrCode);
                  } else {
                    return false;
                  }
                },
                child: IgnorePointer(
                  child: IconButton(
                    icon: Icon(CustomIcons.fromName('CustomIcons.0xe80f')),
                    iconSize: 30,
                    color: Color(0xFF919395),
                    onPressed: () {},
                  ),
                )),
            Expanded(
              child: Container(
                  height: 40,
                  // decoration: _boxDecoration,
                  child: new TextFormField(
                    onChanged: (text) => onchange(text),
                    textInputAction: TextInputAction.search,
                    textAlignVertical: TextAlignVertical.center,
                    controller: textController,
                    initialValue: initValue,
                    decoration: InputDecoration(
                      hintText: hintText,
                      filled: true,
                      hintStyle: TextStyle(color: AppTheme.txtHintColor),
                      prefixIcon:
                          Icon(CustomIcons.fromName('CustomIcons.0xe810')),
                      fillColor: AppTheme.backgroundTextFile,
                      isDense: true,
                      contentPadding: EdgeInsets.all(2),
                    ),
                  )),
            )
          ],
        ));
  }

  Widget textSearch(
      {@required void Function(String text) onchange,
      String hintText,
      TextEditingController textController,
      String initValue}) {
    return Container(
        padding: EdgeInsets.only(left: 8, right: 8, top: 8),
        child: Row(
          children: [
            Expanded(
              child: Container(
                  height: 40,
                  // decoration: _boxDecoration,
                  child: new TextFormField(
                    onChanged: (text) => onchange(text),
                    textInputAction: TextInputAction.search,
                    textAlignVertical: TextAlignVertical.center,
                    controller: textController,
                    initialValue: initValue,
                    decoration: InputDecoration(
                      hintText: hintText,
                      filled: true,
                      hintStyle: TextStyle(color: AppTheme.txtHintColor),
                      prefixIcon:
                          Icon(CustomIcons.fromName('CustomIcons.0xe810')),
                      fillColor: AppTheme.backgroundTextFile,
                      isDense: true,
                      contentPadding: EdgeInsets.all(2),
                    ),
                  )),
            )
          ],
        ));
  }

  Widget totalRows(RxInt obs) {
    return Padding(
      padding: EdgeInsets.only(left: 20, top: 10, bottom: 5),
      child: Obx(() => Text(
            "${'All'.tr} (${CommonUI.formatNumber(obs.value)})",
            style: TextStyle(fontWeight: FontWeight.bold),
          )),
    );
  }

  Widget sliverTotalRows(RxInt obs) {
    return SliverToBoxAdapter(
        // alignment: Alignment.centerLeft,
        child: this.totalRows(obs));
  }

  Widget divider({padding: AppTheme.cardDividerPadding}) {
    return Container(
        child: Divider(
          color: Theme.of(this).dividerColor,
          height: 1,
          thickness: 1,
        ),
        padding: padding);
  }

  Widget normalText(String text,{color : Colors.black,size : 13.0}) {
    return Text(text,
        style: TextStyle(
            color: color, fontWeight: FontWeight.normal, fontSize: size));
  }
  Widget normalTextOneLine(String text,{color : Colors.black}) {
    return Text(text,
        maxLines: 2,
        overflow: TextOverflow.ellipsis,
        style: TextStyle(
            color: color, fontWeight: FontWeight.normal, fontSize: 13));
  }

  Widget dividerVertical({padding: AppTheme.cardDividerPadding}) {
    return Container(
        decoration: new BoxDecoration(
            border: Border(
                right: true
                    ? BorderSide(color: AppTheme.divider)
                    : BorderSide.none)));
  }

  Widget wrapListChipGroup<T>(List<T> list,
      {Rx<T> obs, onSelected, String Function(T) display}) {
    return Wrap(
        spacing: 0,
        runSpacing: -8,
        children: this.listChipGroup(list,
            obs: obs, onSelected: onSelected, display: display));
  }

  List<Widget> listChipGroup<T>(List<T> list,
      {Rx<T> obs, onSelected, String Function(T) display}) {
    List<Widget> choices = [];
    list.forEach((item) {
      choices.add(Container(
        padding: const EdgeInsets.only(left: 2, right: 2),
        child: ChoiceChip(
          label: Text(
            display == null ? item.toString() : display(item),
            style: item == obs.value //controller.repackingPlanningStatus
                ? TextStyle(color: AppTheme.white)
                : TextStyle(color: AppTheme.textBold),
          ),
          labelStyle: TextStyle(color: Colors.black, fontSize: 12.0),
          shape: RoundedRectangleBorder(
            side: BorderSide(color: AppTheme.backgroundStrokeTextFile),
            borderRadius: BorderRadius.circular(16.0),
          ),
          backgroundColor: Theme.of(this).backgroundColor,
          selectedColor: Theme.of(this).primaryColor,
          selected: obs.value == item,
          onSelected: (selected) {
            obs.value = item;
            // print(item.name);
            // Get.find<RepackController>().typePackSelect(item);
            // Get.find<RepackController>().getListRepacking();
            // controller.repackingPlanningStatus = selected;
            if (onSelected != null) onSelected();
          },
        ),
      ));
    });
    return choices;
  }
  Widget listItemDescription(String description,
      {background: AppTheme.white,
      styleLb: AppTheme.normal,
      styleVl: AppTheme.normalBold}) {
    return Container(
        padding: AppTheme.cardFieldPadding,
        color: background,
        child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
          Container(
            child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  FittedBox(
                      fit: BoxFit.contain,
                      child: Text(
                        ('description'.tr),
                        style: styleVl,
                      )),
                ]),
          ),
          (description != null && description.isNotEmpty)
              ? Container(
                  padding: AppTheme.edgeTop,
                  child: Text(
                    (description),
                    style: styleLb,
                    maxLines: 5,
                  ),
                )
              : SizedBox(),
        ]));
  }

  showBottomSheet<T>(
      Widget title,
      Function(BuildContext context, T item, int index) itemBuilder,
      LoadingMoreBase<T> sourceList) {
    showModalBottomSheet(
      context: this,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.vertical(
          top: Radius.circular(8),
        ),
      ),
      clipBehavior: Clip.antiAliasWithSaveLayer,
      builder: (context) {
        return BottomSheetList(title, itemBuilder, sourceList);
      },
    );
  }
  showBottomSheetV2<T>(Widget container) {
    showModalBottomSheet(
      context: this,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.vertical(
          top: Radius.circular(8),
        ),
      ),
      clipBehavior: Clip.antiAliasWithSaveLayer,
      builder: (context) {
        return container;
      },
    );
  }
}
