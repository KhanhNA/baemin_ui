import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AEStaffComponent } from './a-e-staff.component';

describe('AEStaffComponent', () => {
  let component: AEStaffComponent;
  let fixture: ComponentFixture<AEStaffComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AEStaffComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AEStaffComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
