import {Component, Injector} from '@angular/core';
import {FormBuilder} from '@angular/forms';
import {Router} from '@angular/router';
import {HttpParams} from '@angular/common/http';
import {TranslateService} from '@ngx-translate/core';
import {CustomerModel} from '../../../../_models/customer.model';
import {AuthoritiesUtils} from '../../../../base/utils/authorities.utils';
import {
  ApiService,
  BaseSearchLayout,
  ButtonFields,
  ColumnFields,
  FormStateService,
  SelectModel, UtilsService,
} from '@next-solutions/next-solutions-base';
import {CustomerStatusEnum} from '../../../../_models/enums/CustomerStatusEnum';

@Component({
  selector: 'app-customer',
  templateUrl: './list-customer.component.html',
  styleUrls: ['./list-customer.component.scss'],
})
export class CustomerComponent extends BaseSearchLayout {

  moduleName = 'customer';

  columns: ColumnFields[] = [];
  buttons: ButtonFields[] = [];
  statusValues: SelectModel[] = [];

  constructor(protected formBuilder: FormBuilder, protected router: Router, protected apiService: ApiService,
              protected utilsService: UtilsService, protected formStateService: FormStateService,
              protected translateService: TranslateService, protected injector: Injector) {
    super(router, apiService, utilsService, formStateService, translateService, injector,
      formBuilder.group({
        staf: [''],
        name: [''],
        address: [''],
        phone: [''],
        status: ['_'],
      }));

    this.columns.push(
      {
        columnDef: 'stt',
        header: 'stt',
        title: (e: any) => `${UtilsService.calcPosition(e, this.results, this.paging)}`,
        cell: (e: any) => `${UtilsService.calcPosition(e, this.results, this.paging)}`,
        className: 'mat-column-stt',
      },
      {
        columnDef: 'code', header: 'code',
        title: (c: CustomerModel) => `${c.code}`,
        cell: (c: CustomerModel) => `${c.code}`,
        className: 'mat-column-code',
      },
      {
        columnDef: 'name', header: 'name',
        title: (c: CustomerModel) => `${c.name}`,
        cell: (c: CustomerModel) => `${c.name}`,
        className: 'mat-column-name',
      },
      {
        columnDef: 'address', header: 'address',
        title: (c: CustomerModel) => `${c.address}`,
        cell: (c: CustomerModel) => `${c.address}`,
        className: 'mat-column-address',
      },
      {
        columnDef: 'phone', header: 'phone',
        title: (c: CustomerModel) => `${c.phone}`,
        cell: (c: CustomerModel) => `${c.phone}`,
        className: 'mat-column-phone',
      },
      {
        columnDef: 'status',
        header: 'status',
        title: (c: CustomerModel) => `${
          this.utilsService.getEnumValueTranslated(CustomerStatusEnum, c.status === true ? '1' : '0')}`,
        cell: (c: CustomerModel) => `${
          this.utilsService.getEnumValueTranslated(CustomerStatusEnum, c.status === true ? '1' : '0')}`,
        className: 'mat-column-status',
      },
    );

    this.buttons.push(
      {
        columnDef: 'resetPass',
        color: 'warn',
        icon: 'loop',
        click: 'resetPass',
        display: (c: CustomerModel) => c && true,
      },
      {
        columnDef: 'active',
        color: 'warn',
        icon: 'done_all',
        click: 'active',
        disabled: (c: CustomerModel) =>
          UtilsService.getEnumValue(CustomerStatusEnum, c.status === true ? 'ACTIVE' : 'INACTIVE') !== CustomerStatusEnum._0,
        display: (c: CustomerModel) => c && AuthoritiesUtils.hasAuthority('post/customers/active'),
      },
      {
        columnDef: 'edit',
        color: 'warn',
        icon: 'edit',
        header: {
          columnDef: 'add',
          icon: 'add',
          color: 'warn',
          click: 'addOrEdit',
          display: (c: CustomerModel) => true, // AuthoritiesUtils.hasAuthority('post/customers'),
        },
        click: 'addOrEdit',
        display: (c: CustomerModel) => c && true, //AuthoritiesUtils.hasAuthority('patch/customers/{id}'),
      },
      {
        columnDef: 'deactivate',
        color: 'warn',
        icon: 'clear',
        click: 'deactivate',
        disabled: (c: CustomerModel) =>
          UtilsService.getEnumValue(CustomerStatusEnum, c.status === true ? '1' : '0') !== CustomerStatusEnum._1,
        display: (c: CustomerModel) => c && AuthoritiesUtils.hasAuthority('post/customers/deactivate'),
      },
    );
  }

  ngOnInit = async () => {
    Object.keys(CustomerStatusEnum).forEach(key => {
      const value = UtilsService.getEnumValue(CustomerStatusEnum, key.replace('_', ''));
      this.translateService.get(value).subscribe(res => {
        this.statusValues.push(new SelectModel(key, res));
      });
    });

    super.ngOnInit();

    this.onSubmit();
  };

  search() {
    const status = this.searchForm.get('status').value.replace('_', '');
    const params = new HttpParams()
      .set('code', this.searchForm.get('code').value)
      .set('name', this.searchForm.get('name').value)
      .set('address', this.searchForm.get('address').value)
      .set('phone', this.searchForm.get('phone').value)
      .set('status', status)
    this._fillData('/customers', params);
  }

  addOrEdit(customer: CustomerModel) {
    if (customer) {
      this.router.navigate([this.router.url, 'edit', customer.id]);
    } else {
      this.router.navigate([this.router.url, 'add']);
    }
  }

  active(row: CustomerModel, index: number) {
    this.utilsService.execute(this.apiService.post('/customers/active', row),
      this.onSuccessFunc, this.moduleName + '.success.active',
      this.moduleName + '.confirm.active');
  }

  deactivate(row: CustomerModel, index: number) {
    this.utilsService.execute(this.apiService.post('/customers/deactivate', row),
      this.onSuccessFunc, this.moduleName + '.success.deactivate',
      this.moduleName + '.confirm.deactivate');
  }

  resetPass(customer: CustomerModel) {
    this.utilsService.execute(this.apiService.post('/customers/reset-pass', customer),
      this.onSuccessFunc, this.moduleName + '.confirm.reset-pass',
      this.moduleName + '.confirm.reset-pass');
  }

}
