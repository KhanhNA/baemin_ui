import {BrowserModule} from '@angular/platform-browser';
import {CUSTOM_ELEMENTS_SCHEMA, ErrorHandler, NgModule} from '@angular/core';

import {NgxPaginationModule} from 'ngx-pagination';
import {CookieService} from 'ngx-cookie-service';
import {HTTP_INTERCEPTORS, HttpClient, HttpClientModule} from '@angular/common/http';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {A11yModule} from '@angular/cdk/a11y';
import {BidiModule} from '@angular/cdk/bidi';
import {ObserversModule} from '@angular/cdk/observers';
import {OverlayModule} from '@angular/cdk/overlay';
import {PlatformModule} from '@angular/cdk/platform';
import {PortalModule} from '@angular/cdk/portal';
import {ScrollDispatchModule} from '@angular/cdk/scrolling';
import {CdkStepperModule} from '@angular/cdk/stepper';
import {CdkTableModule} from '@angular/cdk/table';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {TranslateLoader, TranslateModule} from '@ngx-translate/core';
import {TranslateHttpLoader} from '@ngx-translate/http-loader';
import {NgxChartsModule} from '@swimlane/ngx-charts';
import {FlexLayoutModule} from '@angular/flex-layout';

import {AppComponent} from './app.component';
import {LoginComponent} from './login/login.component';
import {AuthenticationService} from './_services/authentication.service';
import {ErrorInterceptor} from './_helpers/error.interceptor';
import {NavService} from './_services/nav.service';
import {MenuListItemComponent} from './_helpers/menu-list-item/menu-list-item.component';
import {TopNavComponent} from './_helpers/top-nav/top-nav.component';
import {DashboardComponent} from './dashboard/dashboard.component';
import {LogoutComponent} from './logout/logout.component';
import {MaterialModule} from './modules/material.module';
import {AppRoutingModule} from './modules/app-routing.module';
import {ToastrModule} from 'ngx-toastr';
import {JsogService} from 'jsog-typescript';
import {FileSaverModule} from 'ngx-filesaver';
import {DatePipe, HashLocationStrategy, LocationStrategy} from '@angular/common';
import {GlobalErrorHandler} from './_services/errorHandle/global-error-handler';
import {BusinessComponents} from './modules/businessComponents';
import {environment} from '../environments/environment';
import {NextSolutionsModules} from '@next-solutions/next-solutions-base';
import { AddEditStaffComponent } from './components/category/staff/a-e-staff/a-e-staff.component';
import { StaffComponent } from './components/category/staff/list-staff/list-staff.component';

export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}

@NgModule({
  exports: [
    // CDK
    A11yModule,
    BidiModule,
    ObserversModule,
    OverlayModule,
    PlatformModule,
    PortalModule,
    ScrollDispatchModule,
    CdkStepperModule,
    CdkTableModule,
  ],
  declarations: [
    AppComponent,
    LoginComponent,
    LogoutComponent,
    MenuListItemComponent,
    TopNavComponent,
    DashboardComponent,

    BusinessComponents,



    AddEditStaffComponent,

    StaffComponent,

  ],
  // Mấy ông mà gọi Modal là phải cho vào đây nhé @@
  entryComponents: [LoginComponent],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    HttpClientModule,
    NgxPaginationModule,
    BrowserAnimationsModule,
    FileSaverModule,
    ToastrModule.forRoot(), // ToastrModule added
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient],
      },
    }),
    FormsModule,
    FlexLayoutModule,
    MaterialModule,
    NgxChartsModule,
    NextSolutionsModules.forRoot({
      data: {
        BASE_URL: environment.BASE_URL,
        PAGE_SIZE: environment.PAGE_SIZE,
        PAGE_SIZE_OPTIONS: environment.PAGE_SIZE_OPTIONS,
        API_DATE_FORMAT: environment.API_DATE_FORMAT,
        DIS_DATE_FORMAT: environment.DIS_DATE_FORMAT,
      },
    }),
  ],
  providers: [
    {provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true},
    {provide: LocationStrategy, useClass: HashLocationStrategy},
    {provide: ErrorHandler, useClass: GlobalErrorHandler},
    JsogService,
    DatePipe,
    CookieService,
    LoginComponent,
    AuthenticationService,
    NavService,
  ],
  bootstrap: [AppComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
})

export class AppModule {
}

