import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {DashboardComponent} from '../dashboard/dashboard.component';
import {CustomerComponent} from '../components/category/customer/list-customer/list-customer.component';
import {StaffComponent} from '../components/category/staff/list-staff/list-staff.component';
import {AddEditCustomerComponent} from '../components/category/customer/a-e-customer/a-e-customer.component';
import{AddEditStaffComponent} from '../components/category/staff/a-e-staff/a-e-staff.component';
import {LogoutComponent} from '../logout/logout.component';

const routes: Routes = [
  {path: '', component: DashboardComponent},
  {path: 'home', component: DashboardComponent},

  {path: 'category-customer', component: CustomerComponent},
  {path: 'category-customer/add', component: AddEditCustomerComponent},
  {path: 'category-customer/edit/:id', component: AddEditCustomerComponent},
  {path: 'category-van', component: CustomerComponent},
  {path: 'category-staff', component: StaffComponent},
  {path: 'category-staff/add', component: AddEditStaffComponent},
  {path: 'logout', component: LogoutComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {
      useHash: true,
    }
  )
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
