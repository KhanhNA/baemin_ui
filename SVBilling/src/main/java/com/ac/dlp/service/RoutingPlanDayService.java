package com.ac.dlp.service;

import com.ac.dlp.Utils.Utils;
import com.ac.dlp.entity.BillLadingDetail;
import com.ac.dlp.entity.RoutingPlanDay;
import com.ac.dlp.enums.WarehouseType;
import org.hibernate.internal.SessionImpl;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

@Service
public class RoutingPlanDayService {

    @PersistenceContext
    protected EntityManager entityManager;

    @Transactional
    public void getBillLading() throws Exception {

        Connection cnn = entityManager.unwrap(SessionImpl.class).connection();
        String sql =
                "  select COALESCE(w.longitude, b.longitude) as longitude, " +
                        "       COALESCE(w.latitude, b.latitude) as latitude, " +
//                        "       b.name as orderNumber, " +
                        "       b.expected_from_time, " +
                        "       b.expected_to_time, " +
                        "       b.warehouse_id, " +
                        "       b.warehouse_type, " +
                        "       b.status, " +
                        "       b.zone_area_id, " +
                        "       w.depot_id," +
                        "       b.bill_lading_id," +
                        "       b.id " +
                        " from sharevan_bill_lading_detail as b " +
                        "       left join share_van_warehouse w on b.warehouse_id = w.id " +
//                        "       left join share_van_area a on a.id = w.area_id " +
//                        " where b.status = '1' " +
                        " order by bill_lading_id, warehouse_type";
        List<Object> params = new ArrayList<>();
        HashMap columnsMap;
        PreparedStatement st = cnn.prepareStatement(sql);
        int i = 1;
        for (Object obj : Utils.safe(params)) {
            st.setObject(i++, obj);
        }

        ResultSet rs = st.executeQuery();
        if (rs == null) {
            return;
        }
//        ResultSetMetaData metaData = rs.getMetaData();
//        columnsMap = loadColumn(metaData);
//        System.out.println(columnsMap.toString());

        List<BillLadingDetail> result = new ArrayList<>();
        i = 0;
        List<Integer> billDetailIds = new ArrayList<>();
        Integer pickBillId = null, deliveryBillId, tmpBillId;
        Integer pickZoneAreaId = null, deliveryZoneAreaId = null, tmpZoneAreaId;
        Integer pickDepotId = null, deliveryDepotId = null, tmpDepotId;
        WarehouseType warehouseType;
        while (rs.next()) {//duyet bill_lading_detail
            i++;
            tmpBillId = rs.getInt("bill_lading_id");
            tmpDepotId = rs.getInt("depot_id");
            tmpZoneAreaId = rs.getInt("zone_area_id");
            warehouseType = WarehouseType.of(rs.getString("warehouse_type"));

            billDetailIds.add(rs.getInt("id"));


            if (warehouseType == WarehouseType.PICKUP) {
                pickZoneAreaId = tmpZoneAreaId;
                pickBillId = tmpBillId;
                pickDepotId = tmpDepotId;
            } else {
                if (pickZoneAreaId == null) {
                    throw new Exception("khong co kho nhan");
                }
                if (pickBillId == null || tmpBillId == null || pickBillId != tmpBillId) {
                    throw new Exception("khong co don nhan");
                }
                deliveryZoneAreaId = tmpZoneAreaId;
                deliveryDepotId = tmpDepotId;
            }
            if (pickZoneAreaId == null) {
                throw new Exception("kho null:" + pickZoneAreaId + "_" + deliveryZoneAreaId);
            }
            insertRoutingPlanDay(rs, warehouseType, pickZoneAreaId, deliveryZoneAreaId, pickDepotId, deliveryDepotId);
        }
        updateBillStatus(cnn, billDetailIds);
        System.out.println("a:" + result.toString());

    }

    private void updateBillStatus(Connection cnn, List<Integer> billIds) throws SQLException {
        if (billIds == null || billIds.size() == 0) {
            return;
        }
        String ids = org.apache.commons.lang3.StringUtils.join(billIds, ",");
        String sql = "update sharevan_bill_lading_detail set status = '2' where id in (" + ids + ")";
        PreparedStatement stmt = cnn.prepareStatement(sql);
        stmt.executeUpdate();

    }

    private void insertRoutingPlanDay(ResultSet rs, WarehouseType warehouseType, Integer pickupZoneAreaId, Integer deliveryZoneAreaId,
                                      Integer pickupDepotId, Integer deliveryDepotId) throws SQLException {

        Integer warehouseId = rs.getInt("warehouse_id") == 0 ? null : rs.getInt("warehouse_id");
        RoutingPlanDay routingPlanDay = new RoutingPlanDay();

        routingPlanDay.setLatitude(rs.getDouble("latitude"));
        routingPlanDay.setLongitude(rs.getDouble("longitude"));
//        routingPlanDay.setOrderNumber(rs.getString("orderNumber"));
        routingPlanDay.setExpectedFromTime(rs.getTimestamp("expected_from_time"));
        routingPlanDay.setExpectedToTime(rs.getTimestamp("expected_to_time"));

        routingPlanDay.setWarehouseType(warehouseType.getValue());
        routingPlanDay.setStatus("running");
        routingPlanDay.setDepotId(pickupDepotId);
        routingPlanDay.setWarehouseId(warehouseId);
        if (warehouseType == WarehouseType.PICKUP || pickupZoneAreaId == deliveryZoneAreaId) {
            routingPlanDay.setZoneAreaId(pickupZoneAreaId);
            entityManager.merge(routingPlanDay);
            return;
        }

//            routingPlanDay.setZoneAreaId(pickupZoneId);
        //1. insert 1 bg delivery toi depot cua zone nhan
        routingPlanDay.setDepotId(pickupDepotId);
        routingPlanDay.setWarehouseId(null);
        routingPlanDay.setWarehouseType(WarehouseType.DELIVERY.getValue());
        routingPlanDay.setZoneAreaId(pickupZoneAreaId);
        entityManager.merge(routingPlanDay);
        //2. insert 1 delivery toi depot cua zone tra
        routingPlanDay.setDepotId(deliveryDepotId);
        routingPlanDay.setWarehouseId(null);
        routingPlanDay.setWarehouseType(WarehouseType.DELIVERY.getValue());
        routingPlanDay.setZoneAreaId(deliveryZoneAreaId);
        entityManager.merge(routingPlanDay);
        //3. insert 1 delivery toi warehouse cua khach hang
        routingPlanDay.setDepotId(deliveryDepotId);
        routingPlanDay.setWarehouseId(warehouseId);
        routingPlanDay.setWarehouseType(WarehouseType.DELIVERY.getValue());
        routingPlanDay.setZoneAreaId(deliveryZoneAreaId);
        entityManager.merge(routingPlanDay);


    }


    private HashMap loadColumn(ResultSetMetaData metaData) throws Exception {
        HashMap<String, String> columns = new HashMap<>();
        for (int i = 1; i <= metaData.getColumnCount(); i++) {
            columns.put(metaData.getColumnLabel(i).replaceAll("_", "").toLowerCase(), metaData.getColumnLabel(i));
        }
        return columns;
    }

    private <T> T convert2Class(Class<T> clazz, ResultSet rs, HashMap columnNames)
            throws Exception {
        if (clazz == Long.class) {
            return (T) rs.getObject(1);
        }
        Field[] fields = clazz.getDeclaredFields();
        String fieldName;
        T returnClass = clazz.newInstance();
        for (Field field : fields) {
            int modifier = field.getModifiers();
            if (Modifier.isStatic(modifier) || Modifier.isFinal(modifier)) {
                continue;
            }
            if (Modifier.isPrivate(modifier)) {
                field.setAccessible(true);
            }
            fieldName = field.getName();
            String columnName = (String) columnNames.get(fieldName.toLowerCase());
            if (StringUtils.isEmpty(columnName)) {
                continue;
            }

            Object value = rs.getObject(columnName, field.getType());
            field.set(returnClass, value);

        }
        return returnClass;
    }

}
