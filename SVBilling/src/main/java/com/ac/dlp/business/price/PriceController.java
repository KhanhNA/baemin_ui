package com.ac.dlp.business.price;


import com.ac.dlp.entity.BillLadingDetail;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import static com.ac.dlp.ApiConstant.API_V1;


@RestController
@RequestMapping(API_V1)
@RequiredArgsConstructor
public class PriceController {

    private final PriceService priceService;

    @PostMapping("/calPrice")
    public ResponseEntity<BillLadingDetail> calPrice(BillLadingDetail resource) {
        return ResponseEntity.ok(resource);
    }

}
