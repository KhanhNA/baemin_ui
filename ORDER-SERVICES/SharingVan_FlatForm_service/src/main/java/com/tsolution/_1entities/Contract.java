package com.tsolution._1entities;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonView;
import com.tsolution._1entities.base.SuperEntity;
import com.tsolution._1entities.enums.JsonEntityViewer;
import com.voodoodyne.jackson.jsog.JSOGGenerator;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "contract")
@JsonIdentityInfo(generator = JSOGGenerator.class)
public class Contract extends SuperEntity implements Serializable {
    private static final long serialVersionUID = 1L;

    @Column(name = "contract_code")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private String contractCode;

    @Column(name = "phone1")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private String phone1;

    @Column(name = "phone2")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private String phone2;

    @Column(name = "fax")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private String fax;

    @Column(name = "address")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private String address;

    @Column(name = "website")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private String website;

    @Column(name = "contract_name")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private String contractName;

    @Column(name = "saleman_id")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private Long salemanId;

    @Column(name = "status")
    private Integer status;

    public String getContractCode() {
        return contractCode;
    }

    public void setContractCode(String contractCode) {
        this.contractCode = contractCode;
    }

    public String getPhone1() {
        return phone1;
    }

    public void setPhone1(String phone1) {
        this.phone1 = phone1;
    }

    public String getPhone2() {
        return phone2;
    }

    public void setPhone2(String phone2) {
        this.phone2 = phone2;
    }

    public String getFax() {
        return fax;
    }

    public void setFax(String fax) {
        this.fax = fax;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public String getContractName() {
        return contractName;
    }

    public void setContractName(String contractName) {
        this.contractName = contractName;
    }



    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String toString() {
      return "Contract{contractCode=" + contractCode + 
        ", phone1=" + phone1 + 
        ", phone2=" + phone2 + 
        ", fax=" + fax + 
        ", address=" + address + 
        ", website=" + website + 
        ", contractName=" + contractName + 
        ", salemanId=" + salemanId + 
        ", status=" + status + 
        "}";
    }
}