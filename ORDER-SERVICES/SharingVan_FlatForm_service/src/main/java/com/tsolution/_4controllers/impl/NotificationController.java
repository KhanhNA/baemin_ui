package com.tsolution._4controllers.impl;

import com.tsolution._1entities.Notification;
import com.tsolution._1entities.dto.PushNotificationRequest;
import com.tsolution._3services.NotificationService;
import com.tsolution._4controllers.BaseController;
import com.tsolution.excetions.BusinessException;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.text.ParseException;

@RestController
@RequestMapping("/notification")
public class NotificationController extends BaseController {

    @Autowired
    private NotificationService notificationService;

    @RequestMapping("/all")
    @PreAuthorize("hasAuthority('get/notification/all')")
    public ResponseEntity<Object> findAll() {
        return this.notificationService.findAll();
    }


    @GetMapping("/getMailBox")
    @ApiOperation(value = "token",
            notes = "get message for app by token Authorization",
            response = PushNotificationRequest.class,
            responseContainer = "String")
    @PreAuthorize("hasAuthority('get/notification/getMailBox')")
    public ResponseEntity<Object> findByToken(
            @RequestHeader("Authorization") String authorization,
            @RequestHeader("Accept-Language") String acceptLanguage) throws BusinessException {
        return this.notificationService.findNotificationByToken(authorization);
    }

    @PostMapping
    @PreAuthorize("hasAuthority('post/notification')")
    public ResponseEntity<Object> create(
            @RequestHeader("Authorization") String authorization,
            @RequestHeader("Accept-Language") String acceptLanguage,
            @RequestBody Notification source) throws BusinessException, ParseException {
        return this.notificationService.create(acceptLanguage,source);
    }

    @PatchMapping("/{id}")
    @PreAuthorize("hasAuthority('patch/notification/{id}')")
    public ResponseEntity<Object> update(
            @RequestHeader("Authorization") String authorization,
            @RequestHeader("Accept-Language") String acceptLanguage,
            @PathVariable("id") Long id,
            @RequestBody Notification source) throws BusinessException, ParseException {
        return this.notificationService.update(acceptLanguage,id,source);
    }
}
