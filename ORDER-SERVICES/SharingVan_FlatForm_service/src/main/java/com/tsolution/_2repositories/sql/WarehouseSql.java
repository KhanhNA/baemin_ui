package com.tsolution._2repositories.sql;

public class WarehouseSql {

	private WarehouseSql() {
	}

	public static final String FIND_QUERY = " FROM warehouse w WHERE (:organizationId is null or w.organization_id = :organizationId) "
			+ " AND (:maxDistance is null OR :currentLat is null OR :currentLng is null "
			+ "		 OR calcDistanceByLatLng(:currentLat, :currentLng, w.lat, w.lng) < :maxDistance )"
			+ " AND (:code is null OR LOWER(w.code) like LOWER(CONCAT('%',:code,'%'))) "
			+ "	AND (:name is null OR LOWER(w.name) like LOWER(CONCAT('%',:name,'%'))) "
			+ "	AND (:description is null OR LOWER(w.description) like LOWER(CONCAT('%',:description,'%'))) "
			+ "	AND (:address is null OR LOWER(w.address) like LOWER(CONCAT('%',:address,'%'))) "
			+ " AND (:status is null OR w.status = :status) ";

	public static final String FIND_WAREHOUSE_BY_CUSTOMER_RENT = " FROM customer_rent_detail crd "
			+ " 	JOIN customer_rent cr ON cr.id = crd.customer_rent_id AND cr.customer_id = :customerId "
			+ " 	JOIN warehouse w on w.id = crd.warehouse_id ";
}
