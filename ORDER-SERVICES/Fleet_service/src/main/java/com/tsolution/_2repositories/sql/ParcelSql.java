package com.tsolution._2repositories.sql;

public class ParcelSql {
	private ParcelSql() {
	}

	public static final String FIND = " FROM parcel p WHERE (:code is null OR LOWER(p.code) LIKE LOWER(CONCAT('%',:code,'%'))) AND (:status is null OR p.status = :status) ";
}
