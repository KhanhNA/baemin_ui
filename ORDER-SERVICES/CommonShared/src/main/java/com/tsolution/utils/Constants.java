package com.tsolution.utils;

public final class Constants {
	public static final String DATE_PATTERN = "yyyy-MM-dd HH:mm:ss.SSS'Z'";
	public static final String TIME_PATTERN = "HH:mm:ss.SSS'Z'";
	public static final int QR_CODE_SIZE = 222;
	public static final int IMPORT_BILL = 1;
	public static final int EXPORT_BILL = 2;

	public static final Integer NOT_COMPLETE = 0;
	public static final Integer COMPLETE = 1;

	public static final Integer CONFIRMED_BILL = 1;
	public static final Integer COMPLETED_BILL = 2;
	private Constants() {
	}
}
