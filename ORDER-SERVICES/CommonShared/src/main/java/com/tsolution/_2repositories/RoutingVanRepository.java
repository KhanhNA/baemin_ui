package com.tsolution._2repositories;

import com.tsolution._1entities.RoutingVan;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.List;

public interface RoutingVanRepository extends JpaRepository<RoutingVan, Long>,
        JpaSpecificationExecutor<RoutingVan>, RoutingVanRepositoryCustom {

}