package com.tsolution._2repositories.impl;

import com.tsolution._1entities.RoutingPlanDay;
import com.tsolution._2repositories.RoutingPlanDayRepositoryCustom;
import com.tsolution.excetions.BusinessException;
import com.tsolution.utils.Constants;
import org.codehaus.jackson.map.Serializers;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
@Repository
public class RoutingPlanDayRepositoryCustomImpl implements RoutingPlanDayRepositoryCustom {
    @PersistenceContext
    private EntityManager em;


    @Override
    public List<RoutingPlanDay> getRoutPlanForVanToday(Long id) throws BusinessException {
        StringBuilder sb = new StringBuilder();
        sb.append("select * from routing_plan_day rpd ");
        sb.append("where rpd.routing_van_id = :id ") ;
        sb.append("order by rpd.order_number");
        HashMap<String, Object> params = new HashMap<>();
        params.put("id",id);
        return BaseRepository.getResultListNativeQuery(this.em,sb.toString(),params, RoutingPlanDay.class);
    }

    @Override
    public List<RoutingPlanDay> getRoutPlanForVanByDate(Long vanId, LocalDateTime fDate, LocalDateTime tDate) throws BusinessException {
        StringBuilder sb = new StringBuilder();
        sb.append("select * from routing_plan_day where van_id = :id ");
        sb.append("and DATE(due_time) >= :ftime and DATE(due_time) <= :totime ");
        sb.append("order by order_number");
        HashMap<String,Object> params = new HashMap<>();
        params.put("id", vanId);
        params.put("ftime", fDate);
        params.put("totime", tDate);
        return BaseRepository.getResultListNativeQuery(em, sb.toString(),params,RoutingPlanDay.class);
    }

    @Override
    public List<RoutingPlanDay> getNextDestinationInRouting(Long routingId) throws BusinessException {
        StringBuilder sb = new StringBuilder();
        sb.append("select * from routing_plan_day rpd ");
        sb.append(" where rpd.routing_van_id = :routingId and rpd.status = :status ");
        sb.append(" and rpd.order_number = (");
        sb.append(" select min(rpd1.order_number) ");
        sb.append(" from routing_plan_day  rpd1 ");
        sb.append(" where rpd1.routing_van_id = :routingId ");
        sb.append(" and rpd1.status = :status limit 1)");
        HashMap<String, Object> params = new HashMap<>();
        params.put("routingId", routingId);
        params.put("status", Constants.NOT_COMPLETE);
        return BaseRepository.getResultListNativeQuery(em,sb.toString(),params,RoutingPlanDay.class);
    }
}
