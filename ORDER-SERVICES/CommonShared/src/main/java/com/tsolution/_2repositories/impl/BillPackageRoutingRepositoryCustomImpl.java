package com.tsolution._2repositories.impl;

import com.tsolution._1entities.BillPackageRouting;
import com.tsolution._2repositories.BillPackageRoutingRepositoryCustom;
import com.tsolution.excetions.BusinessException;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.HashMap;
import java.util.List;
@Repository
public class BillPackageRoutingRepositoryCustomImpl implements BillPackageRoutingRepositoryCustom {
    @PersistenceContext
    private EntityManager em;
    @Override
    public List<BillPackageRouting> getPackageByRoutingPlan(Long routingPlanDayId) throws BusinessException {
        StringBuilder sb = new StringBuilder();
        sb.append("select bpr.*, rpd.id routing_plan_detail_id  from routing_plan_detail rpd ");
        sb.append("join bill_package_routing bpr on rpd.bill_package_routing_id = bpr.id ");
        sb.append( "where rpd.routing_plan_day_id = :id");
        HashMap<String,Object> params = new HashMap<>();
        params.put("id",routingPlanDayId);
        return BaseRepository.getResultListNativeQuery(em, sb.toString(),params, BillPackageRouting.class);
    }
}
