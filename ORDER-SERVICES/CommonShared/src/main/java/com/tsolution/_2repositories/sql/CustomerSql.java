package com.tsolution._2repositories.sql;

public class CustomerSql {

	private CustomerSql() {
	}

	public static final String FIND_CUSTOMER_RENT_WAREHOUSE = " FROM customer_rent_detail crd "
			+ " 	JOIN customer_rent cr on cr.id = crd.customer_rent_id "
			+ " 	JOIN customer c on c.id = cr.customer_id " + " WHERE crd.warehouse_id = :warehouseId ";
}
