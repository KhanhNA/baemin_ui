package com.tsolution._2repositories;

import com.tsolution._1entities.AppParam;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import com.tsolution._1entities.ApparamValue;
import org.springframework.data.jpa.repository.Query;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

public interface ApparamValueRepository extends JpaRepository<ApparamValue, Long>, JpaSpecificationExecutor<ApparamValue> {
    Optional<ApparamValue> findByParamCode (String code);

    Optional<ApparamValue> findByParamCodeAndAndCreateDate(String paramCode, LocalDateTime createDate);
    @Query(value="SELECT av.* FROM apparam_value av LEFT JOIN app_param ap ON av.param_id = ap.id " +
            " inner join apparam_group apg on ap.app_param_group_id = apg.id and apg.group_code = ? ",
            nativeQuery = true)
    List<ApparamValue> findByGroupCode(String groupCode);
    @Query(value="SELECT av.param_code FROM apparam_value av LEFT JOIN app_param ap ON av.param_id = ap.id " +
            " inner join apparam_group apg on ap.app_param_group_id = apg.id and apg.group_code = ? ",
            nativeQuery = true)
    List<Object> findNameByGroupCode(String groupCode);
}