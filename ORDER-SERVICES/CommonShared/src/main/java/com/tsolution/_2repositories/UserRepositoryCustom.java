package com.tsolution._2repositories;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.tsolution._1entities.User;
import com.tsolution.excetions.BusinessException;

public interface UserRepositoryCustom {
	Page<User> find(User user, Pageable pageable) throws BusinessException;
}
