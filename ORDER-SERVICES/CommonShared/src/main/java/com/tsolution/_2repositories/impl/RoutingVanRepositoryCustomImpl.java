package com.tsolution._2repositories.impl;

import com.tsolution._1entities.RoutingVan;
import com.tsolution._2repositories.RoutingVanRepositoryCustom;
import com.tsolution.config.LocalDateTimeConverter;
import com.tsolution.excetions.BusinessException;
import com.tsolution.utils.Constants;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.sql.Date;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;

@Repository
public class RoutingVanRepositoryCustomImpl implements RoutingVanRepositoryCustom {
    @PersistenceContext
    private EntityManager em;

    @Override
    public List<RoutingVan> getRoutingVanDay(Long id) throws BusinessException {
        StringBuilder sb = new StringBuilder();
        sb.append("select u.* from routing_van u where u.from_time <= date_sub(sysdate(),interval 10 day) and u.to_time >= date_sub(sysdate(),interval 10 day) and u.van_id= :id");
        HashMap<String,Object> params = new HashMap<>();
        params.put("id",id);
        return BaseRepository.getResultListNativeQuery(this.em,sb.toString(),params, RoutingVan.class);
    }

    @Override
    public List<RoutingVan> getRoutingVanByTime(LocalDateTime fromTime, LocalDateTime toTime, Long driverId) throws BusinessException {
        Date fdate  = LocalDateTimeConverter.convertToJavaSqlDate(fromTime);
        Date tdate  = LocalDateTimeConverter.convertToJavaSqlDate(toTime);
        StringBuilder sb = new StringBuilder();
        sb.append("select rv.* from routing_van rv ");
        sb.append(" join driver_van dv on dv.id = rv.driver_van_id ");
        sb.append(" where rv.status = :status and DATE(rv.from_time) <= :toTime and DATE(rv.to_time) >= :fromTime ");
        sb.append(" and dv.staff_id = :driverId");
        HashMap<String,Object> params = new HashMap<>();
        params.put("status", Constants.COMPLETE);
        params.put("toTime", tdate);
        params.put("fromTime",fdate);
        params.put("driverId",driverId);
        return BaseRepository.getResultListNativeQuery(em,sb.toString(),params,RoutingVan.class);
    }
}
