package com.tsolution._2repositories.impl;

import com.tsolution._1entities.BillLading;
import com.tsolution._2repositories.BillLadingRepositoryCustom;
import com.tsolution.config.LocalDateTimeConverter;
import com.tsolution.excetions.BusinessException;
import com.tsolution.utils.StringUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.math.BigDecimal;
import java.sql.Date;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
public class BillLadingRepositoryCustomImpl implements BillLadingRepositoryCustom {

    @PersistenceContext
    private EntityManager em;

    @Override
    public List<BillLading> findByEntity(BillLading billLading) throws BusinessException {
        StringBuilder sb = new StringBuilder();
        Map<String, Object> params = new HashMap<>();
        sb.append(" FROM bill_lading bill  WHERE 1=1 ");
        if (billLading.getId() != null) {
            sb.append("AND bill.id = :id");
            params.put("id", billLading.getId());

        }
        if (StringUtils.isNullOrEmpty(billLading.getBillLadingCode())) {
            sb.append("AND bill.bill_lading_code = :code");
            params.put("code", billLading.getBillLadingCode());

        }
        if (billLading.getCustomer().getId() != null) {
            sb.append("AND bill.customer_id = :customerId");
            params.put("customerId", billLading.getCustomer().getId());

        }
        if (billLading.getInsurance().getId() != null) {
            sb.append("AND bill.insurance_id = :insuranceId");
            params.put("insuranceId", billLading.getInsurance().getId());

        }
        if (billLading.getTotalWeight() != null) {
            sb.append("AND bill.total_weight = :totalWeight");
            params.put("totalWeight", billLading.getTotalWeight());

        }
        if (billLading.getTotalAmount() != null) {
            sb.append("AND bill.total_amount = :totalAmount");
            params.put("amount", billLading.getTotalAmount());

        }
        if (billLading.getTotalPayment() != null) {
            sb.append("AND bill.total_payment = :totalPayment");
            params.put("amount", billLading.getTotalPayment());

        }
        if (billLading.getBillCycle().getId() != null) {
            sb.append("AND bill.bill_cycle_id = :billCycleId");
            params.put("billCycleId", billLading.getBillCycle().getId());

        }
        if (billLading.getReleaseType() != null) {
            sb.append("AND bill.release_type = :releaseType");
            params.put("releaseType", billLading.getReleaseType());

        }
        if (billLading.getStatus() != null) {
            sb.append("AND bill.status = :status");
            params.put("status", billLading.getStatus());

        }

        if (billLading.getCreateDate() != null) {
            sb.append("AND bill.create_date = :createDate");
            params.put("create_date", billLading.getCreateDate());

        }
        if (billLading.getUpdateDate() != null) {
            sb.append("AND bill.update_date = :updateDate");
            params.put("update_date", billLading.getUpdateDate());
        }
        sb.append(" ORDER BY create_date,update_date DESC");


        StringBuilder strQuery = new StringBuilder(" SELECT bill.* ");
        strQuery.append(sb);

        return BaseRepository.getResultListNativeQuery(this.em, strQuery.toString(), params, BillLading.class);
    }

    public Page<BillLading> findByCustomerIdAndDate(String billLadingCode,Integer status, Long customerId, LocalDateTime fromDate, LocalDateTime toDate,Integer billCycleType, Pageable pageable) throws BusinessException {
        StringBuilder sb = new StringBuilder();
        Date fdate  = LocalDateTimeConverter.convertToJavaSqlDate(fromDate);
        Date tdate  = LocalDateTimeConverter.convertToJavaSqlDate(toDate);

        Map<String, Object> params = new HashMap<>();
        sb.append(" FROM bill_lading bill ");
        sb.append(" Left join bill_cycle bc on bill.bill_cycle_id = bc.id ");
        sb.append(" WHERE 1=1 ");
        sb.append(" AND bill.customer_id = :customerId ");
        params.put("customerId", customerId);
        if(status != null){
            sb.append(" AND bill.status = :status ");
            params.put("status", status);
        }
        if(!StringUtils.isNullOrEmpty(billLadingCode)){
            sb.append(" AND LOWER(bill.bill_lading_code) like LOWER(CONCAT('%',:billLadingCode,'%')) ");
            params.put("billLadingCode", billLadingCode);

        }
        if(fdate != null){
            sb.append(" AND DATE(bill.create_date) >= :fromDate ");
            params.put("fromDate",fdate);
        }
        if(tdate != null){
            sb.append(" AND DATE(bill.create_date) <= :toDate ");
            params.put("toDate", tdate);
        }
        if(billCycleType!= null){
            sb.append(" AND bc.cycle_type =:cycle_type ");
            params.put("cycle_type",billCycleType);
        }
        StringBuilder strListQuery = new StringBuilder(" SELECT bill.* ");
        strListQuery.append(sb);
        StringBuilder strCountQuery = new StringBuilder(" SELECT count(DISTINCT bill.id) ");
        strCountQuery.append(sb);
        return BaseRepository.getPagedNativeQuery(this.em, strListQuery.toString(), strCountQuery.toString(),
                params, pageable, BillLading.class);
    }


}
