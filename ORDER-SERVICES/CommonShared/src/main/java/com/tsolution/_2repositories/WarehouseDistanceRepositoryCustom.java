package com.tsolution._2repositories;

import com.tsolution._1entities.WarehouseDistance;
import com.tsolution.excetions.BusinessException;

import java.util.List;

public interface WarehouseDistanceRepositoryCustom {
    List<WarehouseDistance> findListWarehouseDistanceByCustomerId(Long id) throws BusinessException;
}
