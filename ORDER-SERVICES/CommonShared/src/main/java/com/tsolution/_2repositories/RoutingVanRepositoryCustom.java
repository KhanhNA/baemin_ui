package com.tsolution._2repositories;

import com.tsolution._1entities.RoutingVan;
import com.tsolution.excetions.BusinessException;

import java.time.LocalDateTime;
import java.util.List;

public interface RoutingVanRepositoryCustom {
    List<RoutingVan> getRoutingVanDay(Long vanId) throws BusinessException;
    List<RoutingVan> getRoutingVanByTime(LocalDateTime fromTime, LocalDateTime toTime, Long driverId) throws BusinessException;
}
