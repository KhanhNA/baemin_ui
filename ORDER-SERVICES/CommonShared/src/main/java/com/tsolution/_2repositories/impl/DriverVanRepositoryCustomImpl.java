package com.tsolution._2repositories.impl;

import com.tsolution._1entities.DriverVan;
import com.tsolution._2repositories.DriverVanRepositoryCustom;
import com.tsolution.config.LocalDateTimeConverter;
import com.tsolution.excetions.BusinessException;
import com.tsolution.utils.Constants;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.sql.Date;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
@Repository
public class DriverVanRepositoryCustomImpl implements DriverVanRepositoryCustom {
    @PersistenceContext
    private EntityManager em;

    @Override
    public List<DriverVan> getVanForDriver(Long driverId, LocalDateTime dateCheck) throws BusinessException {
        Date fdate  = LocalDateTimeConverter.convertToJavaSqlDate(dateCheck);
        StringBuilder sb = new StringBuilder();
        sb.append("select * from driver_van dv where ");
        sb.append(" dv.staff_id = :driverId and dv.status = :status ");
        sb.append(" and Date(dv.from_date) <= :dateCheck ");
        sb.append(" and Date(dv.to_date) >= :dateCheck ");
        HashMap<String,Object> params = new HashMap<>();
        params.put("driverId", driverId);
        params.put("status", Constants.COMPLETE);
        params.put("dateCheck", fdate);
        return BaseRepository.getResultListNativeQuery(em,sb.toString(),params,DriverVan.class);
    }
}
