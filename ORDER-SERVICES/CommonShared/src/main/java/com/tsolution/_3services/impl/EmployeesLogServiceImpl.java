package com.tsolution._3services.impl;

import com.tsolution._1entities.EmployeeLog;
import com.tsolution._2repositories.EmployeesLogRepository;
import com.tsolution._3services.EmployeesLogService;
import com.tsolution.excetions.BusinessException;
import com.tsolution.utils.StringUtils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class EmployeesLogServiceImpl extends BaseServiceImpl implements EmployeesLogService {

	@Autowired
	private EmployeesLogRepository employeesLogRepository;

	@Override
	public ResponseEntity<Object> findByEmployeeCode(String code) throws BusinessException {
		return new ResponseEntity<>(this.employeesLogRepository.findAllByEmployeeCode(code), HttpStatus.OK);
	}

	@Override
	public ResponseEntity<Object> create(String authorization, String acceptLanguage, EmployeeLog entities)
			throws BusinessException {
		if (entities == null) {
			throw new BusinessException(this.translator.toLocale("common.input.info.invalid"));
		}
		Optional<EmployeeLog> oEmployeeLog = null;
		if (entities.getEmployeeCode() != null) {
			String employeeCode = entities.getEmployeeCode();
			oEmployeeLog = this.employeesLogRepository.findEmployeeCodeAndIsActve(employeeCode);
		}

		this.validEmployeeLog(entities);

		// Tìm lại bản ghi emplyeeLog hiện tại để thay đổi ToTime và update IsActive
		oEmployeeLog.get().setIsActive(0);
		oEmployeeLog.get().setToTime(this.getSysdate());
		this.employeesLogRepository.save(oEmployeeLog.get());

		EmployeeLog employeeLog = this.employeesLogRepository.save(entities);

		// Tao bản ghi mới

		entities.setPreId(oEmployeeLog.get().getId());
		entities.setIsActive(1);
		entities.setStatus(1);
		entities.setFromTime(oEmployeeLog.get().getToTime());

		EmployeeLog result = employeesLogRepository.save(entities);

		return new ResponseEntity<>(result, HttpStatus.OK);
	}

	@Override
	public ResponseEntity<Object> update(String authorization, String acceptLanguage, Long employeeLogId,
			EmployeeLog entities) throws BusinessException {
		Optional<EmployeeLog> oEmployeeLog = this.employeesLogRepository.findById(employeeLogId);

		if (entities == null) {
			throw new BusinessException(this.translator.toLocale("common.input.info.invalid"));
		}

		if (employeeLogId == null || !(employeeLogId == oEmployeeLog.get().getId())) {
			throw new BusinessException(this.translator.toLocale("employeelog.id.err"));
		}
		if (!oEmployeeLog.isPresent()) {
			throw new BusinessException(this.translator.toLocaleByFormatString("employeelog.id.not.exists"));
		}

		// Valid Các field của Employee
		this.validEmployeeLog(entities);

		// Update employeeLog.

		EmployeeLog employeeLog = oEmployeeLog.get();

		employeeLog.setEmployeeCode(entities.getEmployeeCode());
		employeeLog.setEmployeeName(entities.getEmployeeName());
		employeeLog.setAppParam(entities.getAppParam());
		employeeLog.setHasImage(entities.getHasImage());
		employeeLog.setHasAttachment(entities.getHasAttachment());
		employeeLog.setBirthday(entities.getBirthday());
		employeeLog.setProvince(entities.getProvince());
		employeeLog.setNation(entities.getNation());
		employeeLog.setCountry(entities.getCountry());
		employeeLog.setAddress(entities.getAddress());
		employeeLog.setPhone(entities.getPhone());
		employeeLog.setEmail(entities.getEmail());
		employeeLog.setDriverLicenceType(entities.getDriverLicenceType());
		employeeLog.setDriverLicenceId(entities.getDriverLicenceId());
		employeeLog.setHireDate(entities.getHireDate());
		employeeLog.setLeaveDate(entities.getLeaveDate());
		employeeLog.setLaborRate(entities.getLaborRate());
		employeeLog.setBillingRate(entities.getBillingRate());
		employeeLog.setSSN(entities.getSSN());
		employeeLog.setLatitude(entities.getLatitude());
		employeeLog.setDescription(entities.getDescription());
		employeeLog.setLongitude(entities.getLongitude());
		employeeLog.setStateProvince(entities.getStateProvince());
		employeeLog.setLicenseNote(entities.getLicenseNote());
		employeeLog.setEmail(entities.getEmail());
		employeeLog.setPoint(entities.getPoint());
		employeeLog.setObjectType(entities.getObjectType());
		employeeLog.setWarehouse(entities.getWarehouse());
		employeeLog.setIdNo(entities.getIdNo());
		employeeLog.setStatus(entities.getStatus());
		employeeLog.setImei(entities.getImei());
		employeeLog.setStatus(entities.getStatus());
		employeeLog.setIsActive(entities.getIsActive());

		employeeLog = this.employeesLogRepository.save(employeeLog);

		return new ResponseEntity<>(employeeLog, HttpStatus.OK);
	}

	@Override
	public ResponseEntity<Object> findAll() {
		return new ResponseEntity<>(this.employeesLogRepository.findAll(), HttpStatus.OK);
	}

	@Override
	public ResponseEntity<Object> findById(Long id) throws BusinessException {
		Optional<EmployeeLog> oEmployeeLog = this.employeesLogRepository.findById(id);
		if (!oEmployeeLog.isPresent()) {
			throw new BusinessException(this.translator.toLocaleByFormatString("employeelog.id.not.exists"));
		}
		return new ResponseEntity<>(oEmployeeLog, HttpStatus.OK);
	}

	private void validEmployeeLog(EmployeeLog employeeLog) throws BusinessException {
		if (employeeLog.getFromTime() == null) {
			throw new BusinessException(this.translator.toLocale("employeeLog.input.missing.from.time"));
		}
		if (employeeLog.getWarehouse() == null) {
			throw new BusinessException(this.translator.toLocale("employeeLog.input.missing.warehouse"));
		}
		if (employeeLog.getAppParam() == null) {
			throw new BusinessException(this.translator.toLocale("employeeLog.input.missing.apparam"));
		}
	}

}
