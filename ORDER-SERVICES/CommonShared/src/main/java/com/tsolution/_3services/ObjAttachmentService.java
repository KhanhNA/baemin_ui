package com.tsolution._3services;

import com.tsolution._1entities.ObjAttachment;
import com.tsolution.excetions.BusinessException;
import org.springframework.http.ResponseEntity;
import org.springframework.web.multipart.MultipartFile;

import java.util.Collection;
import java.util.List;

public interface ObjAttachmentService  {

    ResponseEntity<Object> findById(Long id) throws BusinessException;

    ResponseEntity<Object> findByObjId(Long id, int attType);

    ResponseEntity<Object> create(ObjAttachment entity) throws BusinessException;

    ResponseEntity<Object> update(List<ObjAttachment> listObj)
            throws BusinessException;
    ResponseEntity<Object> delete(ObjAttachment entity) throws BusinessException;

    boolean saveAttachment(Long id, Collection<ObjAttachment> attachments, int type, String attachName);

}
