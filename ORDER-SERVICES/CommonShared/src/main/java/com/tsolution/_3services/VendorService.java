package com.tsolution._3services;


import com.tsolution._1entities.Vendor;
import com.tsolution.excetions.BusinessException;
import org.springframework.http.ResponseEntity;

public interface VendorService{
    ResponseEntity<Object> findAll();
    ResponseEntity<Object> findById(Long id) throws BusinessException;
    ResponseEntity<Object> create(Vendor entities) throws BusinessException;
    ResponseEntity<Object> update(Vendor entities) throws BusinessException;
}
