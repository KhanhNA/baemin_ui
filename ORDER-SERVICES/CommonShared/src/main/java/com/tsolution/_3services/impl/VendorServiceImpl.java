package com.tsolution._3services.impl;

import com.tsolution._1entities.Vendor;
import com.tsolution._2repositories.VendorRepository;
import com.tsolution._3services.VendorService;
import com.tsolution.excetions.BusinessException;
import com.tsolution.utils.CommonUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Service
public class VendorServiceImpl extends BaseServiceImpl implements VendorService {

    @Autowired
    private VendorRepository vendorRepository;
    private static Integer insertAction = 1;
    private static Integer updateAction = 2;

    @Override
    public ResponseEntity<Object> findAll() {
        return  new ResponseEntity<>(vendorRepository.findAll(), HttpStatus.OK);
    }

    @Override
    public ResponseEntity<Object> findById(Long id) throws BusinessException {
        if(id == null){
            throw new BusinessException(this.translator.toLocale("vendor.input.id.not.null"));
        }
        Optional<Vendor> oVendor = vendorRepository.findById(id);
        if(oVendor.isPresent()){
            return  new ResponseEntity<>(oVendor,HttpStatus.OK);
        }
        return null;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public ResponseEntity<Object> create(Vendor entities) throws BusinessException {
        // check valid input value
        validate(entities,insertAction);
        Vendor result = vendorRepository.save(entities);
        return new ResponseEntity<>(result,HttpStatus.OK);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public ResponseEntity<Object> update(Vendor entity) throws BusinessException {
        validate(entity,updateAction);
        Vendor result = vendorRepository.save(entity);
        return new ResponseEntity<>(result,HttpStatus.OK);
    }

    public void validate(Vendor entity,Integer action) throws BusinessException {
        if(entity == null ){
            throw new BusinessException(this.translator.toLocale("common.input.info.invalid"));
        }
        if(entity.getVendorName() == null){
            throw new BusinessException(this.translator.toLocale("vendor.input.vendor.name.not.null"));
        }
        if(entity.getVendorName().length() > 100){
            throw new BusinessException(this.translator.toLocale("vendor.input.vendor.name.out.of.bound"));
        }
        if(entity.getContactName() == null){
            throw new BusinessException(this.translator.toLocale("vendor.input.contact.name.not.null"));
        }
        if(entity.getContactName().length() > 100){
            throw new BusinessException(this.translator.toLocale("vendor.input.contact.name.out.of.bound"));
        }
        if(entity.getPhone1()!= null && !CommonUtil.isPhoneNumber(entity.getPhone1())){
            throw new BusinessException(this.translator.toLocale("vendor.input.phone1.is.invalid"));
        }
        if(entity.getPhone2()!= null && !CommonUtil.isPhoneNumber(entity.getPhone2())){
            throw new BusinessException(this.translator.toLocale("vendor.input.phone2.is.invalid"));
        }
        if(entity.getFax() != null && !CommonUtil.isFaxNumber(entity.getFax())){
            throw new BusinessException(this.translator.toLocale("vendor.input.fax.is.invalid"));
        }
        if(action == updateAction) {
            if (entity.getId() == null) {
                throw new BusinessException(this.translator.toLocale("vendor.input.id.not.null"));
            }
            Optional<Vendor> vendor = this.vendorRepository.findVendorById(entity.getId());
            if (!vendor.isPresent()) {
                throw new BusinessException(this.translator.toLocaleByFormatString("vendor.input.id.not.find", entity.getId()));
            }
            Vendor v = vendor.get();
            if (v.getVendorName() != entity.getVendorName()) {
                throw new BusinessException(this.translator.toLocaleByFormatString("vendor.input.vendor.name.not.edit"));
            }
        }
        if (action == insertAction) {
            Optional<Vendor> oVendor = vendorRepository.findVendorByVendorNameAndContactName(entity.getVendorName(), entity.getContactName());
            if (oVendor.isPresent()) {
                throw new BusinessException(this.translator.toLocaleByFormatString("vendor.input.vendor.already.exits"));
            }
        }

    }
}
