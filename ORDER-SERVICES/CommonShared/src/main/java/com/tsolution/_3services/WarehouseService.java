package com.tsolution._3services;

import com.tsolution._1entities.Warehouse;
import com.tsolution.excetions.BusinessException;
import org.springframework.http.ResponseEntity;


public interface WarehouseService extends BaseService<Warehouse> {


    ResponseEntity<Object> find(Warehouse warehouse, Integer pageNumber, Integer pageSize) throws BusinessException;

    ResponseEntity<Object> active(Long id) throws BusinessException;

    ResponseEntity<Object> deactive(Long id) throws BusinessException;

    ResponseEntity<Object> findCustomerRentWarehouse(Long warehouseId, Integer pageNumber, Integer pageSize)
            throws BusinessException;

    ResponseEntity<Object> getListWarehouseByCustomer(Long customerId) throws BusinessException;

    ResponseEntity<Object> getListProductType() throws BusinessException;

    ResponseEntity<Object> findListWareHouse(Integer customerId,String code, String name, Integer status,Integer pageNumber, Integer pageSize)throws BusinessException;

    ResponseEntity<Object> getListWarehouseByCustomerToken() throws BusinessException;


}
