package com.tsolution._3services;

import com.tsolution._1entities.Van;
import com.tsolution._1entities.dto.VanDto;
import com.tsolution.excetions.BusinessException;
import org.springframework.core.io.Resource;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;


public interface VanService {

    ResponseEntity<Object> findAll();

    ResponseEntity<Object> findById(Long id) throws BusinessException;

    ResponseEntity<Object> findByCondition(VanDto searchData, Pageable pageable) throws BusinessException;

    ResponseEntity<Object> create(Van entity) throws BusinessException;

    ResponseEntity<Object> update(Van entity)
            throws BusinessException;

    ResponseEntity<Object> deactivate(Long id) throws BusinessException;

    ResponseEntity<Object> activate(Long id) throws BusinessException;

    ResponseEntity<Object> addOrEditAttachments(Van entity) throws BusinessException;

    ResponseEntity<Object> importExcel(MultipartFile file) throws BusinessException;

    Resource getExcelTemplate(String fileName) throws Exception;
}
