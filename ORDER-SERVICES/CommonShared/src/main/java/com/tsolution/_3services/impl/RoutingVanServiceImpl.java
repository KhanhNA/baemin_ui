package com.tsolution._3services.impl;

import com.tsolution._1entities.*;
import com.tsolution._1entities.dto.WarehouseParcelDto;
import com.tsolution._2repositories.*;
import com.tsolution._3services.RoutingPlanDayService;
import com.tsolution._3services.RoutingVanService;

import com.tsolution.excetions.BusinessException;
import com.tsolution.utils.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class RoutingVanServiceImpl extends BaseServiceImpl implements RoutingVanService  {
    @Autowired
    private RoutingVanRepository routingVanRepository;
    @Autowired
    private RoutingPlanDayRepository routingPlanDayRepository;
    @Autowired
    private BillPackageRoutingRepository billPackageRoutingRepository;
    @Autowired
    private WarehouseRepository warehouseRepository;
    @Autowired
    private RoutingPlanDetailRepository routingPlanDetailRepository;
    @Autowired
    private RatingRepository ratingRepository;
    @Autowired
    private ClaimReportRepository claimReportRepository;


    private List<RoutingPlanDay> getDetailRoutingVanWithListWarehouse(List<RoutingPlanDay> routingPlanDayList) throws BusinessException {

        for(RoutingPlanDay r: routingPlanDayList){
            //lay Rating
            Rating rating = this.ratingRepository.findByRoutingPlanDayId(r.getId());
            r.setRating(rating);

            //lay so package can van chuyen chuyen tai moi diem giao nhan
            List<BillPackageRouting> billPackageRoutingList = this.billPackageRoutingRepository.getPackageByRoutingPlan(r.getId());
            int n = billPackageRoutingList.size();
            int check[] = new int[n];
            for(int i = 0; i< n; i++){
                check[i] = 0;
            }
            //lay danh sach kho (kho tra doi voi lenh nhap hang, kho nhan doi voi lenh tra hang
            List<WarehouseParcelDto> lstWarehousePackage = new ArrayList<>();
            for(int i = 0; i < n-1; i++){
                if(check[i]==0){
                    WarehouseParcelDto parcelDto = new WarehouseParcelDto();
                    Optional<Warehouse> toWarehouse;
                    BillPackageRouting billPackageRouting = billPackageRoutingList.get(i);
                    RoutingPlanDetail routingPlanDetail = this.routingPlanDetailRepository.getRoutingPlanDetailByRoutingVanAndBillPackage(r,billPackageRouting).get(0);
                    //danh sach su co
                    List<ClaimReport> lstClaimReport = this.claimReportRepository.findAllByRoutingPlanDetail(routingPlanDetail);
                    billPackageRouting.setLstClaimReport(lstClaimReport);

                    billPackageRouting.setRoutingPlanDetai(routingPlanDetail);
                    if(r.getType().equals(Constants.IMPORT_BILL)){
                        toWarehouse = this.warehouseRepository.findById(billPackageRouting.getToWarehouseId());

                    }
                    else{
                        toWarehouse = this.warehouseRepository.findById(billPackageRouting.getFromWarehouseId());

                    }
                    parcelDto.setWarehouse(toWarehouse.get());
                    List<BillPackageRouting> lstPackage = new ArrayList<>();
                    lstPackage.add(billPackageRouting);
                    check[i] = 1;
                    for(int j = i+1; j< n; j++){
                        BillPackageRouting billPackageRouting1 = billPackageRoutingList.get(j);
                        RoutingPlanDetail routingPlanDetail1 = this.routingPlanDetailRepository.getRoutingPlanDetailByRoutingVanAndBillPackage(r,billPackageRouting1).get(0);
                        //danh sach su co
                        List<ClaimReport> lstClaimReport1 = this.claimReportRepository.findAllByRoutingPlanDetail(routingPlanDetail1);
                        billPackageRouting1.setLstClaimReport(lstClaimReport);
                        billPackageRouting1.setRoutingPlanDetai(routingPlanDetail1);
                        if(r.getType().equals(Constants.IMPORT_BILL) //la hoa don nhap
                                && billPackageRouting1.getToWarehouseId().equals(billPackageRouting.getToWarehouseId())){//cung kho den
                            lstPackage.add(billPackageRouting1);
                            check[j] = 1;
                        }
                        else if(r.getType().equals(Constants.EXPORT_BILL) //la hoa don xuat
                                && billPackageRouting1.getFromWarehouseId().equals(billPackageRouting.getFromWarehouseId())){ //cung kho nhap
                            lstPackage.add(billPackageRouting1);
                            check[j] = 1;
                        }
                    }
                    parcelDto.setPackageRoutingList(lstPackage);
                    lstWarehousePackage.add(parcelDto);
                }

            }
            r.setWarehouseParcelDtos(lstWarehousePackage);

        }

        return routingPlanDayList;
    }

    @Override
    public List<RoutingVan> getRoutPlanForVanToday(Long vanId) throws BusinessException {
        //lay tuyen dau ngay cho xe
        List<RoutingVan> lstRoutingVan = this.routingVanRepository.getRoutingVanDay(vanId);
        if(lstRoutingVan.isEmpty()){
            throw new BusinessException("null ecxeption");
        }
        for(RoutingVan rv: lstRoutingVan){
            //lay lich trinh di tuyen (danh sach kho, thu tu)
            if(rv == null || rv.getId() == null){
                throw new BusinessException(this.translator.toLocale("routingvan.is.invalid"));
            }
            List<RoutingPlanDay> routingPlanDayList = this.routingPlanDayRepository.getRoutPlanForVanToday(rv.getId());
            if(routingPlanDayList.isEmpty()){
                throw new BusinessException("error parcel rout");
            }
            routingPlanDayList = getDetailRoutingVanWithListWarehouse(routingPlanDayList);
            rv.setPlanDayList(routingPlanDayList);
        }
        return lstRoutingVan;
    }

    @Override
    public List<RoutingPlanDay> getNextDestinationInRouting(Long routingId) throws BusinessException {
        if(routingId == null){
            throw new BusinessException(this.translator.toLocale("null exception!"));
        }
        List<RoutingPlanDay> routingPlanDayList = this.routingPlanDayRepository.getNextDestinationInRouting(routingId);
        if(routingPlanDayList.isEmpty()){
            return new ArrayList<>();
        }
        routingPlanDayList = getDetailRoutingVanWithListWarehouse(routingPlanDayList);
        return routingPlanDayList;
    }

    @Override
    public List<RoutingPlanDay> getRoutingPlanDayHistory(LocalDateTime fromTime, LocalDateTime toTime, Long driverId) throws BusinessException {
        List<RoutingPlanDay> result = new ArrayList<>();
        if(driverId == null){
            throw new BusinessException(this.translator.toLocale("common.input.info.invalid"));
        }
        if(fromTime == null){
            fromTime = LocalDateTime.now();
        }
        if(toTime == null){
            toTime = LocalDateTime.now();
        }
        List<RoutingVan> lstRoutingVan = this.routingVanRepository.getRoutingVanByTime(fromTime,toTime,driverId);
        if(!lstRoutingVan.isEmpty()){
            for(RoutingVan rv: lstRoutingVan){
                if(rv == null){
                    throw new BusinessException(this.translator.toLocale("common.input.info.invalid"));
                }
                List<RoutingPlanDay> lstRoutingPlanDay = this.routingPlanDayRepository.getRoutPlanForVanToday(rv.getId());
                if(!lstRoutingPlanDay.isEmpty()){
                    lstRoutingPlanDay = getDetailRoutingVanWithListWarehouse(lstRoutingPlanDay);
                    rv.setPlanDayList(lstRoutingPlanDay);
                    result.addAll(lstRoutingPlanDay);
                }
            }
        }

        return result;
    }



    @Override
    public ResponseEntity<Object> findAll() {
        return null;
    }

    @Override
    public ResponseEntity<Object> findById(Long id) throws BusinessException {
        return null;
    }

    @Override
    public ResponseEntity<Object> create(List<RoutingVan> entities) throws BusinessException {
        return null;
    }

    @Override
    public ResponseEntity<Object> update(Long id, RoutingVan source) throws BusinessException {
        return null;
    }
//    private RoutingVan validateRoutingVan(RoutingVan source){
//
//    }
//    private RoutingVan addOrUpdateRoutingVan(RoutingVan source){
//        RoutingVan routingVan;
//
//    }
}
