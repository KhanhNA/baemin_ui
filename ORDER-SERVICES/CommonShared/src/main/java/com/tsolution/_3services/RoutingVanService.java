package com.tsolution._3services;

import com.tsolution._1entities.RoutingPlanDay;
import com.tsolution._1entities.RoutingVan;
import com.tsolution.excetions.BusinessException;

import java.time.LocalDateTime;
import java.util.List;

public interface RoutingVanService extends BaseService<RoutingVan> {
    List<RoutingVan> getRoutPlanForVanToday(Long id) throws BusinessException;
    List<RoutingPlanDay> getNextDestinationInRouting(Long routingId) throws BusinessException;
    List<RoutingPlanDay> getRoutingPlanDayHistory(LocalDateTime fromTime, LocalDateTime toTime, Long driverId) throws BusinessException;

}
