package com.tsolution._3services;

import com.tsolution._1entities.Fleet;
import com.tsolution.excetions.BusinessException;
import org.springframework.http.ResponseEntity;

public interface FleetService {
    ResponseEntity<Object> findAll() throws BusinessException;
    ResponseEntity<Object> findById(Long id) throws BusinessException;
    ResponseEntity<Object> create(String acceptLanguage, Fleet entities) throws BusinessException;
    ResponseEntity<Object> update(String acceptLanguage, Long id, Fleet entities) throws BusinessException;

}
