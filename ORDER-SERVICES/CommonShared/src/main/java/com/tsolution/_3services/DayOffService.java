package com.tsolution._3services;

import com.tsolution._1entities.DayOff;
import com.tsolution.excetions.BusinessException;
import org.springframework.http.ResponseEntity;

import java.time.LocalDateTime;

public interface DayOffService {
    ResponseEntity<Object> create(String acceptLanguage, DayOff entities) throws BusinessException;
    ResponseEntity<Object> update(String acceptLanguage, Long DayOffId, DayOff entities) throws BusinessException;
    ResponseEntity<Object> findAll();
    ResponseEntity<Object> findById(Long id) throws BusinessException;
    ResponseEntity<Object> find(Long staffId, LocalDateTime fdate, LocalDateTime tdate, Integer spec, Integer status) throws BusinessException;
}
