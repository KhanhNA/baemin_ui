package com.tsolution._3services.impl;

import java.sql.Date;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import com.tsolution._1entities.*;
import com.tsolution._1entities.enums.StatusType;
import com.tsolution._1entities.oauth2.OAuth2AuthenticationDto;
import com.tsolution._2repositories.CustomerRepository;
import com.tsolution._2repositories.OrganizationRepository;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

//import com.tsolution._1entities.dto.FreeVolumeDto;
//import com.tsolution._1entities.enums.CustomerRentStatus;
import com.tsolution._1entities.enums.WarehouseStatus;
//import com.tsolution._2repositories.ParcelRepository;
//import com.tsolution._2repositories.WarehouseImageRepository;
//import com.tsolution._2repositories.WarehousePalletRepository;
//import com.tsolution._2repositories.WarehousePriceRepository;
import com.tsolution._2repositories.WarehouseRepository;
import com.tsolution._3services.WarehouseService;
import com.tsolution.config.LocalDateTimeConverter;
import com.tsolution.excetions.BusinessException;
import com.tsolution.utils.QRCodeGenerator;
import com.tsolution.utils.StringUtils;

@Service
public class WarehouseServiceImpl extends BaseServiceImpl implements WarehouseService {

	private static final String WAREHOUSE_IMAGE_ID_NOT_EXISITS = "warehouse.image.id.not.exisits";

	private static final String WAREHOUSE_ID_NOT_EXISITS = "warehouse.id.not.exisits";

	private static final Logger log = LogManager.getLogger(WarehouseServiceImpl.class);
//
//	@Autowired
//	private WarehousePriceRepository warehousePriceRepository;
//
//	@Autowired
//	private WarehouseImageRepository warehouseImageRepository;
//
//	@Autowired
//	private WarehousePalletRepository warehousePalletRepository;

	@Autowired
	private FileStorageService fileStorageService;

	@Autowired
	private WarehouseRepository warehouseRepository;

	@Autowired
	private OrganizationRepository organizationRepository;
	@Autowired
	private CustomerRepository customerRepository;

//	@Autowired
//	private ParcelRepository parcelRepository;

	@Override
	public ResponseEntity<Object> findAll() {
		return new ResponseEntity<>(this.warehouseRepository.findAllByStatus(WarehouseStatus.ACTIVED.getValue()), HttpStatus.OK);
	}

//	private List<WarehousePrice> generateCurrentWarehousePrices(Warehouse warehouse, List<Parcel> parcels,
//			List<WarehousePrice> warehousePrices) {
//		List<WarehousePrice> tempWarehousePrices = warehousePrices.stream()
//				.filter(x -> x.getWarehouseId().equals(warehouse.getId())).collect(Collectors.toList());
//		List<Long> parcelsHavedPrice = tempWarehousePrices.stream().map(WarehousePrice::getParcel).map(Parcel::getId)
//				.collect(Collectors.toList());
//		for (Parcel parcel : parcels) {
//			if (parcelsHavedPrice.contains(parcel.getId())) {
//				continue;
//			}
//			WarehousePrice warehousePrice = new WarehousePrice();
//			warehousePrice.setWarehouseId(warehouse.getId());
//			warehousePrice.setParcel(parcel);
//			tempWarehousePrices.add(warehousePrice);
//		}
//		return tempWarehousePrices;
//	}

//	private List<FreeVolumeDto> generateFreeVolumes(Warehouse warehouse, List<Parcel> parcels,
//			List<FreeVolumeDto> freeVolumeDtos) {
//		Optional<FreeVolumeDto> oFreeVolumeDto = freeVolumeDtos.stream()
//				.filter(x -> x.getWarehouseId().equals(warehouse.getId())).findFirst();
//		FreeVolumeDto freeVolumeDto;
//		if (oFreeVolumeDto.isPresent()) {
//			freeVolumeDto = oFreeVolumeDto.get();
//		} else {
//			freeVolumeDto = new FreeVolumeDto();
//			freeVolumeDto.setWarehouseId(warehouse.getId());
//			freeVolumeDto.setWarehouseCode(warehouse.getCode());
//			// freeVolumeDto.setFreeVolume(warehouse.getTotalVolume());
//		}
//
//		List<FreeVolumeDto> result = new ArrayList<>();
//		for (Parcel parcel : parcels) {
//			FreeVolumeDto temp = new FreeVolumeDto();
//			temp.setDate(freeVolumeDto.getDate());
//			temp.setWarehouseId(freeVolumeDto.getWarehouseId());
//			temp.setWarehouseCode(freeVolumeDto.getWarehouseCode());
//			temp.setParcelId(parcel.getId());
//			temp.setParcelCode(parcel.getCode());
//			temp.setFreeVolume(freeVolumeDto.getFreeVolume() / parcel.getVolume());
//			result.add(temp);
//		}
//		return result;
//	}

	@Override
	public ResponseEntity<Object> findById(Long id) throws BusinessException {
		Optional<Warehouse> oWarehouse = this.warehouseRepository.findById(id);
		if (!oWarehouse.isPresent()) {
			throw new BusinessException(
					this.translator.toLocaleByFormatString(WarehouseServiceImpl.WAREHOUSE_ID_NOT_EXISITS, id));
		}
		Warehouse warehouse = oWarehouse.get();

		return new ResponseEntity<>(warehouse, HttpStatus.OK);
	}

//	@Override
//	public ResponseEntity<Object> findWarehousePricesById(Long id, Integer pageNumber, Integer pageSize)
//			throws BusinessException {
//		Optional<Warehouse> oWarehouse = this.warehouseRepository.findById(id);
//		if (!oWarehouse.isPresent()) {
//			throw new BusinessException(
//					this.translator.toLocaleByFormatString(WarehouseServiceImpl.WAREHOUSE_ID_NOT_EXISITS, id));
//		}
//		return new ResponseEntity<>(this.warehousePriceRepository.findWarehousePricesById(oWarehouse.get().getId(),
//				PageRequest.of(pageNumber - 1, pageSize)), HttpStatus.OK);
//	}

//	@Override
//	public ResponseEntity<Object> findWarehouseImagesById(Long id, Integer pageNumber, Integer pageSize)
//			throws BusinessException {
//		Optional<Warehouse> oWarehouse = this.warehouseRepository.findById(id);
//		if (!oWarehouse.isPresent()) {
//			throw new BusinessException(
//					this.translator.toLocaleByFormatString(WarehouseServiceImpl.WAREHOUSE_ID_NOT_EXISITS, id));
//		}
//		return new ResponseEntity<>(this.warehouseImageRepository.findWarehouseImagesById(oWarehouse.get().getId(),
//				PageRequest.of(pageNumber - 1, pageSize)), HttpStatus.OK);
//	}

	@Override
	public ResponseEntity<Object> findCustomerRentWarehouse(Long warehouseId, Integer pageNumber, Integer pageSize)
			throws BusinessException {
		Optional<Warehouse> oWarehouse = this.warehouseRepository.findById(warehouseId);
		if (!oWarehouse.isPresent()) {
			throw new BusinessException(
					this.translator.toLocaleByFormatString(WarehouseServiceImpl.WAREHOUSE_ID_NOT_EXISITS, warehouseId));
		}
		return new ResponseEntity<>(this.customerRespository.findCustomerRentWarehouse(oWarehouse.get().getId(),
				PageRequest.of(pageNumber - 1, pageSize)), HttpStatus.OK);
	}

	@Override
	public ResponseEntity<Object> getListWarehouseByCustomer(Long customerId) throws BusinessException {
		List<Warehouse> lst = this.warehouseRepository.getListWarehouseByCustomer(customerId);
		return new ResponseEntity<>(lst, HttpStatus.OK);
	}

	@Override
	public ResponseEntity<Object> getListProductType() throws BusinessException {
		List<ApparamValue> lst = this.warehouseRepository.getListProductType();
		return new ResponseEntity<>(lst, HttpStatus.OK);
	}

	@Override
	public ResponseEntity<Object> findListWareHouse(Integer customerId, String code, String name, Integer status, Integer pageNumber, Integer pageSize) throws BusinessException {
		return new ResponseEntity<Object>(this.warehouseRepository.findListWareHouse(customerId,code,name,status,PageRequest.of(pageNumber - 1, pageSize)),HttpStatus.OK);
	}

	@Override
	public ResponseEntity<Object> find(Warehouse input, Integer pageNumber, Integer pageSize) throws BusinessException {
		Organization organization = this.getCurrentOrganization();
		Long organizationId = organization == null ? null : organization.getId();
		Double currentLat = input.getLat() == null ? null : input.getLat().doubleValue();
		Double currentLng = input.getLng() == null ? null : input.getLng().doubleValue();
		String code = input.getCode();
		String name = input.getName();
		String description = input.getDescription();
		String add = input.getAddress();
		Integer status = input.getStatus();

		return null;
	}

	private void validWarehouse(Warehouse warehouse) throws BusinessException {

		if (StringUtils.isNullOrEmpty(warehouse.getName())) {
			throw new BusinessException(this.translator.toLocale("warehouse.input.missing.name"));
		}
		if (StringUtils.isNullOrEmpty(warehouse.getAddress())) {
			throw new BusinessException(this.translator.toLocale("warehouse.input.missing.address"));
		}
		if (warehouse.getStatus() == null) {
			throw new BusinessException(this.translator.toLocale("warehouse.input.missing.status"));
		}
		if (warehouse.getLat() == null) {
			throw new BusinessException(this.translator.toLocale("warehouse.input.missing.lat"));
		}
		if (warehouse.getLng() == null) {
			throw new BusinessException(this.translator.toLocale("warehouse.input.missing.lng"));
		}
//		if (warehouse.getArea() == null) {
//			throw new BusinessException(this.translator.toLocale("warehouse.input.missing.amountOfPallets"));
//		}

	}

	private Warehouse addOrEditWarehouse(Warehouse warehouse) throws BusinessException {
		Warehouse result;
		validWarehouse(warehouse);
		if (warehouse.getId() == null) {
			result = new Warehouse();
			result.setCode(warehouse.getCode());
			result.setStatus(StatusType.WAITING.getValue());
		} else {
			Optional<Warehouse> oWarehouse = this.warehouseRepository.findById(warehouse.getId());
			if (!oWarehouse.isPresent()) {
				throw new BusinessException(this.translator
						.toLocaleByFormatString(WarehouseServiceImpl.WAREHOUSE_ID_NOT_EXISITS, warehouse.getId()));
			}
			result = oWarehouse.get();
		}
		result.setName(warehouse.getName());
		result.setDescription(StringUtils.isNullOrEmpty(warehouse.getDescription()) ? "" : warehouse.getDescription());
		result.setAddress(warehouse.getAddress());
		result.setPhone(StringUtils.isNullOrEmpty(warehouse.getPhone().trim()) ? "" : warehouse.getPhone().trim());
		result.setArea(warehouse.getArea());

		// bo sung
		//check customer
		Long customerId = warehouse.getCustomer().getId();
		if(customerId == null){
			throw new BusinessException(this.translator.toLocale("customer.id.not.exists"));
		}
		Customer customer = this.customerRepository.findById(customerId).get();
		if(customer == null){
			throw new BusinessException(this.translator.toLocaleByFormatString("customer.id.not.exists",customerId));
		}
		result.setCustomer(customer);
		//check organization
		Long idOrganization = warehouse.getOrganization().getId();
		if(idOrganization != null){
			Organization organization = this.organizationRepository.findById(idOrganization).get();
			if(organization == null){
				throw new BusinessException(this.translator.toLocaleByFormatString("organization.id.not.exists",idOrganization));
			}
			else{
				result.setOrganization(organization);
			}
		}
		else
			result.setOrganization(this.getCurrentOrganization());
		result.setLat(warehouse.getLat());
		result.setLng(warehouse.getLng());

		return result;
	}

//	private WarehousePrice addOrEditWarehousePrice(Long warehouseId, WarehousePrice warehousePrice)
//			throws BusinessException {
//		WarehousePrice result;
//		if (warehousePrice.getId() == null) {
//			result = new WarehousePrice();
//			result.setWarehouseId(warehouseId);
//			this.validFromDateToDatePlusOne(warehousePrice.getFromDate(), warehousePrice.getToDate(), true,
//					this.getSysdate());
//			result.setFromDate(warehousePrice.getFromDate());
//			if ((warehousePrice.getParcel() == null) || (warehousePrice.getParcel().getId() == null)) {
//				throw new BusinessException(this.translator.toLocale("warehouse.price.input.missing.parcel"));
//			}
//			Optional<Parcel> oParcel = this.parcelRepository.findById(warehousePrice.getParcel().getId());
//			if (!oParcel.isPresent()) {
//				throw new BusinessException(this.translator.toLocaleByFormatString("parcel.id.not.exisits",
//						warehousePrice.getParcel().getId()));
//			}
//			result.setParcel(oParcel.get());
//		} else {
//			Optional<WarehousePrice> oWarehousePrice = this.warehousePriceRepository.findById(warehousePrice.getId());
//			if (!oWarehousePrice.isPresent()) {
//				throw new BusinessException(this.translator.toLocaleByFormatString("warehouse.price.id.not.exisits",
//						warehousePrice.getId()));
//			}
//			result = oWarehousePrice.get();
//			if (!result.getWarehouseId().equals(warehouseId)
//					|| !result.getParcel().getId().equals(warehousePrice.getParcel().getId())) {
//				throw new BusinessException(this.translator.toLocaleByFormatString("warehouse.price.not.in.warehouse",
//						warehousePrice.getId(), warehouseId));
//			}
//			if ((result.getToDate() != null) && result.getToDate().isBefore(this.getSysdate())) {
//				throw new BusinessException(
//						this.translator.toLocale("warehouse.price.can.not.modify.price.end.of.life"));
//			}
//		}
//		result.setToDate(warehousePrice.getToDate());
//		result.setPrice(warehousePrice.getPrice());
//		return result;
//	}
//
//	private WarehouseImage addOrEditWarehouseImage(Long warehouseId, WarehouseImage warehouseImage)
//			throws BusinessException {
//		WarehouseImage result;
//		if (warehouseImage.getId() == null) {
//			result = new WarehouseImage();
//			result.setWarehouseId(warehouseId);
//			result.setStatus(true);
//		} else {
//			Optional<WarehouseImage> oWarehouseImage = this.warehouseImageRepository.findById(warehouseImage.getId());
//			if (!oWarehouseImage.isPresent()) {
//				throw new BusinessException(this.translator.toLocaleByFormatString(
//						WarehouseServiceImpl.WAREHOUSE_IMAGE_ID_NOT_EXISITS, warehouseImage.getId()));
//			}
//			result = oWarehouseImage.get();
//			if (!result.getWarehouseId().equals(warehouseId)) {
//				throw new BusinessException(this.translator.toLocaleByFormatString("warehouse.image.not.in.warehouse",
//						warehouseImage.getId(), warehouseId));
//			}
//		}
//		result.setUrl(warehouseImage.getUrl());
//		result.setDescription(warehouseImage.getDescription());
//		return result;
//	}

	private void addWarehousePrice(Warehouse warehouse, Warehouse source) throws BusinessException {

	}

	private void addWarehouseImage(Warehouse warehouse, Warehouse source) throws BusinessException {

	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public ResponseEntity<Object> create(List<Warehouse> entities) throws BusinessException {
		List<Warehouse> warehouses = new ArrayList<>();
		for (Warehouse entity : entities) {
			if (entity.getId() != null) {
				throw new BusinessException(this.translator.toLocale("warehouse.input.can.not.exists.id"));
			}
			Warehouse warehouse = this.warehouseRepository.save(this.addOrEditWarehouse(entity));
			warehouse.setCode("KHO" + warehouse.getId());
			this.warehouseRepository.save(warehouse);
			warehouses.add(warehouse);
		}
		return new ResponseEntity<>(warehouses, HttpStatus.OK);
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public ResponseEntity<Object> update(Long id, Warehouse source) throws BusinessException {
		if ((id == null) || (source == null) || (source.getId() == null) || !id.equals(source.getId())) {
			throw new BusinessException(this.translator.toLocale("warehouse.update.missing.info"));
		}

		Warehouse warehouse = this.warehouseRepository.save(this.addOrEditWarehouse(source));
		return new ResponseEntity<>(warehouse, HttpStatus.OK);
	}

	@Override
	public ResponseEntity<Object> getListWarehouseByCustomerToken() throws BusinessException {

		OAuth2AuthenticationDto oAuth2AuthenticationDto = this.getCurrentOAuth2Details();
		String userName = oAuth2AuthenticationDto.getName();
		Optional<Customer> optionalCustomer = this.customerRepository.findByCode(userName);
		if (!optionalCustomer.isPresent()) {
			throw new BusinessException(this.translator.toLocale("staff.input.invalid.not.exist.user.name"));
		}
		Long customerId= optionalCustomer.get().getId();
		List<Warehouse> lst = this.warehouseRepository.getListWarehouseByCustomer(customerId);
		return new ResponseEntity<>(lst, HttpStatus.OK);

	}

	//	@Override
//	@Transactional(rollbackFor = Exception.class)
//	public ResponseEntity<Object> updateWarehousePrice(Long id, List<WarehousePrice> warehousePrices)
//			throws BusinessException {
//		Optional<Warehouse> oWarehouse = this.warehouseRepository.findById(id);
//		if (!oWarehouse.isPresent()) {
//			throw new BusinessException(
//					this.translator.toLocaleByFormatString(WarehouseServiceImpl.WAREHOUSE_ID_NOT_EXISITS, id));
//		}
//		List<WarehousePrice> results = new ArrayList<>();
//		for (WarehousePrice warehousePrice : warehousePrices) {
//			LocalDateTime sysdate = this.getSysdate();
//			this.validFromDateToDatePlusOne(warehousePrice.getFromDate(), warehousePrice.getToDate(),
//					warehousePrice.getId() == null, sysdate);
//			if (warehousePrice.getPrice().compareTo(0L) <= 0) {
//				throw new BusinessException(this.translator.toLocale("price.must.be.greater.than.zero"));
//			}
//			WarehousePrice result = this.addOrEditWarehousePrice(id, warehousePrice);
//			if (result.getId() == null) {
//				Integer deletedRow = this.warehousePriceRepository.deleteWarehousePriceNotInUse(id,
//						result.getParcel().getId());
//				WarehouseServiceImpl.log.info("updateWarehousePrice - deletedRow: %d", deletedRow);
//				Integer updatedRow = this.warehousePriceRepository.updateToDateWarehousePrice(id,
//						result.getParcel().getId());
//				WarehouseServiceImpl.log.info("updateWarehousePrice - updatedRow: %d", updatedRow);
//			}
//			results.add(this.warehousePriceRepository.save(result));
//		}
//		return new ResponseEntity<>(results, HttpStatus.OK);
//	}
//
//	private void deleteOldWarehouseImages(List<WarehouseImage> deletes) throws BusinessException {
//		for (WarehouseImage warehouseImageDto : deletes) {
//			if (warehouseImageDto.getId() == null) {
//				throw new BusinessException(this.translator.toLocale("warehouse.image.input.invalid.data.struct"));
//			}
//			Optional<WarehouseImage> oWarehouseImage = this.warehouseImageRepository
//					.findById(warehouseImageDto.getId());
//			if (!oWarehouseImage.isPresent()) {
//				throw new BusinessException(this.translator.toLocaleByFormatString(
//						WarehouseServiceImpl.WAREHOUSE_IMAGE_ID_NOT_EXISITS, warehouseImageDto.getId()));
//			}
//			WarehouseImage warehouseImage = oWarehouseImage.get();
//			warehouseImage.setStatus(false);
//			this.warehouseImageRepository.save(warehouseImage);
//		}
//	}

//	private void addOrEditWarehouseImages(Long warehouseId, List<WarehouseImage> addOrEdits,
//			MultipartFile[] multipartFiles) throws BusinessException {
//		for (int i = 0; i < multipartFiles.length; i++) {
//			MultipartFile multipartFile = multipartFiles[i];
//			String fileName = this.fileStorageService.saveFile("warehouse-image", multipartFile);
//			WarehouseServiceImpl.log.info(fileName);
//			WarehouseImage warehouseImage = addOrEdits.get(i);
//			if (warehouseImage.getId() == null) {
//				warehouseImage.setWarehouseId(warehouseId);
//			} else {
//				Optional<WarehouseImage> oWarehouseImage = this.warehouseImageRepository
//						.findById(warehouseImage.getId());
//				if (!oWarehouseImage.isPresent()) {
//					throw new BusinessException(this.translator.toLocaleByFormatString(
//							WarehouseServiceImpl.WAREHOUSE_IMAGE_ID_NOT_EXISITS, warehouseImage.getId()));
//				}
//				if (!oWarehouseImage.get().getWarehouseId().equals(warehouseId)) {
//					throw new BusinessException(this.translator.toLocale("warehouse.image.input.invalid.warehouse.id"));
//				}
//			}
//			warehouseImage.setUrl(fileName);
//			warehouseImage.setStatus(true);
//			this.warehouseImageRepository.save(warehouseImage);
//		}
//	}
//
//	@Override
//	@Transactional(rollbackFor = Exception.class)
//	public ResponseEntity<Object> updateWarehouseImage(Long id, List<WarehouseImage> deletes,
//			List<WarehouseImage> addOrEdits, MultipartFile[] multipartFiles) throws BusinessException {
//		Optional<Warehouse> oWarehouse = this.warehouseRepository.findById(id);
//		if (!oWarehouse.isPresent()) {
//			throw new BusinessException(
//					this.translator.toLocaleByFormatString(WarehouseServiceImpl.WAREHOUSE_ID_NOT_EXISITS, id));
//		}
//		if ((deletes != null) && !deletes.isEmpty()) {
//			this.deleteOldWarehouseImages(deletes);
//		}
//		if ((addOrEdits != null) && !addOrEdits.isEmpty() && (multipartFiles != null)
//				&& (multipartFiles.length == addOrEdits.size())) {
//			this.addOrEditWarehouseImages(id, addOrEdits, multipartFiles);
//		}
//		return new ResponseEntity<>(HttpStatus.OK);
//	}
//
//	private WarehousePallet addOrEditWarehousePallet(Warehouse warehouse, WarehousePallet warehousePallet,
//			Integer index) throws BusinessException {
//		WarehousePallet result;
//		if (warehousePallet.getId() == null) {
//			result = new WarehousePallet();
//			result.setWarehouseId(warehouse.getId());
//			String code = String.format("%s-P_%d", warehouse.getCode(), index + 1);
//			result.setCode(code);
//			result.setqRCode(QRCodeGenerator.generate(code));
//			result.setLength(Pallet.LENGTH);
//			result.setWidth(Pallet.WIDTH);
//			result.setHeight(Pallet.HEIGHT);
//			result.setStatus(true);
//		} else {
//			Optional<WarehousePallet> oWarehousePallet = this.warehousePalletRepository
//					.findById(warehousePallet.getId());
//			if (!oWarehousePallet.isPresent()) {
//				throw new BusinessException(this.translator.toLocaleByFormatString("warehouse.pallet.id.not.exisits",
//						warehousePallet.getId()));
//			}
//			result = oWarehousePallet.get();
//			if (!result.getWarehouseId().equals(warehouse.getId())) {
//				throw new BusinessException(this.translator.toLocale("warehouse.pallet.input.invalid.warehouse.id"));
//			}
//			result.setStatus(warehousePallet.getStatus());
//		}
//		result.setDescription(warehousePallet.getDescription());
//		return result;
//	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public ResponseEntity<Object> active(Long id) throws BusinessException {

		Optional<Warehouse> oWarehouse = this.warehouseRepository.findById(id);
		if (!oWarehouse.isPresent()) {
			throw new BusinessException(
					this.translator.toLocaleByFormatString(WarehouseServiceImpl.WAREHOUSE_ID_NOT_EXISITS, id));
		}
		Warehouse warehouse = oWarehouse.get();
		if (WarehouseStatus.ACTIVED.getValue().equals(warehouse.getStatus())) {
			throw new BusinessException(this.translator.toLocale("warehouse.can.not.active.acitved.warehouse"));
		}

		warehouse.setApproveDate(this.getSysdate());
		warehouse.setApproveUser(this.getCurrentUsername());
		warehouse.setStatus(StatusType.RUNNING.getValue());
		warehouse = this.warehouseRepository.save(warehouse);
		return new ResponseEntity<>(warehouse, HttpStatus.OK);
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public ResponseEntity<Object> deactive(Long id) throws BusinessException {
		Optional<Warehouse> oWarehouse = this.warehouseRepository.findById(id);
		if (!oWarehouse.isPresent()) {
			throw new BusinessException(
					this.translator.toLocaleByFormatString(WarehouseServiceImpl.WAREHOUSE_ID_NOT_EXISITS, id));
		}
		Warehouse warehouse = oWarehouse.get();
		// TODO: Cần phải kiểm tra trạng thái thuê trước khi chém
		warehouse.setStatus(StatusType.STOPPED.getValue());
		warehouse = this.warehouseRepository.save(warehouse);
		return new ResponseEntity<>(warehouse, HttpStatus.OK);
	}

}
