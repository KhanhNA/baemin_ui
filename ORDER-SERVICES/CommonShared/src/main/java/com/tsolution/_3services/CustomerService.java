package com.tsolution._3services;

import com.tsolution._1entities.Customer;
import com.tsolution.excetions.BusinessException;
import org.springframework.http.ResponseEntity;

public interface CustomerService {
     ResponseEntity<Object> findAll() ;
     ResponseEntity<Object> findById(Long id) throws BusinessException ;
     ResponseEntity<Object> find(Customer customer, Integer pageNumber, Integer pageSize)
            throws BusinessException;
     ResponseEntity<Object> create(String authorization, String acceptLanguage, Customer customerInput)
            throws BusinessException;
     ResponseEntity<Object> update(String authorization, String acceptLanguage, Long customerId,
                                         Customer customerInput) throws BusinessException;
     ResponseEntity<Object> activeOrDeactivate(String authorization, String acceptLanguage,
                                                     Customer customerInput, boolean isActive) throws BusinessException;
     ResponseEntity<Object> resetPassword(String authorization, String acceptLanguage, Customer customer)
            throws BusinessException;
}
