package com.tsolution._3services.impl;

import com.tsolution._1entities.Customer;
import com.tsolution._1entities.User;
import com.tsolution._1entities.enums.CustomerStatus;
import com.tsolution._1entities.oauth2.RoleDto;
import com.tsolution._3services.CustomerService;
import com.tsolution.excetions.BusinessException;
import com.tsolution.utils.CommonUtil;
import com.tsolution.utils.MathUtils;
import com.tsolution.utils.SingleSignOnUtils;
import com.tsolution.utils.StringUtils;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

@Service
public class CustomerServiceImpl extends BaseServiceImpl implements CustomerService {
    private SingleSignOnUtils singleSignOnUtils;

    public CustomerServiceImpl(SingleSignOnUtils singleSignOnUtils) {
        this.singleSignOnUtils = singleSignOnUtils;
    }

    @Override
    public ResponseEntity<Object> findAll() {
        return new ResponseEntity<>(this.customerRespository.findAll(), HttpStatus.OK);
    }

    @Override
    public ResponseEntity<Object> findById(Long id) throws BusinessException {
        Optional<Customer> oCustomer = this.customerRespository.findById(id);
        if (!oCustomer.isPresent()) {
            throw new BusinessException(this.translator.toLocaleByFormatString("customer.id.not.exists", id));
        }
        return new ResponseEntity<>(oCustomer.get(), HttpStatus.OK);
    }

    @Override
    public ResponseEntity<Object> find(Customer customer, Integer pageNumber, Integer pageSize)
            throws BusinessException {
        return new ResponseEntity<>(this.customerRespository.find(customer, PageRequest.of(pageNumber - 1, pageSize)),
                HttpStatus.OK);
    }


    private void validCustomer(Customer customer) throws BusinessException {
        /*if (StringUtils.isNullOrEmpty(customer.getCode())) {
            throw new BusinessException(this.translator.toLocale("customer.input.missing.code"));
        }*/
        if (StringUtils.isNullOrEmpty(customer.getName())) {
            throw new BusinessException(this.translator.toLocale("customer.input.missing.name"));
        }
        if (StringUtils.isNullOrEmpty(customer.getAddress())) {
            throw new BusinessException(this.translator.toLocale("customer.input.missing.address"));
        }
        if (customer.getPhone() == null) {
            throw new BusinessException(this.translator.toLocale("customer.input.missing.phone"));
        }
        if (!CommonUtil.isPhoneNumber(customer.getPhone())) {
            throw new BusinessException(this.translator.toLocale("customer.input.wrong.phone.partern"));
        }
        if (customer.getPhone1() != null && !CommonUtil.isPhoneNumber(customer.getPhone1())) {
            throw new BusinessException(this.translator.toLocale("customer.input.wrong.phone.partern"));
        }
        if (customer.getPhone2() != null && !CommonUtil.isPhoneNumber(customer.getPhone2())) {
            throw new BusinessException(this.translator.toLocale("customer.input.wrong.phone.partern"));
        }
        if (customer.getMobilePhone() != null && !CommonUtil.isPhoneNumber(customer.getMobilePhone())) {
            throw new BusinessException(this.translator.toLocale("customer.input.wrong.phone.partern"));
        }
        if (customer.getFax() != null && !CommonUtil.isPhoneNumber(customer.getFax())) {
            throw new BusinessException(this.translator.toLocale("customer.input.wrong.phone.partern"));
        }
        if (customer.getStatus() == null) {
            throw new BusinessException(this.translator.toLocale("customer.input.missing.status"));
        }
        if (customer.getVip() == null) {
            throw new BusinessException(this.translator.toLocale("customer.input.missing.vip"));
        }
        if (customer.getTaxCode() == null) {
            throw new BusinessException(this.translator.toLocale("customer.input.missing.taxcode"));
        }
        if (customer.getContactName() == null) {
            throw new BusinessException(this.translator.toLocale("customer.input.missing.contact.name"));
        }
        if (customer.getAppParamValueId() == null) {
            throw new BusinessException(this.translator.toLocale("customer.input.missing.contact.app.param.value"));
        }
        if (customer.getEmail() != null && !CommonUtil.isEmail(customer.getEmail())) {
            throw new BusinessException(this.translator.toLocale("customer.input.email.wrong.partern"));
        }
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public ResponseEntity<Object> create(String authorization, String acceptLanguage, Customer customerInput)
            throws BusinessException {
        if (null == customerInput) {
            throw new BusinessException(this.translator.toLocale("common.input.info.invalid"));
        }
        Optional<Customer> oCustomer;
        if (customerInput.getId() != null) {
            Long customerId = customerInput.getId();
            oCustomer = this.customerRespository.findById(customerId);
            if (oCustomer.isPresent()) {
                throw new BusinessException(this.translator.toLocaleByFormatString("customer.id.existed", customerId));
            }
        }

        customerInput.setStatus(false);
        this.validCustomer(customerInput);

        Long count = this.customerRespository.getOneResult("STO");
        String customerCode;
        if (count != null) {
            customerCode = "STO" + MathUtils.fixLengthString(++count, 4);
            boolean checkCustomer = false;
            while (!checkCustomer) {
                oCustomer = this.customerRespository.getCustomerByCodeByCode(customerCode);
                if (oCustomer.isPresent()) {
                    customerCode = "STO" + MathUtils.fixLengthString(++count, 4);
                } else {
                    checkCustomer = true;
                }
            }
        } else {
            customerCode = "STO" + MathUtils.fixLengthString(++count, 4);
        }
        customerInput.setCode(customerCode);
        Customer customer = this.customerRespository.save(customerInput);
        User user = this.mapCustomerToUser(customer);

        ResponseEntity<Object> result = this.singleSignOnUtils.post(authorization, acceptLanguage, "/user", user);
        if (!HttpStatus.OK.equals(result.getStatusCode())) {
            throw new BusinessException(this.translator.toLocale("customer.add.failed"));
        }
        return new ResponseEntity<>(customer, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<Object> update(String authorization, String acceptLanguage, Long customerId,
                                         Customer customerInput) throws BusinessException {
        if (customerInput == null) {
            throw new BusinessException(this.translator.toLocale("common.input.info.invalid"));
        }
        if (customerId == null || customerInput.getId() == null || !customerId.equals(customerInput.getId())) {
            throw new BusinessException(this.translator.toLocale("customer.input.missing.id"));
        }
        Optional<Customer> optionalCustomer = this.customerRespository.findById(customerId);
        if (!optionalCustomer.isPresent()) {
            throw new BusinessException(this.translator.toLocaleByFormatString("customer.id.not.exists", customerId));
        }

        this.validCustomer(customerInput);

        Customer customer = optionalCustomer.get();
        customer.setName(customerInput.getName());
        customer.setAddress(customerInput.getAddress());
        customer.setStatus(customerInput.getStatus());
        customer.setVip(customer.getVip());
        customer.setPhone(customerInput.getPhone());
        customer.setEmail(customerInput.getEmail());
        customer.setAddress1(customerInput.getAddress1());
        customer.setAddress2(customerInput.getAddress2());
        customer.setTaxCode(customerInput.getTaxCode());
        customer.setContactName(customerInput.getContactName());
        customer.setCity(customerInput.getCity());
        customer.setStateProvince(customerInput.getStateProvince());
        customer.setPostalCode(customerInput.getPostalCode());
        customer.setPhone1(customerInput.getPhone1());
        customer.setPhone2(customerInput.getPhone2());
        customer.setFax(customerInput.getFax());
        customer.setWebsite(customerInput.getWebsite());
        customer.setHasImage(customerInput.getHasImage());
        customer.setHasImage(customerInput.getHasImage());
        customer.setPaymentType(customerInput.getPaymentType());
        customer.setPayByDay(customerInput.getPayByDay());
        customer.setDiscountType(customerInput.getDiscountType());
        customer.setAppParamValueId(customerInput.getAppParamValueId());
        customer.setMobilePhone(customerInput.getMobilePhone());
        customer.setIdNo(customerInput.getIdNo());
        this.customerRespository.save(customer);
        User user = this.mapCustomerToUser(customer);

        Map<String, Long> pathVariables = new HashMap<>();
        pathVariables.put("id", 0L);

        ResponseEntity<Object> result = this.singleSignOnUtils.patch(authorization, acceptLanguage, "/user/{id}",
                pathVariables, user);

        if (!HttpStatus.OK.equals(result.getStatusCode())) {
            throw new BusinessException(this.translator.toLocale("customer.update.failed"));
        }

        return new ResponseEntity<>(customer, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<Object> activeOrDeactivate(String authorization, String acceptLanguage,
                                                     Customer customerInput, boolean isActive) throws BusinessException {
        if (customerInput == null) {
            throw new BusinessException(this.translator.toLocale("common.input.info.invalid"));
        }
        if (customerInput.getId() == null) {
            throw new BusinessException(this.translator.toLocale("customer.input.missing.id"));
        }

        Optional<Customer> optionalCustomer = this.customerRespository.findById(customerInput.getId());
        if (!optionalCustomer.isPresent()) {
            throw new BusinessException(
                    this.translator.toLocaleByFormatString("customer.id.not.exists", customerInput.getId()));
        }

        Customer customer = optionalCustomer.get();
        customer.setStatus(isActive ? CustomerStatus.ACTIVE.getValue() : CustomerStatus.DEACTIVE.getValue());
        this.customerRespository.save(customer);

        if (isActive) {
            ResponseEntity<Object> result = this.singleSignOnUtils.post(authorization, acceptLanguage, "/user/active",
                    customerInput.getCode());

            if (!HttpStatus.OK.equals(result.getStatusCode())) {
                throw new BusinessException(this.translator.toLocale("customer.active.failed"));
            }
        } else {
            ResponseEntity<Object> result = this.singleSignOnUtils.post(authorization, acceptLanguage, "/user/deactive",
                    customerInput.getCode());

            if (!HttpStatus.OK.equals(result.getStatusCode())) {
                throw new BusinessException(this.translator.toLocale("customer.deactivate.failed"));
            }
        }

        return new ResponseEntity<>(HttpStatus.OK);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public ResponseEntity<Object> resetPassword(String authorization, String acceptLanguage, Customer customer)
            throws BusinessException {
        Optional<Customer> optionalCustomer = this.customerRespository.findById(customer.getId());
        if (!optionalCustomer.isPresent()) {
            throw new BusinessException(
                    this.translator.toLocaleByFormatString("customer.id.not.exists", customer.getId()));
        }

        ResponseEntity<Object> result = this.singleSignOnUtils.post(authorization, acceptLanguage,
                "/user/reset-password", customer.getCode());
        if (!HttpStatus.OK.equals(result.getStatusCode())) {
            throw new BusinessException(this.translator.toLocale("customer.reset.password.failed"));
        }

        return new ResponseEntity<>(HttpStatus.OK);
    }

    private User mapCustomerToUser(Customer customer) {
        User result = new User();
        if (!StringUtils.isNullOrEmpty(customer.getCode())) {
            result.setUsername(customer.getCode());
        }
        if (!StringUtils.isNullOrEmpty(customer.getName())) {
            result.setLastName(customer.getName());
        }
        result.setFirstName("");
        result.setRoles(Collections.singletonList(new RoleDto(92L)));
        return result;
    }

}