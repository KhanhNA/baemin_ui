package com.tsolution._3services;

import com.tsolution._1entities.AppParam;
import com.tsolution._1entities.ApparamValue;
import com.tsolution.excetions.BusinessException;
import org.springframework.http.ResponseEntity;

public interface AppParamValueService {

    ResponseEntity<Object> create(ApparamValue entities) throws BusinessException;
    ResponseEntity<Object> findAll() throws BusinessException;
    ResponseEntity<Object> findById(Long id) throws BusinessException;
    ResponseEntity<Object> update(Long id,ApparamValue entities) throws BusinessException;

}
