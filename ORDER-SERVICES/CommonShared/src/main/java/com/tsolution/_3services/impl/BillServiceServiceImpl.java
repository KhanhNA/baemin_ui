package com.tsolution._3services.impl;


import com.tsolution._1entities.BillService;
import com.tsolution._1entities.enums.StatusType;
import com.tsolution._2repositories.BillServiceRepository;
import com.tsolution._3services.BillServiceService;
import com.tsolution.excetions.BusinessException;
import com.tsolution.utils.CommonUtil;
import com.tsolution.utils.SingleSignOnUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@org.springframework.stereotype.Service
public class BillServiceServiceImpl extends BaseServiceImpl implements BillServiceService {

    @Autowired
    BillServiceRepository serviceRepository;

    @Autowired
    public BillServiceServiceImpl(){
        
    }

    @Override
    public ResponseEntity<Object> findServiceByType(int type) {
        return new ResponseEntity<>(this.serviceRepository.findServiceByType(type),
                HttpStatus.OK);
    }

    @Override
    public ResponseEntity<Object> findAll() {
        return new ResponseEntity<>(this.serviceRepository.findAll(),HttpStatus.OK);
    }

    @Override
    public ResponseEntity<Object> findByActive() {
        return new ResponseEntity<>(this.serviceRepository.findByStatus(StatusType.RUNNING.getValue()),HttpStatus.OK);
    }

    @Override
    public ResponseEntity<Object> findById(Long id) throws BusinessException {
        return new ResponseEntity<>(this.serviceRepository.findById(id),HttpStatus.OK);
    }

    @Override
    public ResponseEntity<Object> find(BillService service, Integer pageNumber, Integer pageSize) throws BusinessException {
//        return new ResponseEntity<>(this.serviceRepository.find())
        return null;
    }

    @Override
    @Transactional
    public ResponseEntity<Object> create(BillService entity){
        try{
            String msg = validateCreate(entity);
            if (!StringUtils.isBlank(msg)) {
                return new ResponseEntity<>(this.translator.toLocale(msg),
                        HttpStatus.BAD_REQUEST);
            }
            entity.setStatus(1);
            BillService entities1 = this.serviceRepository.save(entity);
            return new ResponseEntity<>(entities1, HttpStatus.OK);
        } catch (Exception be){
            be.printStackTrace();
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    private String validateCreate(BillService entity) {
        if (entity == null || entity.getId()!=null) {
            return "common.input.info.invalid";
        }
        if(CommonUtil.isBlank(entity.getServiceName())){
            return "bill-service.validate.serviceName.empty";
        }
        if(CommonUtil.isBlank(entity.getServiceCode())){
            return "bill-service.validate.serviceCode.empty";
        }
        if(!CommonUtil.isPhoneNumber(entity.getPhone())){
            return "bill-service.validate.phoneNumber.invalid";
        }
        if(entity.getPrice() == null || entity.getPrice().equals(Double.valueOf(0))){
            return "bill-service.validate.price.empty";
        }
        Optional<BillService> oService;
        oService = this.serviceRepository.findByServiceCode(entity.getServiceCode());
        if (oService.isPresent()) {
            return "bill-service.validate.serviceCode.duplicate";
        }
        oService = this.serviceRepository.findByServiceName(entity.getServiceName());
        if (oService.isPresent()) {
            return "bill-service.validate.serviceName.duplicate";
        }
        return null;
    }

    @Override
    @Transactional
    public ResponseEntity<Object> update(BillService entity) throws BusinessException {
        try{
            String msg = validateUpdate(entity);
            if (!StringUtils.isBlank(msg)) {
                return new ResponseEntity<>(this.translator.toLocale(msg),
                        HttpStatus.BAD_REQUEST);
            }
            BillService entities1 = this.serviceRepository.save(entity);
            return new ResponseEntity<>(entities1, HttpStatus.OK);
        } catch (Exception be){
            be.printStackTrace();
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    private String validateUpdate(BillService entity){
        if (entity == null || entity.getId() == null) {
            return "common.input.info.invalid";
        }
        if(CommonUtil.isBlank(entity.getServiceName())){
            return "bill-service.validate.serviceName.empty";
        }
        if(CommonUtil.isBlank(entity.getServiceCode())){
            return "bill-service.validate.serviceCode.empty";
        }
        if(!CommonUtil.isPhoneNumber(entity.getPhone())){
            return "bill-service.validate.phoneNumber.invalid";
        }
        if(entity.getPrice() == null || entity.getPrice().equals(Double.valueOf(0))){
            return "bill-service.validate.price.empty";
        }
        Optional<BillService> oService;
        oService = this.serviceRepository.findByServiceCode(entity.getServiceCode());
        if (oService.isPresent() && !oService.get().getId().equals(entity.getId())) {
            return "bill-service.validate.serviceCode.duplicate";
        }
        Optional<BillService> oServiceN = this.serviceRepository.findByServiceName(entity.getServiceCode());
        if (oServiceN.isPresent() && !oServiceN.get().getId().equals(entity.getId())) {
            return "bill-service.validate.serviceName.duplicate";
        }
        return null;
    }
}
