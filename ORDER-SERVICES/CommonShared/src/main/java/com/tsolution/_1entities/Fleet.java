package com.tsolution._1entities;

import com.fasterxml.jackson.annotation.JsonView;
import com.tsolution._1entities.base.SuperEntity;
import com.tsolution._1entities.enums.JsonEntityViewer;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name = "fleet")
public class Fleet extends SuperEntity implements Serializable {
    private static final long serialVersionUID = 1L;

    @Column(name = "fleet_code")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private String fleetCode;

    @Column(name = "fleet_name")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private String fleetName;

    @Column(name = "business_registration")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private String businessRegistration;

    @Column(name = "address_registration")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private String addressRegistration;

    @Column(name = "address1")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private String address1;

    @Column(name = "address2")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private String address2;

    @Column(name = "contact_name")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private String contactName;

    @Column(name = "status")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private Integer status;

    @Column(name = "city")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private String city;

    @Column(name = "postal_code")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private String postalCode;

    @Column(name = "tax_code")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private String taxCode;

    @Column(name = "phone1")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private String phone1;

    @Column(name = "phone2")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private String phone2;

    @Column(name = "fax")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private String fax;

    @Column(name = "website")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private String website;

    @Column(name = "email")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private String email;

    @Column(name = "parent_id")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private Long parentId;

    public String getFleetCode() {
        return fleetCode;
    }

    public void setFleetCode(String fleetCode) {
        this.fleetCode = fleetCode;
    }

    public String getFleetName() {
        return fleetName;
    }

    public void setFleetName(String fleetName) {
        this.fleetName = fleetName;
    }

    public String getBusinessRegistration() {
        return businessRegistration;
    }

    public void setBusinessRegistration(String businessRegistration) {
        this.businessRegistration = businessRegistration;
    }

    public String getAddressRegistration() {
        return addressRegistration;
    }

    public void setAddressRegistration(String addressRegistration) {
        this.addressRegistration = addressRegistration;
    }

    public String getAddress1() {
        return address1;
    }

    public void setAddress1(String address1) {
        this.address1 = address1;
    }

    public String getAddress2() {
        return address2;
    }

    public void setAddress2(String address2) {
        this.address2 = address2;
    }

    public String getContactName() {
        return contactName;
    }

    public void setContactName(String contactName) {
        this.contactName = contactName;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public String getTaxCode() {
        return taxCode;
    }

    public void setTaxCode(String taxCode) {
        this.taxCode = taxCode;
    }

    public String getPhone1() {
        return phone1;
    }

    public void setPhone1(String phone1) {
        this.phone1 = phone1;
    }

    public String getPhone2() {
        return phone2;
    }

    public void setPhone2(String phone2) {
        this.phone2 = phone2;
    }

    public String getFax() {
        return fax;
    }

    public void setFax(String fax) {
        this.fax = fax;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Long getParentId() {
        return parentId;
    }

    public void setParentId(Long parentId) {
        this.parentId = parentId;
    }

    public String toString() {
        return "Fleet{fleetCode=" + fleetCode +
                ", fleetName=" + fleetName +
                ", businessRegistration=" + businessRegistration +
                ", addressRegistration=" + addressRegistration +
                ", address1=" + address1 +
                ", address2=" + address2 +
                ", contactName=" + contactName +
                ", city=" + city +
                ", status=" + status +
                ", postalCode=" + postalCode +
                ", taxCode=" + taxCode +
                ", phone1=" + phone1 +
                ", phone2=" + phone2 +
                ", fax=" + fax +
                ", website=" + website +
                ", email=" + email +
                "}";
    }
}