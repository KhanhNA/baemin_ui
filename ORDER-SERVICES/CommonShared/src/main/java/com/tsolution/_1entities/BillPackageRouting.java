package com.tsolution._1entities;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonView;
import com.tsolution._1entities.base.SuperEntity;
import com.tsolution._1entities.enums.JsonEntityViewer;
import com.voodoodyne.jackson.jsog.JSOGGenerator;

import java.io.Serializable;
import java.util.List;

import javax.persistence.*;

@Table(name = "bill_package_routing")
@Entity
@JsonIdentityInfo(generator = JSOGGenerator.class)
public class BillPackageRouting extends SuperEntity implements Serializable {
    private static final long serialVersionUID = 1L;

    @Column(name = "quantity")
    @JsonView(JsonEntityViewer.Human.Summary.class)

    private Integer quantity ;


    @Column(name = "length")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private Float length ;

    @Column(name = "width")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private Float width ;

    @Column(name = "height")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private Float height ;

    @Column(name = "total_weight")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private Float totalWeight ;




    @Column(name = "note")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private String note ;


//    @ManyToOne
//    @JoinColumn(name = "from_warehouse_id", referencedColumnName = "id")
//    @JsonView(JsonEntityViewer.Human.Summary.class)
//    private Warehouse fromWarehouse ;
//
//    @ManyToOne
//    @JoinColumn(name = "to_warehouse_id", referencedColumnName = "id")
//    @JsonView(JsonEntityViewer.Human.Summary.class)
//    private Warehouse toWarehouse;

    @Column(name = "from_warehouse_id")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private Long fromWarehouseId ;


    @Column(name = "to_warehouse_id")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private Long toWarehouseId;

    @ManyToOne
    @JoinColumn(name = "bill_package_id", referencedColumnName = "id")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private BillPackage billPackage ;

    @ManyToOne
    @JoinColumn(name = "bill_service_id",referencedColumnName = "id")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private BillService billService;

    @ManyToOne
    @JoinColumn(name = "app_param_value_id", referencedColumnName = "id")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private ApparamValue appParamValue ;


    @ManyToOne
    @JoinColumn(name = "insurance_id", referencedColumnName = "id")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private Insurance insurance ;

    @Column(name = "QRchar")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private String QRchar;

    @Column(name = "capacity")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private Double capacity;



    public Double getCapacity() {
        return capacity;
    }

    public void setCapacity(Double capacity) {
        this.capacity = capacity;
    }

    public String getQRchar() {
        return QRchar;
    }

    public void setQRchar(String QRchar) {
        this.QRchar = QRchar;
    }

    @Transient
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private RoutingPlanDetail routingPlanDetai;

    @Transient
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private Integer part;

    @Transient
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private List<ClaimReport> lstClaimReport;

    public Integer getPart() {
        return part;
    }

    public void setPart(Integer part) {
        this.part = part;
    }

    public List<ClaimReport> getLstClaimReport() {
        return lstClaimReport;
    }

    public void setLstClaimReport(List<ClaimReport> lstClaimReport) {
        this.lstClaimReport = lstClaimReport;
    }

    public RoutingPlanDetail getRoutingPlanDetai() {
        return routingPlanDetai;
    }

    public void setRoutingPlanDetai(RoutingPlanDetail routingPlanDetai) {
        this.routingPlanDetai = routingPlanDetai;
    }

    //    @Transient
//    @JoinColumn(name = "id", referencedColumnName = "bill_package_routing_id", insertable = false, updatable = false)
//    @JsonView(JsonEntityViewer.Human.Detail.class)
//    private List<RoutingPlanDetail> routingPlanDetails;



    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public Float getLength() {
        return length;
    }

    public void setLength(Float length) {
        this.length = length;
    }

    public Float getWidth() {
        return width;
    }

    public void setWidth(Float width) {
        this.width = width;
    }

    public Float getHeight() {
        return height;
    }

    public void setHeight(Float height) {
        this.height = height;
    }

    public Float getTotalWeight() {
        return totalWeight;
    }

    public void setTotalWeight(Float totalWeight) {
        this.totalWeight = totalWeight;
    }

    public BillPackage getBillPackage() {
        return billPackage;
    }

    public void setBillPackage(BillPackage billPackage) {
        this.billPackage = billPackage;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public ApparamValue getAppParamValue() {
        return appParamValue;
    }

    public void setAppParamValue(ApparamValue appParamValue) {
        this.appParamValue = appParamValue;
    }

    public Long getFromWarehouseId() {
        return fromWarehouseId;
    }

    public void setFromWarehouseId(Long fromWarehouseId) {
        this.fromWarehouseId = fromWarehouseId;
    }

    public Long getToWarehouseId() {
        return toWarehouseId;
    }

    public void setToWarehouseId(Long toWarehouseId) {
        this.toWarehouseId = toWarehouseId;
    }

    public BillService getBillService() {
        return billService;
    }

    public void setBillService(BillService billService) {
        this.billService = billService;
    }

    public Insurance getInsurance() {
        return insurance;
    }

    public void setInsurance(Insurance insurance) {
        this.insurance = insurance;



    }
}