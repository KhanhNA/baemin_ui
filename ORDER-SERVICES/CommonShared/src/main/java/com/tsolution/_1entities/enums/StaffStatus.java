package com.tsolution._1entities.enums;

public enum StaffStatus {
	STATUS_ACTIVE(1), STATUS_DEACTIVE(0);

	private Integer value;

	StaffStatus(Integer value) {
		this.value = value;
	}

	public Integer getValue() {
		return this.value;
	}
}
