package com.tsolution._1entities;

import com.fasterxml.jackson.annotation.JsonView;
import com.tsolution._1entities.base.SuperEntity;
import com.tsolution._1entities.enums.JsonEntityViewer;

import java.io.Serializable;
import java.time.LocalDate;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Table(name = "position")
@Entity
public class Position extends SuperEntity implements Serializable {
    private static final long serialVersionUID = 1L;

    @Column(name = "position_code")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private String positionCode;

    @Column(name = "van_id")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private Long vanId;

    @Column(name = "lat")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private Double lat;

    @Column(name = "lng")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private Integer lng;

    @Column(name = "time")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private LocalDate time;

    @Column(name = "speed")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private Double speed;

    @Column(name = "flue_percent")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private Float fluePercent;

    @Column(name = "motion")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private String motion;

    public String getPositionCode() {
        return positionCode;
    }

    public void setPositionCode(String positionCode) {
        this.positionCode = positionCode;
    }

    public Long getVanId() {
        return vanId;
    }

    public void setVanId(Long vanId) {
        this.vanId = vanId;
    }

    public Double getLat() {
        return lat;
    }

    public void setLat(Double lat) {
        this.lat = lat;
    }

    public Integer getLng() {
        return lng;
    }

    public void setLng(Integer lng) {
        this.lng = lng;
    }

    public LocalDate getTime() {
        return time;
    }

    public void setTime(LocalDate time) {
        this.time = time;
    }

    public Double getSpeed() {
        return speed;
    }

    public void setSpeed(Double speed) {
        this.speed = speed;
    }

    public Float getFluePercent() {
        return fluePercent;
    }

    public void setFluePercent(Float fluePercent) {
        this.fluePercent = fluePercent;
    }

    public String getMotion() {
        return motion;
    }

    public void setMotion(String motion) {
        this.motion = motion;
    }

    public String toString() {
      return "Position{positionCode=" + positionCode + 
        ", vanId=" + vanId + 
        ", lat=" + lat + 
        ", lng=" + lng + 
        ", time=" + time + 
        ", speed=" + speed + 
        ", fluePercent=" + fluePercent + 
        ", motion=" + motion + 
        "}";
    }
}