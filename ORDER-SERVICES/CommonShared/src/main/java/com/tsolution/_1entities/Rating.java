package com.tsolution._1entities;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonView;
import com.tsolution._1entities.base.SuperEntity;
import com.tsolution._1entities.enums.JsonEntityViewer;
import com.voodoodyne.jackson.jsog.JSOGGenerator;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "rating")
@JsonIdentityInfo(generator = JSOGGenerator.class)
public class Rating extends SuperEntity implements Serializable {
    private static final long serialVersionUID = 1L;

    @Column(name = "routing_plan_day_id")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private Long routingPlanDayId;

    @Column(name = "num_rating")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private Integer numRating;

    @Column(name = "note")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private String note;

    @Column(name = "rating_type")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private Integer ratingType;

    @ManyToOne
    @JoinColumn(name = "driver_id", nullable = false)
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private Staff staff;

    @Column(name = "status")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private Integer status;


    @Column(name = "point")
    @JsonView(JsonEntityViewer.Human.Summary.class)
    private Integer point;


    public Long getRoutingPlanDayId() {
        return routingPlanDayId;
    }

    public void setRoutingPlanDayId(Long routingPlanDayId) {
        this.routingPlanDayId = routingPlanDayId;
    }

    public Integer getNumRating() {
        return numRating;
    }

    public void setNumRating(Integer numRating) {
        this.numRating = numRating;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public Staff getStaff() {
        return staff;
    }

    public void setStaff(Staff staff) {
        this.staff = staff;
    }

    public Integer getRatingType() {
        return ratingType;
    }

    public void setRatingType(Integer ratingType) {
        this.ratingType = ratingType;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getPoint() {
        return point;
    }

    public void setPoint(Integer point) {
        this.point = point;
    }

    public String toString() {
        return "Rating{routingPlanDayId=" + routingPlanDayId +
                ", numRating=" + numRating +
                ", note=" + note +
                ", driverId=" + "" +
                "}";
    }
}