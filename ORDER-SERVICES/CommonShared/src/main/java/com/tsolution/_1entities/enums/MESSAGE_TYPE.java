package com.tsolution._1entities.enums;

public enum MESSAGE_TYPE {
	CUSTOMER_APP(1), DRIVER_APP(3), SALE_APP(4);

	private Integer value;

	MESSAGE_TYPE(Integer value) {
		this.value = value;
	}

	public Integer getValue() {
		return this.value;
	}
}
