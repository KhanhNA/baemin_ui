package com.tsolution._4controllers.impl;

import com.tsolution._1entities.Customer;
import com.tsolution._1entities.dto.PushNotificationRequest;
import com.tsolution._3services.CustomerService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import com.tsolution._4controllers.BaseController;
import com.tsolution.excetions.BusinessException;

@RestController
@RequestMapping("/customers")
public class CustomerController extends BaseController {

    @Autowired
    private CustomerService customerService;

    @GetMapping("/all")
    @ApiOperation(value = "none",
            notes = "get all customer",
            response = Customer.class,
            responseContainer = "")
    @PreAuthorize("hasAuthority('get/customers/all')")
    public ResponseEntity<Object> findAll() {
        return this.customerService.findAll();
    }

    @GetMapping("/{id}")
    @ApiOperation(value = "token",
            notes = "get customer by id",
            response = Customer.class,
            responseContainer = "long")
    @PreAuthorize("hasAuthority('get/customers/{id}')")
    public ResponseEntity<Object> findById(@PathVariable("id") Long id) throws BusinessException {
        return this.customerService.findById( id );
    }

    @GetMapping
    @ApiOperation(value = "token",
            notes = "get customer by code , name, phone, address, vip",
            response = Customer.class,
            responseContainer = "String")
    @PreAuthorize("hasAuthority('get/customers')")
    public ResponseEntity<Object> find(@RequestParam(required = false) String code,
                                       @RequestParam(required = false) String name,
                                       @RequestParam(required = false) String phone,
                                       @RequestParam(required = false) String address,
                                       @RequestParam(required = false) Boolean status,
                                       @RequestParam(required = false) Boolean vip,
                                       @RequestParam Integer pageNumber,
                                       @RequestParam Integer pageSize) throws BusinessException {
        Customer customer = new Customer();
        customer.setCode( code );
        customer.setName( name );
        customer.setPhone( phone );
        customer.setAddress( address );
        customer.setStatus( status );
        customer.setVip( vip );
        return this.customerService.find( customer, pageNumber, pageSize );
    }

    @PostMapping
    @PreAuthorize("hasAuthority('post/customers')")
    @ApiOperation(value = "customer",
            notes = "tao khach hang",
            response = Customer.class,
            responseContainer = "object")
    public ResponseEntity<Object> create(@RequestHeader("Authorization") String authorization,
                                         @RequestHeader("Accept-Language") String acceptLanguage,
                                         @RequestBody Customer customer)
            throws BusinessException {
        return this.customerService.create( authorization, acceptLanguage, customer );
    }

    @PatchMapping("/{id}")
    @ApiOperation(value = "customer",
            notes = "update khach hang",
            response = Customer.class,
            responseContainer = "object,id")
    @PreAuthorize("hasAuthority('patch/customers/{id}')")
    public ResponseEntity<Object> update(@RequestHeader("Authorization") String authorization,
                                         @RequestHeader("Accept-Language") String acceptLanguage,
                                         @PathVariable("id") Long id,
                                         @RequestBody Customer customer)
            throws BusinessException {
        return this.customerService.update( authorization, acceptLanguage, id, customer );
    }

    @PostMapping("/active")
    @ApiOperation(value = "customer",
            notes = "active  khach hang",
            response = Customer.class,
            responseContainer = "object")
    @PreAuthorize("hasAuthority('post/customers/active')")
    public ResponseEntity<Object> active(@RequestHeader("Authorization") String authorization,
                                         @RequestHeader("Accept-Language") String acceptLanguage,
                                         @RequestBody Customer customer)
            throws BusinessException {
        return this.customerService.activeOrDeactivate( authorization, acceptLanguage, customer, true );
    }

    @PostMapping("/deactivate")
    @ApiOperation(value = "customer",
            notes = "deactive  khach hang",
            response = Customer.class,
            responseContainer = "object")
    @PreAuthorize("hasAuthority('post/customers/deactivate')")
    public ResponseEntity<Object> deactivate(@RequestHeader("Authorization") String authorization,
                                             @RequestHeader("Accept-Language") String acceptLanguage,
                                             @RequestBody Customer customer)
            throws BusinessException {
        return this.customerService.activeOrDeactivate( authorization, acceptLanguage, customer, false );
    }

    @PostMapping("/reset-pass")
    @ApiOperation(value = "customer",
            notes = "reset-pass  khach hang",
            response = Customer.class,
            responseContainer = "object")
    @PreAuthorize("hasAuthority('post/customers/reset-pass')")
    public ResponseEntity<Object> resetPassword(@RequestHeader("Authorization") String authorization,
                                                @RequestHeader("Accept-Language") String acceptLanguage,
                                                @RequestBody Customer customer) throws BusinessException {
        return this.customerService.resetPassword( authorization, acceptLanguage, customer );
    }
}
