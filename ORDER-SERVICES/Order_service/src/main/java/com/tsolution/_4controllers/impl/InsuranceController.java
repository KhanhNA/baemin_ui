package com.tsolution._4controllers.impl;


import com.tsolution._1entities.Insurance;
import com.tsolution._3services.InsuranceService;
import com.tsolution._4controllers.BaseController;
import com.tsolution.excetions.BusinessException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/insurances")
public class InsuranceController extends BaseController {

    @Autowired
    private InsuranceService insuranceService;

    @PostMapping
    @PreAuthorize("hasAuthority('post/insurances')")
    public ResponseEntity<Object> create(
            @RequestHeader("Accept-Language") String acceptLanguage,
            @RequestBody Insurance entity
    )
            throws BusinessException {
        return this.insuranceService.create(acceptLanguage, entity);
    }

    @PatchMapping("/{id}")
    @PreAuthorize("hasAuthority('patch/insurances/{id}')")
    public ResponseEntity<Object> update(
            @RequestHeader("Accept-Language") String acceptLanguage,
            @RequestBody Insurance entity,
            @PathVariable("id") Long id
    )
            throws BusinessException {
        return this.insuranceService.update(acceptLanguage, id, entity);
    }

    @GetMapping("/all")
    @PreAuthorize("hasAuthority('get/insurances/all')")
    public ResponseEntity<Object> findAll() {
        return this.insuranceService.findAll();
    }

    @GetMapping("/{id}")
    @PreAuthorize("hasAuthority('get/insurances/{id}')")
    public ResponseEntity<Object> findById(@PathVariable("id") Long id) throws BusinessException {
        System.out.println(this.insuranceService.findById(id));
        return this.insuranceService.findById(id);
    }

    // TODO: Cần phải tối ưu hàm tìm kiếm khi cần thiết
    @GetMapping("/available")
    //@PreAuthorize("hasAuthority('get/insurances/all')")
    public ResponseEntity<Object> findAvaiable() {
        return this.insuranceService.findAvaiable();
    }
}
