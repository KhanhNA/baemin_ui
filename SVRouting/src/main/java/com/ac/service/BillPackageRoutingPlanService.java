package com.ac.service;

import com.ac.entity.RoutingPlanDay;
import com.ac.entity.SharevanBillPackageRoutingPlan;
import com.ac.model.RoutingDetail;
import org.hibernate.internal.SessionImpl;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
@Service
public class BillPackageRoutingPlanService {
    @PersistenceContext
    protected EntityManager entityManager;
    public List<SharevanBillPackageRoutingPlan> getListBillRoutingPlanByLstRoutingId(Integer[] ids){
        //String lstIds = org.apache.commons.lang3.StringUtils.join(ids,",");
        String sql = "select routing from SharevanBillPackageRoutingPlan routing where routing.routingPlanDayId in " ;
        String idInput = "(";
        for(int i =0; i < ids.length; i++){
            idInput += ":id" + i + ",";
        }
        idInput = idInput.substring(0,idInput.length()-1) + ")";
        sql += idInput + " order by routing.routingPlanDayId";

        Query query = entityManager.createQuery(sql,SharevanBillPackageRoutingPlan.class);
        for(int i =0; i < ids.length; i++){
            query.setParameter("id" + i,ids[i]);
        }
        List<SharevanBillPackageRoutingPlan> result = query.getResultList();

        return result;
    }
    public HashMap<RoutingPlanDay, RoutingDetail> getMapBillPackageRoutingPlan(Integer[] ids, List<RoutingPlanDay> lstClaim,
                                                                               HashMap<RoutingPlanDay, RoutingDetail> mapPackagePlan){
        List<SharevanBillPackageRoutingPlan> result = getListBillRoutingPlanByLstRoutingId(ids);
       // HashMap<RoutingPlanDay,SharevanBillPackageRoutingPlan> mapPackagePlan = new HashMap<>();
        if(result != null && !result.isEmpty()){
            for(RoutingPlanDay routing: lstClaim){
                List<SharevanBillPackageRoutingPlan> lstNewPlan = new ArrayList<>();
                for(SharevanBillPackageRoutingPlan plan : result){
                    if(routing.getId().equals(plan.getRoutingPlanDayId())){
                        lstNewPlan.add(plan);
                    }
                }
                RoutingDetail detail = mapPackagePlan.get(routing);
                detail.setBillPackageRoutingPlan(lstNewPlan);
            }
        }
        return mapPackagePlan;
    }
}
