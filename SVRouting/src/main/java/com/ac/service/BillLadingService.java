package com.ac.service;

import com.ac.entity.SharevanBillLading;
import com.ac.enums.StatusType;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.Transactional;
import java.time.DayOfWeek;
import java.time.Duration;
import java.time.LocalDate;
import java.util.List;
import java.util.spi.LocaleServiceProvider;
import java.time.temporal.ChronoUnit;

import static java.time.temporal.ChronoUnit.DAYS;

@Service
public class BillLadingService {
    @PersistenceContext
    protected EntityManager entityManager;

    public List<SharevanBillLading> getLstBOLToday(String datePlan){
        Query query = entityManager.createQuery("select sbl from SharevanBillLading sbl\n" +
                "  where   sbl.startDate <= to_date(:datePlan,'dd/mm/yyyy')\n" +
                "  and sbl.endDate >= to_date(:datePlan,'dd/mm/yyyy')\n" +
                "  and sbl.status = :status\n" +
                "  and (case when sbl.frequency in (4,5) and sbl.startDate = to_date(:datePlan,'dd/mm/yyyy') then 1     " +
                "            when sbl.frequency in (1) then 1    \n" +
                "            when sbl.frequency in (2) and sbl.dayOfWeek = EXTRACT (dow from to_date(:datePlan,'dd/mm/yyyy')) then 1    " +
                "            when sbl.frequency in (3) and sbl.dayOfMonth = extract(day from to_date(:datePlan,'dd/mm/yyyy')) then 1  \n" +
                "       else 0   \n" +
                "     end) =1 \n" +
                " and (sbl.lastScan is null or sbl.lastScan < to_date(:datePlan,'dd/mm/yyyy'))",SharevanBillLading.class);
        query.setParameter("datePlan",datePlan);
        query.setParameter("status", StatusType.RUNNING.getValue());
        List<SharevanBillLading> lstBOL = query.getResultList();
        return lstBOL;
    }
    public List<SharevanBillLading> getListBOL(){
        Query query  = entityManager.createQuery("select bld from SharevanBillLading bld " +
                " where bld.orderNum is null or bld.orderNum = 0", SharevanBillLading.class);
        List<SharevanBillLading> lstBOL = query.getResultList();
        return lstBOL;
    }
    @Transactional
    public void updateOrderNumOfBOL(){
        List<SharevanBillLading> lstBOL = getListBOL();
        for(SharevanBillLading bld : lstBOL){
            bld = calOrderNumOfBOL(bld);
            entityManager.merge(bld);
        }
    }
    public SharevanBillLading calOrderNumOfBOL(SharevanBillLading bld){
            int frequency = bld.getFrequency();
            LocalDate startDate = bld.getStartDate();
            LocalDate endDate = bld.getEndDate();
            int dayOfWeek = bld.getDayOfWeek();
            int dayOfMonth = bld.getDayOfMonth();
            int dem = 0;
            if( frequency == 4 || frequency == 5){
                dem = 1;
            }
            else if( frequency == 1){

                dem = endDate.getDayOfYear() - startDate.getDayOfYear() + 1;
            }
            else if(frequency == 2){
                while (startDate.compareTo(endDate) <= 0){
                    DayOfWeek dayWeek = DayOfWeek.from(startDate);
                    int dayOfWeekCheck = week_check(dayWeek.getValue());
                    if(dayOfWeekCheck == dayOfWeek){
                        dem = 1;
                        break;
                    }
                    else{
                        startDate = startDate.plusDays(1);
                    }
                }
                startDate = startDate.plusDays(7);
                while (startDate.compareTo(endDate) <= 0){
                    dem ++;
                    startDate = startDate.plusDays(7);
                }

            }
            else if(frequency == 3){
                int yearStart = startDate.getYear();
                int monthStart = startDate.getMonthValue();
                int dayStart = startDate.getDayOfMonth();
                LocalDate dateCheck = null;
                int yearEnd = endDate.getYear();
                int monthEnd = endDate.getMonthValue();
                int dayEnd = endDate.getDayOfMonth();
                if(yearEnd > yearStart){
                    while (monthStart <= 12){

                        dateCheck = LocalDate.of(yearStart,monthStart,dayStart);
                        if(dateCheck.compareTo(startDate) >= 0){
                            dem ++;
                        }
                        monthStart ++;
                    }
                    yearStart ++;
                    while(yearStart < yearEnd){
                        dem += 12;
                        yearStart ++;
                    }
                    //date_check =  str(year_end) + "-" + str(month_end) + "-" + str(day_of_month)
                    dateCheck = LocalDate.of(yearEnd,monthEnd,dayOfMonth);
                    if(dateCheck.compareTo(endDate) <= 0){
                        dem ++;
                    }
                    monthEnd --;
                    while (monthEnd >= 1){
                        dem ++;
                        monthEnd--;
                    }
                }
                else{
                    //date_check =     str(year_end) + "-" + str(month_start) + "-" + str(day_of_month)
                    dateCheck = LocalDate.of(yearEnd,monthStart,dayOfMonth);
                    if(dateCheck.compareTo(startDate) >= 0){
                        dem ++;
                    }
                    monthStart ++;
                    while (monthStart <= monthEnd){
//                        date_check =   str(year_end) + "-" + str(month_start) + "-" + str(day_of_month)
//                        date_check = datetime.strptime(str(date_check),"%Y-%m-%d").date()
                        dateCheck = LocalDate.of(yearEnd,monthStart,dayOfMonth);
                        if(dateCheck.compareTo(endDate) <= 0){
                            dem ++;
                        }
                        monthStart++;
                    }
                }
            }
            bld.setOrderNum(dem);
            return bld;
    }
    public int week_check(int day){
        int newDay = 0;
        switch (day){
            case 0: newDay = 1;
                    break;
            case 1: newDay = 2;
                    break;
            case 2: newDay = 3;
                break;
            case 3: newDay = 4;
                break;
            case 4: newDay = 5;
                break;
            case 5: newDay = 6;
                break;
            case 6: newDay = 0;
                break;
        }
        return newDay;
    }


}
