package com.ac.tasks;

import com.ac.entity.LocationData;
import com.ac.entity.WarehouseLog;
import com.ac.model.LatLng;
import com.ac.repository.LocationDataRepository;
import com.ac.repository.WarehouseLogRepository;
import com.ac.service.BillRoutingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class BillRoutingSync {
    @Autowired
    private BillRoutingService billRoutingService;
    @Autowired
    private WarehouseLogRepository warehouseLogRepository;
    @Autowired
    private LocationDataRepository dataRepository;

    List<LocationData> lstLocation;

    @Scheduled(initialDelay = 0, fixedDelay = 10000)
    public void run() {
        System.out.println("hello");
//        try {
//            LocalDateTime ldt = LocalDateTime.now();
//            //       LocalDateTime ldt = LocalDateTime.of(2020,11,03,0,0);
//            billRoutingService.createListBillRoutingToday(DateUtils.toString(ldt, DateUtils.DD_MM_YYYY));
//
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
    }
    @Scheduled(initialDelay = 0, fixedDelay = 180000)
    public void computeCostWarehouseLog(){
        System.out.println("tien trinh warehouse log");
        computeWarehouseLogNew();
    }
    @Scheduled(initialDelay = 0, fixedDelay = 30000)
    public void cleanCostWarehouseLog(){
        System.out.println("cleanWarehouseLog");
        cleanWarehouseLog();
        System.out.println("end clean warehouse log");
    }

    public void cleanWarehouseLog(){
        List<WarehouseLog> lstWarehouseLog = warehouseLogRepository.getLstWarehouseLogClean(false);
        //   List<WarehouseLog> lstWarehouseLog = warehouseLogRepository.findAll();
        if(lstLocation == null){
            lstLocation = dataRepository.findAll();
        }
        for(WarehouseLog log : lstWarehouseLog){
            String timeKey = LocationData.getLocationCode(log.getFromLatitude(), log.getFromLongitude(),
                    log.getToLatitude(), log.getToLongitude()); //"F" + from.getCode() + "-T" + to.getCode();
            int index = lstLocation.indexOf(new LocationData(timeKey));
            if(index != -1){
                LocationData data = lstLocation.get(index);
                log.setDistance(data.getCost());
                log.setTime(data.getMinutes());
                log.setScanCheck(true);
                System.out.println("clean log"+ log.getFromName()+"-"+log.getToName());
            }
            warehouseLogRepository.save(log);
        }
    }
    public void computeWarehouseLog(){
        List<WarehouseLog> lstWarehouseLog = warehouseLogRepository.getLstWarehouseLogCompute(false);
     //   List<WarehouseLog> lstWarehouseLog = warehouseLogRepository.findAll();
        if(lstLocation == null){
            lstLocation = dataRepository.findAll();
        }
        for(WarehouseLog log : lstWarehouseLog){
            String timeKey = LocationData.getLocationCode(log.getFromLatitude(), log.getFromLongitude(),
                    log.getToLatitude(), log.getToLongitude()); //"F" + from.getCode() + "-T" + to.getCode();
            int index = lstLocation.indexOf(new LocationData(timeKey));
            if(index != -1){
                LocationData data = lstLocation.get(index);
                log.setDistance(data.getCost());
                log.setTime(data.getMinutes());
                log.setScanCheck(true);
            }
            else {
                LocationData data = GoogleDistanceAPI.getDistance(log.getFromLatitude(), log.getFromLongitude(),
                        log.getToLatitude(), log.getToLongitude());
                System.out.println(data.getCode());
                if(data.getMinutes().equals(0.0) && data.getCost().equals(0.0) && data.getFromLatitude() != data.getToLatitude()){

                    break;
                }
                data = dataRepository.save(data);
                lstLocation.add(data);
                log.setDistance(data.getCost());
                log.setTime(data.getMinutes());
                log.setScanCheck(true);
            }

            warehouseLogRepository.save(log);
        }
    }

    public void computeWarehouseLogNew(){
        List<WarehouseLog> lstWarehouseLog = warehouseLogRepository.getLstWarehouseLogCompute(false);
        //   List<WarehouseLog> lstWarehouseLog = warehouseLogRepository.findAll();
        if(lstLocation == null){
            lstLocation = dataRepository.findAll();
        }
        List<LatLng> lstUpdate = new ArrayList<>();
        List<WarehouseLog> success = new ArrayList<>();
        for(WarehouseLog log : lstWarehouseLog){
            if(log.getFromLatitude()== 0.0 ||log.getFromLongitude()== 0.0||log.getToLatitude()== 0.0 ||log.getToLongitude()== 0.0){
                log.setScanCheck(true);
                warehouseLogRepository.save(log);
            }else{
                String timeKey = LocationData.getLocationCode(log.getFromLatitude(), log.getFromLongitude(),
                        log.getToLatitude(), log.getToLongitude()); //"F" + from.getCode() + "-T" + to.getCode();
                int index = lstLocation.indexOf(new LocationData(timeKey));
                if(index != -1){
                    LocationData data = lstLocation.get(index);
                    log.setDistance(data.getCost());
                    log.setTime(data.getMinutes());
                    log.setScanCheck(true);
                    warehouseLogRepository.save(log);
                    System.out.println(" update old data: "+ log.getFromName()+ " to: "+ log.getToName());
                }
                else {
                    lstUpdate.add(new LatLng(log.getFromLatitude(),log.getFromLongitude()));
                    lstUpdate.add(new LatLng(log.getToLatitude(),log.getToLongitude()));
                    success.add(log);
                    if (lstUpdate.size()==10){
                        break;
                    }
                }
            }
        }
        if (lstUpdate.size()>0){
            List<LocationData>dataList =GoogleDistanceAPI.getListDistance(lstUpdate);
            for (WarehouseLog log : success){
                for ( LocationData locationData : dataList){
                    String timeKey = LocationData.getLocationCode(log.getFromLatitude(), log.getFromLongitude(),
                            log.getToLatitude(), log.getToLongitude()); //"F" + from.getCode() + "-T" + to.getCode();
                    int index = lstLocation.indexOf(new LocationData(timeKey));
                    if(index == -1){
                        locationData.setCode(timeKey);
                        locationData = dataRepository.save(locationData);
                        lstLocation.add(locationData);
                    }
                    if(locationData.getFromLatitude().equals(log.getFromLatitude()) && locationData.getFromLongtitude().equals(log.getFromLongitude())
                            && locationData.getToLatitude().equals(log.getToLatitude()) && locationData.getToLongitude().equals(log.getToLongitude())){
                        log.setDistance(locationData.getCost());
                        log.setTime(locationData.getMinutes());
                        log.setScanCheck(true);
                    }
                }
                warehouseLogRepository.save(log);
            }
        }
    }
//    public void computeWarehouseLogLast(){
//        List<WarehouseLog> lstWarehouseLog = warehouseLogRepository.getLstWarehouseLogCompute(false);
//        //   List<WarehouseLog> lstWarehouseLog = warehouseLogRepository.findAll();
//        if(lstLocation == null){
//            lstLocation = dataRepository.findAll();
//        }
//        List<LatLng> lstUpdate = new ArrayList<>();
//        List<WarehouseLog> success = new ArrayList<>();
//        List<WarehouseLog> extraList = new ArrayList<>();
//        for(WarehouseLog log : lstWarehouseLog){
//            if(log.getFromLatitude()== 0.0 ||log.getFromLongitude()== 0.0||log.getToLatitude()== 0.0 ||log.getToLongitude()== 0.0){
//                log.setScanCheck(true);
//                warehouseLogRepository.save(log);
//            }else{
//                String timeKey = LocationData.getLocationCode(log.getFromLatitude(), log.getFromLongitude(),
//                        log.getToLatitude(), log.getToLongitude()); //"F" + from.getCode() + "-T" + to.getCode();
//                int index = lstLocation.indexOf(new LocationData(timeKey));
//                if(index != -1){
//                    LocationData data = lstLocation.get(index);
//                    log.setDistance(data.getCost());
//                    log.setTime(data.getMinutes());
//                    log.setScanCheck(true);
//                    warehouseLogRepository.save(log);
//                    System.out.println(" update old data: "+ log.getFromName()+ " to: "+ log.getToName());
//                }else if (lstUpdate.size()<10) {
//                    lstUpdate.add(new LatLng(log.getFromLatitude(),log.getFromLongitude()));
//                    lstUpdate.add(new LatLng(log.getToLatitude(),log.getToLongitude()));
//                    success.add(log);
//                }else{
//                    extraList.add(log);
//                }
//            }
//        }
//        if (lstUpdate.size()>0){
//            GoogleCheckResult googleCheckResult = GoogleDistanceAPI.getGoogleCheckResult(lstUpdate,extraList);
//            List<LocationData>dataList =googleCheckResult.getLocationData();
////            update default list
//            for (WarehouseLog log : success){
//                for ( LocationData locationData : dataList){
//                    String timeKey = LocationData.getLocationCode(log.getFromLatitude(), log.getFromLongitude(),
//                            log.getToLatitude(), log.getToLongitude()); //"F" + from.getCode() + "-T" + to.getCode();
//                    int index = lstLocation.indexOf(new LocationData(timeKey));
//                    if(index == -1){
//                        locationData.setCode(timeKey);
//                        locationData = dataRepository.save(locationData);
//                        lstLocation.add(locationData);
//                    }
//                    if(locationData.getFromLatitude().equals(log.getFromLatitude()) && locationData.getFromLongtitude().equals(log.getFromLongitude())
//                            && locationData.getToLatitude().equals(log.getToLatitude()) && locationData.getToLongitude().equals(log.getToLongitude())){
//                        log.setDistance(locationData.getCost());
//                        log.setTime(locationData.getMinutes());
//                        log.setScanCheck(true);
//                    }
//                }
//                warehouseLogRepository.save(log);
//            }
//            //            update extra list
//            for (WarehouseLog log: extraList){
//                for ( LocationData locationData : dataList){
//                    String timeKey = LocationData.getLocationCode(log.getFromLatitude(), log.getFromLongitude(),
//                            log.getToLatitude(), log.getToLongitude()); //"F" + from.getCode() + "-T" + to.getCode();
//                    int index = lstLocation.indexOf(new LocationData(timeKey));
//                    if(index == -1){
//                        locationData.setCode(timeKey);
//                        locationData = dataRepository.save(locationData);
//                        lstLocation.add(locationData);
//                    }
//                    if(locationData.getFromLatitude().equals(log.getFromLatitude()) && locationData.getFromLongtitude().equals(log.getFromLongitude())
//                            && locationData.getToLatitude().equals(log.getToLatitude()) && locationData.getToLongitude().equals(log.getToLongitude())){
//                        log.setDistance(locationData.getCost());
//                        log.setTime(locationData.getMinutes());
//                        log.setScanCheck(true);
//                    }
//                }
//                warehouseLogRepository.save(log);
//            }
//        }
//    }
//    @Scheduled(initialDelay = 0, fixedDelay = 10000000)
//    public void run1() {
//        System.out.println("hello");
//        try {
////            Date date = new Date();
//            LocalDateTime ldt = LocalDateTime.now();
//            //       LocalDateTime ldt = LocalDateTime.of(2020,11,03,0,0);
////            String date = DateTimeFormatter.ofPattern("yyyy/MM/dd").format(ldt);
////            routingPlanDayService.getBillLading("30-09-2020");
//            billLadingService.updateOrderNumOfBOL();
//
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }

}
