package com.ac.enums;

public enum StatusRouting {
    DELETED("-1"), //XOA don tu luc tao
    NOT_CONFIRM("0"),
    DRIVER_CONFIRM("1"),
    COMPLETED("2"),
    CANCELLED("3"),
    CLAIM("4"),
    DRAFT("5");
    private String value;

    StatusRouting(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
    public static StatusRouting of(String value) throws Exception {
        if(NOT_CONFIRM.getValue().equals(value)){
            return NOT_CONFIRM;
        }
        if(DRIVER_CONFIRM.getValue().equals(value)){
            return DRIVER_CONFIRM;
        }
        if(COMPLETED.getValue().equals(value)){
            return COMPLETED;
        }
        if(CANCELLED.getValue().equals(value)){
            return CANCELLED;
        }
        if(CLAIM.getValue().equals(value)){
            return CLAIM;
        }
        if(DRAFT.getValue().equals(value)){
            return DRAFT;
        }
        if(DELETED.getValue().equals(value)){
            return DELETED;
        }
        throw new Exception ("Wrong value!");
    }
}
