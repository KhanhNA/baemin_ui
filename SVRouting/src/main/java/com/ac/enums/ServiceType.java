package com.ac.enums;

public enum ServiceType {
    GENETIC("1"),
    PLAN("0");
    private String value;

    public String getValue() {
        return value;
    }
    ServiceType(String value) {
        this.value = value;
    }

    public static ServiceType of(String value) throws Exception {
        if(GENETIC.getValue().equals(value)){
            return GENETIC;
        }
        if(PLAN.getValue().equals(value)){
            return PLAN;
        }

        throw new Exception ("Wrong value!");
    }
}
