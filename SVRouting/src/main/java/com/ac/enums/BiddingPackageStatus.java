package com.ac.enums;

public enum BiddingPackageStatus {

    OVERTIME_BIDDING("-1"),
    NOT_BIDDING ("0"),
    BIDDING_ALREADY("1"),
    WAITING_ACCEPT("2");

    //            # ('-1', 'Quá hạn bidding'),
//            # ('0', 'Chưa bidding'),
//            # ('1', 'Đã được bid'),
//            # ('2', 'Chờ xác nhận')
    String value;

    public String getValue() {
        return value;
    }


    BiddingPackageStatus(String s) {
        this.value = s;
    }
    public static BiddingPackageStatus  of(String v) throws Exception {
        if(OVERTIME_BIDDING.getValue().equals(v)){
            return OVERTIME_BIDDING;
        }
        else if(NOT_BIDDING.getValue().equals(v)){
            return NOT_BIDDING;
        }
        else if(BIDDING_ALREADY.getValue().equals(v)){
            return BIDDING_ALREADY;
        }
        else if(WAITING_ACCEPT.getValue().equals(v)){
            return WAITING_ACCEPT;
        }
        else{
            throw new Exception("WrongValue");
        }
    }
}
