package com.ac.enums;

public enum ShipType {
    IN_ZONE("0"), LONG_HOLD("1");
    //Instance variable
    private String value;
    //Constructor to initialize the instance variable


    public String getValue() {
        return value;
    }

    ShipType(String value) {
        this.value = value;
    }

    public static ShipType of(String v){
        if(v.equals("0")){
            return IN_ZONE;
        }
        else if(v.equals("1")){
            return LONG_HOLD;
        }
        return null;
    }
}
