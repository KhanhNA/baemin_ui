package com.ac.repository;

import com.ac.entity.bidding_entity.SharevanBiddingPackage;
import com.ac.repository.custom.SharevanBiddingPackageRepositoryCustom;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface SharevanBiddingPackageRepository extends JpaRepository<SharevanBiddingPackage, Integer>, JpaSpecificationExecutor<SharevanBiddingPackage>, SharevanBiddingPackageRepositoryCustom {

}