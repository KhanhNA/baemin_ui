package com.ac.model;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
@Getter
@Setter
public class RoutingClaim implements Serializable {
    private Integer[] lstIdRouting;
    private String type;

}
