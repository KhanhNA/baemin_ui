package com.ac.entity;

import lombok.Data;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Entity
@Table(name="Solution_Day")
@Data
public class SolutionDay {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name="id")
  private Integer id;

  @Column(name="hard_Score")
  private long hardScore;

  @Column(name="soft_Score")
  private long softScore;

  @Column(name="solve_time")
  private LocalDateTime solveTime;

  @Column(name="date_Plan")
  private LocalDate datePlan;

  @Column(name="group_code")
  private String groupCode;

  @Column(name="name")
  private String name;

  @Column(name = "status")
  private String status;
}
