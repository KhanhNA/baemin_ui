package com.ac.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Getter
@Setter
@Table(name = "sharevan_warehouse")
public class SharevanWarehouse {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @Column(name="name")
    private String name;

    @Column(name = "warehouse_code")
    private String warehouseCode;

    @Column(name = "address")
    private String address;

    @Column(name = "area_id")
    private Integer areaId;

    @Column(name = "country_id")
    private Integer countryId;

    @Column(name = "latitude")
    private Double latitude;

    @Column(name = "longitude")
    private Double longitude;


}


