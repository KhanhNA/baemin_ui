package com.ac.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Getter
@Setter
@Table(name = "sharevan_warehouse_log")
public class WarehouseLog  {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Integer id;

    /**
     * From name
     */
    @Column(name = "from_name")
    private String fromName;

    /**
     * To name
     */
    @Column(name = "to_name")
    private String toName;

    /**
     * To name
     */
    @Column(name = "from_latitude")
    private Double fromLatitude;

    /**
     * To name
     */
    @Column(name = "from_longitude_moved0")
    private String fromLongitudeMoved0;

    /**
     * To name
     */
    @Column(name = "to_latitude_moved0")
    private String toLatitudeMoved0;

    /**
     * To name
     */
    @Column(name = "to_longitude_moved0")
    private String toLongitudeMoved0;

    /**
     * Scan check
     */
    @Column(name = "scan_check")
    private Boolean scanCheck;

    /**
     * Description
     */
    @Column(name = "description")
    private String description;

    /**
     * Created by
     */
    @Column(name = "create_uid")
    private Integer createUid;

    /**
     * Created on
     */
    @Column(name = "create_date")
    private java.sql.Timestamp createDate;

    /**
     * Last Updated by
     */
    @Column(name = "write_uid")
    private Integer writeUid;

    /**
     * Last Updated on
     */
    @Column(name = "write_date")
    private java.sql.Timestamp writeDate;

    /**
     * To longitude
     */
    @Column(name = "from_longitude")
    private Double fromLongitude;

    /**
     * To latitude
     */
    @Column(name = "to_latitude")
    private Double toLatitude;

    /**
     * To longitude
     */
    @Column(name = "to_longitude")
    private Double toLongitude;

    /**
     * Time
     */
    @Column(name = "time")
    private Double time;

    /**
     * Distance
     */
    @Column(name = "distance")
    private Double distance;

}
