package com.ac.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(name="fleet_vehicle")
@Getter
@Setter
public class FleetVehicle {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id")
    private Integer id;

    @Column(name="name")
    private String name;

    @Column(name="active")
    private Boolean active;

    @Column(name="company_Id")
    private Integer companyId;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "parking_point_id", insertable = false, updatable = false)
    private ParkingPoint parkingPoint;

    @Column(name="license_Plate")
    private String licensePlate;

    @Column(name="vin_Sn")
    private String vinSn;

    @Column(name="latitude")
    private Double latitude;

    @Column(name="longitude")
    private Double longitude;

    @Column(name="capacity")
    private Double capacity;

    @Column(name="cost")
    private Double cost;

    @Column(name="cost_Center")
    private Double costCenter;

    @Column(name="cost_per_unit")
    private Double costPerUnit;

    @Column(name = "state_id")
    private Integer stateId;

    @Column(name = "priority")
    private Integer priority;
}

