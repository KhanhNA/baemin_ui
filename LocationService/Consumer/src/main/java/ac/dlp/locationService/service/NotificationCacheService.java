/**
 * Copyright (c) 2016-2019 Nikita Koksharov
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ac.dlp.locationService.service;

import ac.dlp.locationService.model.FleetVehicle;
import ac.dlp.locationService.model.FleetVehicleLocationLog;
import ac.dlp.locationService.model.FleetVehicleTokens;
import ac.dlp.locationService.model.PushNotificationEvent;
import ns.utils.JPAUtility;
import org.redisson.Redisson;
import org.redisson.api.RLiveObjectService;
import org.redisson.api.RedissonClient;
import org.redisson.config.Config;
import org.springframework.context.ApplicationEventPublisher;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;

public class NotificationCacheService {
    protected static HashMap<Integer, LocalDateTime> notificationVehicle = new HashMap<>();
    protected static final long PLUS_SECONDS = 1;//1 MINUTE

    public static FleetVehicle add(FleetVehicle vehicle) {
        if (notificationVehicle == null) {
            notificationVehicle = new HashMap<>();
        }
        LocalDateTime last = notificationVehicle.get(vehicle.getId());
        if (last == null || vehicle.getLastUpdateLocationTime() == null ||
                last.plusSeconds(PLUS_SECONDS).compareTo(LocalDateTime.now()) <= 0) {
            notificationVehicle.put(vehicle.getId(), LocalDateTime.now());
            return vehicle;
        }
        return null;
    }

    public static void updateCache(Object source, ApplicationEventPublisher applicationEventPublisher, HashMap<Integer, FleetVehicleLocationLog> mapLastLocation) {


    }


}
