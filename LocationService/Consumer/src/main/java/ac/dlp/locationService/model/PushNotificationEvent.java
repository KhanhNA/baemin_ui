package ac.dlp.locationService.model;

import lombok.Getter;
import org.springframework.context.ApplicationEvent;

import java.util.List;

@Getter
public class PushNotificationEvent extends ApplicationEvent {
    private FleetVehicle vehicle;
    private List<LocLatLng> lstTracing;
    private String fullName;
    private String mobilePhone;

    public PushNotificationEvent(Object source, FleetVehicle vehicle, List<LocLatLng> tracings, String fullName, String mobilePhone) {
        super(source);
        this.vehicle = vehicle;
        this.fullName = fullName;
        this.mobilePhone = mobilePhone;
        this.lstTracing = tracings;
    }
}
