package ac.dlp.locationService.model;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.time.LocalDateTime;

@Getter
@Setter
public class LocLatLng implements Serializable {
    public double latitude;
    public double longitude;
    public String time;
    public LocLatLng(double latitude, double longitude, String time) {
        this.latitude = latitude;
        this.longitude = longitude;
        this.time = time;
    }

}
