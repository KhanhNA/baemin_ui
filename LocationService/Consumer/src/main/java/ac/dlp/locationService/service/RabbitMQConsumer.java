package ac.dlp.locationService.service;

import ac.dlp.locationService.model.*;
import lombok.RequiredArgsConstructor;
import ns.utils.JPAUtility;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Component;

import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

@Component
@RequiredArgsConstructor
public class RabbitMQConsumer {
	public static DateTimeFormatter dtFormater = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
	private final ApplicationEventPublisher applicationEventPublisher;
	private static double delta = 0.1;
	@RabbitListener(queues = "${location.rabbitmq.queue}")
	public void recievedMessage(List<LocationLog> locationLogs) {
		if(locationLogs == null){
			return;
		}
		FleetVehicleLocationLog lastLocation = null;
//		FleetVehicleLocationLog vehicleLocation = null;
		HashMap<Integer, FleetVehicleLocationLog> mapLastLocation = new HashMap<>();
		HashMap<Integer, List<LocLatLng>> mapTracingLocation = new HashMap<>();

		for(LocationLog locationLog: locationLogs){
			if(locationLog.vehicle_id == null){
				continue;
			}
			lastLocation = mapLastLocation.get(locationLog.vehicle_id);

			FleetVehicleLocationLog vehicleLocationLog = new FleetVehicleLocationLog();
			vehicleLocationLog.setLog(locationLog);
			JPAUtility.getInstance().save(vehicleLocationLog);


			if(lastLocation == null ||
					lastLocation.getAssignDateTimeStr().compareTo(locationLog.assign_date_time) < 0){
				lastLocation = vehicleLocationLog;

				lastLocation.setLongitude(lastLocation.getLongitude());
				mapLastLocation.put(locationLog.vehicle_id, lastLocation);
			}
			List<LocLatLng> lstTracing = mapTracingLocation.get(locationLog.vehicle_id);
			if(lstTracing == null){
				lstTracing = new ArrayList<>();
				mapTracingLocation.put(locationLog.vehicle_id, lstTracing);
			}
			lstTracing.add(new LocLatLng(locationLog.latitude, locationLog.longitude, locationLog.assign_date_time));
			JPAUtility.getInstance().save(vehicleLocationLog);
		}
		List<FleetVehicle> lstNotificationVehicle = VehicleCacheService.updateCache(this, applicationEventPublisher, mapLastLocation);
		for(FleetVehicle vehicle: lstNotificationVehicle) {
			List<LocLatLng>lstTracing = mapTracingLocation.get(vehicle.getId());
//			vehicle.setLstTracing(lstTracing);
			applicationEventPublisher.publishEvent(new PushNotificationEvent(this, vehicle, lstTracing, "thonv", "0389397968"));
		}
		System.out.println("Recieved Message From RabbitMQ: " + locationLogs);
	}
}