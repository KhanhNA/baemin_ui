package ac.dlp.locationService.controller;

import ac.dlp.locationService.service.VehicleCacheService;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/register/")
public class RegisterController {


    @PostMapping(value = "/add")
    public String registerNotify(@RequestParam("vehicleId") Integer vehicleId, @RequestParam("token")  String token) {

        VehicleCacheService.registerNotify(vehicleId, token);

        return "Message sent to the RabbitMQ JavaInUse Successfully";
    }

}

