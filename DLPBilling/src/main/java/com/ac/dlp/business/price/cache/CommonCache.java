package com.ac.dlp.business.price.cache;

import com.ac.dlp.WebSecurityConfigurer;
import com.ac.dlp.entity.*;
import ns.utils.DateUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;

@Component
public class CommonCache {
    public static final String CACHE_PRICE_DISTANCE = "01";
    public static final String CACHE_ORDER_PACKAGE = "02";
    public static final String CACHE_SERVICE_TYPE = "03";
    public static final String CACHE_ZONE_PRICE = "04";
    public static final String CACHE_PRODUCT_TYPE = "05";
    public static final String CACHE_COMPANY = "06";
    public static final String CACHE_AWARD_LEVEL = "07";
    public static final String CACHE_INSURANCE = "08";
    public static final String CACHE_PRICE_QUOTATION_TYPE_EXPRESS = "express";
    public static final String CACHE_PRICE_QUOTATION_TYPE_ECONOMY = "economy";
    public static final String CACHE_PRICE_QUOTATION_TYPE_JUST_IN_TIME = "just_in_time";
    public static final String CACHE_PRICE_QUOTATION_TYPE_NORMAL = "normal";

    public static HashMap<String, Object> commonCache = new HashMap<>();
    private static final Logger logger = LogManager.getLogger(WebSecurityConfigurer.class);
    public static DistanceCompute getCacheHubPrice(String hub1, String hub2) {
        Map map = (Map) commonCache.get(CACHE_PRICE_DISTANCE);
        if (map == null || map.size() == 0) {
            map = DBService.getInstance().getHubPrices();
            commonCache.put(CACHE_PRICE_DISTANCE, map);
        }
        return (DistanceCompute) map.get(hub1 + "-" + hub2);
    }

    public static OrderPackage getCacheOrderPackage(String id) {
        Map map = (Map) commonCache.get(CACHE_ORDER_PACKAGE);
        if (map == null || map.size() == 0) {
            map = DBService.getInstance().getOrderPackage();
            commonCache.put(CACHE_ORDER_PACKAGE, map);
        }
        return (OrderPackage) map.get(id);
    }

    public static ServiceType getCacheServiceType(String id) {
        Map map = (Map) commonCache.get(CACHE_SERVICE_TYPE);
        if (map == null || map.size() == 0) {
            map = DBService.getInstance().getServiceType();
            commonCache.put(CACHE_SERVICE_TYPE, map);
        }
        return (ServiceType) map.get(id);
    }

    public static ZonePrice getCacheZonePrice(String id) {
        Map map = (Map) commonCache.get(CACHE_ZONE_PRICE);
        if (map == null || map.size() == 0) {
            map = DBService.getInstance().getZonePrice();
            commonCache.put(CACHE_ZONE_PRICE, map);
        }
        return (ZonePrice) map.get(id);
    }

    public static ProductType getCacheProductType(String id) {
        Map map = (Map) commonCache.get(CACHE_PRODUCT_TYPE);
        if (map == null || map.size() == 0) {
            map = DBService.getInstance().getProductType();
            commonCache.put(CACHE_PRODUCT_TYPE, map);
        }
        return (ProductType) map.get(id);
    }

    public static Company getCacheCompany(String id) {
        Map map = (Map) commonCache.get(CACHE_COMPANY);
        if (map == null || map.size() == 0) {
            map = DBService.getInstance().getCompany();
            commonCache.put(CACHE_COMPANY, map);
        }
        return (Company) map.get(id);
    }

    public static Insurance getCacheInsurance(String id) {
        Map map = (Map) commonCache.get(CACHE_INSURANCE);
        if (map == null || map.size() == 0) {
            map = DBService.getInstance().getInsurance();
            commonCache.put(CACHE_INSURANCE, map);
        }
        return (Insurance) map.get(id);
    }

    public static TitleAward getCacheTitleAward(String id) {
        Map map = (Map) commonCache.get(CACHE_AWARD_LEVEL);
        if (map == null || map.size() == 0) {
            map = DBService.getInstance().getTitleAward();
            commonCache.put(CACHE_AWARD_LEVEL, map);
        }
        return (TitleAward) map.get(id);
    }

    public static PriceQuotation getCachePriceQuotation(String type, String level) {
        Map map = null;
        switch (type) {
            case CACHE_PRICE_QUOTATION_TYPE_NORMAL:
                map = (Map) commonCache.get(CACHE_PRICE_QUOTATION_TYPE_NORMAL);
                if (map == null || map.size() == 0) {
                    map = DBService.getInstance().getPriceQuotationNormal();
                    commonCache.put(CACHE_PRICE_QUOTATION_TYPE_NORMAL, map);
                }
                break;
            case CACHE_PRICE_QUOTATION_TYPE_EXPRESS:
                map = (Map) commonCache.get(CACHE_PRICE_QUOTATION_TYPE_EXPRESS);
                if (map == null || map.size() == 0) {
                    map = DBService.getInstance().getCachePriceQuotationExpress();
                    commonCache.put(CACHE_PRICE_QUOTATION_TYPE_EXPRESS, map);
                }
                break;
            case CACHE_PRICE_QUOTATION_TYPE_JUST_IN_TIME:
                map = (Map) commonCache.get(CACHE_PRICE_QUOTATION_TYPE_JUST_IN_TIME);
                if (map == null || map.size() == 0) {
                    map = DBService.getInstance().getCachePriceQuotationJustInTime();
                    commonCache.put(CACHE_PRICE_QUOTATION_TYPE_JUST_IN_TIME, map);
                }
                break;
            case CACHE_PRICE_QUOTATION_TYPE_ECONOMY:
                map = (Map) commonCache.get(CACHE_PRICE_QUOTATION_TYPE_ECONOMY);
                if (map == null || map.size() == 0) {
                    map = DBService.getInstance().getCachePriceQuotationEconomy();
                    commonCache.put(CACHE_PRICE_QUOTATION_TYPE_ECONOMY, map);
                }
                break;
            default:
                break;
        }
        assert map != null;
        return (PriceQuotation) map.get(level);
    }

    // The important information have been updated every 10 minutes
    @Scheduled(initialDelay = 0, fixedDelay = 600000)
    public void run() {
        CommonCache.logger.info("run every 10 minutes");
        Map map = null;
        try {
            map = DBService.getInstance().getInsurance();
            commonCache.put(CACHE_INSURANCE, map);
            map = DBService.getInstance().getCompany();
            commonCache.put(CACHE_COMPANY, map);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

//    The system information have been updated everyday at 23 hour
    @Scheduled(cron = "0 0 23 * * *")
    public void runOnePerday() {
        CommonCache.logger.info("run everyday at 23h ");
        Map map = null;
        try {
            map = DBService.getInstance().getProductType();
            commonCache.put(CACHE_PRODUCT_TYPE, map);

            map = DBService.getInstance().getCachePriceQuotationEconomy();
            commonCache.put(CACHE_PRICE_QUOTATION_TYPE_ECONOMY, map);
            map = DBService.getInstance().getCachePriceQuotationJustInTime();
            commonCache.put(CACHE_PRICE_QUOTATION_TYPE_JUST_IN_TIME, map);
            map = DBService.getInstance().getPriceQuotationNormal();
            commonCache.put(CACHE_PRICE_QUOTATION_TYPE_NORMAL, map);
            map = DBService.getInstance().getCachePriceQuotationExpress();
            commonCache.put(CACHE_PRICE_QUOTATION_TYPE_EXPRESS, map);
            map = DBService.getInstance().getTitleAward();
            commonCache.put(CACHE_AWARD_LEVEL, map);
            map = DBService.getInstance().getServiceType();
            commonCache.put(CACHE_SERVICE_TYPE, map);

            map = DBService.getInstance().getHubPrices();
            commonCache.put(CACHE_PRICE_DISTANCE, map);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
