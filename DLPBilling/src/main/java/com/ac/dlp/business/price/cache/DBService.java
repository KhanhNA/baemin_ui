package com.ac.dlp.business.price.cache;

import com.ac.dlp.entity.*;
import ns.utils.DateUtils;
import ns.utils.JPAUtility;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DBService {
    private static DBService instance;

    public static DBService getInstance() {
        if (instance == null) {
            instance = new DBService();
        }
        return instance;
    }

    public Map<String, DistanceCompute> getHubPrices() {

//        return JPAUtility.getInstance().jpaFindAll("from DistanceCompute where status = 'running'", null);

        List<DistanceCompute> list = JPAUtility.getInstance().jpaFindAll("from DistanceCompute where status = 'running'", null);
        Map<String, DistanceCompute> map = new HashMap<>();
        for (DistanceCompute element : list) {
            map.put(element.getFromSeq() + "-" + element.getToSeq(), element);
        }
        return map;
    }

    public Map<String, OrderPackage> getOrderPackage() {

//        return JPAUtility.getInstance().jpaFindAll("from DistanceCompute where status = 'running'", null);

        List<OrderPackage> list = JPAUtility.getInstance().jpaFindAll("from OrderPackage where status = 'running'", null);
        Map<String, OrderPackage> map = new HashMap<>();
        for (OrderPackage element : list) {
            map.put(element.getId().toString(), element);
        }
        return map;
    }

    public Map<String, ServiceType> getServiceType() {

//        return JPAUtility.getInstance().jpaFindAll("from DistanceCompute where status = 'running'", null);

        List<ServiceType> list = JPAUtility.getInstance().jpaFindAll("from ServiceType where status = 'running'", null);
        Map<String, ServiceType> map = new HashMap<>();
        for (ServiceType element : list) {
            map.put(element.getId().toString(), element);
        }
        return map;
    }

    public Map<String, ZonePrice> getZonePrice() {
        List<ZonePrice> list = JPAUtility.getInstance().jpaFindAll("from ZonePrice where status = 'running' and" +
                " fromDate <= CURRENT_DATE  and (toDate is null or toDate > CURRENT_DATE)", null);
        Map<String, ZonePrice> map = new HashMap<>();
        for (ZonePrice element : list) {
            map.put(element.getZoneId().toString(), element);
        }
        return map;
    }

    public Map<String, ProductType> getProductType() {
        List<ProductType> list = JPAUtility.getInstance().jpaFindAll("from ProductType where status = 'running' and" +
                " type = '0' ", null);
        Map<String, ProductType> map = new HashMap<>();
        for (ProductType element : list) {
            map.put(element.getId().toString(), element);
        }
        return map;
    }

    public Map<String, Company> getCompany() {
        List<Company> list = JPAUtility.getInstance().jpaFindAll("from Company where status = 'running' and" +
                " companyType in ('1','2','3') ", null);
        Map<String, Company> map = new HashMap<>();
        for (Company element : list) {
            map.put(element.getId().toString(), element);
        }
        return map;
    }

    public Map<String, TitleAward> getTitleAward() {
        List<TitleAward> list = JPAUtility.getInstance().jpaFindAll("from TitleAward where status = 'running' and" +
                " titleAwardType = 'customer' ", null);
        Map<String, TitleAward> map = new HashMap<>();
        for (TitleAward element : list) {
            map.put(element.getId().toString(), element);
        }
        return map;
    }

    public Map<String, Insurance> getInsurance() {
        List<Insurance> list = JPAUtility.getInstance().jpaFindAll("from Insurance where status = 'running' ", null);
        Map<String, Insurance> map = new HashMap<>();
        for (Insurance element : list) {
            map.put(element.getId().toString(), element);
        }
        return map;
    }

    public Map<String, PriceQuotation> getPriceQuotationNormal() {
        List<PriceQuotation> list = JPAUtility.getInstance().jpaFindAll("from PriceQuotation where status = 'running'  and type = 'normal' ", null);
        Map<String, PriceQuotation> map = new HashMap<>();
        for (PriceQuotation element : list) {
            map.put(element.getLevel(), element);
        }
        return map;
    }

    public Map<String, PriceQuotation> getCachePriceQuotationExpress() {
        List<PriceQuotation> list = JPAUtility.getInstance().jpaFindAll("from PriceQuotation where status = 'running'  and type = 'express' ", null);
        Map<String, PriceQuotation> map = new HashMap<>();
        for (PriceQuotation element : list) {
            map.put(element.getLevel(), element);
        }
        return map;
    }

    public Map<String, PriceQuotation> getCachePriceQuotationJustInTime() {
        List<PriceQuotation> list = JPAUtility.getInstance().jpaFindAll("from PriceQuotation where status = 'running'  and type = 'just_in_time' ", null);
        Map<String, PriceQuotation> map = new HashMap<>();
        for (PriceQuotation element : list) {
            map.put(element.getLevel(), element);
        }
        return map;
    }

    public Map<String, PriceQuotation> getCachePriceQuotationEconomy() {
        List<PriceQuotation> list = JPAUtility.getInstance().jpaFindAll("from PriceQuotation where status = 'running'  and type = 'economy' ", null);
        Map<String, PriceQuotation> map = new HashMap<>();
        for (PriceQuotation element : list) {
            map.put(element.getLevel(), element);
        }
        return map;
    }

    public void updateRoutingPlan(Long solId, String insertInfo) throws Exception {

        String sqlUpdate = "INSERT INTO routing_plan_day (id, van_id, next_id, previous_id, routing_time, update_date, update_user) VALUES " + insertInfo +
                " ON DUPLICATE KEY UPDATE routing_time = value(routing_time), van_id = value(van_id), " +
                "                         next_id = value(next_id), previous_id = value(previous_id)," +
                "                         update_date = sysdate(), update_user = 'thonv'";

        JPAUtility.getInstance().executeJdbcUpdate(sqlUpdate, null);

    }

    public Long insertSolutionDay(String groupCode, LocalDate datePlan, Long softScore, Long hardScore) throws Exception {

        List<Object> params = new ArrayList<>();
        params.add(DateUtils.toDate(datePlan));
        params.add(softScore);
        params.add(hardScore);

        String sql = "INSERT INTO solution_day (date_plan, solve_time, soft_score, hard_score, group_code) VALUES(?,sysdate(),?,?,'" + groupCode + "')" +
                " ON DUPLICATE KEY UPDATE date_plan = value(date_plan), solve_time = sysdate(), soft_score = value(soft_score), " +
                "                           hard_score = value(hard_score), group_code = '" + groupCode + "'";


        return JPAUtility.getInstance().executeJdbcInsert(sql, params);
    }
}
