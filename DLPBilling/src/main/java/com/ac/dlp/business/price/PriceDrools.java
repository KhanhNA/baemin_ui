package com.ac.dlp.business.price;

import com.ac.dlp.config.DroolsBeanFactory;
import org.kie.api.runtime.KieSession;

import java.io.IOException;

public class PriceDrools {

    public static void fireDrools(Object orderDto) {
        KieSession kieSession=new DroolsBeanFactory().getKieSession();
        kieSession.insert(orderDto);
        kieSession.fireAllRules();
    }
}
