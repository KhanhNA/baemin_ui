package com.ac.dlp.entity;

import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@Entity
@Table(name = "sharevan_price_quotation")
@Data
@NoArgsConstructor()
public class PriceQuotation {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @Column(name = "name")
    private String name;

    @Column(name = "net_weight")
    private Double netWeight;

    @Column(name = "in_province_price")
    private Double inProvincePrice;

    @Column(name = "in_zone_price")
    private Double inZonePrice;

    @Column(name = "out_zone_price")
    private Double outZonePrice;

    @Column(name = "good_transport_price")
    private Double goodTransportPrice;

    @Column(name = "extra_weight_cost")
    private Double extraWeightCost;

    @Column(name = "extra_weight_price")
    private Double extraWeightPrice;

    @Column(name = "type")
    private String type;

    @Column(name = "level")
    private String level;

    @Column(name = "description")
    private String description;

    @Column(name = "status")
    private String status;

    @Column(name = "parity_price")
    private Double parityPrice;

}
