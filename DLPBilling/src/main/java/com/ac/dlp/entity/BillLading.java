package com.ac.dlp.entity;

import com.ac.dlp.enums.StatusType;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class BillLading {
    private Integer id;

    private String bill_lading_code;
    private transient OdooRelType partner_id = new OdooRelType();
    private Integer company_id;
    private Insurance insurance;
    private Double total_weight;
    private Double total_parcel;
    private Double total_amount;
    private Double promotion_code;
    private Double total_volume;
    private Double tolls;
    private Double surcharge;
    private Double price_not_discount;
    private Double insurance_price;
    private Double service_price;
    private Integer order_package_id;
    private Double vat;
    private OdooRelType bill_cycle_id = new OdooRelType();
    private List<BillLadingDetail> arrBillLadingDetail;
    private String name_seq = "New";
    private String name;
    private transient String update_all;
    private OdooRelType recurrent_id= new OdooRelType();
    private OrderPackage order_package;
    private String status = StatusType.RUNNING;

//    private String start_date;
//    private String end_date;

    private String startDateStr;

//    private Insurance insurance;
//    private RecurrentModel recurrent;
//    // properties for ui
//    @DeserializeOnly
//    private BillLadingDetail currentBillDetail;


    public void addTotalAmount(Double value){
        total_amount = total_amount == null || total_amount == 0?value:value;
    }
    public void addPriceNotDicount(Double value){
        price_not_discount = price_not_discount == null || price_not_discount == 0?value:value;
    }
    public void addInsurancePrice(Double value){
        insurance_price = insurance_price == null || insurance_price == 0?value:value;
    }
    public void addServicePrice(Double value){
        service_price = service_price == null || service_price == 0?value:value;
    }
}
