package com.ac.dlp.entity;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Area {
    private Integer id;
    private String name;
    private Integer parent_id;
}
