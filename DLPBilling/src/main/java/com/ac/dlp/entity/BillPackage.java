package com.ac.dlp.entity;

import com.ac.dlp.enums.StatusType;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class BillPackage {
    private Integer id;

    private Double net_weight;
    private Integer quantity_package;
    private Double length;
    private Double width;
    private Double height;
    private ProductType productType;
}
