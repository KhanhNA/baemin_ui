package com.ns.odoo.core.rpc.helper.utils.gson;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

import java.lang.reflect.Type;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.SimpleTimeZone;
import java.util.TimeZone;

public class MyDateSerializer implements JsonSerializer<Date> {
    @Override
    public JsonElement serialize(Date src, Type typeOfSrc, JsonSerializationContext context) {
//        if(typeOfSrc instanceof ParameterizedType && ((ParameterizedType) typeOfSrc).getRawType() == OdooRelType.class){
        if(typeOfSrc == Date.class && src != null){
            long milisec = src.getTime();
            long offset = TimeZone.getDefault().getOffset(milisec);

            return new JsonPrimitive(milisec - offset);
        }else{
            return null;
        }
    }
}
