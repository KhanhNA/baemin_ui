/**
 * Odoo, Open Source Management Solution
 * Copyright (C) 2012-today Odoo SA (<http:www.odoo.com>)
 * <p/>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version
 * <p/>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details
 * <p/>
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http:www.gnu.org/licenses/>
 * <p/>
 * Created on 22/4/15 11:56 AM
 */
package com.ns.odoo.core.rpc.helper;

import java.util.HashMap;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class OdooSessionDto {
    public static final String TAG = OdooSessionDto.class.getSimpleName();
    private String username, db;
    private Integer uid;
    private boolean is_system;
    private boolean is_admin;
    private String name;
    private String partner_display_name;
    private Integer company_id;
    private Integer partner_id;
    private String show_effect;
    private boolean display_switch_company_menu;
    private String session_id;
    private Integer out_of_office_message;
    private boolean odoobot_initialized;
    private HashMap<String, OdooUserCurrency> currencies;

}
