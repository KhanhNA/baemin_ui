package com.ns.odoo.core.rpc.helper.utils.gson;

import com.google.gson.Gson;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;
import java.util.List;

public class ArrayDeserializer<T> implements JsonDeserializer<List<T>> {
    @Override
    public List<T> deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        if(json.isJsonNull()) {
            return null;
        }
        if (json.isJsonPrimitive()) {
            if (json.getAsJsonPrimitive().isBoolean() ) {
                return null;
            }
        }
        return new Gson().fromJson(json, typeOfT);
    }
}
