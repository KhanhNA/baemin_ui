package com.ns.service;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Url;

public interface OdooServices {

    @POST
    @Headers( {"Content-Type: application/json","Cookie: session_id=e74457246f65a805cdde30581e3d02ba91e02b84"} )
    Call<Object> getDataJson(
            @Url String url,
            @Body RequestBody body);
}
