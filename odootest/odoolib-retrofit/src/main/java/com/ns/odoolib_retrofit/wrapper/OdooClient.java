package com.ns.odoolib_retrofit.wrapper;

import android.content.Context;

import com.google.gson.JsonElement;
import com.ns.odoolib_retrofit.listener.IOdooResponse;
import com.ns.odoolib_retrofit.model.OdooFields;
import com.ns.odoolib_retrofit.model.OdooResponseDto;
import com.ns.odoolib_retrofit.service.OdooServices;
import com.ns.odoolib_retrofit.utils.OObjectUtils;

import org.json.JSONObject;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.Random;

import lombok.Getter;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;

@Getter
public class OdooClient{

    BaseClient<OdooServices> client;
    protected String sessionCokie = "";
    public OdooClient(Context context, String baseURL) {
        client = new BaseClient(context, baseURL, OdooServices.class);
//        mOdoo = client.createService(OdooServices.class);
    }

    public void authenticate(final String username, final String password, final String database, IOdooResponse callback) {
        try {
            String url = "/web/session/authenticate";
            JSONObject params = new JSONObject();
            params.put("db", database);
            params.put("login", username);
            params.put("password", password);
            params.put("context", new JSONObject());
            retrofitRequest(url, params, callback);

        } catch (Exception e) {
            callback.onResponse(null, e);
        }
    }
    public void callMethod(String model, String method, OArguments arguments,
                           HashMap<String, Object> kwargs, HashMap<String, Object> context,
                           IOdooResponse callback) {
        String url = "/web/dataset/call_kw";
        try {
            JSONObject params = new JSONObject();
            params.put("model", model);
            params.put("method", method);
            params.put("args", arguments.getArray());
            params.put("kwargs", (kwargs != null)
                    ? new JSONObject(client.gson.toJson(kwargs)) : new JSONObject());
//            params.put("context", (context != null) ?
//                    new JSONObject(gson.toJson(updateContext(context)))
//                    : odooSession.getUserContext());
            retrofitRequest(url, params, callback);
        } catch (Exception e) {

            callback.onResponse(null, e);
        }
    }


    private JSONObject createRequestWrapper(JSONObject params) throws Exception {
        JSONObject requestData = new JSONObject();
        try {
            int randomId = getRequestID();
            JSONObject newParams = params;
            if (newParams == null) {
                newParams = new JSONObject();
            }

            requestData.put("jsonrpc", "2.0");
            requestData.put("method", "call");
            requestData.put("params", newParams);
            requestData.put("id", randomId);
//            if (callback != null)
//                responseQueue.add(randomId, callback);
        } catch (Exception e) {
            throw e;
        }
        return requestData;
    }
    private int getRequestID() {
        return Math.abs(new Random().nextInt(9999));
    }

    public void searchRead(String model, OdooFields fields, ODomain domain,
                           int offset, int limit, String sort, final IOdooResponse callback) {
        try {
            String url =  "/web/dataset/search_read";
            JSONObject params = new JSONObject();
            params.put("model", model);
            if (fields == null) {
                fields = new OdooFields();
            }
            params.put("fields", fields.get().getJSONArray("fields"));
            if (domain == null) {
                domain = new ODomain();
            }
            params.put("domain", domain.getArray());
//            JSONObject context = updateCTX(odooSession.getUserContext());
//            params.put("context", context);
            params.put("offset", offset);
            params.put("limit", limit);
            params.put("sort", (sort == null) ? "" : sort);

            retrofitRequest(url, params, callback);
        } catch (Exception e) {
//            OdooLog.e(e, e.getMessage());
            callback.onResponse(null, e);
        }
    }


    public void callRoute(final String route, JSONObject params,
                          IOdooResponse odooResponse) throws Exception {
//        String url = serverURL + route;
        retrofitRequest(route, params, odooResponse);
    }



    public void retrofitRequest(final String url, JSONObject params,
                                IOdooResponse odooResponse) throws Exception{
        retrofitRequest(url,"application/json", params, odooResponse );
    }


    public void retrofitRequest(final String url,  String mediaTye,
                                JSONObject params,
                                IOdooResponse odooResponse) throws Exception {
        final JSONObject postData = createRequestWrapper(params);

        RequestBody requestBody = RequestBody.create(MediaType.parse(mediaTye), postData.toString());
        client.getServices().getDataJsonCokies(sessionCokie, url, requestBody).enqueue(new Callback<OdooResponseDto<JsonElement>>() {
            @Override
            public void onResponse(Call<OdooResponseDto<JsonElement>> call, retrofit2.Response<OdooResponseDto<JsonElement>> response) {
                if(response.errorBody() != null){
                    try {
                        odooResponse.onResponse(null, new Throwable(response.errorBody().string()));
                    } catch (IOException e) {
                        odooResponse.onResponse(null, e);
                    }
                    return;
                }
                sessionCokie = response.headers().get("Set-Cookie");
                System.out.println("success");
                Type type = OObjectUtils.getType(odooResponse);
                Object o = client.gson.fromJson(response.body().result, type);
                odooResponse.onResponse(o, null);
            }

            @Override
            public void onFailure(Call<OdooResponseDto<JsonElement>> call, Throwable t) {

                odooResponse.onResponse(null, t);
            }
        });
    }


}
